#!/bin/bash

if [ $# -ne 7 ]; then
    echo ">> Usage:"
    echo ">>      update_cloudformation_stack.sh <aws profile> <aws region> <template file> <parameter file> <stack name> <environment> <timeout seconds>"
    echo ">> e.g. update_cloudformation_stack.sh gitlab-ci-123456 eu-central-1 cloudformation-templates/student-site.cfn.yml cloudformation-templates/development.launch-params.cfn.json StudentSite development 60"
    echo ">> e.g. update_cloudformation_stack.sh gitlab-ci-123456 eu-central-1 s3://my-app/my-app.cfn.yml cloudformation-templates/development.launch-params.cfn.json MyApp development 300"
    exit 1
fi

AWS_PROFILE=$1
AWS_REGION=$2
TEMPLATE_FILE=$3
PARAMETER_FILE=$4
STACK_NAME=$5
ENVIRONMENT=$6
TIMEOUT_SECONDS=$7

SCRIPT_PATH=$( cd $(dirname $0) ; pwd -P )
cd ${SCRIPT_PATH}

python python_scripts/check_stack_exists.py --stack-name=${STACK_NAME} --region=${AWS_REGION} --profile=${AWS_PROFILE}
STACK_EXISTS_RETURN_CODE=$?

echo "Stack return code:${STACK_EXISTS_RETURN_CODE}"
echo "SCRIPT_PATH:${SCRIPT_PATH}"
echo "PARAMETER_FILE:${PARAMETER_FILE}"

if [ ${STACK_EXISTS_RETURN_CODE} -lt 0 ] || [ ${STACK_EXISTS_RETURN_CODE} -gt 1 ]
then
    echo ">> Unexpected return code when checking whether stack exists: ${STACK_EXISTS_RETURN_CODE}"
    exit 1
fi

if [ ${STACK_EXISTS_RETURN_CODE} -eq 0 ]
then
    echo ">> Waiting up to ${TIMEOUT_SECONDS}s for any previous stack update to complete"
    python python_scripts/wait_for_stack.py --stack-name=${STACK_NAME} --region=${AWS_REGION} --profile=${AWS_PROFILE} --timeout=${TIMEOUT_SECONDS}

    printf "\nUpdating '${STACK_NAME}' Cloud Formation stack\n\n"
    bash run_cloudformation_stack.sh update ${TEMPLATE_FILE} ${PARAMETER_FILE} ${STACK_NAME} ${AWS_PROFILE} ${ENVIRONMENT}
    RETURN_CODE=$?
    echo ">> Return code from run_cloudformation_stack.sh: ${RETURN_CODE}"
    if [ ${RETURN_CODE} -ne 0 ]
    then
        exit ${RETURN_CODE}
    fi

else
    printf "\nCreating '${STACK_NAME}' Cloud Formation stack\n\n"
    bash run_cloudformation_stack.sh create ${TEMPLATE_FILE} ${PARAMETER_FILE} ${STACK_NAME} ${AWS_PROFILE} ${ENVIRONMENT}
    RETURN_CODE=$?
    echo ">> Return code from run_cloudformation_stack.sh: ${RETURN_CODE}"
    if [ ${RETURN_CODE} -ne 0 ]
    then
        exit ${RETURN_CODE}
    fi
fi

echo ">> Waiting up to ${TIMEOUT_SECONDS}s for the stack update to complete"
python python_scripts/wait_for_stack.py --stack-name=${STACK_NAME} --region=${AWS_REGION} --profile=${AWS_PROFILE} --timeout=${TIMEOUT_SECONDS}

RETURN_CODE=$?
echo ">> Return code from stack update: ${RETURN_CODE}"

exit ${RETURN_CODE}
