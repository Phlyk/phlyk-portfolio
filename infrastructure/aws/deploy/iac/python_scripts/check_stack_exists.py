#!/usr/bin/python

import getopt
import sys
import traceback

import boto3
import botocore.exceptions


def usage():
    print('Usage:')
    print('     python check_stack_exists.py --stack-name=<stack name> --region=<region> --profile=<profile>')
    print('e.g. python check_stack_exists.py --stack-name=StudentSite --region=eu-central-1 --profile=gitlab-ci-353')
    print('Return code is 0 if the stack exists, non-zero otherwise')


def main(stack_name, region, profile):
    session = boto3.Session(region_name=region, profile_name=profile)
    cf_client = session.client('cloudformation')
    try:
        stack_events = cf_client.describe_stack_events(StackName=stack_name)['StackEvents']
    except botocore.exceptions.ClientError as e:
        http_response_code = e.response['ResponseMetadata']['HTTPStatusCode']
        error_code = e.response['Error']['Code']
        error_message = e.response['Error']['Message']
        if http_response_code == 400 and error_code == 'ValidationError' and 'does not exist' in error_message:
            print('No stack exists with name: {stack_name:s}'.format(stack_name=stack_name))
            return 1
        else:
            print('Unexpected boto error getting stack events for stack with stack with name {stack_name:s}'.format(stack_name=stack_name))
            traceback.print_exc(file=sys.stdout)
            return 2
    except:
        print('Unexpected non boto error getting stack events for stack with name {stack_name:s}'.format(stack_name=stack_name))
        traceback.print_exc(file=sys.stdout)
        return 2

    if len(stack_events) == 0:
        print('No stack exists with name: {stack_name:s}'.format(stack_name=stack_name))
        return 1

    try:
        top_level_events = [e for e in stack_events if e['LogicalResourceId'] == stack_name]
        most_recent_event = top_level_events[0]
        status = most_recent_event['ResourceStatus']
    except:
        print('Unexpected error analysing the stack status for stack with name {stack_name:s}'.format(stack_name=stack_name))
        traceback.print_exc(file=sys.stdout)
        return 2

    if status == 'ROLLBACK_COMPLETE':
        print('Failed to create stack with name \'{stack_name:s}\' successfully.  It has been completely rolled back.' \
              ' Please confirm this via the amazon console and delete the stack manually before fixing the issue ' \
              'with the cloud formation script and trying again.'.format(stack_name=stack_name))
        return 2

    print('Stack with name {stack_name:s} does exist and status is: {status:s}'.format(
        stack_name=stack_name, status=status))
    return 0

if '__main__' == __name__:

    try:
        options, arguments = getopt.getopt(sys.argv[1:], '', ['stack-name=', 'region=', 'profile='])
    except getopt.GetoptError as err:
        print(str(err))
        usage()
        sys.exit(1)

    opt_stack_name = None
    opt_region = None
    opt_profile = None
    for o, a in options:
        if o == '--stack-name':
            opt_stack_name = a
        elif o == '--region':
            opt_region = a
        elif o == '--profile':
            opt_profile = a

    if opt_stack_name is None or opt_region is None or opt_profile is None:
        usage()
        sys.exit(1)

    sys.exit(main(opt_stack_name, opt_region, opt_profile))
