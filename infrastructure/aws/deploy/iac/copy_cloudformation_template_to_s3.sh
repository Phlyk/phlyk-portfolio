#!/bin/bash

if (( $# < 4 ));
then
    echo ">> Usage:"
    echo ">>   copy_cfn_template_to_s3.sh <template dir> <application name> <aws profile> <environment>"
    echo ">>     template dir:       the directory which contains templates for the stack"
    echo ">>     application name:   the folder name to store the cfn templates under"
    echo ">>     aws profile:        the profile to use to connect to AWS"
    echo ">>     environment:        the environment to run the stacks into (development for now)"
    exit 1
fi

TEMPLATE_DIR=$1
APPLICATION_NAME=$2
PROFILE=$3
ENVIRONMENT="$(echo $4 | tr '[A-Z]' '[a-z]')"

AWS_REGION=eu-central-1

AWS_CLI=$(type aws)
if [ $? -ne 0 ];
then
    echo ">> Failed to find the AWSCLI, please install it via 'pip3 install awscli'"
    exit 1
fi

S3_BUCKET_NAME="s3://COMPANYNAME-cfn-stacks-${ENVIRONMENT}/cloudformation-templates/${APPLICATION_NAME}"

printf "\nCopy Cloudformation Templates\n"
echo "TEMPLATE_DIR: ${TEMPLATE_DIR}"
echo "APPLICATION_NAME: ${APPLICATION_NAME}"
echo "S3_BUCKET_NAME: ${S3_BUCKET_NAME}"
echo "PROFILE: ${PROFILE}"
echo "AWS_REGION: ${AWS_REGION}"

aws s3 sync ${TEMPLATE_DIR} ${S3_BUCKET_NAME} \
        --profile ${PROFILE} \
        --region ${AWS_REGION} \
        --delete \
        --include "**/*.yaml" \
        --exclude "/**/*launch-params.cfn.json" \
        --exclude "/**/.DS_Store" \
        --exclude "/**/*.pem"

echo ">> Copy complete"
