#!/bin/bash

# This script will be run by the CI to provision all the resources required to deploy our applications on AWS
# It does the following
# 1) generates an AWS Profile from the specified credentials tied to the CI runner IAM user
# 2) validates the cloudformation template for application speciied
# 3) copies the application cloudformation templates to s3
# 4) creates or updates the cloud formation stack as needed

if [ $# -lt 4 ]; then
    echo ">> Usage:"
    echo ">>      update_application_infrastructure.sh <CI_AWS_ACCESS_KEY_ID> <CI_AWS_SECRET_ACCESS_KEY> <APPLICATION_NAME> <ENVIRONMENT> <TIME_TO_DEPLOY>"
    echo ">>      update_application_infrastructure.sh omitted omitted student-site development 60"
    exit 1
fi


CLOUDFORMATION_AWS_ACCESS_KEY_ID=$1
CLOUDFORMATION_AWS_SECRET_ACCESS_KEY=$2
APPLICATION_NAME=$3
ENVIRONMENT=$4

if [ $# -lt 5 ]; then
    TIME_TO_DEPLOY=60
else
    TIME_TO_DEPLOY=$5
fi

if [ "${ENVIRONMENT}" != "development" ]
then
    echo ">> ENVIRONMENT can only be 'development' (for now)"
    exit 1
fi

PROJECT_DIR=$( pwd -P )
SCRIPT_DIR=$( cd $(dirname $0) ; pwd -P )
echo ">> PROJECT_DIR:${PROJECT_DIR}"
echo ">> WORKING_DIR:${SCRIPT_DIR}"

AWS_REGION="eu-central-1"
AWS_PROFILE=$(bash ${SCRIPT_DIR}/../add_aws_credentials_profile.sh ${CLOUDFORMATION_AWS_ACCESS_KEY_ID} ${CLOUDFORMATION_AWS_SECRET_ACCESS_KEY} ${AWS_REGION})

APP_TEMPLATES_DIR=${PROJECT_DIR}/cloudformation-templates
TEMPLATE_FILENAME="${APPLICATION_NAME}.cfn.yml"
TEMPLATE_FILEPATH="${APP_TEMPLATES_DIR}/${TEMPLATE_FILENAME}"
CFN_BUCKET_NAME="COMPANYNAME-cfn-stacks-${ENVIRONMENT}/cloudformation-templates"

if [ ! -f ${TEMPLATE_FILEPATH} ]
then
    echo ">> Template '${TEMPLATE_FILEPATH}' does not exist, exiting..."
    echo ">> ${TEMPLATE_FILEPATH}"
    exit 1
fi

TEMPLATE_PATH_S3="https://s3-${AWS_REGION}.amazonaws.com/${CFN_BUCKET_NAME}/${APPLICATION_NAME}/${TEMPLATE_FILENAME}"
STACK_NAME="${APPLICATION_NAME}-stack"

printf "\nUpdate-infrastructure:\n"
echo "AWS_PROFILE:${AWS_PROFILE}"
echo "TEMPLATE_PATH_S3:${TEMPLATE_PATH_S3}"
echo "APP_TEMPLATES_DIR:${APP_TEMPLATES_DIR}"
echo "TEMPLATE_FILENAME:${TEMPLATE_FILENAME}"
echo "APPLICATION_NAME:${APPLICATION_NAME}"
echo "STACK_NAME:${STACK_NAME}"
echo "ENVIRONMENT:${ENVIRONMENT}"
echo "TIME_TO_DEPLOY:${TIME_TO_DEPLOY}"

printf "\nTesting if '${CFN_BUCKET_NAME}' bucket exists\n"
S3_BUCKET_LIST_OUTPUT=$(aws s3 ls s3://${CFN_BUCKET_NAME} 2>&1)
if [[ $? -ne 0 ]]
then
    echo ">> Bucket for CFN templates does not exist, creating..."
    bash ${SCRIPT_DIR}/create_s3_cfn_stack_bucket.sh ${ENVIRONMENT} ${AWS_PROFILE}
else
    echo ">> Bucket exists"
fi

printf "\nCopying templates to S3\n"
bash ${SCRIPT_DIR}/copy_cloudformation_template_to_s3.sh \
    ${APP_TEMPLATES_DIR} \
    ${APPLICATION_NAME} \
    ${AWS_PROFILE} \
    ${ENVIRONMENT}

printf "\nValidating template '${TEMPLATE_PATH_S3}' in S3\n"
aws cloudformation validate-template --profile ${AWS_PROFILE} --template-url ${TEMPLATE_PATH_S3}

if [ $? -ne 0 ]; then
    echo ">> Error Validating CFN Template, exiting..."
    exit 1
else
    echo ">> Template Valid"
fi

printf "\nUpdating Cloudformation stack\n\n"
bash ${SCRIPT_DIR}/update_cloudformation_stack.sh \
    ${AWS_PROFILE} \
    ${AWS_REGION} \
    ${TEMPLATE_PATH_S3} \
    ${APP_TEMPLATES_DIR}/${ENVIRONMENT}.launch-params.cfn.json \
    ${STACK_NAME} \
    ${ENVIRONMENT} \
    ${TIME_TO_DEPLOY}

RETURN_CODE=$?
if [ ${RETURN_CODE} -ne 0 ]
then
    echo ">> Failed to create or update cloud formation stack"
    exit 1
fi

echo ">> AWS Infrastructure has been updated!"
exit 0
