#!/bin/sh

if [ $# -ne 2 ]; then
    echo "Usage:"
    echo "  create_s3_cfn_stack_bucket.sh <environment> <aws_profile>"
    echo "    environment:       the environment to connect to (development for now)"
    echo "    aws_profile:       the profile to use to connect to AWS"
    exit 1
fi

ENVIRONMENT="$1"
PROFILE="$2"\

AWS_REGION=eu-central-1

AWS_CLI=$(type aws)
if [ $? -ne 0 ];
then
    echo "Failed to find the AWSCLI, please install it via 'pip3 install awscli'"
    exit 1
fi

aws s3api create-bucket \
   --create-bucket-configuration LocationConstraint=${AWS_REGION} \
   --acl private \
   --region ${AWS_REGION} \
   --bucket "COMPANYNAME-cfn-stacks-${ENVIRONMENT}" \
   --profile ${PROFILE}
