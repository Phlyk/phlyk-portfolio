#!/bin/bash

# This script will be run by the CI to provision all the resources required to deploy our applications on AWS
# It does the following
# 1) generates an AWS Profile from the specified credentials tied to the CI runner IAM user
# 2) validates the cloudformation template for application specified
# 3) copies the application cloudformation templates to s3 under the application dir
# 4) creates or updates the cloud formation stack as needed

if [ $# -ne 4 ]; then
    echo ">> Usage:"
    echo ">>      update_core_infrastructure.sh <CI_AWS_ACCESS_KEY_ID> <CI_AWS_SECRET_ACCESS_KEY> <STACK_NAME> <ENVIRONMENT>"
    echo ">>      update_core_infrastructure.sh omitted omitted student-site development"
    exit 1
fi


CLOUDFORMATION_AWS_ACCESS_KEY_ID=$1
CLOUDFORMATION_AWS_SECRET_ACCESS_KEY=$2
STACK_NAME=$3
ENVIRONMENT=$4

PROJECT_DIR=$( pwd -P )
SCRIPT_DIR=$( cd $(dirname $0) ; pwd -P )
echo ">> working dir is ${PROJECT_DIR}"
echo ">> scripts dir is ${SCRIPT_DIR}"

if [ "${ENVIRONMENT}" != "development" ]
then
    echo ">> ENVIRONMENT can only be 'development' (for now)"
    exit 1
fi
STACK_UPDATE_TIMEOUT=60
STACK_NAME_LOWER="$(echo ${STACK_NAME} | tr '[A-Z]' '[a-z]')"
case "${STACK_NAME_LOWER}" in
    "COMPANYNAME-vpc" )
        APPLICATION_DIR="VPC"
        TEMPLATE_FILENAME="COMPANYNAME-vpc.cfn.yml"
    ;;
    "db-security-group" | "backend-db-securitygroup" )
        APPLICATION_DIR="SecurityGroups/RDS"
        TEMPLATE_FILENAME="db-security-group.cfn.yml"
    ;;
    "home-security-group" | "home-sg" )
        APPLICATION_DIR="SecurityGroups/EC2"
        TEMPLATE_FILENAME="COMPANYNAME-home-access-sg.cfn.yml"
    ;;
    "public-access-sg" )
        APPLICATION_DIR="SecurityGroups/EC2"
        TEMPLATE_FILENAME="backend-public-access-sg.cfn.yml"
    ;;
    "database" | "backend-db" | "rds" )
        APPLICATION_DIR="RDS"
        TEMPLATE_FILENAME="backend-db.cfn.yml"
        STACK_UPDATE_TIMEOUT=600
    ;;
esac

if [ -z ${TEMPLATE_FILENAME} ];
then
    echo ">> Failed to find a template for stack '${STACK_NAME}', please ensure it is correct or add it to the template list"
    exit 1
fi

TEMPLATE_DIR="cloudformation-templates"
APP_TEMPLATES_DIR="${PROJECT_DIR}/${TEMPLATE_DIR}/${APPLICATION_DIR}"
TEMPLATE_FILEPATH="${APP_TEMPLATES_DIR}/${TEMPLATE_FILENAME}"
PARAMETER_FILENAME="${ENVIRONMENT}.launch-params.cfn.json"
PARAMETER_FILEPATH="${APP_TEMPLATES_DIR}/${PARAMETER_FILENAME}"

if [ ! -e "${TEMPLATE_FILEPATH}" ];
then
    echo ">> The template file '${TEMPLATE_FILEPATH}' does not exist, please check that it has not been renamed or removed"
    exit 1
fi

AWS_REGION="eu-central-1"
AWS_PROFILE=$(bash deploy/aws/add_aws_credentials_profile.sh ${CLOUDFORMATION_AWS_ACCESS_KEY_ID} ${CLOUDFORMATION_AWS_SECRET_ACCESS_KEY} ${AWS_REGION})

CFN_BUCKET_NAME="COMPANYNAME-cfn-stacks-${ENVIRONMENT}"
TEMPLATE_PATH_S3="https://s3-${AWS_REGION}.amazonaws.com/${CFN_BUCKET_NAME}/cloudformation-templates/${APPLICATION_DIR}/${TEMPLATE_FILENAME}"
STACK_NAME="${STACK_NAME}-stack"

printf "\nUpdate-infrastructure:\n"
echo "AWS_PROFILE:${AWS_PROFILE}"
echo "TEMPLATE_PATH_S3:${TEMPLATE_PATH_S3}"
echo "APP_TEMPLATES_DIR:${APP_TEMPLATES_DIR}"
echo "TEMPLATE_FILENAME:${TEMPLATE_FILENAME}"
echo "PARAMETER_FILENAME:${PARAMETER_FILENAME}"
echo "APPLICATION_DIR:${APPLICATION_DIR}"
echo "STACK_NAME:${STACK_NAME}"
echo "ENVIRONMENT:${ENVIRONMENT}"
echo "STACK_UPDATE_TIMEOUT:${STACK_UPDATE_TIMEOUT}"

printf "\nTesting if '${CFN_BUCKET_NAME}' bucket exists\n"
S3_BUCKET_LIST_OUTPUT=$(aws s3 ls s3://${CFN_BUCKET_NAME} 2>&1)
if [[ $? -ne 0 ]]
then
    echo ">> Bucket for CFN templates does not exist, creating..."
    bash ${SCRIPT_DIR}/create_s3_cfn_stack_bucket.sh ${ENVIRONMENT} ${AWS_PROFILE}
else
    echo ">> Bucket exists"
fi

printf "\nCopying templates to S3\n"
bash ${SCRIPT_DIR}/copy_cloudformation_template_to_s3.sh \
    ${APP_TEMPLATES_DIR} \
    ${APPLICATION_DIR} \
    ${AWS_PROFILE} \
    ${ENVIRONMENT}

printf "\nValidating template '${TEMPLATE_PATH_S3}' in S3\n"
aws cloudformation validate-template --profile ${AWS_PROFILE} --template-url ${TEMPLATE_PATH_S3}

if [ $? -ne 0 ]; then
    echo ">> Error Validating CFN Template, exiting..."
    exit 1
else
    echo ">> Template Valid"
fi

printf "\nUpdating Cloudformation stack\n\n"
bash ${SCRIPT_DIR}/update_cloudformation_stack.sh \
    ${AWS_PROFILE} \
    ${AWS_REGION} \
    ${TEMPLATE_PATH_S3} \
    ${PARAMETER_FILEPATH} \
    ${STACK_NAME} \
    ${ENVIRONMENT} \
    ${STACK_UPDATE_TIMEOUT}

RETURN_CODE=$?
if [ ${RETURN_CODE} -ne 0 ]
then
    echo ">> Failed to create or update cloud formation stack"
    exit 1
fi

echo ">> AWS Infrastructure has been updated!"
exit 0
