#!/bin/sh

if [ $# -ne 1 ]; then
    echo ">> Usage:"
    echo ">>    bash install_aws_cli.sh <PKG_INSTALLER>"
    echo ">>    PKG_INSTALLER - the package installer to use (eg: apk or apt-get)"
    exit 1;
fi

PKG_INSTALLER=$1

echo ">> Installing neccessary packages..."
if [ ${PKG_INSTALLER} == "apk" ]; then
    apk apk update && apk upgrade
    apk add --no-cache bash gettext
elif [ ${PKG_INSTALLER} == "apt-get" ]; then
    apt-get update && apt-get upgrade
    apt-get install -y gettext-base > /dev/null
else
    echo "'${PKG_INSTALLER}' not recognised, exiting..."
    exit 1
fi

echo ">> Installing AWS CLI..."
pip3 install -q --no-cache-dir --upgrade awscli boto3

echo ">> Checking correctly installed"
type aws
aws --version
