#!/bin/bash

if [ $# -ne 1 ]; then
    echo ">> Usage:"
    echo ">>    bash add_aws_config.sh <AWS_REGION>"
    echo ">>    bash add_aws_config.sh eu-central-1"
    exit 1
fi

AWS_REGION=$1

AWS_CONFIG_DIR=${HOME}/.aws
AWS_CONFIG_FILE=${AWS_CONFIG_DIR}/config

mkdir -p ${AWS_CONFIG_DIR}

echo "[default]" > ${AWS_CONFIG_FILE}
echo "region = ${AWS_REGION}" >> ${AWS_CONFIG_FILE}
echo "output = json" >> ${AWS_CONFIG_FILE}
FILE_APPEND_RESULT=$?

if [ ${FILE_APPEND_RESULT} -ne 0 ]; then
    echo ">> Issue writing to file"
    exit ${FILE_APPEND_RESULT}
fi

export AWS_CONFIG_FILE
echo ">> '${AWS_CONFIG_FILE}' created"
