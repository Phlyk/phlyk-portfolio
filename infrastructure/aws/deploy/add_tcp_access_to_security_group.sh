#!/bin/bash

if [ $# -lt 3 ]; then
    echo ">> Usage:"
    echo ">> bash add_tcp_access_to_security_group <SECURITY_GROUP_ID> <IP_TO_ADD> <TCP_PORT_TO_ALLOW> [<DESCRIPTION>]"
    echo ">> bash add_tcp_access_to_security_group sg-234234234 23.23.12.12 80 \"http access for my friend bob\""
    exit 1
fi

SG_ID=$1
CIDR_BLOCK="$2/32"
PORT_TO_ALLOW=$3

DESCRIPTION="$4"
if [ -z "${DESCRIPTION}" ]; then
    DESCRIPTION="Access allowed via bash script"
fi

echo "add_tcp_access_to_security_group:"
echo "SG_ID:${SG_ID}"
echo "CIDR_BLOCK:${CIDR_BLOCK}"
echo "PORT_TO_ALLOW:${PORT_TO_ALLOW}"
echo "DESCRIPTION:${DESCRIPTION}"

INGRESS_RULE_EXISTS=$(aws ec2 describe-security-groups --group-ids ${SG_ID} | grep ${CIDR_BLOCK} | wc -l)
if [ ${INGRESS_RULE_EXISTS} -ne 0 ]; then
    echo ">> Rule already exists, skipping..."
    exit 0
fi

aws ec2 authorize-security-group-ingress \
    --group-id ${SG_ID} \
    --ip-permissions IpProtocol=tcp,FromPort=${PORT_TO_ALLOW},ToPort=${PORT_TO_ALLOW},IpRanges="[{CidrIp=${CIDR_BLOCK},Description='${DESCRIPTION}'}]"
