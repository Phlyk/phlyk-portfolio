#!/bin/bash

set -e

if [ $# -lt 5 ]; then
    echo ">> Usage:"
    echo ">> bash ssh_wrapper.sh <PATH_TO_KEY> <SSH_USER> <SSH_HOST> <EXEC_IN_DIR> <SCRIPT_TO_RUN> [<SCRIPT_PARAMS>]"
    echo ">> eg: bash ssh_wrapper.sh /path/to/key.pen ubuntu ec2-123-123-123-9.eu-central-1.compute.amazonaws.com backend \"python manage.py migrate\" --settings=local"
    echo ">> eg: bash ssh_wrapper.sh /path/to/key.pen ec2-user ec2-129-1-23-9.eu-central-1.compute.amazonaws.com . my_deploy_script.sh arg1 arg2"
    exit 1
fi

PATH_TO_KEY=$1
SSH_USER=$2
SSH_HOST=$3
EXEC_IN_DIR=$4
SCRIPT_TO_RUN=$5
SCRIPT_PARAMS=${@:6}

echo ">> Ensuring correct permissions on private key"
chmod 0600 ${PATH_TO_KEY}

echo "ssh_wrapper.sh:"
echo "PATH_TO_KEY:${PATH_TO_KEY}"
echo "SSH_USER:${SSH_USER}"
echo "SSH_HOST:${SSH_HOST}"
echo "EXEC_IN_DIR:${EXEC_IN_DIR}"
echo "SCRIPT_TO_RUN:${SCRIPT_TO_RUN}"
echo "SCRIPT_PARAMS:${SCRIPT_PARAMS}"

SSH_CONNECTION_STRING="${SSH_USER}@${SSH_HOST}"

ssh -oStrictHostKeyChecking=accept-new -i ${PATH_TO_KEY} ${SSH_CONNECTION_STRING} "cd ${EXEC_IN_DIR}; ${SCRIPT_TO_RUN} ${SCRIPT_PARAMS}"
