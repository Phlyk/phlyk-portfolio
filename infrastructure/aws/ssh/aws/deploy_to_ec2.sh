#!/bin/bash

set -e 

if [ $# -ne 4 ]; then
    echo ">>    bash deploy_to_ec2.sh <VERSION> <KEY_PATH> <INSTANCE_IP> <INSTANCE_REGION>"
    echo ">>    eg: bash deploy_to_ec2.sh develop path/to/key.pem 12.34.56.78 eu-central-1"
    exit 1
fi

SCRIPT_PATH=$( cd $(dirname $0); pwd -P)
ZIP_FILE=$1
KEY_PATH=$2
INSTANCE_IP=$3
INSTANCE_REGION=$4

INSTANCE_IP_STRING=$(tr '.' '-' <<< ${INSTANCE_IP})
EC2_HOST=ec2-${INSTANCE_IP_STRING}.${INSTANCE_REGION}.compute.amazonaws.com

ZIP_FILE_NAME=$(basename ${ZIP_FILE})
if [ ! -f ${ZIP_FILE} ]; then
    echo ">> zip file '${ZIP_FILE}' not found, please zip it in the build dir of the containing application and try again"
    exit 1
fi

echo "deploy_to_ec2.sh"
echo "ZIP_FILE:${ZIP_FILE}"
echo "KEY_PATH:${KEY_PATH}"
echo "INSTANCE_IP:${INSTANCE_IP}"
echo "INSTANCE_REGION:${INSTANCE_REGION}"

echo ">> Ensuring correct permissions on private key"
chmod 0600 ${KEY_PATH}

echo ">> Copying application code to EC2 instance"
scp -oStrictHostKeyChecking=accept-new -i ${KEY_PATH} ${ZIP_FILE} ubuntu@${EC2_HOST}:~/.

SCP_RETURN_CODE=$?
if [ ${SCP_RETURN_CODE} -ne 0 ]; then
    echo ">> Issue copying zip to ec2 instance"
    exit ${SCP_RETURN_CODE}
fi

echo ">> Moving old code"
bash ${SCRIPT_PATH}/aws_ssh_wrapper.sh ${KEY_PATH} ${INSTANCE_IP} ${INSTANCE_REGION} . "sudo rm -rf .backend_old; [ ! -d backend ] || mv -f backend .backend_old"

echo ">> Unzipping"
bash ${SCRIPT_PATH}/aws_ssh_wrapper.sh ${KEY_PATH} ${INSTANCE_IP} ${INSTANCE_REGION} . unzip ${ZIP_FILE_NAME} -d backend

echo ">> Deploy successful!"

