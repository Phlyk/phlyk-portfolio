#!/bin/bash

if [ $# -lt 5 ]; then
    echo ">> Usage:"
    echo ">>    bash aws_ssh_wrapper.sh <PATH_TO_KEY> <EC2_IP> <AWS_REGION> <EXEC_IN_DIR> <SCRIPT_TO_RUN> [<SCRIPT_PARAMS>]"
    echo ">>    eg: bash aws_ssh_wrapper.sh /path/to/key.pem 12.12.56.56 eu-central-1 backend my/delicious/script.py 123 456 foo foo bar..."
    exit 1
fi

SCRIPT_PATH=$( cd $(dirname $0); pwd -P)
PATH_TO_KEY=$1
EC2_IP=$2
AWS_REGION=$3
EXEC_IN_DIR=$4
SCRIPT_TO_RUN=$5
SCRIPT_PARAMS=${@:6}

INSTANCE_IP_STRING=$(tr '.' '-'<<<"${EC2_IP}")
EC2_USER="ubuntu"
EC2_HOST="ec2-${INSTANCE_IP_STRING}.${AWS_REGION}.compute.amazonaws.com"

HOSTS_ENTRY_EXISTS=$(cat /etc/hosts | grep ${EC2_IP} | wc -l)
if [ ${HOSTS_ENTRY_EXISTS} -ne 1 ]; then
    echo ">> Adding SSH IP & Host to hosts file"
    echo -e "${EC2_IP}\t${EC2_HOST}" >> /etc/hosts
fi

bash ${SCRIPT_PATH}/../ssh_wrapper.sh ${PATH_TO_KEY} ${EC2_USER} ${EC2_HOST} ${EXEC_IN_DIR} ${SCRIPT_TO_RUN} ${SCRIPT_PARAMS}
SSH_WRAPPER_RETURN_CODE=$?

if [ ${SSH_WRAPPER_RETURN_CODE} -eq 255 ]; then
    echo ">> Issue SSHing onto EC2 Instance. Consider IP changes & security group permissions"
    exit ${SSH_WRAPPER_RETURN_CODE}
elif [ ${SSH_WRAPPER_RETURN_CODE} -ne 0 ]; then
    echo ">> Issue with the commands being run by SSH, please review"
    exit ${SSH_WRAPPER_RETURN_CODE}
fi
