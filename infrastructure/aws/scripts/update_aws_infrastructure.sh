#!/bin/bash

# This script will be run by the CI to provision all the resources required to deploy our applications on AWS
# It does the following
# 1) generates an AWS Profile from the specified credentials tied to the CI runner IAM user
# 2) validates the cloudformation template
# 3) copies the application cloudformation templates to s3
# 4) creates or updates the cloud formation stack as needed

# function fecho(){ builtin printf "\n>> "$@"\n"; }
# export -f fecho

if [ $# -ne 4 ]; then
    echo ">> Usage:"
    echo ">>      update_aws_infrastructure.sh <CI_AWS_ACCESS_KEY_ID> <CI_AWS_SECRET_ACCESS_KEY> <APPLICATION_NAME> <ENVIRONMENT>"
    echo ">>      update_aws_infrastructure.sh omitted omitted student-site development"
    exit 1
fi


CLOUDFORMATION_AWS_ACCESS_KEY_ID=$1
CLOUDFORMATION_AWS_SECRET_ACCESS_KEY=$2
APPLICATION_NAME=$3
ENVIRONMENT=$4

if [ "${ENVIRONMENT}" != "development" ]
then
    echo ">> ENVIRONMENT can only be 'development' (for now)"
    exit 1
fi

WORKING_DIRECTORY=$( pwd -P )
SCRIPTS_DIR="${WORKING_DIRECTORY}/deploy/aws/infrastructure"
echo ">> working dir is ${WORKING_DIRECTORY}"
echo ">> scripts dir is ${SCRIPTS_DIR}"

AWS_PROFILE=$(bash deploy/aws/add_aws_credentials_profile.sh ${CLOUDFORMATION_AWS_ACCESS_KEY_ID} ${CLOUDFORMATION_AWS_SECRET_ACCESS_KEY})

APP_TEMPLATES_DIR=${WORKING_DIRECTORY}/cloudformation-templates
TEMPLATE_FILE="cloudformation-templates/${APPLICATION_NAME}.cfn.yml"
CFN_BUCKET_NAME="COMPANYNAME-cfn-stacks-${ENVIRONMENT}"

if [ ! -f ${TEMPLATE_FILE} ]
then
    echo ">> Template '${TEMPLATE_FILE}' does not exist, exist, exiting..."
    echo ">> ${TEMPLATE_FILE}"
    exit 1
fi

AWS_REGION="eu-central-1"
TEMPLATE_PATH_S3="https://s3-${AWS_REGION}.amazonaws.com/${CFN_BUCKET_NAME}/${TEMPLATE_FILE}"
STACK_NAME="${APPLICATION_NAME}-stack"

printf "\nUpdate-infrastructure:\n"
echo "AWS_PROFILE:${AWS_PROFILE}"
echo "TEMPLATE_PATH_S3:${TEMPLATE_PATH_S3}"
echo "APP_TEMPLATES_DIR:${APP_TEMPLATES_DIR}"
echo "TEMPLATE_FILE:${TEMPLATE_FILE}"
echo "APPLICATION_NAME:${APPLICATION_NAME}"
echo "STACK_NAME:${STACK_NAME}"
echo "ENVIRONMENT:${ENVIRONMENT}"

printf "\nTesting if '${CFN_BUCKET_NAME}' bucket exists\n"
S3_BUCKET_LIST_OUTPUT=$(aws s3 ls s3://${CFN_BUCKET_NAME} 2>&1)
if [[ $? -ne 0 ]]
then
    echo ">> Bucket for CFN templates does not exist, creating..."
    bash ${SCRIPTS_DIR}/create_s3_cfn_stack_bucket.sh ${ENVIRONMENT} ${AWS_PROFILE}
else
    echo ">> Bucket exists"
fi

printf "\nCopying templates to S3\n"
bash ${SCRIPTS_DIR}/copy_cloudformation_template_to_s3.sh \
    ${APP_TEMPLATES_DIR} \
    ${AWS_PROFILE} \
    ${ENVIRONMENT}

printf "\nValidating template '${TEMPLATE_PATH_S3}' in S3\n"
aws cloudformation validate-template --region ${AWS_REGION} --profile ${AWS_PROFILE} --template-url ${TEMPLATE_PATH_S3}

if [ $? -ne 0 ]; then
    echo ">> Error Validating CFN Template, exiting..."
    exit 1
else
    echo ">> Template Valid"
fi

printf "\nUpdating Cloudformation stack\n\n"
bash ${SCRIPTS_DIR}/update_cloudformation_stack.sh \
    ${AWS_PROFILE} \
    ${AWS_REGION} \
    ${TEMPLATE_PATH_S3} \
    ${APP_TEMPLATES_DIR}/${ENVIRONMENT}.launch-params.cfn.json \
    ${STACK_NAME} \
    ${ENVIRONMENT} \
    60

RETURN_CODE=$?
if [ ${RETURN_CODE} -ne 0 ]
then
    echo ">> Failed to create or update cloud formation stack"
    exit 1
fi

exit 0
