#!/bin/bash

# Script to add a new credentials profile to ~/.aws/ folder, will add if exists already

if [ $# -ne 2 ]; then
    echo "Usage:"
    echo "     add-aws-credential-profile.sh <aws access key id> <aws secret access key>"
    echo ""
    echo "Note: this script echos out the new profile name"
    exit 1
fi

AWS_ACCESS_KEY_ID=$1
AWS_SECRET_ACCESS_KEY=$2

AWS_CONFIG_DIR=${HOME}/.aws
mkdir -p ${AWS_CONFIG_DIR}
AWS_CREDENTIALS_FILE="${AWS_CONFIG_DIR}/credentials"

if [ ! -f ${AWS_CREDENTIALS_FILE} ]
then
    touch ${AWS_CREDENTIALS_FILE}
fi

TIMESTAMP=$(date +%s)
PSEUDO_UNIQUE_SUFFIX="${TIMESTAMP}-${RANDOM}"
AWS_PROFILE_NAME=gitlab-ci-${PSEUDO_UNIQUE_SUFFIX}

echo "" >>${AWS_CREDENTIALS_FILE}
echo "[${AWS_PROFILE_NAME}]" >>${AWS_CREDENTIALS_FILE}
echo "aws_access_key_id=${AWS_ACCESS_KEY_ID}" >> ${AWS_CREDENTIALS_FILE}
echo "aws_secret_access_key=${AWS_SECRET_ACCESS_KEY}" >> ${AWS_CREDENTIALS_FILE}
echo ${AWS_PROFILE_NAME}
