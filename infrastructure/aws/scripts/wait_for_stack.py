#!/usr/bin/python

import getopt
import sys
import time

import boto3

UNKNOWN = 'UNKNOWN'
IN_PROGRESS = 'IN_PROGRESS'
COMPLETE = 'COMPLETE'
FAILED = 'FAILED'


def usage():
    print('Usage:')
    print('     python wait_for_stack.py --stack-name=<stack name> --region=<region> --profile=<profile> [--timeout=<timeout s>]')
    print('e.g. python wait_for_stack.py --stack-name=StudentSite --region=eu-central-1 --profile=gitlab-ci-xyx --timeout=60')


def get_status(events):
    if len(events) == 0:
        return UNKNOWN

    most_recent_event = events[0]
    status = most_recent_event['ResourceStatus']
    if 'ROLLBACK' in status and status.endswith('_COMPLETE'):
        return FAILED
    elif status.endswith('_IN_PROGRESS'):
        return IN_PROGRESS
    elif status.endswith('_FAILED'):
        return FAILED
    elif status.endswith('_COMPLETE'):
        return COMPLETE

    return UNKNOWN


def main(stack_name, region, profile, timeout):
    session = boto3.Session(region_name=region, profile_name=profile)
    cf_client = session.client('cloudformation')

    start = time.time()
    elapsed_time = 0
    while elapsed_time < timeout:
        stack_events = cf_client.describe_stack_events(StackName=stack_name)['StackEvents']
        top_level_events = [e for e in stack_events if e['LogicalResourceId'] == stack_name]
        result = get_status(top_level_events)
        if result == FAILED:
            print('stack operation has failed')
            return 1
        elif result == UNKNOWN:
            print('no stack events found for stack')
            return 1
        elif result == COMPLETE:
            print('stack operation has completed after {elapsed_time:.0f} seconds'.format(elapsed_time=elapsed_time))
            return 0

        time.sleep(5)
        elapsed_time = time.time() - start

    print('stack operation is still in progress after waiting for {timeout:.0f} seconds'.format(timeout=timeout))
    return 1


if '__main__' == __name__:

    try:
        options, arguments = getopt.getopt(sys.argv[1:], '', ['stack-name=', 'region=', 'profile=', 'timeout='])
    except getopt.GetoptError as err:
        print(str(err))
        usage()
        sys.exit(1)

    opt_stack_name = None
    opt_region = None
    opt_profile = None
    opt_timeout = 300
    for o, a in options:
        if o == '--stack-name':
            opt_stack_name = a
        elif o == '--region':
            opt_region = a
        elif o == '--profile':
            opt_profile = a
        elif o == '--timeout':
            opt_timeout = a

    if opt_stack_name is None or opt_region is None or opt_profile is None:
        usage()
        sys.exit(1)

    sys.exit(main(opt_stack_name, opt_region, opt_profile, float(opt_timeout)))
