#!/bin/sh

echo ">> Installing AWS CLI..."
apk apk update && apk upgrade
apk add --no-cache bash
pip3 install --no-cache-dir awscli boto3

echo ">> Checking correctly installed"
type aws
