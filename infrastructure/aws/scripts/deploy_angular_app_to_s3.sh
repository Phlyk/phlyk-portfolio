#!/bin/bash

if [ $# -ne 3 ]; then
    echo "Usage:"
    echo "  deploy_angular_app_to_s3.sh <PATH_TO_APP> <S3_BUCKET_ID> <AWS_PROFILE>"
    exit 1
fi

PATH_TO_APP=$1
S3_BUCKET_ID=$2
AWS_PROFILE=$3

AWS_REGION=eu-central-1

AWS_CLI=$(type aws)
if [ -z "${AWS_CLI}" ]; then
    echo "Failed to find the AWSCLI, please install it via 'pip3 install awscli'"
    exit 1
fi

echo ">> Deploying Angular Application to s3"
echo "PATH_TO_APP:${PATH_TO_APP}"
echo "S3_BUCKET_ID:${S3_BUCKET_ID}"
echo "AWS_PROFILE:${AWS_PROFILE}"

aws s3 sync ${PATH_TO_APP} ${S3_BUCKET_ID} \
        --profile ${AWS_PROFILE} \
        --region ${AWS_REGION} \
        --delete \
        --exclude "/**/.DS_Store"
S3_SYNC_RETURN_CODE=$?

if [ ${S3_SYNC_RETURN_CODE} -ne 0 ]; then
    echo ">> Error uploading to S3"
    exit ${S3_SYNC_RETURN_CODE}
else
    echo ">> Deploy successful!"
    exit ${S3_SYNC_RETURN_CODE}
fi
