#!/bin/bash

function usage () {
    echo ">> Usage:"
    echo ">>   run-cloudformation-stack.sh <mode> <template file> <parameter file> <stack name> <aws profile> <environment>"
    echo ">>     mode:               create or update"
    echo ">>     template file:      the path to the template"
    echo ">>     parameter file:     the path to the parameter file"
    echo ">>     stack name:         the name of stack to create"
    echo ">>     aws profile:        the profile to use to connect to AWS"
    echo ">>     environment:        the environment to run the stacks into (development ONLY (for now))"
    echo ""
    exit 1
}

AWS_CLI=$(which aws)

if [ -z "${AWS_CLI}" ];
then
    echo ">> Failed to find the AWSCLI, please install it via 'pip3 install awscli'"
    exit 1
fi

if (( $# < 6 ));
then
    usage
fi

if [ "$1" != "create" ] && [ "$1" != "update" ];
then
    echo ">> The mode must be either 'create' or 'update'"
    usage
fi

MODE=$1
TEMPLATE_FILE=$2
PARAMETER_FILE=$3
STACK_NAME=$4
PROFILE=$5
ENVIRONMENT="$(echo $6 | tr '[A-Z]' '[a-z]')"

AWS_REGION=eu-central-1

if [ "${MODE}" == "create" ]; then
    TAGS_OPTION="--tags Key=Environment,Value=${ENVIRONMENT}"
else
    TAGS_OPTION=""
fi

TEMPLATE_CONTAINS_PROTOCOL=$(echo "${TEMPLATE_FILE}" | grep -e '^\(https\|s3\|file\)')
if [ ! -z $TEMPLATE_CONTAINS_PROTOCOL ]
then
    echo ">> Template file already contains a protocol, ignoring"
else
    echo ">> Adding https:// to TEMPLATE_FILE ${TEMPLATE_FILE}"
    TEMPLATE_FILE="https://${TEMPLATE_FILE}"
fi

PARAMETER_FILE_CONTAINS_PROTOCOL=$(echo "${PARAMETER_FILE}" | grep -e '^\(s3\|file\)')
if [ ! -z $PARAMETER_FILE_CONTAINS_PROTOCOL ]
then
    echo ">> Parameter file already contains a protocol, ignoring"
else
    echo ">> Adding file:// to PARAMETER_FILE ${PARAMETER_FILE}"
    PARAMETER_FILE="file://${PARAMETER_FILE}"
fi

echo ""
printf "\nrun_cloudformation_stack\n"
echo ""
echo "STACK_NAME:${STACK_NAME}"
echo "PARAMETER_FILE:${PARAMETER_FILE}"
echo "TEMPLATE_FILE:${TEMPLATE_FILE}"
echo "TAGS_OPTION:${TAGS_OPTION}"

echo ">> Executing '${MODE}-stack' on stack '${STACK_NAME}'"
aws cloudformation "${MODE}-stack" \
        --stack-name ${STACK_NAME} \
        --template-url "${TEMPLATE_FILE}" \
        --parameters "${PARAMETER_FILE}" \
        --region "${AWS_REGION}" \
        --capabilities "CAPABILITY_IAM" "CAPABILITY_NAMED_IAM" \
        --profile "${PROFILE}" \
        ${TAGS_OPTION} 1>cli.log 2>&1
CLI_RETURN_CODE=$?

NO_UPDATES_MESSAGE_FOUND=$(cat cli.log | grep "ValidationError" | grep "No updates are to be performed" | wc -l)

echo ">> 'cli.log' output:"
cat cli.log

if [ ${NO_UPDATES_MESSAGE_FOUND} -eq 1 ]
then
    exit 0
elif [ ${CLI_RETURN_CODE} -eq 0 ]
then
    echo ">> Waiting 30 seconds for stack update to start"
    sleep 30
fi
exit ${CLI_RETURN_CODE}
