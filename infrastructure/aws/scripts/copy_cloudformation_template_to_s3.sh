#!/bin/bash

if (( $# < 3 ));
then
    echo ">> Usage:"
    echo ">>   copy_cfn_template_to_s3.sh <template dir> <aws profile> <environment>"
    echo ">>     template dir:       the directory which contains templates for the stack"
    echo ">>     aws profile:        the profile to use to connect to AWS"
    echo ">>     environment:        the environment to run the stacks into (development for now)"
    exit 1
fi

TEMPLATE_DIR=$1
PROFILE=$2
ENVIRONMENT="$(echo $3 | tr '[A-Z]' '[a-z]')"

AWS_REGION=eu-central-1

AWS_CLI=$(type aws)
if [ $? -ne 0 ];
then
    echo ">> Failed to find the AWSCLI, please install it via 'pip3 install awscli'"
    exit 1
fi

DIR_CONTAINING_TEMPLATE_DIR=$( cd ${TEMPLATE_DIR}/.. ; pwd -P )
TEMPLATE_DIR_NAME=$( basename ${TEMPLATE_DIR} )
S3_BUCKET_NAME="s3://COMPANYNAME-cfn-stacks-${ENVIRONMENT}/${TEMPLATE_DIR_NAME}"

printf "\nCopy Cloudformation Templates\n"
echo "TEMPLATE_DIR: ${TEMPLATE_DIR}"
echo "S3_BUCKET_NAME: ${S3_BUCKET_NAME}"
echo "PROFILE: ${PROFILE}"
echo "AWS_REGION: ${AWS_REGION}"

aws s3 sync ${TEMPLATE_DIR} ${S3_BUCKET_NAME} \
        --profile ${PROFILE} \
        --region ${AWS_REGION} \
        --delete \
        --include "**/*.yaml" \
        --exclude "/**/*launch-params.cfn.json" \
        --exclude "/**/.DS_Store" \
        --exclude "/**/*.pem"

echo ">> Copy complete"
