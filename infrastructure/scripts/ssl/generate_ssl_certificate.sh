#!/bin/bash

# Instructions:
# 1. Run this script
# 2. Answer question - "Would you be willing to share your email address.... etc"       N
# 3. Answer question - "NOTE: The IP of this machine will be publicly logged...."       Y
# 4. Log into www.dnsmadeeasy.com and set up the TXT record with TTL 60 pointing to the value displayed above the prompt
# 5. Hit enter
# 6. New certs should be in fsm/deploy/docker/httpd/ssl/new-certs

if [ $# -eq 0 ]; then
    echo "Usage:"
    echo "     generate_ssl_certificate.sh $0 <domain1> <domain2>"
    echo "e.g. generate_ssl_certificate.sh $0 *.local.COMPANYNAME.com"
    exit 1
fi

scriptDir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd -P )

newCertsDir=${scriptDir}/new-certs

mkdir -p ${newCertsDir}

domainArguments=""

for domain in "$@"
do
    domainArguments="${domainArguments}-d ${domain} "
done

echo "generate_ssl_certificate.sh"
echo "domainArguments:${domainArguments}"

docker run \
    -ti \
    -v ${newCertsDir}:/etc/letsencrypt/archive \
    certbot/certbot \
    certonly \
    --server https://acme-v02.api.letsencrypt.org/directory \
    --manual \
    --preferred-challenges dns \
    --email dev@COMPANYNAME.com \
    --agree-tos \
    "${domainArguments}"

echo "The new SSL certificates should be in ${newCertsDir}"

