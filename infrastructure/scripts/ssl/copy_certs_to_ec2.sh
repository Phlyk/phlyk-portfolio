#!/bin/bash

if [ $# -ne 4 ]; then
    echo ">> Usage:"
    echo ">>    copy_certs_to_ec2.sh <FULLCHAIN_CERT> <PRIVKEY_CERT> <SSH_ACCESS_KEY> <EC2_IP>" 
    echo ">>    copy_certs_to_ec2.sh /path/to/fullchain.pem /path/to/privkey.pem /path/to/ssh.pem 12.34.56.78" 
    exit 1
fi

set -e 

FULLCHAIN_CERT=$1
PRIVKEY_CERT=$2
SSH_ACCESS_KEY=$3
EC2_IP=$4

INSTANCE_IP_STRING=$(tr '.' '-'<<<"${EC2_IP}")
EC2_USER="ubuntu"
EC2_HOST="ec2-${INSTANCE_IP_STRING}.eu-central-1.compute.amazonaws.com"

echo ">> Ensuring correct permissions on private key"
chmod 0600 ${SSH_ACCESS_KEY}

HOME_SSL_CERT_DIRECTORY="certs"

echo ">> Copying certs"
ssh -i ${SSH_ACCESS_KEY} ${EC2_USER}@${EC2_HOST} "mkdir -p ${HOME_SSL_CERT_DIRECTORY}"
scp -i ${SSH_ACCESS_KEY} ${FULLCHAIN_CERT} ${EC2_USER}@${EC2_HOST}:${HOME_SSL_CERT_DIRECTORY}/COMPANYNAME_server_fullchain.pem
scp -i ${SSH_ACCESS_KEY} ${PRIVKEY_CERT} ${EC2_USER}@${EC2_HOST}:${HOME_SSL_CERT_DIRECTORY}/COMPANYNAME_server_privkey.pem

echo ">> Certs deployed"
