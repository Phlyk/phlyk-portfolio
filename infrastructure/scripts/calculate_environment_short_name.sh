#!/bin/sh

if [ $# -ne 1 ]; then
    echo ">> Usage:"
    echo "bash calculate_environment_short_name.sh <ENVIRONMENT_FULL_NAME>"
    exit 1;
fi

DEVELOPMENT="dev"
STAGING="stag"
PRODUCTION="prod"

ENVIRONMENT_FULL_NAME=$1

if [ $ENVIRONMENT_FULL_NAME = "development" ]; then
    echo $DEVELOPMENT
    exit 0
elif [ $ENVIRONMENT_FULL_NAME = "staging" ]; then
    echo $STAGING
    exit 0
elif [ $ENVIRONMENT_FULL_NAME = "production" ]; then
    echo $PRODUCTION
    exit 0
else
    echo ">> Environment '${ENVIRONMENT_FULL_NAME}' not recognised, exiting..."
    exit 1
fi
