#!/bin/bash

if [ $# -ne 1 ]; then
    echo ">> Usage:"
    echo ">>    bash create_and_activate_venv.sh <VENV_NAME>"
    echo ">>    eg: bash create_and_activate_venv.sh backendvenv"
    exit 1
fi

WORKING_DIR=$(pwd -P)
VENV_NAME=$1
if [ ! -d ${VENV_NAME} ]; then
    echo ">> Virtualenv '${VENV_NAME}' does not exist in ${WORKING_DIR}"
    echo ">> Creating..."
    virtualenv ${VENV_NAME}
fi

echo ">> Activating '${VENV_NAME}'"
source ${WORKING_DIR}/${VENV_NAME}/bin/activate

echo ">> Checking if '${VENV_NAME}' activated correctly"
if [ -z ${VIRTUAL_ENV} ]; then
    echo ">> Virtualenv not activated, exiting..."
    exit 1
fi
echo "VIRTUAL_ENV:${VIRTUAL_ENV}"

echo ">> Virtualenv '${VENV_NAME}' activated!"
