#!/bin/bash

if [ $# -ne 3 ]; then
    echo ">> Usage:"
    echo ">>    install_correct_fe_s_package.sh <BRANCH_TO_TRY_CHECKOUT> <MERGE_REQUEST_FLAG> <CI_JOB_TOKEN>"
    echo ">>    install_correct_fe_s_package.sh feature/99-delicious-change anything-to-indicate-mr-or-not super123sneaky456token"
    exit 1
fi

SCRIPT_PATH=$(cd $(dirname $0); pwd -P)
PROJECT_DIR=${SCRIPT_PATH}/../..

BRANCH_TO_TRY_CHECKOUT=$1
MERGE_REQUEST_FLAG=$2
CI_JOB_TOKEN=$3

FRONTEND_SERVICES_REPO_URL=https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/COMPANYNAME/frontend-services.git

if [ -z ${MERGE_REQUEST_FLAG} ]; then
    echo ">> Branch is not a merge request, so ignoring as will use the following version"
    npm ls @COMPANYNAME/frontend-services | grep @COMPANYNAME/frontend-services
    exit 0
fi

CHECKOUT_FEATURE_BRANCH_RETURN_OUTPUT=$(git clone --single-branch --branch ${BRANCH_TO_TRY_CHECKOUT} ${FRONTEND_SERVICES_REPO_URL})
CHECKOUT_FEATURE_BRANCH_RETURN_CODE=$?
echo ${CHECKOUT_FEATURE_BRANCH_RETURN_OUTPUT}
if [ ${CHECKOUT_FEATURE_BRANCH_RETURN_CODE} -ne 0 ]; then
    echo ">> Unable to checkout branch '${BRANCH_TO_TRY_CHECKOUT}' in frontend-services project." 
    echo ">> Assuming doesn't exist, defaulting to master and therefore installing from GitHub packages"
    exit 0
fi

set -e

echo SCRIPT_PATH:$SCRIPT_PATH
echo PROJECT_DIR:$PROJECT_DIR

echo ">> Installing packages needed for FE-S"
cd ${PROJECT_DIR}/frontend-services && pwd
npm install --unsafe-perm

echo ">> Building frontend-services with locally downloaded feature branch"
npm --prefix projects/frontend-services run build

echo ">> Removing the global version of FE-S"
npm uninstall @COMPANYNAME/frontend-services

echo ">> Installing the newly built FE-S"
cd ${PROJECT_DIR}
npm i frontend-services/dist/frontend-services
