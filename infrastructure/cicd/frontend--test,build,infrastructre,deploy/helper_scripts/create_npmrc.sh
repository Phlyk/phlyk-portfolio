#!/bin/sh

if [ $# -ne 1 ] && [ $# -ne 2 ]; then
    echo ">> Usage:"
    echo ">>   create_npmrc.sh <ACCESS_AUTH_TOKEN> [OVERRIDE]"
    echo ">>   create_npmrc.sh iamanauthtoken1234 override"
    exit 1;
fi

AUTH_TOKEN=$1
OVERRIDE=$2
NPMRC_FILE_NAME=".npmrc"

if [ -f ${NPMRC_FILE_NAME} ] && [ "${OVERRIDE}" != "override" ]; then 
    echo ".npmrc file already exists & no override param was passed"
    echo "exiting normally..."
    exit 0
fi

SCRIPT_DIR=$( cd $(dirname $0) ; pwd -P )
PROJECT_ROOT=${SCRIPT_DIR}/..
echo "SCRIPT_DIR:$SCRIPT_DIR"
echo "PROJECT_ROOT:${PROJECT_ROOT}"
cd "${PROJECT_ROOT}"

printf "\n>> Creating new '${NPMRC_FILE_NAME}' file with config\n"
rm -f ${NPMRC_FILE_NAME} && touch ${NPMRC_FILE_NAME}
echo "@COMPANYNAME:registry=https://npm.pkg.github.com/COMPANYNAME" >> ${NPMRC_FILE_NAME}
echo "registry= https://registry.npmjs.org" >> ${NPMRC_FILE_NAME}
echo "//npm.pkg.github.com/:_authToken=${AUTH_TOKEN}" >> ${NPMRC_FILE_NAME}
echo "unsafe-perm=true" >> ${NPMRC_FILE_NAME}

printf "\n>> Done.\n"
exit 0
