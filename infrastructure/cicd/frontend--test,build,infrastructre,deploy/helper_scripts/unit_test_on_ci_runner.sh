#!bin/bash

SCRIPT_DIR=$( cd $(dirname $0) ; pwd -P )
PROJECT_ROOT=${SCRIPT_DIR}/../..
echo "SCRIPT_DIR:${SCRIPT_DIR}"
echo "PROJECT_ROOT:${PROJECT_ROOT}"

cd "${PROJECT_ROOT}"

echo ">> Running Unit Tests With Coverage"
npm run test-coverage
TEST_RETURN_CODE=$?

if [ ${TEST_RETURN_CODE} -ne 0 ]; then
    echo ">> Tests Failed, please see output and fix"
    exit ${TEST_RETURN_CODE}
fi

echo ">> See coverage report at ${PROJECT_ROOT}/coverage/index.html"
