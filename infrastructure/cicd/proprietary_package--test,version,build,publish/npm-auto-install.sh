#!/bin/bash

echo ">> Auto installing after build..."

packageDir=$( cd $(dirname $0); pwd -P)
buildDir="${packageDir}/../dist/frontend-services"
adminSiteDir="${packageDir}/../../admin-site/"
studentSiteDir="${packageDir}/../../student-site/"
mobileAppDir="${packageDir}/../../mobile/"

echo "packageDir: ${packageDir}"
if [[ ! -d ${adminSiteDir} && ! -d ${studentSiteDir} && ! -d ${mobileAppDir} ]]; then
    echo ">> Neither AdminSite, StudentSite or MobileApp are present on the local filesystem. Nothing to install..."
    exit 0
fi

echo ">> Building Tarball..."
cd ${buildDir}
packTarballName=$(npm pack)

if [ -d ${adminSiteDir} ]; then
    echo ">> Installing in AS..."
    cd ${adminSiteDir} && npm i ${buildDir}/${packTarballName} --no-save
fi
if [ -d ${studentSiteDir} ]; then
    echo ">> Installing in SS..."
    cd ${studentSiteDir} && npm i ${buildDir}/${packTarballName} --no-save
fi
if [ -d ${mobileAppDir} ]; then
    echo ">> Installing in App..."
    cd ${mobileAppDir} && npm i ${buildDir}/${packTarballName} --no-save
fi
echo -e "\n\n>> Built and installed!"
