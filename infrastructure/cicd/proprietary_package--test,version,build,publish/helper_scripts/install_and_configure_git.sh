#!/bin/sh

if [ $# -ne 4 ]; then
    echo ">> Usage:"
    echo ">>    install_and_configure_git.sh <PATH_TO_PRIVATE_SSH_KEY> <DIR_TO_PUT_KEY> <GIT_RUNNER_USERNAME> <GIT_RUNNER_EMAIL>"
    exit 1;
fi

PATH_TO_PRIVATE_SSH_KEY=$1
SSH_DIR=$2
GIT_RUNNER_USERNAME=$3
GIT_RUNNER_EMAIL=$4

GIT_TEMPORARY_LOCAL_BRANCH_NAME="ci_tag_processing"
SSH_PRIVATE_KEY_FILE_PATH="${SSH_DIR}/gitlab_ci_key"
GITLAB_REPO_URL="git@gitlab.com:COMPANYNAME/frontend-services.git"

echo ">> Updating 'apk'"
apk update && apk upgrade
    
echo ">> Installing Git"
apk add --no-cache git openssh

echo ">> Configuring Git"
git checkout -b ${GIT_TEMPORARY_LOCAL_BRANCH_NAME}
git config --global user.name ${GIT_RUNNER_USERNAME}
git config --global user.email ${GIT_RUNNER_EMAIL}
git remote set-url --push origin "${GITLAB_REPO_URL}"

echo ">> Configuring Git to use SSH"
mkdir ${SSH_DIR}
mv ${PATH_TO_PRIVATE_SSH_KEY} ${SSH_PRIVATE_KEY_FILE_PATH}
chmod 0400 ${SSH_PRIVATE_KEY_FILE_PATH}
git config core.sshCommand "ssh -o IdentitiesOnly=yes -o StrictHostKeyChecking=no -i ${SSH_PRIVATE_KEY_FILE_PATH} -F /dev/null"

printf "\n>> Debugging Git Config\n"
git config --list
