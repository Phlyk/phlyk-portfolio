#!/bin/sh

if [ $# -ne 1 ]; then
    printf ">> Usage:"
    printf ">>   create_npmrc.sh <WRITE_ACCESS_AUTH_TOKEN>"
    exit 1;
fi

AUTH_TOKEN=$1
NPMRC_FILE_NAME=".npmrc"

printf "\n>> Determining Script DIR:\n"
scriptDir=$( cd $(dirname $0) ; pwd -P )
printf $scriptDir

printf "\n>> Determining Project Root DIR:\n"
projectRoot=${scriptDir}/../../
printf $projectRoot
cd "${projectRoot}"

printf "\n>> Creating new '${NPMRC_FILE_NAME}' file with config"
rm -f ${NPMRC_FILE_NAME} && touch ${NPMRC_FILE_NAME}
echo "@COMPANYNAME:registry=https://npm.pkg.github.com/COMPANYNAME" >> ${NPMRC_FILE_NAME}
echo "registry= https://registry.npmjs.org" >> ${NPMRC_FILE_NAME}
echo "//npm.pkg.github.com/:_authToken=${AUTH_TOKEN}" >> ${NPMRC_FILE_NAME}
echo "unsafe-perm=true" >> ${NPMRC_FILE_NAME}

printf "\n>> Done.\n"
exit 0
