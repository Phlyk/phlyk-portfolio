#!/bin/bash

if [ $# -ne 2 ]; then 
    echo ">> Usage:"
    echo ">>    stop_active_deployments.sh <DEPLOYMENT_NAME> <DEPLOYMENT_LIST_OUTPUT_FILE>"
    echo ">>    stop_active_deployments.sh my-test-deployment dm-list-output.txt"
    exit 1
fi

DEPLOYMENT_NAME=$1
DEPLOYMENTS_LIST_OUTPUT_FILE=$2

NB_OF_UNFINISHED_DEPLOYMENTS=$(cat ${DEPLOYMENTS_LIST_OUTPUT_FILE} | sed 1d | grep -v DONE | wc -l)
if [ ${NB_OF_UNFINISHED_DEPLOYMENTS} -ne 0 ]; then
    NB_DEPLOYMENTS_OF_CURRENT_NAME=$(cat ${DEPLOYMENTS_LIST_OUTPUT_FILE} | grep -v DONE | grep ${DEPLOYMENT_NAME} | wc -l)
    if [ ${NB_DEPLOYMENTS_OF_CURRENT_NAME} -ne 0 ]; then
        echo ">> Stopping active deployment of '${DEPLOYMENT_NAME}'"
        gcloud deployment-manager deployments stop ${DEPLOYMENT_NAME}
        exit $?
    else
        echo ">> All deployments of '${DEPLOYMENT_NAME}' are completed but other deployments are in progress"
    fi
else
    echo ">> No active deployments in progress"
fi
