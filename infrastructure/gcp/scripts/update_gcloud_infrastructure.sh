#!/bin/bash

# This script will be run by the CI to provision all the resources required to deploy our applications on Google Cloud
# It does the following:
# Stops any deployment currently happening with the same name
# Creates or updates the new deployment as needed

if [ $# -ne 3 ]; then
    echo ">> Usage:"
    echo ">>     update_gcloud_infrastructure.sh <APPLICATION_NAME> <APPLICATION_URL> <ENVIRONMENT>"
    echo ">>     update_gcloud_infrastructure.sh student-site dev-gc.COMPANYNAME.com development"
    exit 1
fi

APPLICATION_NAME=$1
APPLICATION_URL=$2
ENVIRONMENT=$3

WORKING_DIRECTORY=$( pwd -P )
SCRIPTS_DIR="${WORKING_DIRECTORY}/deploy/gcloud/infrastructure"
echo ">> working dir is ${WORKING_DIRECTORY}"
echo ">> scripts dir is ${SCRIPTS_DIR}"

ENVIRONMENT_SHORT_NAME=$(bash deploy/calculate_environment_short_name.sh ${ENVIRONMENT})
DEPLOYMENT_NAME="${APPLICATION_NAME}-${ENVIRONMENT_SHORT_NAME}-stack"
APP_TEMPLATES_DIR=${WORKING_DIRECTORY}/deployment-manager
APP_TEMPLATE_FILE="${APP_TEMPLATES_DIR}/${APPLICATION_NAME}.dm-template.py"

echo ">> Stopping any active deployments with same name"
DEPLOYMENTS_LIST_OUTPUT_FILE=dm_output_list.txt
gcloud deployment-manager deployments list > ${DEPLOYMENTS_LIST_OUTPUT_FILE}
bash ${SCRIPTS_DIR}/stop_active_deployments.sh ${DEPLOYMENT_NAME} ${DEPLOYMENTS_LIST_OUTPUT_FILE}

DEPLOYMENT_EXISTS=$(cat ${DEPLOYMENTS_LIST_OUTPUT_FILE} | grep -e "^${DEPLOYMENT_NAME}.*DONE")
if [ -z "${DEPLOYMENT_EXISTS}" ]; then
    DEPLOY_METHOD=create
    DESCRIPTION=creating
else
    DEPLOY_METHOD=update
    DESCRIPTION=updating
fi

DESCRIPTION="${DESCRIPTION} deployment stack"

echo -e "\n>> Commiting deployment:\n"
echo "DEPLOYMENT_NAME:${DEPLOYMENT_NAME}"
echo "APP_TEMPLATE_FILE:${APP_TEMPLATE_FILE}"
echo "DESCRIPTION:${DESCRIPTION}"
echo "DEPLOY_METHOD:${DEPLOY_METHOD}"
echo "APPLICATION_URL:${APPLICATION_URL}"

gcloud deployment-manager deployments ${DEPLOY_METHOD} "${DEPLOYMENT_NAME}" \
                    --template ${APP_TEMPLATE_FILE} \
                    --description "${DESCRIPTION}" \
                    --properties student_site_url:"${APPLICATION_URL}"
DEPLOYMENT_RETURN_CODE=$?

if [ $DEPLOYMENT_RETURN_CODE -ne 0 ]; then
    echo ">> Issue updating deployment"
    exit $DEPLOYMENT_RETURN_CODE
else
    echo ">> Deployment success!"
    exit $DEPLOYMENT_RETURN_CODE
fi
