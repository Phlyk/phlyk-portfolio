

BUILD_DIR=$1
DEPLOY_BUCKET_NAME=$2

# Add gs:// to deploy bucket name if not exists

gsutil -m cp -r $BUILD_DIR $DEPLOY_BUCKET
BUCKET_COPY_RETURN_CODE=$?

if [ $BUCKET_COPY_RETURN_CODE -ne 0 ]; then
    echo ">> Issue deploying built app to bucket $DEPLOY_BUCKET_NAME"
    exit $BUCKET_COPY_RETURN_CODE;
else
    echo ">> Deploy succes, please view the app at $DEPLOY_BUCKET_NAME"
    exit $BUCKET_COPY_RETURN_CODE;
fi
