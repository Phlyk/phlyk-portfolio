#!/bin/bash

if [ $# -ne 2 ]; then
    echo ">> Usage:"
    echo ">>    bash change_db_instance_status <OPERATION> <ENV_SHORT_NAME>"
    echo ">>    eg: bash change_db_instance_status start dev"
    exit 1
fi

OPERATION=$1
ENVIRONMENT_SHORT_NAME=$2
    
type gcloud > /dev/null 2>&1
CLI_INSTALLED=$?
if [ ${CLI_INSTALLED} -ne 0 ]; then
    echo "'gcloud' CLI not found, please install and try again"
    exit 1
fi

case "$OPERATION" in
"start")
    echo "starting"
    ACTIVATION_POLICY="ALWAYS"
    ;;
"stop")
    echo "stopping"
    ACTIVATION_POLICY="NEVER"
    ;;
*)
    echo ">> The OPERATION can either be 'start' or 'stop', exiting..."
    exit 1
    ;;
esac

INSTANCE_NAME="COMPANYNAME-${ENVIRONMENT_SHORT_NAME}-db"

echo "Patching instance:"
echo "INSTANCE_NAME:${INSTANCE_NAME}"
echo "OPERATION:${OPERATION}"

gcloud sql instances patch ${INSTANCE_NAME} --activation-policy ${ACTIVATION_POLICY}
PATCH_RETURN_CODE=$?

if [ ${PATCH_RETURN_CODE} -ne 0 ]; then
    echo ">> Issues patching the CloudSQL instance, try again or through the console"
    exit ${PATCH_RETURN_CODE}
else 
    echo ">> Patch complete, instance has been ${OPERATION}ed"
    exit ${PATCH_RETURN_CODE}
fi
