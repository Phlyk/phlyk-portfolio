"""Provisions an instance of CloudSQL with a 'django_db' DB & two users 'root' & 'django_user'."""

def GenerateConfig(context):
    """Generate configuration."""

    cloud_sql_instance_name = "COMPANYNAME-" + context.properties["environment_short_name"] + "-db"
    reference_cloud_sql_instance = "$(ref." + cloud_sql_instance_name + ".name)"
    django_db_resource_name = cloud_sql_instance_name + "-django-db"
    root_user_resource_name = cloud_sql_instance_name + "-root-user"

    resources = []
    outputs = []
    # CloudSQL Instance
    resources.append({
        "name": cloud_sql_instance_name,
        "type": "sqladmin.v1beta4.instance",
        "properties": {
            "instanceType": "CLOUD_SQL_INSTANCE",
            "databaseVersion": "MYSQL_5_7",
            "region": context.properties["region"],
            "settings": {
                "settingsVersion": "1",
                "tier": context.properties["machine_type"],
                "dataDiskSizeGb": context.properties["data_disk_size_gb"],
                "dataDiskType": "PD_SSD",
                "storageAutoResize": True,
                "storageAutoResizeLimit": context.properties["storage_resize_limit"],
                "locationPreference": {
                    "zone": context.properties["zone"]
                },
                "backupConfiguration": {
                    "enabled": False,
                    "binaryLogEnabled": False,
                    "startTime": "00:00"
                },
                "databaseFlags": [
                    {
                        "name": "sql_mode",
                        "value": "TRADITIONAL"
                    },
                    {
                        "name": "log_output",
                        "value": "FILE"
                    },
                    {
                        "name": "slow_query_log",
                        "value": "on"
                    },
                    {
                        "name": "long_query_time",
                        "value": "10"
                    }
                ],
                "ipConfiguration": {
                    "requireSsl": True,
                    "ipv4Enabled": True,
                    "authorizedNetworks": [
                        {
                            "name": "me remote",
                            "value": "X.X.X.X/24"
                        }
                    ]
                }
            }
        }
    })
    # DjangoDB for backend
    resources.append({
        "name": django_db_resource_name,
        "type": "sqladmin.v1beta4.database",
        "properties": {
            "name": "django_db",
            "instance": reference_cloud_sql_instance,
            "charset": "utf8"
        },
        "metadata": {
            "dependsOn": [cloud_sql_instance_name]
        }
    })
    # Root user
    resources.append({
        "name": root_user_resource_name,
        "type": "sqladmin.v1beta4.user",
        "properties": {
            "name": "root",
            "host": "%",
            "instance": reference_cloud_sql_instance,
            "password": context.properties["root_pass"]
        },
        "metadata": {
            "dependsOn": [django_db_resource_name]
        }
    })
    # Django user
    resources.append({
        "name": cloud_sql_instance_name + "-django-user",
        "type": "sqladmin.v1beta4.user",
        "properties": {
            "name": "django_user",
            "host": "%",
            "instance": reference_cloud_sql_instance,
            "password": context.properties["django_user_pass"]
        },
        "metadata": {
            "dependsOn": [root_user_resource_name]
        }
    })

    # CloudSQL connectionName
    outputs.append({
        "name": cloud_sql_instance_name + "connection-name",
        "value": "$(ref." + cloud_sql_instance_name + ".connectionName)"
    })
    # CloudSQL IP Address
    outputs.append({
        "name": cloud_sql_instance_name + "ip-address",
        "value": "$(ref." + cloud_sql_instance_name + ".ipAddresses[0].ipAddress)"
    })
    # CloudSQL Service Account
    outputs.append({
        "name": cloud_sql_instance_name + "service-account",
        "value": "$(ref." + cloud_sql_instance_name + ".serviceAccountEmailAddress)"
    })

    return {"resources": resources, "outputs": outputs}
