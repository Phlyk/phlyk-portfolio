"""Creates a bucket configured to host StudentSite as a static website."""


def GenerateConfig(context):
    """Generate configuration."""

    resources = []
    resources.append(
        {
            "name": context.properties["student_site_url"],
            "type": "storage.v1.bucket",
            "properties": {
                "versioning": {
                    "enabled": False
                },
                "website": {
                    "mainPageSuffix": "index.html",
                    "notFoundPage": "index.html"
                },
                "storageClass": "STANDARD",
                "location": "EUROPE-WEST3",
                "iamConfiguration": {
                    "uniformBucketLevelAccess": {
                        "enabled": True
                    }
                }
            },
            "accessControl": {
                "gcpIamPolicy": {
                    "bindings": [
                        {
                        "role": "roles/storage.objectViewer",
                        "members": [
                            "allUsers",
                            "projectViewer:{project_name}".format(project_name=context.env["project"])
                          ]
                        },
                        {
                            "role": "roles/storage.legacyBucketOwner",
                            "members": [
                            "projectEditor:{project_name}".format(project_name=context.env["project"]),
                            "projectOwner:{project_name}".format(project_name=context.env["project"])
                            ]
                        }
                    ]
                }
            }
        }
    )

    return {"resources": resources}
