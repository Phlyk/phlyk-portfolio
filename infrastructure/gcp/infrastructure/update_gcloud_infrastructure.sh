#!/bin/bash

# This script will be run by the CI to provision all the resources required to deploy our applications on Google Cloud
# It does the following:
#   Stops any deployment currently happening with the same name
#   Creates or updates the new deployment as needed

if [ $# -ne 5 ]; then
    echo ">> Usage:"
    echo ">>     update_gcloud_infrastructure.sh <APPLICATION_NAME> <REGION> <ENVIRONMENT> <ROOT_PASSWORD> <DJANGO_USER_PASSWORD>"
    echo ">>     update_gcloud_infrastructure.sh backend-db europe-west3 development supersafepass1 supersafepass2"
    exit 1
fi

APPLICATION_NAME=$1
REGION=$2
ENVIRONMENT=$3
ROOT_PASSWORD=$4
DJANGO_USER_PASSWORD=$5

PROJECT_DIR=$( pwd -P )
SCRIPT_DIR=$( cd $(dirname $0) ; pwd -P )
echo ">> PROJECT_DIR:${PROJECT_DIR}"
echo ">> WORKING_DIR:${SCRIPT_DIR}"

ENVIRONMENT_SHORT_NAME=$(bash ${SCRIPT_DIR}/../../calculate_environment_short_name.sh ${ENVIRONMENT})
DEPLOYMENT_NAME="${APPLICATION_NAME}-${ENVIRONMENT_SHORT_NAME}-stack"
APP_TEMPLATES_DIR=${PROJECT_DIR}/deployment-manager
APP_TEMPLATE_FILE="${APP_TEMPLATES_DIR}/${APPLICATION_NAME}.dm-template.py"

echo ">> Stopping any active deployments with same name"
DEPLOYMENTS_LIST_OUTPUT_FILE=dm_output_list.txt
gcloud deployment-manager deployments list > ${DEPLOYMENTS_LIST_OUTPUT_FILE}
bash ${SCRIPT_DIR}/stop_active_deployments.sh ${DEPLOYMENT_NAME} ${DEPLOYMENTS_LIST_OUTPUT_FILE}

DEPLOYMENT_EXISTS=$(cat ${DEPLOYMENTS_LIST_OUTPUT_FILE} | grep -e "^${DEPLOYMENT_NAME}.*DONE")
if [ -z "${DEPLOYMENT_EXISTS}" ]; then
    DEPLOY_METHOD=create
    DESCRIPTION=creating
else
    DEPLOY_METHOD=update
    DESCRIPTION=updating
fi

DESCRIPTION="${DESCRIPTION} deployment stack"

echo -e "\n>> Commiting deployment:\n"
echo "DEPLOYMENT_NAME:${DEPLOYMENT_NAME}"
echo "APP_TEMPLATE_FILE:${APP_TEMPLATE_FILE}"
echo "DESCRIPTION:${DESCRIPTION}"
echo "DEPLOY_METHOD:${DEPLOY_METHOD}"
echo "REGION:${REGION}"
echo "ENVIRONMENT_SHORT_NAME:${ENVIRONMENT_SHORT_NAME}"
echo "ROOT_PASSWORD:[hidden]"
echo "DJANGO_USER_PASSWORD:[hidden]"

gcloud deployment-manager deployments ${DEPLOY_METHOD} "${DEPLOYMENT_NAME}" \
    --template ${APP_TEMPLATE_FILE} \
    --description "${DESCRIPTION}" \
    --properties environment_short_name:${ENVIRONMENT_SHORT_NAME},region:${REGION},root_pass:${ROOT_PASSWORD},django_user_pass:${DJANGO_USER_PASSWORD}
DEPLOYMENT_RETURN_CODE=$?

if [ $DEPLOYMENT_RETURN_CODE -ne 0 ]; then
    echo ">> Issue updating deployment"
    exit $DEPLOYMENT_RETURN_CODE
else
    echo ">> Deployment success!"
    exit $DEPLOYMENT_RETURN_CODE
fi
