import { LexemeSearchArguments, LexemesService } from '@COMPANYNAME/frontend-services';
import { Injectable } from '@angular/core';
import { concat, Observable, of, Subject } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { SidebarLexemeSearchResults } from 'src/app/modules/sidebar/models/sidebar-lexeme-search-results.model';
import { StartStopStream, StreamState } from '../pipes/with-loading.pipe';

export enum SearchSource {
    WORDBOOK = "Wordbook",
    DICTIONARY = "Dictionary"
}

export enum Searcher {
    SIDENAV = "Sidenav",
    LANDING_PAGE = "Landing"
}

@Injectable({ providedIn: 'root' })
export class WordbookSearchService {
    searchTermListener = new Subject<LexemeSearchArguments>();
    searchLexemesTrigger$ = new Subject<LexemeSearchArguments>();
    
    private wordbookSearchResultsSource$ = new Subject<StartStopStream<SidebarLexemeSearchResults>>();

    constructor(
        private lexemesService: LexemesService,
    ) {
        this.searchLexemesTrigger$.pipe(
            switchMap((searchArguments: LexemeSearchArguments) => concat(
                of({ type: "start" as StreamState }),
                this.lexemesService.searchLexemes(searchArguments).pipe(
                    map(searchResults => {
                        const value = !searchResults ? null : { ...searchResults, searchSource: SearchSource.DICTIONARY }
                        return { type: "finish", value };
                    })
            )))
        ).subscribe(this.wordbookSearchResultsSource$);
    }

    getWordbookSearchResultsStream(): Observable<StartStopStream<SidebarLexemeSearchResults>> {
        return this.wordbookSearchResultsSource$.asObservable();
    }

    searchLexemesFor(lexemeSearchArguments: LexemeSearchArguments) {
        if (lexemeSearchArguments.searchTerm) {
            this.searchLexemesTrigger$.next(lexemeSearchArguments);
        } else {
            this.wordbookSearchResultsSource$.next({type: "finish", value: null});
        }
    }

    wordbookSearchResultsReceived(lexemeSearchResults: StartStopStream<SidebarLexemeSearchResults>) {
        this.wordbookSearchResultsSource$.next(lexemeSearchResults);
    }
}
