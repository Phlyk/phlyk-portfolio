import { Inject, Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { CourseConfigPairDTO, CourseConfigParamDTO, CourseConfigValueDTO, CourseDTO, CourseTranslationDTO, CourseWrittenLanguageDTO, StudentCourseDTO } from '../../models';
import { CourseWrapper } from '../../wrappers';
import { GrammarService } from '../../LB_fe-s/projects/frontend-services/src/lib/services/linguistic/grammar.service';
import { CourseByIdGQL, CourseConfigParamsGQL, CourseConfigParamValuePairsGQL, CoursesByUserIdGQL, CoursesGQL, DeleteCourseConfigParamGQL, DeleteCourseConfigValueGQL, DeleteCourseWrittenLanguageGQL, GQLCourseConfigParamValuePairs, InsertCourseConfigParamGQL, InsertCourseGQL, InsertCourseWrittenLanguageGQL, InsertUpdateCourseConfigValueGQL, StudentCoursesGQL, UpdateCourseConfigParamGQL, UpdateCourseGQL, UpdateCourseTranslationGQL, UpdateCourseWrittenLanguageGQL } from '../../LB_fe-s/projects/frontend-services/src/lib/services/learning/courses-data.service.graphql.generated';
import { FeedsDataService } from '../../LB_fe-s/projects/frontend-services/src/lib/services/learning/feeds-data.service';

@Injectable({
    providedIn: 'root'
})
export class CoursesDataService {

    constructor(
        @Inject(FeedsDataService) private feedsService: FeedsDataService,
        @Inject(GrammarService) private grammarService: GrammarService,
        private courseByIdQuery: CourseByIdGQL,
        private coursesQuery: CoursesGQL,
        private studentCoursesQuery: StudentCoursesGQL,
        private coursesByUserIdQuery: CoursesByUserIdGQL,
        private courseConfigParamsQuery: CourseConfigParamsGQL,
        private courseConfigParamValuePairsQuery: CourseConfigParamValuePairsGQL,
        private insertCourseMutation: InsertCourseGQL,
        private insertCourseWrittenLanguageMutation: InsertCourseWrittenLanguageGQL,
        private updateCourseWrittenLanguageMutation: UpdateCourseWrittenLanguageGQL,
        private deleteCourseWrittenLanguageMutation: DeleteCourseWrittenLanguageGQL,
        private updateCourseMutation: UpdateCourseGQL,
        private updateCourseTranslationMutation: UpdateCourseTranslationGQL,
        private insertUpdateCourseConfigValueMutation: InsertUpdateCourseConfigValueGQL,
        private deleteCourseConfigValueMutation: DeleteCourseConfigValueGQL,
        private insertCourseConfigParamMutation: InsertCourseConfigParamGQL,
        private updateCourseConfigParamMutation: UpdateCourseConfigParamGQL,
        private deleteCourseConfigParamMutation: DeleteCourseConfigParamGQL,
    ) { }

    getCourseById(id: string): Observable<CourseWrapper> {
        return this.courseByIdQuery.watch({ id })
            .valueChanges
            .pipe(
                map(payload => new CourseWrapper(payload.data.course)),
                switchMap(courseWrapper => {
                    return this.feedsService.getFeedsByCourseId(courseWrapper.course.id)
                        .pipe(map(feedNodes => {
                            courseWrapper.feeds = feedNodes;
                            return courseWrapper;
                        }))
                })
            );
    }

    getCourses(): Observable<CourseDTO[]> {
        return this.coursesQuery.fetch()
            .pipe(map(payload => payload.data.courses.edges.map(edge => edge.node)));
    }

    getStudentCourses(userId: string): Observable<StudentCourseDTO[]> {
        return this.studentCoursesQuery.watch({userId})
            .valueChanges
            .pipe(map(payload => payload.data.courses.edges.map(edge => edge.node)));
    }

    getCoursesByUserId(userId: string): Observable<CourseDTO[]> {
        return this.coursesByUserIdQuery.watch({ userId })
            .valueChanges
            .pipe(map(payload => payload.data.courses.edges.map(edge => edge.node)));
    }

    insertCourse(languageId: string, userId: string, name: string): Observable<CourseDTO> {
        return this.insertCourseMutation.mutate(
            { languageId, userId, name },
            {
                refetchQueries: [{
                    query: this.coursesQuery.document
                }]
            }
        ).pipe(map(payload => payload.data.insertCourse.course));
    }

    updateCourse(courseWrapper: CourseWrapper): Observable<CourseDTO> {
        return this.updateCourseMutation.mutate({
            id: courseWrapper.course.id,
            languageId: courseWrapper.course.language.id,
            name: courseWrapper.course.name,
            userId: courseWrapper.course.user.id
        }).pipe(map(payload => payload.data.updateCourse.course))
    }

    // ----------------- WrittenLanguages ----------------------------------------

    insertCourseWrittenLanguage(
        courseId: string,
        courseWrittenLanguage: CourseWrittenLanguageDTO,
        courseTranslation: CourseTranslationDTO
    ): Observable<CourseWrittenLanguageDTO> {
        return this.insertCourseWrittenLanguageMutation.mutate(
            {
                courseId,
                languageId: courseWrittenLanguage.language.id,
                position: courseWrittenLanguage.position,
                translationName: courseTranslation.name,
                translationDescription: courseTranslation.description
            }
        ).pipe(map(payload => payload.data.insertCourseWrittenLanguage.courseWrittenLanguage));
    }

    updateCourseWrittenLanguage(courseWrittenLanguage: CourseWrittenLanguageDTO): Observable<CourseWrittenLanguageDTO> {
        return this.updateCourseWrittenLanguageMutation.mutate({
            id: courseWrittenLanguage.id,
            languageId: courseWrittenLanguage.language.id,
            position: courseWrittenLanguage.position
        }).pipe(map(payload => payload.data.updateCourseWrittenLanguage.courseWrittenLanguage));
    }

    deleteWrittenLanguage(courseWrapper: CourseWrapper, writtenLanguageId: string): Observable<CourseWrittenLanguageDTO> {
        return this.deleteCourseWrittenLanguageMutation.mutate({ id: writtenLanguageId })
            .pipe(
                tap(_ => this.updateCourseWrittenLanguagePosition(courseWrapper)), // this needs to be moved out, completely unrelated
                map(payload => payload.data.deleteCourseWrittenLanguage.courseWrittenLanguage)
            );
    }

    private updateCourseWrittenLanguagePosition(courseWrapper: CourseWrapper): Observable<CourseWrittenLanguageDTO[]> {
        const joinedObservables: Observable<CourseWrittenLanguageDTO>[] = [];
        courseWrapper.writtenLanguages.forEach((courseWrittenLanguage, i) => {
            if (courseWrittenLanguage.position !== i) {
                courseWrittenLanguage.position = i;
                if (courseWrittenLanguage.id !== null) {
                    const observable = this.updateCourseWrittenLanguage(courseWrittenLanguage);
                    joinedObservables.push(observable);
                }
            }
        });
        return forkJoin(joinedObservables);
    }

    updateCourseTranslation(courseWrittenLanguage: CourseWrittenLanguageDTO, courseTranslation: CourseTranslationDTO): Observable<CourseTranslationDTO> {
        return this.updateCourseTranslationMutation.mutate({
            courseWrittenLanguageId: courseWrittenLanguage.id,
            name: courseTranslation.name,
            description: courseTranslation.description
        }).pipe(map(payload => payload.data.updateCourseTranslation.courseTranslation));
    }
    
    // -------- CourseDTO Config ------------------------------------------------------

    getCourseConfigParams(): Observable<CourseConfigParamDTO[]> {
        return this.courseConfigParamsQuery.watch({}, { fetchPolicy: "network-only" })
            .valueChanges
            .pipe(map(payload => payload.data.courseConfigParams.edges.map(edge => edge.node)));
    }

    getCourseConfig(courseId: string): Observable<CourseConfigPairDTO[]> {
        return this.courseConfigParamValuePairsQuery.watch({ courseId }, { fetchPolicy: "network-only" })
            .valueChanges
            .pipe(map(payload => this.setDefaultConfigParamPairValues(payload.data)));
    }

    private setDefaultConfigParamPairValues(queryData: GQLCourseConfigParamValuePairs.Query): CourseConfigPairDTO[] {
        const courseConfig = queryData.courseConfigParamValuePairs.map((pair) => {
            const pairClone = Object.assign({}, pair);
            if (pairClone.courseConfigValue == null) {
                pairClone.courseConfigValue = { id: null, value: pairClone.courseConfigParam.default };
            }
            return pairClone;
        });
        return courseConfig;
    }

    insertUpdateCourseConfigValue(courseId: string, courseConfigParamId: string, value: string): Observable<CourseConfigValueDTO> {
        return this.insertUpdateCourseConfigValueMutation.mutate({ courseId, courseConfigParamId, value })
            .pipe(map(payload => payload.data.insertUpdateCourseConfigValue.courseConfigValue));
    }

    deleteCourseConfigValue(courseId: string, courseConfigParamId: string): Observable<CourseConfigValueDTO> {
        return this.deleteCourseConfigValueMutation.mutate({ courseId, courseConfigParamId })
            .pipe(map(payload => payload.data.deleteCourseConfigValue.courseConfigValue));
    }

    insertCourseConfigParam(node: CourseConfigParamDTO): Observable<CourseConfigParamDTO> {
        return this.insertCourseConfigParamMutation.mutate({ default: node.default, key: node.key })
            .pipe(map(payload => payload.data.insertCourseConfigParam.courseConfigParam));
    }

    updateCourseConfigParam(node: CourseConfigParamDTO): Observable<CourseConfigParamDTO> {
        return this.updateCourseConfigParamMutation.mutate({
            id: node.id,
            key: node.key,
            default: node.default
        }).pipe(map(payload => payload.data.updateCourseConfigParam.courseConfigParam))
    }

    deleteCourseConfigParam(id: string): Observable<CourseConfigParamDTO> {
        return this.deleteCourseConfigParamMutation.mutate({ id })
            .pipe(map(payload => payload.data.deleteCourseConfigParam.courseConfigParam));
    }
}
