import { Feed } from 'src/app/modules/learning/model/feed/feed.model';
import { Learnable } from 'src/app/modules/learning/model/feed/learnable.model';
import { FeedPath } from "src/app/modules/learning/model/step/feed-path.model";
import { FeedStepsFactory } from 'src/app/modules/learning/model/step/feed-steps.factory';
import { FeedSteps } from 'src/app/modules/learning/model/step/feed-steps.model';
import { FeedPathFactory } from 'src/app/modules/learning/model/step/path/feed-path.factory';
import { StepGenerationService } from 'src/app/modules/learning/services/step-generation.service';
import { makeDummyLearnable } from '../test-learning.factory';

jest.mock("src/app/modules/learning/model/feed/feed.model");
jest.mock("src/app/modules/learning/model/step/feed-steps.factory");
jest.mock("src/app/modules/learning/model/step/path/feed-path.factory");
describe('StepGenerationService (isolated test)', () => {
    let sut: StepGenerationService;
    let mockFeedStepsFactory: jest.Mocked<FeedStepsFactory>;
    let mockFeedPathFactory: jest.Mocked<FeedPathFactory>;

    beforeEach(() => {
        mockFeedStepsFactory = new FeedStepsFactory(null) as any;
        mockFeedPathFactory = new FeedPathFactory(null) as any;

        sut = new StepGenerationService(mockFeedStepsFactory, mockFeedPathFactory);
    });

    test("should generate both feed steps and feed path upon initialisation", () => {
        const dummyLearnables: Learnable[] = [makeDummyLearnable(), makeDummyLearnable()];
        const mockFeed: jest.Mocked<Feed> = new Feed(null, null, null, null) as any;
        mockFeed.getLearnables.mockImplementationOnce(() => dummyLearnables);

        const dummyFeedSteps = new FeedSteps(new Map(), new Map());
        mockFeedStepsFactory.generateFeedSteps.mockImplementation(() => dummyFeedSteps);
        const dummyFeedPath = new FeedPath(new Map(), [], 33, 99);
        mockFeedPathFactory.generateInitialFeedPath.mockImplementation(() => dummyFeedPath);

        sut.initialiseFeedSteps(mockFeed);
        expect(mockFeedStepsFactory.generateFeedSteps).toBeCalledWith(mockFeed);
        expect(mockFeedPathFactory.generateInitialFeedPath).toBeCalledWith(mockFeed);

        expect(sut.getFeedSteps()).toBe(dummyFeedSteps);
        expect(sut.getFeedPath()).toBe(dummyFeedPath);
        expect(sut.getFeedLearnables()).toBe(dummyLearnables);
    });
});
