import { EnvironmentConfig } from 'src/app/core/config/environment.config';
import { FeedConfig } from 'src/app/core/config/feed.config';
import { CorrectionResults } from "src/app/modules/learning/model/correction/correction-results.model";
import { Target } from 'src/app/modules/learning/model/exercise/enquiry/target.model';
import { Translation } from 'src/app/modules/learning/model/exercise/enquiry/translation.model';
import { Review } from 'src/app/modules/learning/model/exercise/review.exercise';
import { Learnable } from 'src/app/modules/learning/model/feed/learnable.model';
import { FeedPathLearnableData } from 'src/app/modules/learning/model/step/feed-path-learnable-data.model';
import { FeedPath } from 'src/app/modules/learning/model/step/feed-path.model';
import { FeedProgress } from 'src/app/modules/learning/model/step/feed-progress.model';
import { ExerciseFeedStep } from "src/app/modules/learning/model/step/feed-step/exercise-feed-step.model";
import { FeedStep } from 'src/app/modules/learning/model/step/feed-step/feed-step.model';
import { ReviewFeedStep } from "src/app/modules/learning/model/step/feed-step/review-feed-step.model";
import { FeedSteps } from 'src/app/modules/learning/model/step/feed-steps.model';
import { FeedPathStrategy } from 'src/app/modules/learning/model/step/path/feed-path.strategy';
import { StepBuilder } from 'src/app/modules/learning/model/step/step-builder';
import { StepType } from 'src/app/modules/learning/model/step/step-type.enum';
import { NextStepService } from "src/app/modules/learning/services/next-step.service";
import { StepGenerationService } from 'src/app/modules/learning/services/step-generation.service';
import { AbilityLevel } from 'src/app/shared/models/ability/ability-level.enum';
import { AbilityLevelService } from 'src/app/shared/services/ability-level.service';
import { makeDummyLearnable } from '../test-learning.factory';

jest.mock("src/app/modules/learning/model/step/feed-path.model");
jest.mock("src/app/modules/learning/model/step/feed-steps.model");
jest.mock("src/app/modules/learning/services/step-generation.service");
jest.mock("src/app/shared/services/ability-level.service");

describe("Next Step Service (isolated test)", () => {
    let sut: NextStepService;
    let mockFeedPath: jest.Mocked<FeedPath>;
    let mockFeedSteps: jest.Mocked<FeedSteps>;
    let mockStepGenService: jest.Mocked<StepGenerationService>;
    let mockAbilityLevelService: jest.Mocked<AbilityLevelService>;

    let mockFeedPathStratCalculateNextStep: jest.Mock;

    let testDummyLearnable1: Learnable;
    let dummyLearnableData1: FeedPathLearnableData;
    let testDummyLearnable2: Learnable;
    let dummyLearnableData2: FeedPathLearnableData;
    let dummyEnvConf: EnvironmentConfig;
    let learnable1DummySteps: ExerciseFeedStep[];
    let testAbility = AbilityLevel.NOVICE;

    beforeEach(() => {
        testDummyLearnable1 = makeDummyLearnable("iBeSmartBoy123");
        dummyLearnableData1 = {
            learnable: testDummyLearnable1,
            learnableBeginningScore: 39,
            beginningStepType: StepType.CHOOSE_PHRASE_FROM_TARGET
        }
        testDummyLearnable2 = makeDummyLearnable("iBeStoopid149");
        dummyLearnableData2 = {
            learnable: testDummyLearnable2,
            learnableBeginningScore: 3,
            beginningStepType: StepType.WORD_TRANSLATE
        }

        mockFeedPath = new FeedPath(new Map(), [], 0, 0) as any;
        mockFeedPath.getNextLearnableData.mockReturnValue(dummyLearnableData1);

        mockFeedSteps = new FeedSteps(new Map(), new Map()) as any;
        const dummyStep1 = new ExerciseFeedStep(new StepBuilder().withLearnable(testDummyLearnable1).withAbilityLevel(testAbility).withType(StepType.CHOOSE_PHRASE_FROM_TARGET));
        const dummyStep2 = new ExerciseFeedStep(new StepBuilder().withLearnable(testDummyLearnable2).withAbilityLevel(testAbility).withType(StepType.WORD_TRANSLATE));
        const dummyStep3 = new ExerciseFeedStep(new StepBuilder().withLearnable(testDummyLearnable1).withAbilityLevel(testAbility).withType(StepType.WORD_TRANSLATE));
        learnable1DummySteps = [dummyStep1, dummyStep3];
        mockFeedSteps.getExerciseStepsFor.mockImplementation((learnable: Learnable, _ability) => {
            if (learnable === testDummyLearnable2) {
                return [dummyStep2];
            }
            return learnable1DummySteps;
        });

        mockStepGenService = new StepGenerationService(null, null) as any;
        mockStepGenService.getFeedPath.mockReturnValue(mockFeedPath);
        mockStepGenService.getFeedSteps.mockReturnValue(mockFeedSteps);
        mockStepGenService.getFeedLearnables.mockReturnValue([testDummyLearnable1, testDummyLearnable2]);

        mockAbilityLevelService = new AbilityLevelService(null) as any;
        mockAbilityLevelService.mapScoreToAbilityLevel.mockReturnValue(testAbility);

        mockFeedPathStratCalculateNextStep = jest.fn((arrayOfPossibleSteps: FeedStep[]) => arrayOfPossibleSteps[0]);
        const mockFeedPathStrategy: jest.Mock<FeedPathStrategy> = jest.fn(() => ({
            calculateNextStep: mockFeedPathStratCalculateNextStep
        }));

        dummyEnvConf = makeNextStepServiceDummyConfig();

        sut = new NextStepService(
            mockFeedPathStrategy(),
            dummyEnvConf,
            mockAbilityLevelService,
            mockStepGenService
        );
    });

    test("should be able to provide the next exercise step out of many candidates", () => {
        const inProgressFeedProgress = new FeedProgress();
        inProgressFeedProgress.learnableProgress.set(
            testDummyLearnable1, 
            {nbStepsCorrect: 2, nbStepsIncorrect: 1, learnableAbilityScore: 2823}
        );

        const stepRetrieved: FeedStep = sut.getNextStep(inProgressFeedProgress);

        expect(stepRetrieved).toBeInstanceOf(ExerciseFeedStep);
        expect(stepRetrieved.learnable).toBe(testDummyLearnable1);

        expect(mockFeedPath.getNextLearnableData).toHaveBeenCalled();
        expect(mockAbilityLevelService.mapScoreToAbilityLevel).toHaveBeenCalledWith(testDummyLearnable1.abilityScore);
        expect(mockFeedSteps.getExerciseStepsFor).toHaveBeenCalledWith(testDummyLearnable1, testAbility);
        expect(mockFeedPathStratCalculateNextStep).toHaveBeenCalledWith(learnable1DummySteps);
    });

    test("should provide a beginning step type when learnable first occurs", () => {
        sut.getNextStep(new FeedProgress());

        expect(mockFeedPathStratCalculateNextStep).toHaveBeenCalled();
        const calculateNextStepArgs: ExerciseFeedStep[] = mockFeedPathStratCalculateNextStep.mock.calls[0][0];
        expect(calculateNextStepArgs).toHaveLength(1)
        expect(calculateNextStepArgs[0].type).toBe(dummyLearnableData1.beginningStepType);
    });

    test("should provide other step types for subsequent occurrences of the learnable", () => {
        const inProgressFeedProgress = new FeedProgress();
        inProgressFeedProgress.learnableProgress.set(
            testDummyLearnable1, 
            {nbStepsCorrect: 2, nbStepsIncorrect: 1, learnableAbilityScore: 9393}
        );

        sut.getNextStep(inProgressFeedProgress);

        expect(mockFeedPathStratCalculateNextStep).toHaveBeenCalledWith(learnable1DummySteps);
    });

    test("should provide a review step per learnable as set per config", () => {
        mockFeedPath.getNextLearnableData
            .mockReturnValueOnce(dummyLearnableData1)
            .mockReturnValueOnce(dummyLearnableData2)
            .mockReturnValueOnce(dummyLearnableData1);

        mockFeedSteps.getReviewStepFor.mockImplementation((learnable: Learnable) => makeDummyReviewStep(learnable));
        
        const firstStepRetrieved: FeedStep = sut.getNextStep(new FeedProgress());
        expect(firstStepRetrieved).toBeInstanceOf(ExerciseFeedStep);
        expect(firstStepRetrieved.learnable).toBe(testDummyLearnable1);
        expect(mockFeedSteps.getExerciseStepsFor).toHaveBeenCalledWith(testDummyLearnable1, testAbility);

        mockFeedSteps.getExerciseStepsFor.mockClear();
        
        const secondStepRetrieved: FeedStep = sut.getNextStep(new FeedProgress());
        expect(secondStepRetrieved).toBeInstanceOf(ExerciseFeedStep);
        expect(secondStepRetrieved.learnable).toBe(testDummyLearnable2);
        expect(mockFeedSteps.getExerciseStepsFor).toHaveBeenCalledWith(testDummyLearnable2, testAbility);
        
        mockFeedSteps.getExerciseStepsFor.mockClear();
        
        const thirdStepRetrieved: FeedStep = sut.getNextStep(new FeedProgress());
        expect(thirdStepRetrieved).toBeInstanceOf(ReviewFeedStep);
        expect(thirdStepRetrieved.learnable).toBe(testDummyLearnable1);
        expect(mockFeedSteps.getExerciseStepsFor).not.toHaveBeenCalled();
    });
    
    test("should reset the counter when a review step is provided", () => {
        mockFeedSteps.getReviewStepFor.mockImplementation((learnable: Learnable) => makeDummyReviewStep(learnable));

        expect(sut["learnableStepCounter"].get(testDummyLearnable1.id)).toBe(0);

        const firstStepRetrieved: FeedStep = sut.getNextStep(new FeedProgress());
        expect(firstStepRetrieved).toBeInstanceOf(ExerciseFeedStep);
        expect(firstStepRetrieved.learnable).toBe(testDummyLearnable1);
        expect(sut["learnableStepCounter"].get(testDummyLearnable1.id)).toBe(1);
        
        const secondStepRetrieved: FeedStep = sut.getNextStep(new FeedProgress());
        expect(secondStepRetrieved).toBeInstanceOf(ReviewFeedStep);
        expect(secondStepRetrieved.learnable).toBe(testDummyLearnable1);
        expect(sut["learnableStepCounter"].get(testDummyLearnable1.id)).toBe(0);
    });

    test("should provide exercise steps that correspond to the user's learnable ability", () => {
        const stepRetrieved: FeedStep = sut.getNextStep(new FeedProgress())

        expect(stepRetrieved).toBeInstanceOf(ExerciseFeedStep);
        expect(stepRetrieved.abilityLevel).toBe(testAbility);
    });


    test("should indicate feed end (null) when the user has done all the learnables for feed", () => {
        mockFeedPath.getNextLearnableData.mockReturnValue(null);

        const returnedNextStep: FeedStep = sut.getNextStep(new FeedProgress());

        expect(returnedNextStep).toBeNull();
    });

    test("should update the feedpath when a step gets completed", () => {
        const dummyCompleteStep = new ExerciseFeedStep(new StepBuilder());
        const dummyCompleteExerciseData = new CorrectionResults(10322394234);
        dummyCompleteExerciseData.answerCorrect = true;
        sut.stepComplete(dummyCompleteStep, dummyCompleteExerciseData);

        expect(mockFeedPath.adjustPathForCompletedStep).toBeCalledWith(dummyCompleteStep, dummyCompleteExerciseData);
    });

    test("should be able to give a percentage based completion of the feed", () => {
        const stepsDone = 9;
        const totalStepsInPath = 20;
        const expectedProgressValue = 45;

        mockFeedPath.getNbStepsInFeed.mockReturnValue(totalStepsInPath);

        const progressValue = sut.calculateProgressValue(stepsDone);

        expect(progressValue).toBe(expectedProgressValue);
    });

});

function makeNextStepServiceDummyConfig(): EnvironmentConfig {
    const dummyEnvConf = new EnvironmentConfig();
    const dummyFeedConf = new FeedConfig();
    dummyFeedConf.abilityLevelToAvailableExercises = createDummyAbilityLevelStepConfig();
    dummyEnvConf.feedConfig = dummyFeedConf;
    return dummyEnvConf;
}

function createDummyAbilityLevelStepConfig(): object {
    return {
        NOVICE: {
            minReviewStepFrequency: 1,
        }
    }
}

function makeDummyReviewStep(withLearnable?: Learnable): ReviewFeedStep {
    const dummyReviewExercise = new Review(
        "u nid gud langwej", 
        0,
        null, 
        null, 
        new Target("sqwerpyNerps", false, null, []),
        new Translation("flerpySqwerps", true, []));
    return new StepBuilder()
        .withReview(dummyReviewExercise)
        .withLearnable(withLearnable)
        .withType(StepType.REVIEW_EXAMPLE)
        .build(ReviewFeedStep);
}
