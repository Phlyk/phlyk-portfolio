import { LogService } from "@COMPANYNAME/frontend-services";
import { EnvironmentConfig } from "src/app/core/config/environment.config";
import { FeedConfig } from "src/app/core/config/feed.config";
import { ProcessedDiff } from "src/app/modules/learning/model/correction/correction-result.model";
import { CorrectionResults, HARD_FAIL_SCORE } from "src/app/modules/learning/model/correction/correction-results.model";
import { Correction } from "src/app/modules/learning/model/correction/correction.enum";
import { CharacterDiffCalculator } from "src/app/modules/learning/model/correction/diff/calculator/character-diff-calculator";
import { Diff, DiffCompare } from "src/app/modules/learning/model/correction/diff/diff.model";
import { DiffProcessor } from "src/app/modules/learning/model/correction/diff/processor/diff-processor";
import { CorrectionLimitException } from "src/app/modules/learning/model/correction/rule/exception/correction-limit.exception";
import { Answerable } from "src/app/modules/learning/model/exercise/exercise.model";
import { CorrectionRulesFactory } from "src/app/modules/learning/model/factory/correction-rules.factory";
import { Learnable } from "src/app/modules/learning/model/feed/learnable.model";
import { FeedStep } from "src/app/modules/learning/model/step/feed-step/feed-step.model";
import { StepState } from "src/app/modules/learning/model/step/feed-step/step-state.enum";
import { StepType } from "src/app/modules/learning/model/step/step-type.enum";
import { CorrectionService } from "src/app/modules/learning/services/correction.service";
import { AbilityLevel } from "src/app/shared/models/ability/ability-level.enum";
import { MaybeMocked, mocked } from "ts-jest/dist/utils/testing";

const mockProcessTokenDiffs = jest.fn();
const mockProcessCharDiffs = jest.fn();
jest.mock("src/app/modules/learning/model/correction/diff/processor/diff-processor", () => ({
    DiffProcessor: jest.fn(() => ({
        processTokenDiffs: mockProcessTokenDiffs,
        processCharacterDiffs: mockProcessCharDiffs,
    })),
}));

const mockCalculateCharDiffs = jest.fn();
jest.mock("src/app/modules/learning/model/correction/diff/calculator/character-diff-calculator", () => ({
    CharacterDiffCalculator: jest.fn(() => ({
        calculateDiff: mockCalculateCharDiffs,
    })),
}));

const mockLogError = jest.fn();
jest.mock("@COMPANYNAME/frontend-services", () => ({
    LogService: jest.fn(() => ({
        logError: mockLogError,
    })),
}));

describe("CorrectionService (isolated test)", () => {
    let sut: CorrectionService;

    let mockDiffProcessor: MaybeMocked<DiffProcessor>;
    let mockCharDiffCalculator: MaybeMocked<CharacterDiffCalculator>;

    const answer = "you wish you were correct";

    beforeEach(() => {
        mockDiffProcessor = mocked(new DiffProcessor(null, 3));
        mockCharDiffCalculator = mocked(new CharacterDiffCalculator());
        mockCharDiffCalculator.calculateDiff.mockImplementation(diffs => diffs);

        const config = new EnvironmentConfig();
        config.feedConfig = { correctionRules: {} } as FeedConfig;
        const correctionRulesFactory = new CorrectionRulesFactory(config);
        sut = new CorrectionService(correctionRulesFactory, config, new LogService(null));
    });

    test("should make correction results for every slot when exercise answer is correct", () => {
        const input = answer;

        const testStep = new DummyFeedStep(StepType.CHARACTER_SLOT_PHRASE_TRANSLATE, answer, input);

        const correctionResults = sut.correctAnswerFor(testStep);

        expect(correctionResults).toBeInstanceOf(CorrectionResults);
        expect(correctionResults.correctionMap.size).toEqual(1); // one slot group
        expect(correctionResults.correctionMap.get(0).length).toEqual(input.split("").length); // one slot for each char
        correctionResults.correctionMap.forEach((tokenDiffs: ProcessedDiff[], _) => {
            expect(tokenDiffs[0].correctionType).toEqual(Correction.EQUAL);
        });

        expect(correctionResults.unprocessedCorrections).toEqual([]);
        expect(mockDiffProcessor.processCharacterDiffs).not.toHaveBeenCalled();
        expect(mockDiffProcessor.processTokenDiffs).not.toHaveBeenCalled();
    });

    test("should make a fail correction for every slot is if the student has not entered any input", () => {
        const correctionResults = sut.correctAnswerFor(new DummyFeedStep(StepType.TOKEN_SLOT_PHRASE_TRANSLATE, answer, ""));

        expect(correctionResults.answerCorrect).toBeFalsy();
        expect(correctionResults.correctedScore).toEqual(HARD_FAIL_SCORE);
        expect(correctionResults.correctionMap.size).toEqual(1); // one slot group
        expect(correctionResults.correctionMap.get(0).length).toEqual(answer.split("").length); // one slot for each char
        correctionResults.correctionMap.forEach((tokenDiffs: ProcessedDiff[], _) => {
            expect(tokenDiffs[0].correctionType).toEqual(Correction.UNMATCHED);
        });
    });

    test("should calculate and apply the token correction rules first, then character rules", () => {
        const input = "wish you were you corekt";

        mockDiffProcessor.processTokenDiffs.mockImplementation((_, diffs, results) => {
            diffs.forEach(diff => {
                const isDiffToBeFilteredOut = [
                    DiffCompare.EQUAL,
                    DiffCompare.WRONG_ORDER_INPUT,
                    DiffCompare.WRONG_ORDER_ANSWER,
                ].includes(diff[0]);
                if (!isDiffToBeFilteredOut) {
                    results.unprocessedCorrections.push([diff]);
                }
            });
            return results;
        });

        const expectedDiffsToBePassedToCharProcessor: Diff[][] = [
            [[DiffCompare.MISSING, "correct", 4]],
            [[DiffCompare.EXTRA, "corekt", 4]],
        ];

        mockDiffProcessor.processCharacterDiffs.mockImplementation((_, _diffs, results) => {
            results.unprocessedCorrections = [];
            return results;
        });

        const correctionResults = sut.correctAnswerFor(new DummyFeedStep(StepType.FREE_TEXT_PHRASE_TRANSLATE, answer, input));

        expect(mockDiffProcessor.processTokenDiffs).toHaveBeenCalled();
        expect(mockCharDiffCalculator.calculateDiff).toHaveBeenCalledWith(expectedDiffsToBePassedToCharProcessor);
        expect(correctionResults.unprocessedCorrections).toEqual([]);
    });

    test("should pass the results of the token correction rules to the character diff calculator", () => {
        const input = "wish you were you corekt";

        const dummyTokenToPassOn: Diff[][] = [[[DiffCompare.MISSING, "correct", 4]], [[DiffCompare.EXTRA, "corekt", 4]]];
        mockDiffProcessor.processTokenDiffs.mockImplementation((_, _diffs, results) => {
            results.setUnprocessedDiffs(dummyTokenToPassOn);
            return results;
        });

        const dummyCalculatedCharDiffs: Diff[][] = [
            [
                [DiffCompare.MISSING, "r", 3],
                [DiffCompare.MISSING, "c", 5],
                [DiffCompare.EXTRA, "k", 4],
            ],
        ];
        mockCharDiffCalculator.calculateDiff.mockImplementationOnce(_diffs => dummyCalculatedCharDiffs);
        mockDiffProcessor.processCharacterDiffs.mockImplementation((_, diffs, results) => {
            const onlyCorrection: ProcessedDiff = {
                message: "mate... seriously??",
                tokenIndex: 4,
                displayAnswer: "c",
                input: "k",
            };
            results.addCharCorrection(onlyCorrection, 4);
            results.setUnprocessedDiffs([[diffs[0][0]]]);
            return results;
        });

        const correctionResults = sut.correctAnswerFor(new DummyFeedStep(StepType.FREE_TEXT_PHRASE_TRANSLATE, answer, input));

        expect(mockDiffProcessor.processTokenDiffs).toHaveBeenCalled();
        expect(mockCharDiffCalculator.calculateDiff).toHaveBeenCalledWith(dummyTokenToPassOn);
        expect(mockDiffProcessor.processCharacterDiffs).toHaveBeenCalled();
        expect(correctionResults.unprocessedCorrections).toEqual([[dummyCalculatedCharDiffs[0][0]]]);
    });

    test("should not apply character corrections if token rules have matched all token diffs", () => {
        const input = "a jim jammy jip I am";

        mockDiffProcessor.processTokenDiffs.mockImplementation((step, diffs, results) => {
            results.unprocessedCorrections = [];
            return results;
        });

        const correctionResults = sut.correctAnswerFor(new DummyFeedStep(StepType.FREE_TEXT_PHRASE_TRANSLATE, answer, input));

        expect(correctionResults.unprocessedCorrections).toEqual([]);
        expect(mockDiffProcessor.processCharacterDiffs).not.toHaveBeenCalled();
    });

    test("should create fail corrections for every slot if CorrectionLimitException is thrown", () => {
        const input = "input doesn't matter here";

        mockDiffProcessor.processTokenDiffs.mockImplementationOnce((_i, _dont, _care) => {
            throw new CorrectionLimitException("Hey bud, you made a few too many mistakes");
        });

        /**
         * This helper function emphasises the fact that we don't have multiple SlotGroups yet
         * and so a phrase is split only by char. This will change and so should this test. 
         */
        const makeFailCorrectionsForEveryChar = answer => {
            const inputSplitByChar = input.split("");
            return answer.split("").map((charOfAnswer, charIdx) => ({
                charIndex: charIdx,
                correctionType: Correction.UNMATCHED,
                displayAnswer: charOfAnswer,
                input: inputSplitByChar[charIdx],
            }));
        };
        const expectedFailCorrections: [number, ProcessedDiff[]][] = [[0, makeFailCorrectionsForEveryChar(answer)]];

        const correctionResults = sut.correctAnswerFor(new DummyFeedStep(StepType.TOKEN_SLOT_PHRASE_TRANSLATE, answer, input));

        expect(correctionResults.correctedScore).toEqual(HARD_FAIL_SCORE);
        expect(correctionResults.correctionMap).toEqual(new Map(expectedFailCorrections));
        expect(correctionResults.answerCorrect).toBeFalsy();
    });

    test("should rethrow any other Error if it is not a CorrectionLimitException", () => {
        const input = "lingbuzza123";

        mockDiffProcessor.processTokenDiffs.mockImplementationOnce((_i, _dont, _care) => {
            throw new Error("Generic non correction related error");
        });

        const correctionResults = () => sut.correctAnswerFor(new DummyFeedStep(StepType.FREE_TEXT_PHRASE_TRANSLATE, answer, input));

        expect(correctionResults).toThrowError("");
    });
});

class DummyFeedStep implements FeedStep {
    type: StepType;
    abilityLevel: AbilityLevel;
    learnable: Learnable;
    showLexemes?: boolean;
    revealButton?: boolean;
    state?: StepState;

    constructor(type: StepType, private dummyAnswer: string, private dummyInput: string) {
        this.type = type;
    }

    getMaximumScore(): number {
        return 55;
    }
    getCorrectAnswer(): Answerable {
        return { fullStringRepresentation: this.dummyAnswer, slotStringRepresentations: this.dummyAnswer.split("") };
    }
    getInput(): Answerable {
        return { fullStringRepresentation: this.dummyInput, slotStringRepresentations: this.dummyInput.split("") };
    }
    isAnswerCorrect(): boolean {
        return this.dummyAnswer == this.dummyInput;
    }

    exerciseCorrected(correctionResult: CorrectionResults): void {
        throw new Error("Method not implemented.");
    }
    processCorrections(correctionResult: CorrectionResults) {
        throw new Error("Method not implemented.");
    }
    addInput(input: string) {
        throw new Error("Method not implemented.");
    }
    isReadyToMark(): boolean {
        throw new Error("Method not implemented.");
    }
    removeLastInput(): boolean {
        throw new Error("Method not implemented.");
    }
    updateState(_: CorrectionResults) {
        throw new Error("Method not implemented.");
    }
}
