import { CorrectionResults } from "src/app/modules/learning/model/correction/correction-results.model";
import { ExerciseFeedStep } from "src/app/modules/learning/model/step/feed-step/exercise-feed-step.model";
import { CorrectionService } from "src/app/modules/learning/services/correction.service";
import { CurrentStepService } from "src/app/modules/learning/services/current-step.service";

jest.mock("src/app/modules/learning/services/correction.service");
jest.mock("src/app/modules/learning/model/step/feed-step/exercise-feed-step.model");

describe("Current Step Service (isolated test)", () => {
    let sut: CurrentStepService;

    let mockCorrectionService: jest.Mocked<CorrectionService>;
    let mockFeedStep: jest.Mocked<ExerciseFeedStep>;
    

    beforeEach(() => {
        mockCorrectionService = new CorrectionService(null, null, null) as any;
        mockFeedStep = new ExerciseFeedStep(null) as any;
        Object.defineProperty(mockFeedStep, "learnable", { value: jest.fn() });

        sut = new CurrentStepService(mockCorrectionService);
    });

    test("should call the correction service to correct the exercises answer", () => {
        const dummyCorrectionResults = new CorrectionResults(99);
        mockCorrectionService.correctAnswerFor.mockReturnValue(dummyCorrectionResults);
        
        sut.changeStep(mockFeedStep);
        sut.markExerciseAndSend();

        expect(mockCorrectionService.correctAnswerFor).toHaveBeenCalledWith(mockFeedStep);
    });
});
