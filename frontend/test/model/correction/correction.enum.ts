export enum Correction {
    UNMATCHED = -1,
    EQUAL = 0,
    EXTRA_CHAR = 1,
    EXTRA_TOKEN = 2,
    MISSING_CHAR = 3,
    MISSING_TOKEN = 4,
    BASE_CHAR = 5,
    SIMILAR_CHAR = 6,
    WRONG_ORDER = 7,
}

export namespace Correction {
    export function charCorrections(): Correction[] {
        return [Correction.EXTRA_CHAR, Correction.MISSING_CHAR, Correction.BASE_CHAR, Correction.SIMILAR_CHAR];
    }
    
    export function isCharCorrection(correction: Correction): boolean {
        return charCorrections().includes(correction);
    }

    export function tokenCorrections(): Correction[] {
        return [Correction.EXTRA_TOKEN, Correction.MISSING_TOKEN, Correction.WRONG_ORDER];
    }

    export function isTokenCorrection(correction: Correction): boolean {
        return tokenCorrections().includes(correction);
    }
}
