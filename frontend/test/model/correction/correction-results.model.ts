import { ProcessedDiff } from "./correction-result.model";
import { Correction } from "./correction.enum";
import { Diff } from "./diff/diff.model";
import { CorrectionRuleResult } from "./rule/correction-rule-result.model";

export const HARD_FAIL_SCORE = -3;
export type Correctness = "correct" | "corrected" | "incorrect"

export class CorrectionResults {
    answerCorrect: boolean;
    correctness: Correctness;
    exerciseMessage: string;
    correctedScore: number;
    
    unprocessedCorrections: Diff[][] = [];
    correctionMap: Map<number, ProcessedDiff[]> = new Map();

    constructor(public readonly exerciseScore: number) {
        this.correctedScore = exerciseScore;
    }

    correctAnswer(corrections: ProcessedDiff[][]): CorrectionResults {
        this.answerCorrect = true;
        this.correctness = "correct";
        this.exerciseMessage = "well done faggot";
        const correctionsAsMap: [number, ProcessedDiff[]][] = corrections.map((tokenCorrection, tokenIdx) => [tokenIdx, tokenCorrection]);
        this.correctionMap = new Map(correctionsAsMap);
        return this;
    }

    hardFail(failCorrections: ProcessedDiff[][]): CorrectionResults {
        this.correctedScore = HARD_FAIL_SCORE;
        this.exerciseMessage = "big fuck up son, u r gr0und3d";
        this.answerCorrect = false;
        this.correctness = "incorrect";

        const correctionsAsMap: [number, ProcessedDiff[]][] = failCorrections.map((tokenCorrection, tokenIdx) => [tokenIdx, tokenCorrection]);
        this.correctionMap = new Map(correctionsAsMap);
        return this;
    }

    applyCorrection(correctedResult: CorrectionRuleResult) {
        this.correctedScore -= correctedResult.penaltyScore;
        const matchedDiffs: ProcessedDiff[][] = correctedResult.matchedDiffs;
        matchedDiffs.forEach((matchedTokenDiffs: ProcessedDiff[]) => {
            matchedTokenDiffs.forEach((diff: ProcessedDiff) => {
                this.addProcessedDiff(diff);
            });
        });
        this.correctness = "corrected";
        this.exerciseMessage = "Close but no cigar my friend";
    }

    getSortedCorrections(): ProcessedDiff[][] {
        return [...this.correctionMap]
            .sort((a: [number, ProcessedDiff[]], b: [number, ProcessedDiff[]]) => {
                return a[0] - b[0];
            })
            .map(mapEntryAsArray => mapEntryAsArray[1])
            .map((processedTokenCorrection: ProcessedDiff[]) =>
                processedTokenCorrection.sort((a, b) => {
                    if (a.correctionType == Correction.MISSING_CHAR) {
                        return -1;
                    }
                    if (b.correctionType == Correction.MISSING_CHAR) {
                        return 1;
                    }
                    return a.charIndex - b.charIndex;
                })
            );
    }

    setUnprocessedDiffs(unprocessedDiffs: Diff[][]) {
        this.unprocessedCorrections = unprocessedDiffs;
    }

    addTokenCorrection(diff: ProcessedDiff) {
        this.correctionMap.set(diff.tokenIndex, [diff]);
    }

    addCharCorrection(diff: ProcessedDiff, tokenIndex: number) {
        const charDiffs = this.correctionMap.get(tokenIndex);
        const updatedCharDiffs: ProcessedDiff[] = this.insertDiffToCharDiffs(diff, charDiffs);
        this.correctionMap.set(tokenIndex, updatedCharDiffs);
    }

    private addProcessedDiff(diff: ProcessedDiff) {
        if (diff.charIndex == null) {
            return this.addTokenCorrection(diff);
        }
        this.addCharCorrection(diff, diff.tokenIndex);
    }

    private insertDiffToCharDiffs(diff: ProcessedDiff, charDiffs: ProcessedDiff[]): ProcessedDiff[] {
        if (!charDiffs) {
            return [diff];
        }
        const insertDiffCharIndex = diff.charIndex;
        const indexToInsert = this.findOrderedInsertIndex(insertDiffCharIndex, charDiffs);
        charDiffs.splice(indexToInsert, 0, diff);
        return charDiffs;
    }

    private findOrderedInsertIndex(insertDiffIndex: number, charDiffs: ProcessedDiff[]): number {
        for (let indexInToken = 0; indexInToken < charDiffs.length; indexInToken++) {
            const charDiff = charDiffs[indexInToken];

            const currDiffCharIndex = charDiff.charIndex;
            if (insertDiffIndex < currDiffCharIndex) {
                return indexInToken;
            }

            const nextDiffIndex = indexInToken + 1;
            const nextDiff = charDiffs[nextDiffIndex];
            if (!nextDiff) {
                return currDiffCharIndex <= insertDiffIndex ? nextDiffIndex : indexInToken;
            }

            const nextDiffCharIndex = nextDiff.charIndex;
            const isDiffInBetweenCurrAndNext = currDiffCharIndex < insertDiffIndex && insertDiffIndex <= nextDiffCharIndex;
            if (isDiffInBetweenCurrAndNext) {
                return nextDiffIndex;
            }
        }
        return 0;
    }
}
