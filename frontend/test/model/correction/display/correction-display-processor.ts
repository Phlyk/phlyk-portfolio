import { CorrectionSlotGroup } from "../../slot/correction/correction-slot-group.model";
import { CorrectionSlot } from "../../slot/correction/correction-slot.model";
import { ProcessedDiff } from "../correction-result.model";
import { CorrectionResults } from "../correction-results.model";

export class CorrectionDisplayProcessor {
    static processSlotExerciseCorrections(correctionResults: CorrectionResults): CorrectionSlotGroup[] {
        const correctionSlotGroups: CorrectionSlotGroup[] = [];

        const sortedCorrections = correctionResults.getSortedCorrections();
        sortedCorrections.forEach((tokenDiffs: ProcessedDiff[]) => {
            const correctionSlots: CorrectionSlot[] = [];
            tokenDiffs.forEach((charCorrection: ProcessedDiff) => {
                correctionSlots.push(...this.createCorrectionSlot(charCorrection));
            });
            correctionSlotGroups.push(new CorrectionSlotGroup(correctionSlots));
        });

        console.log("Corrections Processed:", correctionSlotGroups);
        return correctionSlotGroups;
    }

    private static createCorrectionSlot(correction: ProcessedDiff): CorrectionSlot[] {
        let correctionFullInput = correction.input;
        return correction.displayAnswer.split("").map((correctionChar, charIndex) => {
            const newCorrection: ProcessedDiff = Object.assign({}, correction);
            if (correctionFullInput) {
                correctionFullInput = correction.input.slice(charIndex);
                const inputWithOffset = charIndex == correction.displayAnswer.length - 1
                    ? correctionFullInput 
                    : correctionFullInput[0];
                newCorrection.input = inputWithOffset;
            }
            return new CorrectionSlot(correctionChar, newCorrection);
        });
    }
}
