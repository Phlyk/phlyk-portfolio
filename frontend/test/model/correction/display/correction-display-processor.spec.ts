import { ProcessedDiff } from "src/app/modules/learning/model/correction/correction-result.model";
import { CorrectionResults } from "src/app/modules/learning/model/correction/correction-results.model";
import { CorrectionDisplayProcessor } from "src/app/modules/learning/model/correction/display/correction-display-processor";
import { CorrectionSlotGroup } from "src/app/modules/learning/model/slot/correction/correction-slot-group.model";
import { makeDummyCorrection } from "../correction-spec-helper";

describe("CorrectionResults (isolated test)", () => {
    let _sut: CorrectionDisplayProcessor;

    test("should transform corrections to a CorrectionSlotGroup for display", () => {
        const dummyCorrectionResults = new CorrectionResults(99);
        dummyCorrectionResults.addTokenCorrection(makeDummyCorrection(1, 0));
        dummyCorrectionResults.addTokenCorrection(makeDummyCorrection(2, 1));
        dummyCorrectionResults.addTokenCorrection(makeDummyCorrection(2, 2));
        dummyCorrectionResults.addTokenCorrection(makeDummyCorrection(3, 0));

        const receivedSlotGroups = CorrectionDisplayProcessor.processSlotExerciseCorrections(dummyCorrectionResults);

        expect(receivedSlotGroups).toHaveLength(3);
        receivedSlotGroups.forEach(slotGroup => expect(slotGroup).toBeInstanceOf(CorrectionSlotGroup))
    });

    test("should align the input of the correction with the display answer", () => {
        const dummyAnswer = "schmaps";
        const dummyInput = "scheeps";
        const fancyAssCorrection: ProcessedDiff = {
            charIndex: 0,
            tokenIndex: 0,
            correctionType: -99,
            displayAnswer: dummyAnswer,
            input: dummyInput,
            message: "just a test, don't cry now",
        };

        const dummyCorrectionResults = new CorrectionResults(99);
        dummyCorrectionResults.addTokenCorrection(fancyAssCorrection);

        const displayReadyResults = CorrectionDisplayProcessor.processSlotExerciseCorrections(dummyCorrectionResults);

        expect(displayReadyResults).toHaveLength(1);
        expect(displayReadyResults[0].correctionSlots).toHaveLength(dummyAnswer.length);
        displayReadyResults[0].correctionSlots.forEach((correctionSlotsForChars, charIdx) => {
            expect(correctionSlotsForChars.displayAnswer).toEqual(dummyAnswer[charIdx]);
            expect(correctionSlotsForChars.correction.input).toEqual(dummyInput[charIdx]);
        });
    });

    test("should not align the correct input and display answer if there is no input", () => {
        const dummyAnswer = "sploops";
        const fancyAssCorrection: ProcessedDiff = {
            charIndex: 0,
            tokenIndex: 0,
            correctionType: -99,
            displayAnswer: dummyAnswer,
            message: "just a test, don't cry now",
        };

        const dummyCorrectionResults = new CorrectionResults(99);
        dummyCorrectionResults.addTokenCorrection(fancyAssCorrection);

        const displayReadyResults = CorrectionDisplayProcessor.processSlotExerciseCorrections(dummyCorrectionResults);

        expect(displayReadyResults).toHaveLength(1);
        expect(displayReadyResults[0].correctionSlots).toHaveLength(dummyAnswer.length);
        displayReadyResults[0].correctionSlots.forEach((correctionSlotsForChars, charIdx) => {
            expect(correctionSlotsForChars.displayAnswer).toEqual(dummyAnswer[charIdx]);
            expect(correctionSlotsForChars.correction.input).toBeUndefined();
        });
    });

    test("should align student input and answer as best as it can when there is a difference in length", () => {
        const dummyAnswer = "splops";
        const dummyInput = "schmeeps";
        const fancyAssCorrection: ProcessedDiff = {
            charIndex: 0,
            tokenIndex: 0,
            correctionType: -99,
            displayAnswer: dummyAnswer,
            input: dummyInput,
            message: "just a test, don't cry now",
        };

        const dummyCorrectionResults = new CorrectionResults(99);
        dummyCorrectionResults.addTokenCorrection(fancyAssCorrection);

        const displayReadyResults = CorrectionDisplayProcessor.processSlotExerciseCorrections(dummyCorrectionResults);

        expect(displayReadyResults).toHaveLength(1);
        expect(displayReadyResults[0].correctionSlots).toHaveLength(dummyAnswer.length);
        
        const correctionSlotsThatPairUp = displayReadyResults[0].correctionSlots.slice(0, dummyAnswer.length -1 );
        correctionSlotsThatPairUp.forEach((correctionSlotsForChars, charIdx) => {
            expect(correctionSlotsForChars.displayAnswer).toEqual(dummyAnswer[charIdx]);
            expect(correctionSlotsForChars.correction.input).toEqual(dummyInput[charIdx]);
        });

        const unpairedRemainder = displayReadyResults[0].correctionSlots[dummyAnswer.length -1 ];
        expect(unpairedRemainder.correction.input).toEqual(dummyInput.slice(dummyAnswer.length - 1));
    });
});
