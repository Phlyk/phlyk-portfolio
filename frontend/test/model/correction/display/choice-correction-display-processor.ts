import { AbstractChoiceExercise } from "../../exercise/choice/abstract-choice.exercise";
import { CorrectionChoice } from "../../slot/correction/correction-choice.model";
import { CorrectionSlotGroup } from "../../slot/correction/correction-slot-group.model";
import { CorrectionResults } from "../correction-results.model";
import { CorrectionDisplayProcessor } from "./correction-display-processor";

export class ChoiceCorrectionDisplayProcessor {
    static processChoiceExerciseCorrections(
        choiceExercise: AbstractChoiceExercise,
        correctionResults: CorrectionResults
    ): CorrectionChoice {
        const correctionSlotGroups: CorrectionSlotGroup[] = CorrectionDisplayProcessor.processSlotExerciseCorrections(
            correctionResults
        );
        return new CorrectionChoice(
            correctionSlotGroups,
            choiceExercise.choices.selectedChoiceLabel,
            choiceExercise.choices.provideCorrectAnswerLabel(),
            choiceExercise.isAnswerCorrect()
        );
    }
}
