import { Correction } from "./correction.enum";
import { Diff } from "./diff/diff.model";

export interface CorrectionResult {
    matchedDiffs?: Diff[][];
    unprocessedDiffs: Diff[][];
    penaltyScore: number;
    message: string;
}

export type ProcessedDiff = {
    correctionType?: Correction;
    displayAnswer?: string;
    input?: string;
    inputIndex?: number;
    message?: string;
    tokenIndex?: number;
    charIndex?: number;
};

export interface CorrectionDisplay {
    message: string;
    correctionType: Correction;
    inputIndex?: number;
    input: string;
}


