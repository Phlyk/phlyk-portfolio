import { ProcessedDiff } from "src/app/modules/learning/model/correction/correction-result.model";
import { CorrectionResults } from "src/app/modules/learning/model/correction/correction-results.model";
import { Diff, DiffCompare } from "src/app/modules/learning/model/correction/diff/diff.model";
import { DiffProcessor } from "src/app/modules/learning/model/correction/diff/processor/diff-processor";
import { BaseCharacterCorrectionRule } from "src/app/modules/learning/model/correction/rule/character/base-character-correction-rule";
import { CatchRemainderCorrectionRule } from "src/app/modules/learning/model/correction/rule/character/catch-remainder-correction-rule";
import { ExtraCharacterCorrectionRule } from "src/app/modules/learning/model/correction/rule/character/extra-character-correction-rule";
import { MissingCharacterCorrectionRule } from "src/app/modules/learning/model/correction/rule/character/missing-character-correction-rule";
import { SimilarCharacterCorrectionRule } from "src/app/modules/learning/model/correction/rule/character/similar-character-correction-rule";
import { CorrectionRuleResult } from "src/app/modules/learning/model/correction/rule/correction-rule-result.model";
import { CorrectionRule } from "src/app/modules/learning/model/correction/rule/correction-rule.model";
import { CorrectionLimitException } from "src/app/modules/learning/model/correction/rule/exception/correction-limit.exception";
import { ExtraTokenCorrectionRule } from "src/app/modules/learning/model/correction/rule/token/extra-token-correction-rule";
import { MissingTokenCorrectionRule } from "src/app/modules/learning/model/correction/rule/token/missing-token-correction-rule";
import { WrongOrderTokenCorrectionRule } from "src/app/modules/learning/model/correction/rule/token/wrong-order-token-correction-rule";
import { FeedStep } from "src/app/modules/learning/model/step/feed-step/feed-step.model";
import { StepType } from "src/app/modules/learning/model/step/step-type.enum";
import { makeCharacterDiffsOutOfDiffs, makeFullDiffBetween, makeTokenDiffBetween } from "../../rule/shared-correction-test-helper";

describe("DiffProcessor (isolated test)", () => {
    let sut: DiffProcessor;

    let testCorrectionResults: CorrectionResults;
    let testCharacterRules: Map<StepType, CorrectionRule[]>;
    let testTokenRules: Map<StepType, CorrectionRule[]>;
    const catchRemainderRule = new CatchRemainderCorrectionRule(1);
    let catchRemainderRuleCorrectionSpy: jest.SpyInstance;

    const dummyFeedStep: FeedStep = { type: StepType.WORD_TRANSLATE } as FeedStep;
    const nbCorrectionsPermitted = 2;

    beforeEach(() => {
        testCharacterRules = new Map();
        testTokenRules = new Map();
        catchRemainderRuleCorrectionSpy = jest.spyOn(catchRemainderRule, "correct");
        sut = new DiffProcessor(
            { characterRules: testCharacterRules, tokenRules: testTokenRules, remainderRule: catchRemainderRule },
            nbCorrectionsPermitted
        );
        testCorrectionResults = new CorrectionResults(10);
    });

    test("should apply the token correction rules configured for the stepType given", () => {
        const mockTokenCorrectFn1 = jest.fn((diffs: Diff[][]) => ({
            matchedDiffs: [],
            unprocessedDiffs: diffs,
            penaltyScore: 0,
        }));
        const tokenCorrectionRule1: CorrectionRule = { correct: mockTokenCorrectFn1, failRuleMessage: "", ruleScore: 1 };
        const mockTokenCorrectFn2 = jest.fn((diffs: Diff[][]) => ({
            matchedDiffs: [],
            unprocessedDiffs: diffs,
            penaltyScore: 0,
        }));
        const tokenCorrectionRule2: CorrectionRule = { correct: mockTokenCorrectFn2, failRuleMessage: "", ruleScore: 1 };
        const mockTokenCorrectFn3 = jest.fn((diffs: Diff[][]) => ({
            matchedDiffs: [],
            unprocessedDiffs: diffs,
            penaltyScore: 0,
        }));
        const tokenCorrectionRule3: CorrectionRule = { correct: mockTokenCorrectFn3, failRuleMessage: "", ruleScore: 1 };
        testTokenRules.set(dummyFeedStep.type, [tokenCorrectionRule1, tokenCorrectionRule2, tokenCorrectionRule3]);

        const diffsToCorrect: Diff[] = [
            [DiffCompare.EQUAL, "howdy", 0],
            [DiffCompare.WRONG_ORDER_ANSWER, "incorrect", 1],
            [DiffCompare.EQUAL, "smerps", 2],
            [DiffCompare.WRONG_ORDER_INPUT, "incorrect", 3],
            [DiffCompare.MISSING, "flerps", 3],
            [DiffCompare.EXTRA, "flerpz", 3],
        ];

        sut.processTokenDiffs(dummyFeedStep, diffsToCorrect, testCorrectionResults);

        const tokenisedTokenDiffs = wrapTokenDiffsInArray(diffsToCorrect);
        const nonEqualTokenDiffs = filterEqualTokenDiffs(tokenisedTokenDiffs);

        expect(mockTokenCorrectFn1).toHaveBeenCalledTimes(1);
        expect(mockTokenCorrectFn1).toHaveBeenCalledWith(nonEqualTokenDiffs);
        expect(mockTokenCorrectFn2).toHaveBeenCalledTimes(1);
        expect(mockTokenCorrectFn2).toHaveBeenCalledWith(nonEqualTokenDiffs);
        expect(mockTokenCorrectFn3).toHaveBeenCalledTimes(1);
        expect(mockTokenCorrectFn3).toHaveBeenCalledWith(nonEqualTokenDiffs);
    });

    test("should apply the character correction rules configured for the stepType given", () => {
        const mockCharCorrectFn1 = jest.fn((diffs: Diff[][]) => ({ matchedDiffs: [], unprocessedDiffs: diffs, penaltyScore: 0 }));
        const CharCorrectionRule1: CorrectionRule = { correct: mockCharCorrectFn1, failRuleMessage: "", ruleScore: 1 };
        const mockCharCorrectFn2 = jest.fn((diffs: Diff[][]) => ({ matchedDiffs: [], unprocessedDiffs: diffs, penaltyScore: 0 }));
        const CharCorrectionRule2: CorrectionRule = { correct: mockCharCorrectFn2, failRuleMessage: "", ruleScore: 1 };
        const mockCharCorrectFn3 = jest.fn((diffs: Diff[][]) => ({ matchedDiffs: [], unprocessedDiffs: diffs, penaltyScore: 0 }));
        const CharCorrectionRule3: CorrectionRule = { correct: mockCharCorrectFn3, failRuleMessage: "", ruleScore: 1 };
        testCharacterRules.set(dummyFeedStep.type, [CharCorrectionRule1, CharCorrectionRule2, CharCorrectionRule3]);

        const diffsToCorrect: Diff[][] = [
            [
                [DiffCompare.EQUAL, "how", 0],
                [DiffCompare.EXTRA, "lol", 3],
                [DiffCompare.MISSING, "dy", 3],
            ],
            [[DiffCompare.EQUAL, "smerps", 2]],
            [
                [DiffCompare.MISSING, "s", 3],
                [DiffCompare.EXTRA, "z", 3],
            ],
        ];

        sut.processCharacterDiffs(dummyFeedStep, diffsToCorrect, testCorrectionResults);

        const nonEqualCharDiffs = filterEqualCharDiffs(diffsToCorrect);

        expect(mockCharCorrectFn1).toHaveBeenCalledTimes(1);
        expect(mockCharCorrectFn1).toHaveBeenCalledWith(nonEqualCharDiffs);
        expect(mockCharCorrectFn2).toHaveBeenCalledTimes(1);
        expect(mockCharCorrectFn2).toHaveBeenCalledWith(nonEqualCharDiffs);
        expect(mockCharCorrectFn3).toHaveBeenCalledTimes(1);
        expect(mockCharCorrectFn3).toHaveBeenCalledWith(nonEqualCharDiffs);
    });

    test("should catch any final unprocessed char diffs with the catchRemainderRule", () => {
        testTokenRules.set(dummyFeedStep.type, [new DummyRejectAllCorrectionRule()]);

        const charDiffsToProcess: Diff[][] = [
            [
                [DiffCompare.EQUAL, "mis", 0],
                [DiffCompare.MISSING, "c", 3],
                [DiffCompare.EXTRA, "s", 3],
                [DiffCompare.EQUAL, "schmerps", 4],
            ],
            [
                [DiffCompare.MISSING, "y", 0],
                [DiffCompare.EXTRA, "sqw", 0],
                [DiffCompare.EQUAL, "erpies", 1],
            ],
        ];

        const expectedNonEqualDiffsToPassOn = filterEqualCharDiffs(charDiffsToProcess);

        sut.processCharacterDiffs(dummyFeedStep, charDiffsToProcess, testCorrectionResults);

        expect(catchRemainderRuleCorrectionSpy).toHaveBeenCalledTimes(1);
        expect(catchRemainderRuleCorrectionSpy).toHaveBeenCalledWith(expectedNonEqualDiffsToPassOn);
    });

    test("should not process final remaining diffs when there are no unprocessed char diffs after correction", () => {
        testCharacterRules.set(dummyFeedStep.type, [new SimilarCharacterCorrectionRule(1, { s: ["c"], c: ["s"] })]);

        const charDiffToProcess: Diff[][] = [
            [
                [DiffCompare.EQUAL, "mis", 0],
                [DiffCompare.MISSING, "c", 3],
                [DiffCompare.EXTRA, "s", 3],
                [DiffCompare.EQUAL, "schmerps", 4],
            ],
            [
                [DiffCompare.MISSING, "c", 0],
                [DiffCompare.EXTRA, "s", 0],
                [DiffCompare.EQUAL, "erpies", 1],
            ],
        ];

        sut.processCharacterDiffs(dummyFeedStep, charDiffToProcess, testCorrectionResults);
        expect(catchRemainderRuleCorrectionSpy).toHaveBeenCalledTimes(0);
    });

    test("should allow corrections for tokens under the correction limit to pass and be processed", () => {
        testTokenRules.set(dummyFeedStep.type, [new WrongOrderTokenCorrectionRule(1), new MissingTokenCorrectionRule(1)]);

        const tokenDiffsUnderLimit: Diff[] = makeTokenDiffBetween("not that wrong", "wrong not");

        sut.processTokenDiffs(dummyFeedStep, tokenDiffsUnderLimit, testCorrectionResults);
    });

    test("should limit the number of token corrections that can happen before it becomes a hard fail", () => {
        testTokenRules.set(dummyFeedStep.type, [new WrongOrderTokenCorrectionRule(1), new ExtraTokenCorrectionRule(1)]);

        const tokenDiffsOverLimit: Diff[] = makeTokenDiffBetween("now it's pretty wrong", "pretty it's now wrong extra1 extra2");

        const processDiffsThatShouldExplode = () =>
            sut.processTokenDiffs(dummyFeedStep, tokenDiffsOverLimit, testCorrectionResults);

        expect(processDiffsThatShouldExplode).toThrow(CorrectionLimitException);
    });

    test("should allow corrections for character under the correction limit to pass and be processed ", () => {
        testCharacterRules.set(dummyFeedStep.type, [new BaseCharacterCorrectionRule(1), new MissingCharacterCorrectionRule(1)]);

        const charDiffsUnderLimit: Diff[][] = makeFullDiffBetween("not that bad", "no thât bad");

        sut.processCharacterDiffs(dummyFeedStep, charDiffsUnderLimit, testCorrectionResults);
    });

    test("should limit the number of character corrections that can happen before it becomes a hard fail", () => {
        testCharacterRules.set(dummyFeedStep.type, [new MissingCharacterCorrectionRule(2), new ExtraCharacterCorrectionRule(1)]);

        const charDiffsOverLimit = makeFullDiffBetween("now its serious", "naow it siriosz");

        const processDiffsThatShouldExplode = () =>
            sut.processCharacterDiffs(dummyFeedStep, charDiffsOverLimit, testCorrectionResults);

        expect(processDiffsThatShouldExplode).toThrow(CorrectionLimitException);
    });

    test("should allow corrections for both token and char rules under the correction limit to pass and be processed ", () => {
        testTokenRules.set(dummyFeedStep.type, [new WrongOrderTokenCorrectionRule(1), new MissingTokenCorrectionRule(1)]);
        testCharacterRules.set(dummyFeedStep.type, [
            new BaseCharacterCorrectionRule(1),
            new SimilarCharacterCorrectionRule(1, { z: ["s"], s: ["z"] }),
        ]);

        const tokenDiffsUnderLimit: Diff[] = makeTokenDiffBetween("doing ok boys", "ok døing boyz");
        sut.processTokenDiffs(dummyFeedStep, tokenDiffsUnderLimit, testCorrectionResults);

        const charDiffsThatBreakLimit: Diff[][] = [];
        sut.processCharacterDiffs(dummyFeedStep, charDiffsThatBreakLimit, testCorrectionResults);
    });

    test("should limit the number of corrections that can happen in between token and char corrections", () => {
        testTokenRules.set(dummyFeedStep.type, [new WrongOrderTokenCorrectionRule(1), new ExtraTokenCorrectionRule(1)]);
        testCharacterRules.set(dummyFeedStep.type, [new MissingCharacterCorrectionRule(1), new BaseCharacterCorrectionRule(1)]);

        const answer = "four diffs please sir";
        const input = "sir før diffs plise extrawordofdoom";
        const tokenDiffs = makeTokenDiffBetween(answer, input);
        const expectedDiffsAfterTokenProcessing: Diff[][] = [
            [[DiffCompare.EXTRA, "før", 1]],
            [[DiffCompare.EXTRA, "plise", 3]],
            [[DiffCompare.MISSING, "four", 0]],
            [[DiffCompare.MISSING, "please", 2]],
        ];

        sut.processTokenDiffs(dummyFeedStep, tokenDiffs, testCorrectionResults);
        expect(testCorrectionResults.unprocessedCorrections).toEqual(expectedDiffsAfterTokenProcessing);

        const characterDiffs = makeCharacterDiffsOutOfDiffs(testCorrectionResults.unprocessedCorrections);
        const processingCharDiffsWhichShouldFail = () =>
            sut.processCharacterDiffs(dummyFeedStep, characterDiffs, testCorrectionResults);

        expect(processingCharDiffsWhichShouldFail).toThrowError(CorrectionLimitException);
    });

    test("should reset the counter of calculated corrections after each correction to not accumulate", () => {
        testCharacterRules.set(dummyFeedStep.type, [new MissingCharacterCorrectionRule(2), new ExtraCharacterCorrectionRule(1)]);
        const charDiffsOverLimit = makeFullDiffBetween("now its serious", "naow it siriosz");
        
        const processDiffsThatShouldExplode = () =>
            sut.processCharacterDiffs(dummyFeedStep, charDiffsOverLimit, testCorrectionResults);
        
        expect(processDiffsThatShouldExplode).toThrow(CorrectionLimitException);
        
        testTokenRules.set(dummyFeedStep.type, [new MissingTokenCorrectionRule(1), new ExtraTokenCorrectionRule(1)])
        const newExerciseDiffsUnderLimit = makeTokenDiffBetween("i love schmeeps", "i love schmaps");

        const processTokenDiffsThatShouldBeFineNow = () => 
            sut.processTokenDiffs(dummyFeedStep, newExerciseDiffsUnderLimit, testCorrectionResults);

        expect(processTokenDiffsThatShouldBeFineNow).not.toThrow(CorrectionLimitException);
    });

    test("should pass on the token diffs if there are no correction rules to apply", () => {
        expect(Object.entries(testTokenRules)).toHaveLength(0);

        const tokenDiffsToProcess: Diff[] = [
            [DiffCompare.MISSING, "Don't", 0],
            [DiffCompare.WRONG_ORDER_ANSWER, "forget", 1],
            [DiffCompare.EXTRA, "Yamum", 0],
            [DiffCompare.WRONG_ORDER_INPUT, "forget", 5],
        ];
        const expectedNonEqualDiffsToPassOn = filterEqualTokenDiffs(wrapTokenDiffsInArray(tokenDiffsToProcess));

        sut.processTokenDiffs(dummyFeedStep, tokenDiffsToProcess, testCorrectionResults);
        const unprocessedDiffsAfterToken = testCorrectionResults.unprocessedCorrections;

        expect(unprocessedDiffsAfterToken).toEqual(expectedNonEqualDiffsToPassOn);
    });

    test("should pass on the char diffs if there are no correction rules to apply", () => {
        expect(Object.entries(testCharacterRules)).toHaveLength(0);

        const charDiffsToProcess: Diff[][] = [
            [
                [DiffCompare.MISSING, "Do", 0],
                [DiffCompare.EXTRA, "n't", 0],
                [DiffCompare.MISSING, "flerps", 3],
            ],
            [
                [DiffCompare.EXTRA, "Yamu", 0],
                [DiffCompare.EQUAL, "na", 5],
            ],
        ];
        const expectedNonEqualDiffsToPassOn = filterEqualCharDiffs(charDiffsToProcess);

        sut.processCharacterDiffs(dummyFeedStep, charDiffsToProcess, testCorrectionResults);

        expect(catchRemainderRuleCorrectionSpy).toHaveBeenCalledWith(expectedNonEqualDiffsToPassOn);
    });

    test("unprocessed diffs should not be carried over between token and char processing", () => {
        const tokenDiffsToProcess: Diff[] = [
            [DiffCompare.MISSING, "Don't", 0],
            [DiffCompare.WRONG_ORDER_ANSWER, "forget", 1],
            [DiffCompare.EXTRA, "Yamum", 0],
            [DiffCompare.WRONG_ORDER_INPUT, "forget", 5],
        ];

        testTokenRules.set(dummyFeedStep.type, [new WrongOrderTokenCorrectionRule(1)]);

        sut.processTokenDiffs(dummyFeedStep, tokenDiffsToProcess, testCorrectionResults);
        const unprocessedDiffsAfterToken = testCorrectionResults.unprocessedCorrections;

        testCharacterRules.set(dummyFeedStep.type, [new MissingCharacterCorrectionRule(1)]);

        sut.processCharacterDiffs(dummyFeedStep, unprocessedDiffsAfterToken, testCorrectionResults);
        const unprocessedDiffsAfterChar = testCorrectionResults.unprocessedCorrections;

        expect(unprocessedDiffsAfterChar).not.toEqual(unprocessedDiffsAfterToken);
    });

    test("should process/remove the equal token diffs before processing the erroneous ones", () => {
        testTokenRules.set(dummyFeedStep.type, [new DummyRejectAllCorrectionRule()]);

        const tokenDiffsToProcess: Diff[] = [
            [DiffCompare.MISSING, "Don't", 0],
            [DiffCompare.WRONG_ORDER_ANSWER, "forget", 1],
            [DiffCompare.EXTRA, "Yamum", 0],
            [DiffCompare.EQUAL, "Correcto", 2],
            [DiffCompare.EQUAL, "Mundo", 3],
            [DiffCompare.WRONG_ORDER_INPUT, "forget", 5],
        ];

        sut.processTokenDiffs(dummyFeedStep, tokenDiffsToProcess, testCorrectionResults);

        Array.from(testCorrectionResults.correctionMap.values()).forEach((diffForTokens: ProcessedDiff[]) => {
            diffForTokens.forEach(tokenDiff => {
                expect(tokenDiff.correctionType).toBe(DiffCompare.EQUAL);
            });
        });
    });

    test("should remove the equal char diffs before processing the erroneous ones", () => {
        testTokenRules.set(dummyFeedStep.type, [new DummyRejectAllCorrectionRule()]);

        const charDiffsToProcess: Diff[][] = [
            [
                [DiffCompare.EQUAL, "mis", 0],
                [DiffCompare.MISSING, "c", 3],
                [DiffCompare.EXTRA, "s", 3],
                [DiffCompare.EQUAL, "schmerps", 4],
            ],
            [
                [DiffCompare.MISSING, "y", 0],
                [DiffCompare.EXTRA, "sqw", 0],
                [DiffCompare.EQUAL, "erpies", 1],
            ],
        ];

        sut.processCharacterDiffs(dummyFeedStep, charDiffsToProcess, testCorrectionResults);

        const unprocessedCorrections = testCorrectionResults.unprocessedCorrections;
        const equalUnprocessedDiffs = unprocessedCorrections.filter(
            tokenDiffs => !!tokenDiffs.filter(charDiff => charDiff[0] == DiffCompare.EQUAL).length
        );
        expect(equalUnprocessedDiffs).toEqual([]);
    });
});

class DummyRejectAllCorrectionRule implements CorrectionRule {
    failRuleMessage = "you w0t m8";
    ruleScore: number;

    correct(diffedResult: Diff[][]): CorrectionRuleResult {
        return {
            matchedDiffs: [],
            penaltyScore: 0,
            unprocessedDiffs: diffedResult,
        };
    }
}

export function wrapTokenDiffsInArray(tokenDiffs: Diff[]): Diff[][] {
    return tokenDiffs.map(diff => [diff]);
}

export function filterEqualTokenDiffs(diffs: Diff[][]): Diff[][] {
    return diffs.filter(tokenDiff => tokenDiff[0][0] != DiffCompare.EQUAL);
}

export function filterEqualCharDiffs(diffs: Diff[][]): Diff[][] {
    const nonEqualDiffs: Diff[][] = [];
    diffs.forEach(tokenDiffs => {
        const filteredDiffs = tokenDiffs.filter(charDiff => charDiff[0] != DiffCompare.EQUAL);
        nonEqualDiffs.push(filteredDiffs.length ? filteredDiffs : undefined);
    });
    return nonEqualDiffs;
}
