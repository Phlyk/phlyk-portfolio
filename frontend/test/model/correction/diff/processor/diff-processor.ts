import { CorrectionRules } from "../../../factory/correction-rules.factory";
import { FeedStep } from "../../../step/feed-step/feed-step.model";
import { ProcessedDiff } from "../../correction-result.model";
import { CorrectionResults } from "../../correction-results.model";
import { Correction } from "../../correction.enum";
import { CorrectionRuleResult } from "../../rule/correction-rule-result.model";
import { CorrectionRule } from "../../rule/correction-rule.model";
import { CorrectionLimitException } from "../../rule/exception/correction-limit.exception";
import { Diff, DiffCompare } from "../diff.model";

export class DiffProcessor {
    private nbCorrectionsCalculated = 0;

    constructor(private correctionRules: CorrectionRules, private correctionLimit: number) {}

    processTokenDiffs(step: FeedStep, diffs: Diff[], correctionResults: CorrectionResults) {
        this.nbCorrectionsCalculated = 0;
        const correctionRulesToApply: CorrectionRule[] = this.correctionRules.tokenRules.get(step.type);
        const nonEqualDiffs: Diff[][] = this.processEqualTokenDiffs(diffs, correctionResults);
        this.processDiffs(correctionRulesToApply, nonEqualDiffs, correctionResults);
    }

    processCharacterDiffs(step: FeedStep, diffs: Diff[][], correctionResults: CorrectionResults) {
        const correctionRulesToApply: CorrectionRule[] = this.correctionRules.characterRules.get(step.type);
        const nonEqualDiffs: Diff[][] = this.processEqualCharDiffs(diffs, correctionResults);
        this.processDiffs(correctionRulesToApply, nonEqualDiffs, correctionResults);

        if (correctionResults.unprocessedCorrections.length) {
            this.processRemainingDiffs(correctionResults);
        }
    }

    private processRemainingDiffs(correctionResults: CorrectionResults) {
        const processedRemainingDiffs: CorrectionRuleResult = this.correctionRules.remainderRule.correct(
            correctionResults.unprocessedCorrections
        );
        correctionResults.applyCorrection(processedRemainingDiffs);
        if (processedRemainingDiffs.unprocessedDiffs.length) {
            throw new CorrectionLimitException("couldn't process the unprocessed diffs");
        }
        correctionResults.unprocessedCorrections = [];
    }

    private processEqualTokenDiffs(diffs: Diff[], correctionResults: CorrectionResults): Diff[][] {
        const incorrectDiffs: Diff[][] = [];
        diffs.forEach((tokenDiff: Diff) => {
            if (tokenDiff[0] == DiffCompare.EQUAL) {
                correctionResults.addTokenCorrection(this.processEqualTokenDiff(tokenDiff));
            } else {
                incorrectDiffs.push([tokenDiff]);
            }
        });

        return incorrectDiffs;
    }

    private processEqualTokenDiff([_, tokenString, tokenIndex]: Diff): ProcessedDiff {
        return {
            correctionType: Correction.EQUAL,
            tokenIndex,
            displayAnswer: tokenString,
        };
    }

    private processEqualCharDiffs(diffs: Diff[][], correctionResults: CorrectionResults): Diff[][] {
        const incorrectDiffs: Diff[][] = [[]];
        diffs.forEach((tokenDiff: Diff[], i) => {
            tokenDiff.forEach((diff: Diff) => {
                if (diff[0] == DiffCompare.EQUAL) {
                    correctionResults.addCharCorrection(this.processEqualCharDiff(diff, i), i);
                } else {
                    let existingIncorrectCharDiffs = incorrectDiffs[i];
                    if (existingIncorrectCharDiffs) {
                        existingIncorrectCharDiffs.push(diff);
                    } else {
                        existingIncorrectCharDiffs = [diff];
                    }
                    incorrectDiffs[i] = existingIncorrectCharDiffs;
                }
            });
        });

        return incorrectDiffs;
    }

    private processEqualCharDiff([_, charDiffString, charIndex]: Diff, tokenIndex: number): ProcessedDiff {
        return {
            correctionType: Correction.EQUAL,
            tokenIndex,
            displayAnswer: charDiffString,
            charIndex,
        };
    }

    private processDiffs(correctionRules: CorrectionRule[], diffs: Diff[][], correctionResults: CorrectionResults) {
        if (correctionRules) {
            const unprocessedDiffs = correctionRules.reduce((diffsToProcess: Diff[][], rule: CorrectionRule) => {
                // the correction results applies its rules given a series of Diffs (reducing in volume per each rule)
                const correctedResult: CorrectionRuleResult = rule.correct(diffsToProcess);
                // apply the matchedDiffs to the ongoing CorrectionResult array and deduct penaltyScore
                correctionResults.applyCorrection(correctedResult);
                // see if the accumulated corrections have crossed the correction limit
                this.raiseIfCorrectionsHaveCrossedLimit(correctedResult.matchedDiffs);
                // pass the remaining diffs to the next rule
                return correctedResult.unprocessedDiffs;
            }, diffs);
            correctionResults.setUnprocessedDiffs(unprocessedDiffs);
        } else {
            correctionResults.setUnprocessedDiffs(diffs);
        }
    }

    private raiseIfCorrectionsHaveCrossedLimit(matchedDiffs: ProcessedDiff[][]) {
        const nbCorrections = matchedDiffs.reduce((acc, tokenDiffs) => (acc += tokenDiffs.length), 0);
        this.nbCorrectionsCalculated += nbCorrections;
        if (this.nbCorrectionsCalculated > this.correctionLimit) {
            throw new CorrectionLimitException("Corrections have gone over the set limit");
        }
    }
}
