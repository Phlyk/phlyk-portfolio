export enum DiffCompare {
    WRONG_ORDER_INPUT = 2,
    EXTRA = 1,
    EQUAL = 0,
    MISSING = -1,
    WRONG_ORDER_ANSWER = -2,
}
export type Diff = [DiffCompare, string, number];
