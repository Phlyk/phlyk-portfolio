import diff from "diff-sequences";
import { ArraysHelper } from "src/app/shared/helpers/arrays.helper";
import { Diff, DiffCompare } from "../diff.model";

export class TokenDiffCalculator {
    diffs: Diff[];

    calculateTokenDiffs(answerTokens: string[], inputTokens: string[]): Diff[] {
        const isCommon = (answerTokenIndex, inputTokenIndex) => {
            return answerTokens[answerTokenIndex] === inputTokens[inputTokenIndex];
        };

        let answerIndex = 0;
        let inputIndex = 0;
        this.diffs = [];
        const processDiffSubsequences = (nbCommon: number, answerSeqIndex: number, inputSeqIndex: number) => {
            if (inputIndex !== inputSeqIndex) {
                this.checkOrderAndAddInputDiffs(inputTokens, answerTokens, inputIndex, inputSeqIndex);
            }
            if (answerIndex !== answerSeqIndex) {
                this.checkOrderAndAddAnswerDiffs(answerTokens, inputTokens, answerIndex, answerSeqIndex);
            }

            answerIndex = answerSeqIndex + nbCommon; // number of characters compared in answerTokens
            inputIndex = inputSeqIndex + nbCommon; // number of characters compared in inputTokens

            const equalTokens = inputTokens.slice(inputSeqIndex, inputIndex);
            this.addTokensToDiffs(equalTokens, DiffCompare.EQUAL, answerSeqIndex);
        };

        diff(answerTokens.length, inputTokens.length, isCommon, processDiffSubsequences);

        // After the last common subsequence, push remaining diff items.
        if (answerIndex !== answerTokens.length) {
            this.checkOrderAndAddAnswerDiffs(answerTokens, inputTokens, answerIndex);
        }
        if (inputIndex !== inputTokens.length) {
            this.checkOrderAndAddInputDiffs(inputTokens, answerTokens, inputIndex);
        }

        return this.diffs;
    }

    private checkOrderAndAddAnswerDiffs(
        answerTokens: string[],
        inputTokens: string[],
        answerIndex: number,
        answerSeqIndex?: number
    ) {
        this.compareTokenOrderAndAddDiffs(
            answerTokens,
            inputTokens,
            answerIndex,
            DiffCompare.WRONG_ORDER_ANSWER,
            DiffCompare.MISSING,
            answerSeqIndex
        );
    }

    private checkOrderAndAddInputDiffs(
        inputTokens: string[],
        answerTokens: string[],
        inputIndex: number,
        inputSeqIndex?: number
    ) {
        this.compareTokenOrderAndAddDiffs(
            inputTokens,
            answerTokens,
            inputIndex,
            DiffCompare.WRONG_ORDER_INPUT,
            DiffCompare.EXTRA,
            inputSeqIndex
        );
    }

    private compareTokenOrderAndAddDiffs(
        diffArray: string[],
        otherArray: string[],
        diffIndex: number,
        wrongOrderMarker: DiffCompare,
        incorrectMarker: DiffCompare,
        diffSeqIndex?: number
    ) {
        const diffTokens = diffArray.slice(diffIndex, diffSeqIndex);
        const diffTokensMatched = ArraysHelper.reductiveFind(diffTokens, otherArray);

        diffTokensMatched.forEach((matchedToken, index) => {
            const markerForDiff = matchedToken.index != null ? wrongOrderMarker : incorrectMarker;
            this.addTokensToDiffs([matchedToken.entry], markerForDiff, diffIndex + index);
        });
    }

    private addTokensToDiffs(tokensToAdd: string[], marker: DiffCompare, startingIndex: number) {
        tokensToAdd.forEach((tokenToAdd: string, index: number) => this.diffs.push([marker, tokenToAdd, startingIndex + index]));
    }
}
