import diff from "diff-sequences";
import { ArraysHelper } from "src/app/shared/helpers/arrays.helper";
import { Diff, DiffCompare } from "../diff.model";

export class CharacterDiffCalculator {
    calculateDiff(unprocessedTokenDiffs: Diff[][]): Diff[][] {
        const [unmatchedAnswerTokens, unmatchedInputTokens]: [Diff[], Diff[]] = ArraysHelper.partition(
            unprocessedTokenDiffs.map(tokenDiff => tokenDiff[0]),
            (tokenDiffs: Diff) => [DiffCompare.MISSING, DiffCompare.WRONG_ORDER_ANSWER].includes(tokenDiffs[0])
        );

        if (unmatchedAnswerTokens.length != unmatchedInputTokens.length) {
            throw new Error("cannot calculate character diff for tokens that are not equal");
        }

        return unmatchedAnswerTokens.map((answerTokenItem: Diff, index) => {
            const inputTokenItem: Diff = unmatchedInputTokens[index];
            return this.calculateStringDiff(answerTokenItem[1], inputTokenItem[1]);
        });
    }

    private calculateStringDiff(answerTokens: string, inputTokens: string): Diff[] {
        const isCommon = (aIndex, iIndex) => answerTokens[aIndex] === inputTokens[iIndex];

        let answerIndex = 0;
        let inputIndex = 0;
        const diffs: Diff[] = [];
        const processDiffSubsequences = (nbCommon: number, answerSeqIndex: number, inputSeqIndex: number) => {
            if (inputIndex !== inputSeqIndex) {
                diffs.push([DiffCompare.EXTRA, inputTokens.slice(inputIndex, inputSeqIndex), inputIndex]);
            }
            if (answerIndex !== answerSeqIndex) {
                diffs.push([DiffCompare.MISSING, answerTokens.slice(answerIndex, answerSeqIndex), answerIndex]);
            }

            answerIndex = answerSeqIndex + nbCommon; // number of characters compared in answerTokens
            inputIndex = inputSeqIndex + nbCommon; // number of characters compared in inputTokens
            diffs.push([DiffCompare.EQUAL, inputTokens.slice(inputSeqIndex, inputIndex), inputSeqIndex]);
        };

        diff(answerTokens.length, inputTokens.length, isCommon, processDiffSubsequences);

        // After the last common subsequence, push remaining diff items.
        if (answerIndex !== answerTokens.length) {
            diffs.push([DiffCompare.MISSING, answerTokens.slice(answerIndex), answerIndex]);
        }
        if (inputIndex !== inputTokens.length) {
            diffs.push([DiffCompare.EXTRA, inputTokens.slice(inputIndex), inputIndex]);
        }

        return diffs;
    }
}
