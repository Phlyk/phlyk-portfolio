import { CharacterDiffCalculator } from "src/app/modules/learning/model/correction/diff/calculator/character-diff-calculator";
import { Diff, DiffCompare } from "src/app/modules/learning/model/correction/diff/diff.model";
import { makeNonEqualTokenDiffBetween } from "../../correction/rule/shared-correction-test-helper";

describe("CharacterDiffCalculator (isolated test)", () => {
    let sut: CharacterDiffCalculator;

    beforeEach(() => {
        sut = new CharacterDiffCalculator();
    });

    test("should returns diffs in diffs format", () => {
        const answer = "Doesn't matter what we put here";
        const input = "Matter here what we put Doesn't";

        const calculatedDiff: Diff[][] = sut.calculateDiff(makeNonEqualTokenDiffBetween(answer, input));

        calculatedDiff.forEach(tokenDiffs => tokenDiffs.forEach(charDiff => {
            expect(charDiff[0]).toEqual(expect.any(Number));
            expect(charDiff[1]).toEqual(expect.any(String));
            expect(charDiff[2]).toEqual(expect.any(Number));
        }));
    });

    test("should calculate the diff for each token with the token array", () => {
        const answer = "You're a wizard Harry";
        const input = "u're a wazard Haza";

        const expectedDiff: Diff[][] = [
            [
                [-1, "Yo", 0],
                [0, "u're", 0],
            ],
            [
                [0, "w", 0],
                [1, "a", 1],
                [-1, "i", 1],
                [0, "zard", 2],
            ],
            [
                [0, "Ha", 0],
                [-1, "rry", 2],
                [1, "za", 2],
            ],
        ];

        const calculatedDiff = sut.calculateDiff(makeNonEqualTokenDiffBetween(answer, input));

        expect(calculatedDiff).toEqual(expectedDiff);
    });

    test("should return the starting index of the input token for each equal diff", () => {
        const answer = "superFoxOfLove";
        const input = "zuperFishOffLuve";

        const expectedEqualDiffs: Diff[] = [
            [0, "uperF", 1],
            [0, "O", 9],
            [0, "fL", 11],
            [0, "ve", 14],
        ];

        const calculatedDiff = sut.calculateDiff(makeNonEqualTokenDiffBetween(answer, input));
        const onlyEqualDiffs: Diff[] = calculatedDiff[0].filter(diff => diff[0] == DiffCompare.EQUAL);

        expect(onlyEqualDiffs).toEqual(expectedEqualDiffs);
    });

    test("should return the starting index of each non-equal diff for each token", () => {
        const answer = "herpipes poopipes";
        const input = "hirpipis pewpipess";

        const expectedNonEqualDiffs: Diff[][] = [
            [
                [1, "i", 1],
                [-1, "e", 1],
                [1, "i", 6],
                [-1, "e", 6],
            ],
            [
                [1, "ew", 1],
                [-1, "oo", 1],
                [1, "s", 7],
            ],
        ];

        const calculatedDiff = sut.calculateDiff(makeNonEqualTokenDiffBetween(answer, input));
        const nonEqualDiffs: Diff[][] = calculatedDiff.map(wordDiff => wordDiff.filter(diff => diff[0] != DiffCompare.EQUAL));

        expect(nonEqualDiffs).toEqual(expectedNonEqualDiffs);
    });

    test("should raise exception if the token arrays are not the same length", () => {
        const answer = "I am a giant marsupial";
        const input = "giant marsupial mofugga";

        const callingDaFunction = () => sut.calculateDiff(makeNonEqualTokenDiffBetween(answer, input));

        expect(callingDaFunction).toThrow("cannot calculate character diff for tokens that are not equal");
    });

    test("should not find any wrong order similarities between different tokens", () => {
        const answer = "This is a similar string";
        const input = "a similar string is This";

        const calculatedDiff = sut.calculateDiff(makeNonEqualTokenDiffBetween(answer, input));
        
        const wrongOrderDiffs: Diff[][] = calculatedDiff.map(wordDiff =>
            wordDiff.filter(diff => diff[0] == DiffCompare.WRONG_ORDER_INPUT)
        );
        const newwrongOrderDiffs: Diff[][] = calculatedDiff.filter(
            tokenDiffs => tokenDiffs[0][0] == DiffCompare.WRONG_ORDER_ANSWER || tokenDiffs[0][0] == DiffCompare.WRONG_ORDER_INPUT
        );

        expect(newwrongOrderDiffs).toHaveLength(0);
    });
});
