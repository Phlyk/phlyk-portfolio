import { TokenDiffCalculator } from "src/app/modules/learning/model/correction/diff/calculator/token-diff-calculator";
import { Diff, DiffCompare } from "src/app/modules/learning/model/correction/diff/diff.model";
import { tokenify } from "../../rule/shared-correction-test-helper";

describe("TokenDiffCalculator (isolated test)", () => {
    let sut: TokenDiffCalculator;

    beforeEach(() => {
        sut = new TokenDiffCalculator();
    });

    test("should return any character differences in a token as an extra/missing token pair", () => {
        const answer = "būti";
        const input = "buti";

        const expectedDiffs: Diff[] = [
            [DiffCompare.MISSING, "būti", 0],
            [DiffCompare.EXTRA, "buti", 0],
        ];

        const calculatedDiff = sut.calculateTokenDiffs(tokenify(answer), tokenify(input));

        expect(calculatedDiff).toEqual(expectedDiffs);
    });

    test("should return differences in Diff format, DiffCompare, tokenString, indexOfToken", () => {
        const answer = "Doesn't matter what we put here";
        const input = "Matter here what we put Doesn't";

        const calculatedDiff: Diff[] = sut.calculateTokenDiffs(tokenify(answer), tokenify(input));

        calculatedDiff.forEach(diff => {
            expect(diff[0]).toEqual(expect.any(Number));
            expect(diff[1]).toEqual(expect.any(String));
            expect(diff[2]).toEqual(expect.any(Number));
        });
    });

    test("should return a wrong order token pair identifying answer and input tokens", () => {
        const answer = "you don't exist";
        const input = "don't you exist";

        const expectedDiffs: Diff[] = [
            [DiffCompare.WRONG_ORDER_ANSWER, "you", 0],
            [DiffCompare.EQUAL, "don't", 1],
            [DiffCompare.WRONG_ORDER_INPUT, "you", 1],
            [DiffCompare.EQUAL, "exist", 2],
        ];

        const calculatedDiff = sut.calculateTokenDiffs(tokenify(answer), tokenify(input));

        expect(calculatedDiff).toEqual(expectedDiffs);
    });

    test("should match tokens in wrong order for both answer and input", () => {
        const answer = "you wish you were french";
        const input = "wish you were you french";

        const calculatedDiff = sut.calculateTokenDiffs(tokenify(answer), tokenify(input));

        const diffsForWrongOrder = calculatedDiff.filter(
            tokenDiff =>
                tokenDiff[1] == "you" &&
                (tokenDiff[0] == DiffCompare.WRONG_ORDER_ANSWER || tokenDiff[0] == DiffCompare.WRONG_ORDER_INPUT)
        );

        expect(diffsForWrongOrder).toHaveLength(2);
        expect(diffsForWrongOrder[0][0]).toEqual(DiffCompare.WRONG_ORDER_ANSWER);
        expect(diffsForWrongOrder[1][0]).toEqual(DiffCompare.WRONG_ORDER_INPUT);
    });

    test("should not loose sentence alignment after an extra word", () => {
        const answer = "I want some diffs";
        const input = "I want now some diffs";

        const expectedDiffs: Diff[] = [
            [DiffCompare.EQUAL, "I", 0],
            [DiffCompare.EQUAL, "want", 1],
            [DiffCompare.EXTRA, "now", 2],
            [DiffCompare.EQUAL, "some", 2],
            [DiffCompare.EQUAL, "diffs", 3],
        ];

        const calculatedDiff = sut.calculateTokenDiffs(tokenify(answer), tokenify(input));

        expect(calculatedDiff).toEqual(expectedDiffs);
    });

    test("should not loose sentence alignment after a missing word", () => {
        const answer = "I want some diffs";
        const input = "I some diffs";

        const expectedDiffs: Diff[] = [
            [DiffCompare.EQUAL, "I", 0],
            [DiffCompare.MISSING, "want", 1],
            [DiffCompare.EQUAL, "some", 2],
            [DiffCompare.EQUAL, "diffs", 3],
        ];

        const calculatedDiff = sut.calculateTokenDiffs(tokenify(answer), tokenify(input));

        expect(calculatedDiff).toEqual(expectedDiffs);
    });

    test("should not lose sentence alignment when coming across incorrect (extra/ missing) pairs", () => {
        const answer = "I want some diffs";
        const input = "I some diffs wahnt";

        const expectedDiffs: Diff[] = [
            [DiffCompare.EQUAL, "I", 0],
            [DiffCompare.MISSING, "want", 1],
            [DiffCompare.EQUAL, "some", 2],
            [DiffCompare.EQUAL, "diffs", 3],
            [DiffCompare.EXTRA, "wahnt", 3],
        ];

        const calculatedDiff = sut.calculateTokenDiffs(tokenify(answer), tokenify(input));

        expect(calculatedDiff).toEqual(expectedDiffs);
    });

    test("should maintain correct indexes on equal tokens when coming across wrong order pairs", () => {
        const answer = "you don't exist";
        const input = "don't you exist";

        const expectedDiffs: Diff[] = [
            [DiffCompare.WRONG_ORDER_ANSWER, "you", 0],
            [DiffCompare.EQUAL, "don't", 1],
            [DiffCompare.WRONG_ORDER_INPUT, "you", 1],
            [DiffCompare.EQUAL, "exist", 2],
        ];

        const calculatedDiff = sut.calculateTokenDiffs(tokenify(answer), tokenify(input));

        expect(calculatedDiff).toEqual(expectedDiffs);
    });

    test("should tell us the index of the equal diffs as per the correct answer even if alignment has changed", () => {
        const answer = "you wish you were french";
        const input = "wish you were you french";

        const expectedDiffs: Diff[] = [
            [DiffCompare.WRONG_ORDER_ANSWER, "you", 0],
            [DiffCompare.EQUAL, "wish", 1],
            [DiffCompare.EQUAL, "you", 2],
            [DiffCompare.EQUAL, "were", 3],
            [DiffCompare.WRONG_ORDER_INPUT, "you", 3],
            [DiffCompare.EQUAL, "french", 4],
        ];

        const calculatedDiff = sut.calculateTokenDiffs(tokenify(answer), tokenify(input));

        expect(calculatedDiff).toEqual(expectedDiffs);
    });

    test("should align longest common subsequences of sentence that are in wrong order", () => {
        const answer = "i love kittens not puppies";
        const input = "kittens not puppies i love";

        const expectedDiffs: Diff[] = [
            [DiffCompare.WRONG_ORDER_ANSWER, "i", 0],
            [DiffCompare.WRONG_ORDER_ANSWER, "love", 1],
            [DiffCompare.EQUAL, "kittens", 2],
            [DiffCompare.EQUAL, "not", 3],
            [DiffCompare.EQUAL, "puppies", 4],
            [DiffCompare.WRONG_ORDER_INPUT, "i", 3],
            [DiffCompare.WRONG_ORDER_INPUT, "love", 4],
        ];

        const newcalculatedDiff = sut.calculateTokenDiffs(tokenify(answer), tokenify(input));

        expect(newcalculatedDiff).toEqual(expectedDiffs);
    });

    test("should be able to handle complex amalgamations of phrases", () => {
        const answer = "this is a complex sentence";
        const input = "sentence complex a is this";

        const expectedDiffs: Diff[] = [
            [DiffCompare.WRONG_ORDER_ANSWER, "this", 0],
            [DiffCompare.WRONG_ORDER_ANSWER, "is", 1],
            [DiffCompare.WRONG_ORDER_ANSWER, "a", 2],
            [DiffCompare.WRONG_ORDER_ANSWER, "complex", 3],
            [DiffCompare.EQUAL, "sentence", 4],
            [DiffCompare.WRONG_ORDER_INPUT, "complex", 1],
            [DiffCompare.WRONG_ORDER_INPUT, "a", 2],
            [DiffCompare.WRONG_ORDER_INPUT, "is", 3],
            [DiffCompare.WRONG_ORDER_INPUT, "this", 4],
        ];

        const calculatedDiff = sut.calculateTokenDiffs(tokenify(answer), tokenify(input));

        expect(calculatedDiff).toEqual(expectedDiffs);
    });

    test("should be able to handle sentence ordering failures incorrectnesses", () => {
        const answer = "this is a complex sentence";
        const input = "sentence complex this is a";

        const expectedDiffs: Diff[] = [
            [DiffCompare.WRONG_ORDER_INPUT, "sentence", 0],
            [DiffCompare.WRONG_ORDER_INPUT, "complex", 1],
            [DiffCompare.EQUAL, "this", 0],
            [DiffCompare.EQUAL, "is", 1],
            [DiffCompare.EQUAL, "a", 2],
            [DiffCompare.WRONG_ORDER_ANSWER, "complex", 3],
            [DiffCompare.WRONG_ORDER_ANSWER, "sentence", 4],
        ];

        const calculatedDiff = sut.calculateTokenDiffs(tokenify(answer), tokenify(input));

        expect(calculatedDiff).toEqual(expectedDiffs);
    });

    test("should pick up many rules at once", () => {
        const answer = "I am a random sentence needing validation";
        const input = "I a sentence am niding validation dindon";

        const expectedDiffs: Diff[] = [
            [DiffCompare.EQUAL, "I", 0],
            [DiffCompare.WRONG_ORDER_ANSWER, "am", 1],
            [DiffCompare.EQUAL, "a", 2],
            [DiffCompare.MISSING, "random", 3],
            [DiffCompare.EQUAL, "sentence", 4],
            [DiffCompare.WRONG_ORDER_INPUT, "am", 3],
            [DiffCompare.EXTRA, "niding", 4],
            [DiffCompare.MISSING, "needing", 5],
            [DiffCompare.EQUAL, "validation", 6],
            [DiffCompare.EXTRA, "dindon", 6],
        ];

        const calculatedDiff: Diff[] = sut.calculateTokenDiffs(tokenify(answer), tokenify(input));

        expect(calculatedDiff).toEqual(expectedDiffs);
    });

    test("should be able to catch multiple wrong order tokens of the same word", () => {
        const answer = "I am wrong wrong wrong";
        const input = "wrong I wrong am wrong";

        const expectedDiffs: Diff[] = [
            [DiffCompare.WRONG_ORDER_ANSWER, "I", 0],
            [DiffCompare.WRONG_ORDER_ANSWER, "am", 1],
            [DiffCompare.EQUAL, "wrong", 2],
            [DiffCompare.WRONG_ORDER_INPUT, "I", 1],
            [DiffCompare.EQUAL, "wrong", 3],
            [DiffCompare.WRONG_ORDER_INPUT, "am", 3],
            [DiffCompare.EQUAL, "wrong", 4],
        ];

        const calculatedDiff: Diff[] = sut.calculateTokenDiffs(tokenify(answer), tokenify(input));

        expect(calculatedDiff).toEqual(expectedDiffs);
    });
});
