import { ProcessedDiff } from "src/app/modules/learning/model/correction/correction-result.model";
import { Correction } from "src/app/modules/learning/model/correction/correction.enum";
import { Diff, DiffCompare } from "src/app/modules/learning/model/correction/diff/diff.model";
import { AbstractTokenCorrectionRule } from "src/app/modules/learning/model/correction/rule/abstract-token-correction-rule.model";
import { ArraysHelper } from "src/app/shared/helpers/arrays.helper";
import { makeTokenDiffArray } from "./shared-correction-test-helper";

describe("AbstractTokenCorrectionRule (isolated test)", () => {
    const testRulePenaltyScore = 1;
    let sut: TestDummyCorrectionRule;
    let ruleMatchingSpy: jest.SpyInstance;

    const answer = "Doesn't matter I don't think";
    const input = "matter Doesn't Think I not";
    const dummyDiffs = makeTokenDiffArray(answer, input);

    
    beforeEach(() => {
        sut = new TestDummyCorrectionRule(testRulePenaltyScore);
        ruleMatchingSpy = jest.spyOn<any, string>(sut, "findRuleViolations").mockImplementation(_ => _);
    });


    afterEach(() => {
        ruleMatchingSpy.mockClear();
    });
    
    test("should process the diffs identified by the rule", () => {
        ruleMatchingSpy.mockImplementationOnce((tokenCharDiffs: Diff[][]) => {
            const theOnlyDiffICareAboutForTheLulzOfc = tokenCharDiffs[0][0];
            sut.ruleViolations.push([{
                tokenIndex: theOnlyDiffICareAboutForTheLulzOfc[2],
                displayAnswer: theOnlyDiffICareAboutForTheLulzOfc[1],
                message: sut.failRuleMessage,
                correctionType: Correction.EXTRA_TOKEN,
            }]);
        });

        const correctedResults = sut.correct(dummyDiffs);
        const onlyDiffThatWasProcessed = correctedResults.matchedDiffs[0][0];
        const processedResultOfSaidDiff: ProcessedDiff = {
            tokenIndex: 0,
            displayAnswer: "matter",
            message: sut.failRuleMessage,
            correctionType: Correction.EXTRA_TOKEN,
        };
        expect(onlyDiffThatWasProcessed).toEqual(processedResultOfSaidDiff);
    });

    test("should pass on the diffs not identified by the rule", () => {
        ruleMatchingSpy.mockImplementationOnce((tokenCharDiffs: Diff[][], tokenIndex) => {
            const theOnlyDiffICareAboutForTheLulzOfc = tokenCharDiffs[0][0];
            sut.unmatchedDiffs.push([theOnlyDiffICareAboutForTheLulzOfc]);
        });

        const correctedResults = sut.correct(dummyDiffs);

        expect(correctedResults.unprocessedDiffs).toHaveLength(1);

        expect(correctedResults.unprocessedDiffs[0][0]).toEqual([DiffCompare.WRONG_ORDER_INPUT, "matter", 0]);
    });

    test("should not deduct any score for rule if it did not match", () => {
        const correctedCorrectDiff = sut.correct(dummyDiffs);

        expect(correctedCorrectDiff.penaltyScore).toEqual(0);
    });

    test("should deduct the ruleScore by the number of rule violations", () => {
        const diffTypeToFind = DiffCompare.MISSING;

        ruleMatchingSpy.mockImplementationOnce((tokenCharDiffs: Diff[][]) => {
            const [one, two] = ArraysHelper.partition(tokenCharDiffs, diff => diff[0][0] == diffTypeToFind);
            sut.ruleViolations.push(
                ...one.map((matchedTokenDiff: Diff[]) => ([{
                    message: sut.failRuleMessage,
                    tokenIndex: matchedTokenDiff[0][2],
                    displayAnswer: matchedTokenDiff[0][1],
                    diffType: matchedTokenDiff[0][0]
                }]))
            );
            sut.unmatchedDiffs.push(...two);
        });

        const correctedResult = sut.correct(dummyDiffs);

        const nbOfDiffsAcrossAllTokens = dummyDiffs.reduce((accumulator: number, currentTokenDiffs: Diff[]) => {
            if (currentTokenDiffs[0][0] == diffTypeToFind) accumulator++
            return accumulator;
        }, 0);

        expect(correctedResult.penaltyScore).toEqual(nbOfDiffsAcrossAllTokens * sut.ruleScore);
    });
});

class TestDummyCorrectionRule extends AbstractTokenCorrectionRule {
    failRuleMessage = "I'm just a test dummy leave me alone";
    
    protected findRuleViolations(_: Diff[][]) {
        throw new Error("Spy on (and overwrite) me or I'll explode");

    }

}
