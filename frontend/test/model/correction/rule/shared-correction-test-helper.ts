import { CharacterDiffCalculator } from "src/app/modules/learning/model/correction/diff/calculator/character-diff-calculator";
import { TokenDiffCalculator } from "src/app/modules/learning/model/correction/diff/calculator/token-diff-calculator";
import { Diff, DiffCompare } from "src/app/modules/learning/model/correction/diff/diff.model";

const tokenDiffCalculator = new TokenDiffCalculator();
const diffCalculator = new CharacterDiffCalculator();

export function makeTokenDiffBetween(string1: string, string2: string): Diff[] {
    const string1Tokens = tokenify(string1);
    const string2Tokens = tokenify(string2);
    
    return tokenDiffCalculator.calculateTokenDiffs(string1Tokens, string2Tokens);
}

export function makeTokenDiffArray(string1: string, string2: string): Diff[][] {
    return makeTokenDiffBetween(string1, string2).map(diff => [diff]);
}

export function makeNonEqualTokenDiffBetween(string1: string, string2: string): Diff[][] {
    return makeTokenDiffArray(string1, string2).filter(tokenDiffs => tokenDiffs[0][0] != DiffCompare.EQUAL);
}

///////////////////////////

export function makeCharacterDiffsOutOfDiffs(diffs: Diff[][]): Diff[][] {
    return diffCalculator.calculateDiff(diffs);
}

export function makeCharacterDiffBetween(string1: string, string2: string): Diff[] {
    return diffCalculator["calculateStringDiff"](string1, string2);
}

export function makeFullDiffBetween(string1: string, string2: string): Diff[][] {
    const string1Tokens = tokenify(string1);
    const string2Tokens = tokenify(string2);
    
    let longerArray = string1Tokens,
    shorterArray = string2Tokens;
    if (shorterArray.length > string1Tokens.length) {
        longerArray = shorterArray;
        shorterArray = string1Tokens;
    }
    
    return longerArray.map((longerArrayToken, index) => {
        const shorterArrayToken = shorterArray[index];
        if (!shorterArrayToken) {
            return [[DiffCompare.EXTRA, longerArrayToken, index]];
        }
        return makeCharacterDiffBetween(longerArrayToken, shorterArrayToken);
    });
}

export function makeFullNonEqualDiffBetween(string1: string, string2: string): Diff[][] {
    return makeFullDiffBetween(string1, string2).map(tokenCharDiffs => {
        return tokenCharDiffs.filter((charDiff: Diff) => charDiff[0] != DiffCompare.EQUAL);
    });
}

////////////

export function tokenify(phrase: string) {
    return phrase.split(" ");
}
