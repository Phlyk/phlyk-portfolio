import { ProcessedDiff } from "src/app/modules/learning/model/correction/correction-result.model";
import { Correction } from "src/app/modules/learning/model/correction/correction.enum";
import { Diff, DiffCompare } from "src/app/modules/learning/model/correction/diff/diff.model";
import { AbstractCharCorrectionRule } from "src/app/modules/learning/model/correction/rule/abstract-char-correction-rule.model";
import { ArraysHelper } from "src/app/shared/helpers/arrays.helper";
import { makeFullNonEqualDiffBetween } from "./shared-correction-test-helper";

describe("AbstractCharCorrectionRule (isolated test)", () => {
    const testRulePenaltyScore = 1;
    let sut: TestDummyCorrectionRule;
    let ruleMatchingSpy: jest.SpyInstance;

    const answer = "they say we're going to die one day";
    const input = "they..whaa!?!";
    const dummyDiffs = makeFullNonEqualDiffBetween(answer, input);

    beforeEach(() => {
        sut = new TestDummyCorrectionRule(testRulePenaltyScore);
        ruleMatchingSpy = jest.spyOn<any, string>(sut, "findRuleViolations").mockImplementation(_ => _);
    });

    afterEach(() => {
        ruleMatchingSpy.mockClear();
    });

    test("should attempt to correct all char diffs within the top level token diffs", () => {
        sut.correct(dummyDiffs);

        expect(ruleMatchingSpy).toHaveBeenCalledTimes(dummyDiffs.length);
    });

    test("should pass on the diffs identified by the rule", () => {
        ruleMatchingSpy.mockImplementationOnce((tokenCharDiffs: Diff[], tokenIndex) => {
            const theOnlyDiffICareAboutForTheLulzOfc = tokenCharDiffs[0];
            sut.tokenViolations.push({
                tokenIndex,
                displayAnswer: theOnlyDiffICareAboutForTheLulzOfc[1],
                charIndex: theOnlyDiffICareAboutForTheLulzOfc[2],
                message: sut.failRuleMessage,
                correctionType: Correction.EXTRA_CHAR,
            });
        });

        const correctedResults = sut.correct(dummyDiffs);

        expect(correctedResults.matchedDiffs).toHaveLength(1);
        const onlyDiffThatWasProcessed = correctedResults.matchedDiffs[0];
        const processedResultOfSaidDiff: ProcessedDiff = {
            tokenIndex: 0,
            displayAnswer: "..whaa!?!",
            charIndex: 4,
            message: sut.failRuleMessage,
            correctionType: Correction.EXTRA_CHAR,
        };
        expect(onlyDiffThatWasProcessed).toEqual([processedResultOfSaidDiff]);
    });

    test("should pass on the diffs not identified by the rule as unmatched", () => {
        ruleMatchingSpy.mockImplementationOnce((tokenCharDiffs: Diff[], tokenIndex) => {
            const theOnlyDiffICareAboutForTheLulzOfc = tokenCharDiffs[0];
            sut.tokenUnmatchedDiffs.push(theOnlyDiffICareAboutForTheLulzOfc);
        });

        const correctedResults = sut.correct(dummyDiffs);

        expect(correctedResults.unprocessedDiffs).toHaveLength(1);

        expect(correctedResults.unprocessedDiffs[0]).toEqual([[DiffCompare.EXTRA, "..whaa!?!", 4]]);
    });

    test("should not deduct any score for rule if it did not match", () => {
        const correctedCorrectDiff = sut.correct(dummyDiffs);

        expect(correctedCorrectDiff.penaltyScore).toEqual(0);
    });

    test("should deduct the ruleScore by the number of character rule violations", () => {
        const diffTypeToFind = DiffCompare.MISSING;

        ruleMatchingSpy.mockImplementationOnce((tokenCharDiffs: Diff[], tokenIndex) => {
            const [one, two] = ArraysHelper.partition(tokenCharDiffs, diff => diff[0] == diffTypeToFind);
            sut.tokenViolations.push(
                ...one.map(goodDiff => ({
                    message: sut.failRuleMessage,
                    tokenIndex: tokenIndex,
                    displayAnswer: goodDiff[1],
                    charIndex: goodDiff[2],
                }))
            );
            sut.tokenUnmatchedDiffs.push(...two);
        });

        const correctedResult = sut.correct(dummyDiffs);

        const nbOfDiffsAcrossAllTokens = dummyDiffs.reduce((accumulator: number, currentTokenDiffs: Diff[]) => {
            const nbDiffsFound = currentTokenDiffs.reduce((acc, diff) => diff[0] == diffTypeToFind && acc++, 0);
            return accumulator + nbDiffsFound;
        }, 0);

        expect(correctedResult.penaltyScore).toEqual(nbOfDiffsAcrossAllTokens * sut.ruleScore);
    });
});

class TestDummyCorrectionRule extends AbstractCharCorrectionRule {
    failRuleMessage = "I'm just a test dummy leave me alone"

    /* Will throw unless spiedOn */
    protected findRuleViolations(_: Diff[]): [Diff[], Diff[]] {
        throw new Error("Spy on (and overwrite) me or I'll explode");
    }
}
