import { ProcessedDiff } from "../correction-result.model";
import { Diff } from "../diff/diff.model";
import { CorrectionRuleResult } from "./correction-rule-result.model";
import { CorrectionRule } from "./correction-rule.model";

export abstract class AbstractCharCorrectionRule implements CorrectionRule {
    abstract readonly failRuleMessage: string;

    tokenViolations: ProcessedDiff[];
    tokenUnmatchedDiffs: Diff[];

    constructor(public ruleScore: number) {}
    
    correct(diffedResult: Diff[][]): CorrectionRuleResult {
        const violatingDiffs: ProcessedDiff[][] = [];
        const unmatchedDiffs: Diff[][] = [];

        diffedResult.forEach((tokenDiffs, tokenIndex) => {
            this.tokenViolations = [];
            this.tokenUnmatchedDiffs = [];

            this.findRuleViolations(tokenDiffs, tokenIndex);
            
            this.tokenViolations.length && violatingDiffs.push(this.tokenViolations);
            this.tokenUnmatchedDiffs.length && unmatchedDiffs.push(this.tokenUnmatchedDiffs);
        });

        const penaltyScore = this.calculatePenaltyScore(violatingDiffs);

        return {
            matchedDiffs: violatingDiffs,
            unprocessedDiffs: unmatchedDiffs,
            penaltyScore,
        };
    }

    private calculatePenaltyScore(violatingTokenDiffs: ProcessedDiff[][]): number {
        return violatingTokenDiffs.reduce((tokenAcc: number, tokenCharDiffs: ProcessedDiff[]) => {
            return tokenCharDiffs.reduce((charAcc, _) => charAcc += this.ruleScore, tokenAcc);
        }, 0);
    }

    protected abstract findRuleViolations(tokenDiffs: Diff[], tokenIndex: number);
}
