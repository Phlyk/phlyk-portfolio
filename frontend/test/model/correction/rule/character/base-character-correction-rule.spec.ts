import { ProcessedDiff } from "src/app/modules/learning/model/correction/correction-result.model";
import { Correction } from "src/app/modules/learning/model/correction/correction.enum";
import { Diff, DiffCompare } from "src/app/modules/learning/model/correction/diff/diff.model";
import { BaseCharacterCorrectionRule } from "src/app/modules/learning/model/correction/rule/character/base-character-correction-rule";
import { makeFullDiffBetween, makeFullNonEqualDiffBetween } from "../../correction/rule/shared-correction-test-helper";

describe("BaseCharacterCorrectionRule (isolated test)", () => {
    let sut: BaseCharacterCorrectionRule;

    beforeEach(() => {
        sut = new BaseCharacterCorrectionRule(DiffCompare.EXTRA);
    });

    test("should match when accents have been missed", () => {
        const answer = "flérpsïe";
        const input = "flerpsie";

        const expectedDiffs: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.BASE_CHAR,
                    charIndex: 2,
                    tokenIndex: 0,
                    displayAnswer: "é",
                    input: "e",
                    message: sut.failRuleMessage,
                },
                {
                    correctionType: Correction.BASE_CHAR,
                    charIndex: 6,
                    tokenIndex: 0,
                    displayAnswer: "ï",
                    input: "i",
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedDiffs);
    });

    test("should match when accents have been added", () => {
        const answer = "flerpsie";
        const input = "flērpsié";

        const expectedDiffs: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.BASE_CHAR,
                    charIndex: 2,
                    tokenIndex: 0,
                    displayAnswer: "e",
                    input: "ē",
                    message: sut.failRuleMessage,
                },
                {
                    correctionType: Correction.BASE_CHAR,
                    charIndex: 7,
                    tokenIndex: 0,
                    displayAnswer: "e",
                    input: "é",
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedDiffs);
    });

    test("should match when upper case char has been missed", () => {
        const answer = "FlerpSie";
        const input = "flerpsie";

        const expectedDiffs: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.BASE_CHAR,
                    charIndex: 0,
                    tokenIndex: 0,
                    displayAnswer: "F",
                    input: "f",
                    message: sut.failRuleMessage,
                },
                {
                    correctionType: Correction.BASE_CHAR,
                    charIndex: 5,
                    tokenIndex: 0,
                    displayAnswer: "S",
                    input: "s",
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedDiffs);
    });

    test("should match when lower case char has been missed", () => {
        const answer = "flerpsie";
        const input = "fleRpsie";

        const expectedDiffs: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.BASE_CHAR,
                    charIndex: 3,
                    tokenIndex: 0,
                    displayAnswer: "r",
                    input: "R",
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedDiffs);
    });

    test("should not match diffs that don't have a corresponding diff pair", () => {
        const answer = "flerpsie";
        const input = "flerpsiee";

        const unmatchedViolations: Diff[][] = [[[DiffCompare.EXTRA, "e", 8]]];
        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.unprocessedDiffs).toEqual(unmatchedViolations);
        expect(correctedResult.matchedDiffs).toHaveLength(0);
    });

    test("should not match diffs that do not match the collation rules", () => {
        const answer = "flerpsie";
        const input = "flerpzy";

        const unmatchedViolations: Diff[][] = [
            [
                [DiffCompare.EXTRA, "zy", 5],
                [DiffCompare.MISSING, "sie", 5],
            ],
        ];
        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.unprocessedDiffs).toEqual(unmatchedViolations);
        expect(correctedResult.matchedDiffs).toHaveLength(0);
    });

    test("should be able to split out base chars within a diff string with extra chars and match what it can", () => {
        const answer = "compøsite";
        const input = "compohsîtte";

        const expectedDiffs: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.BASE_CHAR,
                    charIndex: 4,
                    tokenIndex: 0,
                    displayAnswer: "ø",
                    input: "o",
                    message: sut.failRuleMessage,
                },
                {
                    correctionType: Correction.BASE_CHAR,
                    charIndex: 6,
                    tokenIndex: 0,
                    displayAnswer: "i",
                    input: "î",
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const expectedUnmatchedDiff: Diff[][] = [
            [
                [DiffCompare.EXTRA, "h", 5],
                [DiffCompare.EXTRA, "t", 8],
            ],
        ];

        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedDiffs);
        expect(correctedResult.unprocessedDiffs).toEqual(expectedUnmatchedDiff);
    });

    test("should be able to split out the equal parts of diffs", () => {
        const answer = "bûti";
        const input = "tibu";

        const expectedCorrections: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.EQUAL,
                    displayAnswer: "b",
                    input: "b",
                    charIndex: 0,
                    message: null,
                    tokenIndex: 0,
                },
                {
                    correctionType: Correction.BASE_CHAR,
                    displayAnswer: "û",
                    input: "u",
                    charIndex: 1,
                    message: sut.failRuleMessage,
                    tokenIndex: 0,
                },
            ],
        ];

        const correctedResult = sut.correct(makeFullNonEqualDiffBetween(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedCorrections);
        expect(correctedResult.unprocessedDiffs).toHaveLength(0);
    });

    test("should be able to split out base chars within a diff string with missing chars and match what it can", () => {
        const answer = "compøhsitte";
        const input = "composîte";

        const expectedDiffs: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.BASE_CHAR,
                    charIndex: 4,
                    tokenIndex: 0,
                    displayAnswer: "ø",
                    input: "o",
                    message: sut.failRuleMessage,
                },
                {
                    correctionType: Correction.BASE_CHAR,
                    charIndex: 7,
                    tokenIndex: 0,
                    displayAnswer: "i",
                    input: "î",
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const expectedUnmatchedDiff: Diff[][] = [
            [
                [DiffCompare.MISSING, "h", 5],
                [DiffCompare.MISSING, "t", 8],
            ],
        ];

        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedDiffs);
        expect(correctedResult.unprocessedDiffs).toEqual(expectedUnmatchedDiff);
    });

    test("should not match more than one extra base char partial diff", () => {
        const answer = "flerpsie";
        const input = "fléérpsy";

        const expectedCorrections: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.BASE_CHAR,
                    charIndex: 2,
                    tokenIndex: 0,
                    displayAnswer: "e",
                    input: "é",
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const unmatchedViolations: Diff[][] = [
            [
                [DiffCompare.EXTRA, "é", 3],
                [DiffCompare.EXTRA, "y", 7],
                [DiffCompare.MISSING, "ie", 6],
            ],
        ];
        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedCorrections);
        expect(correctedResult.unprocessedDiffs).toEqual(unmatchedViolations);
    });

    test("should not match more than one missing base char partial diff", () => {
        const answer = "flêérps";
        const input = "flerps";

        const expectedCorrections: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.BASE_CHAR,
                    charIndex: 2,
                    tokenIndex: 0,
                    displayAnswer: "ê",
                    input: "e",
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const unmatchedViolations: Diff[][] = [[[DiffCompare.MISSING, "é", 3]]];
        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedCorrections);
        expect(correctedResult.unprocessedDiffs).toEqual(unmatchedViolations);
    });

    test("should reject diffs even when there is an uneven number of diffs", () => {
        const answer = "bûti";
        const input = "sclt";

        const expectedUnmatchedDiffs: Diff[][] = [
            [
                [DiffCompare.EXTRA, "scl", 0],
                [DiffCompare.MISSING, "bû", 0],
                [DiffCompare.MISSING, "i", 3],
            ],
        ];

        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.unprocessedDiffs).toEqual(expectedUnmatchedDiffs);
    });

    test("should be able to handle multiple partial base char diffs", () => {
        const answer = "fléàrps";
        const input = "fleharps";

        const expectedCorrections: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.BASE_CHAR,
                    charIndex: 2,
                    tokenIndex: 0,
                    displayAnswer: "é",
                    input: "e",
                    message: sut.failRuleMessage,
                },
                {
                    correctionType: Correction.BASE_CHAR,
                    charIndex: 3,
                    tokenIndex: 0,
                    displayAnswer: "à",
                    input: "a",
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const unmatchedViolations: Diff[][] = [[[DiffCompare.EXTRA, "h", 3]]];
        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedCorrections);
        expect(correctedResult.unprocessedDiffs).toEqual(unmatchedViolations);
    });
});
