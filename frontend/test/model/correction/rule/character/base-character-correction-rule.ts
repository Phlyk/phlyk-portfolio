import { ProcessedDiff } from "../../correction-result.model";
import { Correction } from "../../correction.enum";
import { Diff, DiffCompare } from "../../diff/diff.model";
import { AbstractCharCorrectionRule } from "../abstract-char-correction-rule.model";

export class BaseCharacterCorrectionRule extends AbstractCharCorrectionRule {
    readonly failRuleMessage = "Watch out for those accents and capitals!";
    private collator: Intl.Collator;

    constructor(ruleScore: number) {
        super(ruleScore);
        this.collator = new Intl.Collator("en", { sensitivity: "base", usage: "search", ignorePunctuation: true });
    }

    findRuleViolations(tokenDiffs: Diff[], tokenIndex: number) {
        const entriesWithExtraChars = tokenDiffs.filter(diff => diff[0] == DiffCompare.EXTRA);
        const entriesWithMissingChars = tokenDiffs.filter(diff => diff[0] == DiffCompare.MISSING);

        let extraDiffIndex = 0;
        for (; extraDiffIndex < entriesWithExtraChars.length; extraDiffIndex++) {
            const extraCharDiff = entriesWithExtraChars[extraDiffIndex];
            const missingCharDiff = entriesWithMissingChars[extraDiffIndex];

            if (!missingCharDiff) {
                this.diffPairDoesNotMatch(extraCharDiff, missingCharDiff);
                continue;
            }

            const [baseCharMatches, nonBaseCharMatches] = this.filterPartialBaseChars(extraCharDiff, missingCharDiff);
            if (!baseCharMatches.length) {
                this.diffPairDoesNotMatch(extraCharDiff, missingCharDiff);
                continue;
            }

            this.processPartialBaseCharPairs(baseCharMatches, tokenIndex);
            this.tokenUnmatchedDiffs.push(...nonBaseCharMatches);
        };
        this.tokenUnmatchedDiffs.push(...entriesWithMissingChars.slice(extraDiffIndex))
    }

    private filterPartialBaseChars(extraCharDiff: Diff, missingCharDiff: Diff): [[Diff, Diff][], Diff[]] {
        const baseCharDiffMatchPairs: [Diff, Diff][] = [];
        const nonBaseCharDiffMatches: Diff[] = [];

        const splitExtraCharDiffs: [Diff, Diff[]][] = this.splitCharDiffAndCounterparts(extraCharDiff);
        const splitMissingCharDiffs: [Diff, Diff[]][] = this.splitCharDiffAndCounterparts(missingCharDiff);

        for (let eIndex = 0; eIndex < splitExtraCharDiffs.length; eIndex++) {
            const [splitExtraCharDiff, splitExtraCharCounterpartDiffs]: [Diff, Diff[]] = splitExtraCharDiffs[eIndex];
            for (let mIndex = 0; mIndex < splitMissingCharDiffs.length; mIndex++) {
                const [splitMissingCharDiff, splitMissingCharCounterpartDiffs]: [Diff, Diff[]] = splitMissingCharDiffs[mIndex];

                const collationNumber = this.collator.compare(splitExtraCharDiff[1], splitMissingCharDiff[1]);
                if (collationNumber != 0) {
                    continue;
                }

                const extraDiffCounterpart = splitExtraCharCounterpartDiffs.pop();
                const missingDiffCounterpart = splitMissingCharCounterpartDiffs.pop();

                baseCharDiffMatchPairs.push([splitExtraCharDiff, splitMissingCharDiff]);
                nonBaseCharDiffMatches.push(...splitExtraCharCounterpartDiffs, ...splitMissingCharCounterpartDiffs);

                if (extraDiffCounterpart && missingDiffCounterpart) {
                    const [recFoundMatches, recFoundNonMatches] = this.filterPartialBaseChars(
                        extraDiffCounterpart,
                        missingDiffCounterpart
                    );
                    baseCharDiffMatchPairs.push(...recFoundMatches);
                    nonBaseCharDiffMatches.push(...recFoundNonMatches);
                } else {
                    extraDiffCounterpart && nonBaseCharDiffMatches.push(extraDiffCounterpart);
                    missingDiffCounterpart && nonBaseCharDiffMatches.push(missingDiffCounterpart);
                }
                return [baseCharDiffMatchPairs, nonBaseCharDiffMatches];
            }
        }
        if (!baseCharDiffMatchPairs.length) {
            nonBaseCharDiffMatches.push(extraCharDiff, missingCharDiff);
        }

        return [baseCharDiffMatchPairs, nonBaseCharDiffMatches];
    }

    private splitCharDiffAndCounterparts([diffMarker, stringToSplit, diffIndex]: Diff): [Diff, Diff[]][] {
        return stringToSplit.split("").map((char, charIdx) => {
            const charsPreCurrentChar = stringToSplit.slice(0, charIdx);
            const charsPostCurrentChar = stringToSplit.slice(charIdx + 1, stringToSplit.length);

            const diffForChar: Diff = [diffMarker, char, diffIndex + charIdx];
            const diffForCharPre: Diff = [diffMarker, charsPreCurrentChar, diffIndex];
            const diffForCharPost: Diff = [diffMarker, charsPostCurrentChar, diffIndex + charIdx + 1];

            const charCounterpartDiffs: Diff[] = [];
            if (charsPreCurrentChar) charCounterpartDiffs.push(diffForCharPre);
            if (charsPostCurrentChar) charCounterpartDiffs.push(diffForCharPost);
            return [diffForChar, charCounterpartDiffs];
        });
    }

    private processPartialBaseCharPairs(baseCharPairs: [Diff, Diff][], tokenIndex: number) {
        baseCharPairs.forEach(([extraDiffBaseChar, missingDiffBaseChar]) => {
            this.tokenViolations.push(this.processMatchingDiff(extraDiffBaseChar, missingDiffBaseChar, tokenIndex));
        });
    }

    private processMatchingDiff(extraCharDiff: Diff, missingCharDiff: Diff, tokenIndex: number): ProcessedDiff {
        const correctionType = extraCharDiff[1] == missingCharDiff[1] ? Correction.EQUAL : Correction.BASE_CHAR;
        const message = extraCharDiff[1] == missingCharDiff[1] ? null : this.failRuleMessage;
        return {
            correctionType,
            displayAnswer: missingCharDiff[1],
            input: extraCharDiff[1],
            tokenIndex,
            charIndex: missingCharDiff[2],
            message,
        };
    }

    private diffPairDoesNotMatch(extraCharDiff: Diff, missingCharDiff: Diff) {
        this.tokenUnmatchedDiffs.push(extraCharDiff);
        missingCharDiff && this.tokenUnmatchedDiffs.push(missingCharDiff);
    }
}
