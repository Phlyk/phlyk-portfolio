import { ProcessedDiff } from "src/app/modules/learning/model/correction/correction-result.model";
import { Correction } from "src/app/modules/learning/model/correction/correction.enum";
import { Diff, DiffCompare } from "src/app/modules/learning/model/correction/diff/diff.model";
import { ExtraCharacterCorrectionRule } from "src/app/modules/learning/model/correction/rule/character/extra-character-correction-rule";
import { makeFullDiffBetween } from "../shared-correction-test-helper";

describe("ExtraCharacterCorrectionRule (isolated test)", () => {
    let sut: ExtraCharacterCorrectionRule;
    const testAnswer = "flerps";

    beforeEach(() => {
        sut = new ExtraCharacterCorrectionRule(1);
    });

    test("should match extra unexpected characters that do not exist in the answer", () => {
        const input = "flerzps";

        const expectedDiffs: ProcessedDiff[][] = [
            [
                {
                    charIndex: 4,
                    tokenIndex: 0,
                    correctionType: Correction.EXTRA_CHAR,
                    displayAnswer: "z",
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const correctedResult = sut.correct(makeFullDiffBetween(testAnswer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedDiffs);
    });

    test("should match multiple extra characters", () => {
        const input = "fflerrpss";

        const expectedDiffs: ProcessedDiff[][] = [
            [
                {
                    charIndex: 1,
                    tokenIndex: 0,
                    displayAnswer: "f",
                    correctionType: Correction.EXTRA_CHAR,
                    message: sut.failRuleMessage,
                },
                {
                    charIndex: 5,
                    tokenIndex: 0,
                    displayAnswer: "r",
                    correctionType: Correction.EXTRA_CHAR,
                    message: sut.failRuleMessage,
                },
                {
                    charIndex: 7,
                    tokenIndex: 0,
                    displayAnswer: "s",
                    correctionType: Correction.EXTRA_CHAR,
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const correctedResult = sut.correct(makeFullDiffBetween(testAnswer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedDiffs);
    });

    test("should not match characters that are incorrect", () => {
        const input = "flerfs";

        const unmatchedViolations: Diff[][] = [
            [
                [DiffCompare.EXTRA, "f", 4],
                [DiffCompare.MISSING, "p", 4],
            ],
        ];
        const correctedResult = sut.correct(makeFullDiffBetween(testAnswer, input));

        expect(correctedResult.matchedDiffs).toHaveLength(0);
        expect(correctedResult.unprocessedDiffs).toEqual(unmatchedViolations);
    });

    test("should not match missing characters", () => {
        const input = "flerp";

        const unmatchedViolations: Diff[][] = [[[DiffCompare.MISSING, "s", 5]]];

        const correctedResult = sut.correct(makeFullDiffBetween(testAnswer, input));

        expect(correctedResult.matchedDiffs).toHaveLength(0);
        expect(correctedResult.unprocessedDiffs).toEqual(unmatchedViolations);
    });

    test("should be able to handle other char diffs while handling extra char diffs", () => {
        const answer = "something complicated";
        const input = "sometingg kompllicatedd";

        const expectedDiffs: ProcessedDiff[][] = [
            [
                {
                    charIndex: 7,
                    tokenIndex: 0,
                    displayAnswer: "g",
                    correctionType: Correction.EXTRA_CHAR,
                    message: sut.failRuleMessage,
                },
            ],
            [
                {
                    charIndex: 4,
                    tokenIndex: 1,
                    displayAnswer: "l",
                    correctionType: Correction.EXTRA_CHAR,
                    message: sut.failRuleMessage,
                },
                {
                    charIndex: 11,
                    tokenIndex: 1,
                    displayAnswer: "d",
                    correctionType: Correction.EXTRA_CHAR,
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const unmatchedViolations: Diff[][] = [
            [[DiffCompare.MISSING, "h", 5]],
            [
                [DiffCompare.EXTRA, "k", 0],
                [DiffCompare.MISSING, "c", 0],
            ],
        ];

        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedDiffs);
        expect(correctedResult.unprocessedDiffs).toEqual(unmatchedViolations);
    });
});
