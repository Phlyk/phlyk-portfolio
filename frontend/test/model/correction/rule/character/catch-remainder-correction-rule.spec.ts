import { ProcessedDiff } from "src/app/modules/learning/model/correction/correction-result.model";
import { Correction } from "src/app/modules/learning/model/correction/correction.enum";
import { Diff, DiffCompare } from "src/app/modules/learning/model/correction/diff/diff.model";
import { CatchRemainderCorrectionRule } from "src/app/modules/learning/model/correction/rule/character/catch-remainder-correction-rule";

describe("CatchRemainderCorrectionRule (isolated test)", () => {
    let sut: CatchRemainderCorrectionRule;

    beforeEach(() => {
        sut = new CatchRemainderCorrectionRule(DiffCompare.EXTRA);
    });

    test("should process remaining uncorrected char diffs with as much detail as possible", () => {
        const uncorrectedCharDiffs: Diff[][] = [
            [
                [DiffCompare.EXTRA, "a", 1],
                [DiffCompare.MISSING, "o", 1],
            ],
            [
                [DiffCompare.EXTRA, "di", 0],
                [DiffCompare.MISSING, "je", 2],
                [DiffCompare.MISSING, "lol", 9],
            ],
            [[DiffCompare.EXTRA, "o", 8]],
        ];

        const expectedRemainderCorrections: ProcessedDiff[][] = [
            [
                {
                    message: "",
                    tokenIndex: 0,
                    charIndex: 1,
                    correctionType: Correction.UNMATCHED,
                    displayAnswer: "o",
                    input: "a",
                    inputIndex: 1,
                },
            ],
            [
                {
                    message: "",
                    tokenIndex: 1,
                    charIndex: 2,
                    correctionType: Correction.UNMATCHED,
                    displayAnswer: "je",
                    input: "di",
                    inputIndex: 0,
                },
                {
                    message: "",
                    tokenIndex: 1,
                    charIndex: 9,
                    correctionType: Correction.MISSING_CHAR,
                    displayAnswer: "lol",
                },
            ],
            [
                {
                    message: "",
                    tokenIndex: 2,
                    inputIndex: 8,
                    correctionType: Correction.EXTRA_CHAR,
                    input: "o",
                },
            ],
        ];

        const correctedResult = sut.correct(uncorrectedCharDiffs);
        expect(correctedResult.matchedDiffs).toEqual(expectedRemainderCorrections);
    });

    test("should create a penalty for each processed correction", () => {
        const uncorrectedCharDiffs: Diff[][] = [
            [
                [DiffCompare.EXTRA, "a", 1],
                [DiffCompare.MISSING, "o", 1],
            ],
            [
                [DiffCompare.EXTRA, "di", 0],
                [DiffCompare.MISSING, "je", 2],
                [DiffCompare.MISSING, "lol", 9],
            ],
            [[DiffCompare.EXTRA, "o", 8]],
        ];

        const correctedResult = sut.correct(uncorrectedCharDiffs);
        expect(correctedResult.penaltyScore).toEqual(4 * sut.ruleScore);
    });
});
