import { ProcessedDiff } from "../../correction-result.model";
import { Correction } from "../../correction.enum";
import { Diff, DiffCompare } from "../../diff/diff.model";
import { AbstractCharCorrectionRule } from "../abstract-char-correction-rule.model";

export class MissingCharacterCorrectionRule extends AbstractCharCorrectionRule {
    readonly failRuleMessage = "You forgot an extra character here";

    findRuleViolations(tokenDiffs: Diff[], tokenIndex: number) {
        const extraCharDiffs = tokenDiffs.filter(diff => diff[0] == DiffCompare.EXTRA);
        const missingCharDiffs = tokenDiffs.filter(diff => diff[0] == DiffCompare.MISSING);

        if (!missingCharDiffs.length) {
            return this.tokenUnmatchedDiffs.push(...extraCharDiffs);
        }

        missingCharDiffs.forEach(missingCharDiff => {
            const matchingIndexExtraCharDiffIndex = extraCharDiffs.findIndex(
                extraCharDiff => extraCharDiff[2] == missingCharDiff[2]
            );
            const matchingIndexExtraCharDiff = extraCharDiffs[matchingIndexExtraCharDiffIndex];
            if (matchingIndexExtraCharDiff) {
                this.tokenUnmatchedDiffs.push(missingCharDiff, matchingIndexExtraCharDiff);
                extraCharDiffs.splice(matchingIndexExtraCharDiffIndex, 1);
            } else {
                const processedDiff: ProcessedDiff = this.processViolatingDiff(missingCharDiff, tokenIndex);
                return this.tokenViolations.push(processedDiff);
            }
        });
        
        this.tokenUnmatchedDiffs.push(...extraCharDiffs);
    }

    private processViolatingDiff(extraCharDiff: Diff, tokenIndex: number): ProcessedDiff {
        return {
            correctionType: Correction.MISSING_CHAR,
            displayAnswer: extraCharDiff[1],
            tokenIndex,
            charIndex: extraCharDiff[2],
            message: this.failRuleMessage,
        };
    }
}
