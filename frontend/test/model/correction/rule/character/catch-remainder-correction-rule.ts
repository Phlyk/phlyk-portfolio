import { ArraysHelper } from "src/app/shared/helpers/arrays.helper";
import { ProcessedDiff } from "../../correction-result.model";
import { Correction } from "../../correction.enum";
import { Diff, DiffCompare } from "../../diff/diff.model";
import { AbstractCharCorrectionRule } from "../abstract-char-correction-rule.model";

export class CatchRemainderCorrectionRule extends AbstractCharCorrectionRule {
    readonly failRuleMessage = "";

    constructor(ruleScore: number) {
        super(ruleScore);
    }

    findRuleViolations(tokenDiffs: Diff[], tokenIndex: number) {
        const [extraCharsDiffs, missingCharDiffs] = ArraysHelper.partition(tokenDiffs, diff => diff[0] == DiffCompare.EXTRA);

        let extraDiffIndex = 0;
        for (; extraDiffIndex < extraCharsDiffs.length; extraDiffIndex++) {
            const extraCharDiff = extraCharsDiffs[extraDiffIndex];
            const missingCharDiff = missingCharDiffs[extraDiffIndex];
            
            const processedDiff = this.processMatchingDiff(extraCharDiff, missingCharDiff, tokenIndex);
    
            this.tokenViolations.push(processedDiff);
        }
        missingCharDiffs.slice(extraDiffIndex).forEach(missingCharDiff => {
            this.tokenViolations.push(this.processMatchingDiff(null, missingCharDiff, tokenIndex));
        });
    }

    private processMatchingDiff(extraCharDiff: Diff, missingCharDiff: Diff, tokenIndex: number): ProcessedDiff {
        const processedDiff: ProcessedDiff = {
            correctionType: Correction.UNMATCHED,
            message: this.failRuleMessage,
            tokenIndex,
        };

        if (missingCharDiff) {
            if (!extraCharDiff) processedDiff.correctionType = Correction.MISSING_CHAR;
            processedDiff.displayAnswer = missingCharDiff[1];
            processedDiff.charIndex = missingCharDiff[2];
        }
        if (extraCharDiff) {
            if (!missingCharDiff) processedDiff.correctionType = Correction.EXTRA_CHAR;
            processedDiff.input = extraCharDiff[1];
            processedDiff.inputIndex = extraCharDiff[2];
        }
        return processedDiff;
    }
}
