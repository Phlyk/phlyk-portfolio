import { ProcessedDiff } from "src/app/modules/learning/model/correction/correction-result.model";
import { Correction } from "src/app/modules/learning/model/correction/correction.enum";
import { Diff, DiffCompare } from "src/app/modules/learning/model/correction/diff/diff.model";
import { SimilarCharacterCorrectionRule } from "src/app/modules/learning/model/correction/rule/character/similar-character-correction-rule";
import { makeFullDiffBetween, makeFullNonEqualDiffBetween } from "../shared-correction-test-helper";

describe("SimilarCharacterCorrectionRule (isolated test)", () => {
    let sut: SimilarCharacterCorrectionRule;
    let similarCharsConfig = {};

    beforeEach(() => {
        sut = new SimilarCharacterCorrectionRule(1, similarCharsConfig);
    });

    afterEach(() => {
        similarCharsConfig = {};
    });

    test("should match similar characters on both sides of the pair", () => {
        const answer = "what's up zombie";
        const input = "what'z up sombie";

        similarCharsConfig["z"] = ["s"];
        similarCharsConfig["s"] = ["z"];

        const expectedDiffs: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.SIMILAR_CHAR,
                    charIndex: 5,
                    tokenIndex: 0,
                    displayAnswer: "s",
                    input: "z",
                    message: sut.failRuleMessage,
                },
            ],
            [
                {
                    correctionType: Correction.SIMILAR_CHAR,
                    charIndex: 0,
                    tokenIndex: 2,
                    displayAnswer: "z",
                    input: "s",
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedDiffs);
    });

    test("should be able to match multiple characters of different length", () => {
        const answer = "wicks";
        const input = "wix";

        similarCharsConfig["x"] = ["cks"];

        const expectedDiffs: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.SIMILAR_CHAR,
                    charIndex: 2,
                    tokenIndex: 0,
                    displayAnswer: "cks",
                    input: "x",
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedDiffs);
    });

    test("should register the index of the diff as the correct answer index", () => {
        const answer = "wicks";
        const input = "wihx";

        similarCharsConfig["x"] = ["cks"];
        similarCharsConfig["cks"] = ["x"];

        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.matchedDiffs).toHaveLength(1);
        expect(correctedResult.matchedDiffs[0]).toHaveLength(1);

        expect(correctedResult.matchedDiffs[0][0].charIndex).toEqual(2);
        expect(correctedResult.unprocessedDiffs).toEqual([[[DiffCompare.EXTRA, "h", 2]]]);
    });

    test("should match a diff if every character in string is a similar char", () => {
        const answer = "wassup zzzboy";
        const input = "wazzup sssboy";

        similarCharsConfig["z"] = ["s"];
        similarCharsConfig["s"] = ["z"];

        const expectedDiffs: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.SIMILAR_CHAR,
                    charIndex: 2,
                    tokenIndex: 0,
                    displayAnswer: "ss",
                    input: "zz",
                    message: sut.failRuleMessage,
                },
            ],
            [
                {
                    correctionType: Correction.SIMILAR_CHAR,
                    charIndex: 0,
                    tokenIndex: 1,
                    displayAnswer: "zzz",
                    input: "sss",
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedDiffs);
    });

    test("should be able to match multiple characters that are similar defined in config", () => {
        const answer = "quick, now quack!";
        const input = "kwick, now cwack!";

        similarCharsConfig["kw"] = ["qu", "cw"];
        similarCharsConfig["qu"] = ["kw", "cw"];
        similarCharsConfig["cw"] = ["kw", "qu"];

        const expectedDiffs: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.SIMILAR_CHAR,
                    charIndex: 0,
                    tokenIndex: 0,
                    displayAnswer: "qu",
                    input: "kw",
                    message: sut.failRuleMessage,
                },
            ],
            [
                {
                    correctionType: Correction.SIMILAR_CHAR,
                    charIndex: 0,
                    tokenIndex: 2,
                    displayAnswer: "qu",
                    input: "cw",
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedDiffs);
    });

    test("should be able to identify similar chars within a composite diff and reject the part that doesnt match", () => {
        const answer = "digging ficks";
        const input = "dihqing fixe";

        similarCharsConfig["q"] = ["gg"];
        similarCharsConfig["gg"] = ["q"];
        similarCharsConfig["x"] = ["cks"];
        similarCharsConfig["cks"] = ["x"];

        const expectedDiffs: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.SIMILAR_CHAR,
                    charIndex: 2,
                    tokenIndex: 0,
                    displayAnswer: "gg",
                    input: "q",
                    message: sut.failRuleMessage,
                },
            ],
            [
                {
                    correctionType: Correction.SIMILAR_CHAR,
                    charIndex: 2,
                    tokenIndex: 1,
                    displayAnswer: "cks",
                    input: "x",
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const expectedUnmatchedDiff: Diff[][] = [[[1, "h", 2]], [[1, "e", 3]]];

        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.matchedDiffs).toMatchObject(expectedDiffs);
        expect(correctedResult.unprocessedDiffs).toEqual(expectedUnmatchedDiff);
    });

    test("should be able to identify partial similar chars within a composite extra diff and reject multiple parts that dont match", () => {
        const answer = "test";
        const input = "tehzht";

        similarCharsConfig["z"] = ["s"];
        similarCharsConfig["s"] = ["z"];

        const expectedDiffs: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.SIMILAR_CHAR,
                    charIndex: 2,
                    tokenIndex: 0,
                    displayAnswer: "s",
                    input: "z",
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const expectedUnmatchedDiff: Diff[][] = [
            [
                [1, "h", 2],
                [1, "h", 4],
            ],
        ];

        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedDiffs);
        expect(correctedResult.unprocessedDiffs).toEqual(expectedUnmatchedDiff);
    });

    test("should be able to identify partial similar chars within a missing diff", () => {
        const answer = "delicious";
        const input = "delshus";

        similarCharsConfig["cio"] = ["sh"];
        similarCharsConfig["sh"] = ["cio"];

        const expectedDiffs: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.SIMILAR_CHAR,
                    charIndex: 4,
                    tokenIndex: 0,
                    displayAnswer: "cio",
                    input: "sh",
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const expectedUnmatchedDiff: Diff[][] = [[[-1, "i", 3]]];

        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedDiffs);
        expect(correctedResult.unprocessedDiffs).toEqual(expectedUnmatchedDiff);
    });

    test("should be able to split out the equal parts of diffs", () => {
        const answer = "zuti";
        const input = "tisu";

        similarCharsConfig["s"] = ["z"];
        similarCharsConfig["z"] = ["s"];

        const expectedCorrections: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.SIMILAR_CHAR,
                    displayAnswer: "z",
                    input: "s",
                    charIndex: 0,
                    message: sut.failRuleMessage,
                    tokenIndex: 0,
                    // inputIndex: 3,
                },
                {
                    correctionType: Correction.EQUAL,
                    displayAnswer: "u",
                    input: "u",
                    charIndex: 1,
                    message: null,
                    tokenIndex: 0,
                    // inputIndex: 3,
                },
            ],
        ];

        const correctedResult = sut.correct(makeFullNonEqualDiffBetween(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedCorrections);
        expect(correctedResult.unprocessedDiffs).toHaveLength(0);
    });

    test("should not match when there is no corresponding diff pair", () => {
        const answer = "tastyjimballs";
        const input = "tastyyjimballss";

        const unmatchedViolations: Diff[][] = [
            [
                [1, "y", 5],
                [1, "s", 13],
            ],
        ];

        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.unprocessedDiffs).toEqual(unmatchedViolations);
    });

    test("should not match when characters are not similar", () => {
        const answer = "oh shit son";
        const input = "oh flap zon";

        const unmatchedViolations: Diff[][] = [
            [
                [1, "flap", 0],
                [-1, "shit", 0],
            ],
            [
                [1, "z", 0],
                [-1, "s", 0],
            ],
        ];

        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.unprocessedDiffs).toEqual(unmatchedViolations);
    });

    test("should reject diffs even when there is an uneven number of diffs", () => {
        const answer = "bûti";
        const input = "sclt";

        const expectedUnmatchedDiffs: Diff[][] = [
            [
                [DiffCompare.EXTRA, "scl", 0],
                [DiffCompare.MISSING, "bû", 0],
                [DiffCompare.MISSING, "i", 3],
            ],
        ];

        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.unprocessedDiffs).toEqual(expectedUnmatchedDiffs);
    });

    test("should match be able to match multiple of the same similar chars in order", () => {
        const answer = "poolipspysle";
        const input = "poozhzle";

        similarCharsConfig["z"] = ["s"];
        similarCharsConfig["s"] = ["z"];

        const expectedMatches: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.SIMILAR_CHAR,
                    charIndex: 6,
                    tokenIndex: 0,
                    displayAnswer: "s",
                    input: "z",
                    message: sut.failRuleMessage,
                },
                {
                    correctionType: Correction.SIMILAR_CHAR,
                    charIndex: 9,
                    tokenIndex: 0,
                    displayAnswer: "s",
                    input: "z",
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const unmatchedViolations: Diff[][] = [
            [
                [DiffCompare.MISSING, "lip", 3],
                [DiffCompare.EXTRA, "h", 4],
                [DiffCompare.MISSING, "py", 7],
            ],
        ];

        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedMatches);
        expect(correctedResult.unprocessedDiffs).toEqual(unmatchedViolations);
    });
});
