import { ProcessedDiff } from "src/app/modules/learning/model/correction/correction-result.model";
import { Correction } from "src/app/modules/learning/model/correction/correction.enum";
import { Diff, DiffCompare } from "src/app/modules/learning/model/correction/diff/diff.model";
import { MissingCharacterCorrectionRule } from "src/app/modules/learning/model/correction/rule/character/missing-character-correction-rule";
import { makeFullDiffBetween } from "../shared-correction-test-helper";

describe("MissingCharacterCorrectionRule (isolated test)", () => {
    let sut: MissingCharacterCorrectionRule;
    const testAnswer = "flerps";

    beforeEach(() => {
        sut = new MissingCharacterCorrectionRule(1);
    });

    test("should match when a character has been missed", () => {
        const input = "fleps";

        const expectedDiffs: ProcessedDiff[][] = [
            [
                {
                    charIndex: 3,
                    correctionType: Correction.MISSING_CHAR,
                    displayAnswer: "r",
                    message: sut.failRuleMessage,
                    tokenIndex: 0,
                },
            ],
        ];

        const correctedResult = sut.correct(makeFullDiffBetween(testAnswer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedDiffs);
    });

    test("should match multiple missed characters", () => {
        const input = "fep";

        const expectedDiffs: ProcessedDiff[][] = [
            [
                {
                    charIndex: 1,
                    correctionType: Correction.MISSING_CHAR,
                    displayAnswer: "l",
                    message: sut.failRuleMessage,
                    tokenIndex: 0,
                },
                {
                    charIndex: 3,
                    correctionType: Correction.MISSING_CHAR,
                    displayAnswer: "r",
                    message: sut.failRuleMessage,
                    tokenIndex: 0,
                },
                {
                    charIndex: 5,
                    correctionType: Correction.MISSING_CHAR,
                    displayAnswer: "s",
                    message: sut.failRuleMessage,
                    tokenIndex: 0,
                },
            ],
        ];

        const correctedResult = sut.correct(makeFullDiffBetween(testAnswer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedDiffs);
    });

    test("should match multiple missed characters per token", () => {
        const answer = "lerpy flerps";
        const input = "lep flep";

        const expectedDiffs: ProcessedDiff[][] = [
            [
                {
                    charIndex: 2,
                    correctionType: Correction.MISSING_CHAR,
                    displayAnswer: "r",
                    message: sut.failRuleMessage,
                    tokenIndex: 0,
                },
                {
                    charIndex: 4,
                    correctionType: Correction.MISSING_CHAR,
                    displayAnswer: "y",
                    message: sut.failRuleMessage,
                    tokenIndex: 0,
                },
            ],
            [
                {
                    charIndex: 3,
                    correctionType: Correction.MISSING_CHAR,
                    displayAnswer: "r",
                    message: sut.failRuleMessage,
                    tokenIndex: 1,
                },
                {
                    charIndex: 5,
                    correctionType: Correction.MISSING_CHAR,
                    displayAnswer: "s",
                    message: sut.failRuleMessage,
                    tokenIndex: 1,
                },
            ],
        ];

        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedDiffs);
    });

    test("should match adjacent missing character", () => {
        const answer = "lerpy flerp";
        const input = "ley fle";

        const expectedDiffs: ProcessedDiff[][] = [
            [
                {
                    charIndex: 2,
                    correctionType: Correction.MISSING_CHAR,
                    displayAnswer: "rp",
                    message: sut.failRuleMessage,
                    tokenIndex: 0,
                },
            ],
            [
                {
                    charIndex: 3,
                    correctionType: Correction.MISSING_CHAR,
                    displayAnswer: "rp",
                    message: sut.failRuleMessage,
                    tokenIndex: 1,
                },
            ],
        ];

        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedDiffs);
    });

    test("should not match characters that are incorrect", () => {
        const input = "flerfs";

        const unmatchedViolations: Diff[][] = [
            [
                [DiffCompare.MISSING, "p", 4],
                [DiffCompare.EXTRA, "f", 4],
            ],
        ];
        const correctedResult = sut.correct(makeFullDiffBetween(testAnswer, input));

        expect(correctedResult.matchedDiffs).toHaveLength(0);
        expect(correctedResult.unprocessedDiffs).toEqual(unmatchedViolations);
    });

    test("should not match extra characters", () => {
        const input = "flerpsz";

        const unmatchedViolations: Diff[][] = [
            [
                [DiffCompare.EXTRA, "z", 6],
            ],
        ];

        const correctedResult = sut.correct(makeFullDiffBetween(testAnswer, input));

        expect(correctedResult.matchedDiffs).toEqual([]);
        expect(correctedResult.unprocessedDiffs).toEqual(unmatchedViolations);
    });

    test("should be able to handle other char diffs while handling missing char diffs", () => {
        const answer = "something complicated";
        const input = "sometingg kompicate";

        const expectedDiffs: ProcessedDiff[][] = [
            [
                {
                    charIndex: 5,
                    tokenIndex: 0,
                    displayAnswer: "h",
                    correctionType: Correction.MISSING_CHAR,
                    message: sut.failRuleMessage,
                },
            ],
            [
                {
                    charIndex: 4,
                    tokenIndex: 1,
                    displayAnswer: "l",
                    correctionType: Correction.MISSING_CHAR,
                    message: sut.failRuleMessage,
                },
                {
                    charIndex: 10,
                    tokenIndex: 1,
                    displayAnswer: "d",
                    correctionType: Correction.MISSING_CHAR,
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const unmatchedViolations: Diff[][] = [
            [[DiffCompare.EXTRA, "g", 7]],
            [
                [DiffCompare.MISSING, "c", 0],
                [DiffCompare.EXTRA, "k", 0],
            ],
        ];

        const correctedResult = sut.correct(makeFullDiffBetween(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedDiffs);
        expect(correctedResult.unprocessedDiffs).toEqual(unmatchedViolations);
    });
});
