import { ProcessedDiff } from "../../correction-result.model";
import { Correction } from "../../correction.enum";
import { Diff, DiffCompare } from "../../diff/diff.model";
import { AbstractCharCorrectionRule } from "../abstract-char-correction-rule.model";

export class SimilarCharacterCorrectionRule extends AbstractCharCorrectionRule {
    readonly failRuleMessage = "The character was not quite right";
    private readonly allCharsSameRegex = /^(.)\1*$/;

    constructor(ruleScore: number, private similarCharsMap: { [x: string]: string[] }) {
        super(ruleScore);
    }

    findRuleViolations(tokenDiffs: Diff[], tokenIndex: number) {
        const entriesWithExtraChars = tokenDiffs.filter(diff => diff[0] == DiffCompare.EXTRA);
        const entriesWithMissingChars = tokenDiffs.filter(diff => diff[0] == DiffCompare.MISSING);
        let extraDiffIndex = 0;
        for (; extraDiffIndex < entriesWithExtraChars.length; extraDiffIndex++) {
            const extraCharDiff = entriesWithExtraChars[extraDiffIndex];
            const missingCharDiff = entriesWithMissingChars[extraDiffIndex];
            if (!missingCharDiff) {
                this.diffPairDoesNotMatch(extraCharDiff, missingCharDiff);
                continue;
            }

            if (
                this.isCharSimilarChar(extraCharDiff, missingCharDiff) ||
                this.isRepeatingSimilarChar(extraCharDiff[1], missingCharDiff[1])
            ) {
                const processedDiff: ProcessedDiff = this.processMatchingDiff(extraCharDiff, missingCharDiff, tokenIndex);
                this.tokenViolations.push(processedDiff);
                continue;
            }

            const [similarExtraCharDiffPairs, dissimilarCharDiffs] = this.filterPartialSimilarChars(
                extraCharDiff,
                missingCharDiff
            );
            if (!similarExtraCharDiffPairs.length) {
                this.diffPairDoesNotMatch(extraCharDiff, missingCharDiff);
                continue;
            }

            this.processPartialSimilarCharPairs(similarExtraCharDiffPairs, tokenIndex);
            this.tokenUnmatchedDiffs.push(...dissimilarCharDiffs);
        };
        this.tokenUnmatchedDiffs.push(...entriesWithMissingChars.slice(extraDiffIndex));
    }

    private filterPartialSimilarChars(extraCharDiff: Diff, missingCharDiff: Diff): [[Diff, Diff][], Diff[]] {
        const similarCharDiffMatchPairs: [Diff, Diff][] = [];
        const nonSimilarCharDiffMatches: Diff[] = [];

        const extraCharDiffSubstrings: [Diff, Diff[]][] = this.calculateDiffSubstringsAndCounterparts(extraCharDiff);
        const missingCharDiffSubstrings: [Diff, Diff[]][] = this.calculateDiffSubstringsAndCounterparts(missingCharDiff);

        for (let eIndex = 0; eIndex < extraCharDiffSubstrings.length; eIndex++) {
            const [extraCharSubstringDiff, extraCharNonSubstringDiff]: [Diff, Diff[]] = extraCharDiffSubstrings[eIndex];
            for (let mIndex = 0; mIndex < missingCharDiffSubstrings.length; mIndex++) {
                const [missingCharSubstringDiff, missingCharNonSubstringDiff]: [Diff, Diff[]] = missingCharDiffSubstrings[mIndex];

                const areExtraMissingSubstringsSimilar = this.isCharSimilarChar(missingCharSubstringDiff, extraCharSubstringDiff);
                const areSplitCharsEqual = missingCharSubstringDiff[1] == extraCharSubstringDiff[1];

                if (!areExtraMissingSubstringsSimilar && !areSplitCharsEqual) {
                    continue;
                }

                const extraNonSubstrings = extraCharNonSubstringDiff.pop();
                const missingNonSubstrings = missingCharNonSubstringDiff.pop();

                similarCharDiffMatchPairs.push([extraCharSubstringDiff, missingCharSubstringDiff]);
                nonSimilarCharDiffMatches.push(...extraCharNonSubstringDiff, ...missingCharNonSubstringDiff);

                if (extraNonSubstrings && missingNonSubstrings) {
                    const [recFoundMatches, recFoundNonMatches] = this.filterPartialSimilarChars(
                        extraNonSubstrings,
                        missingNonSubstrings
                    );
                    similarCharDiffMatchPairs.push(...recFoundMatches);
                    nonSimilarCharDiffMatches.push(...recFoundNonMatches);
                } else {
                    extraNonSubstrings && nonSimilarCharDiffMatches.push(extraNonSubstrings);
                    missingNonSubstrings && nonSimilarCharDiffMatches.push(missingNonSubstrings);
                }
                return [similarCharDiffMatchPairs, nonSimilarCharDiffMatches];
            }
        }
        if (!similarCharDiffMatchPairs.length) {
            nonSimilarCharDiffMatches.push(extraCharDiff, missingCharDiff);
        }
        return [similarCharDiffMatchPairs, nonSimilarCharDiffMatches];
    }

    private calculateDiffSubstringsAndCounterparts([diffMarker, stringToSub, diffIndex]: Diff): [Diff, Diff[]][] {
        const substringDiffs: [Diff, Diff[]][] = [];
        for (let i = 0; i < stringToSub.length; i++) {
            for (let j = i + 1; j <= stringToSub.length; j++) {
                const substr = stringToSub.substring(i, j);
                const notCurrentSubstrPre = stringToSub.substring(0, i);
                const notCurrentSubstrPost = stringToSub.substring(j, stringToSub.length);

                const diffForSubstr: Diff = [diffMarker, substr, diffIndex + i];
                const diffForSubstrPre: Diff = [diffMarker, notCurrentSubstrPre, diffIndex];
                const diffForSubstrPost: Diff = [diffMarker, notCurrentSubstrPost, diffIndex + j];

                const nonSubstrDiffs: Diff[] = [];
                if (notCurrentSubstrPre) nonSubstrDiffs.push(diffForSubstrPre);
                if (notCurrentSubstrPost) nonSubstrDiffs.push(diffForSubstrPost);
                substringDiffs.push([diffForSubstr, nonSubstrDiffs]);
            }
        }
        return substringDiffs;
    }

    private processPartialSimilarCharPairs(similarCharDiffMatches: [Diff, Diff][], tokenIndex: number) {
        similarCharDiffMatches.forEach(([extraDiffSimilarChar, missingDiffSimilarChar]) => {
            this.tokenViolations.push(this.processMatchingDiff(extraDiffSimilarChar, missingDiffSimilarChar, tokenIndex));
        });
    }
    
    private isCharSimilarChar(extraCharDiff: Diff, missingCharDiff: Diff): boolean {
        const similarCharsToExtraCharDiff = this.similarCharsMap[extraCharDiff[1]];
        return similarCharsToExtraCharDiff?.includes(missingCharDiff[1]);
    }

    private isRepeatingSimilarChar(extraCharDiff: string, missingCharDiff: string): boolean {
        const extraCharsSameMatch = extraCharDiff.match(this.allCharsSameRegex);
        const missingCharsSameMatch = missingCharDiff.match(this.allCharsSameRegex);
        if (!extraCharsSameMatch || !missingCharsSameMatch) {
            return false;
        }

        const repeatingExtraChar = extraCharsSameMatch[1];
        const repeatingMissingChar = missingCharsSameMatch[1];
        return this.similarCharsMap[repeatingExtraChar]?.includes(repeatingMissingChar);
    }

    private processMatchingDiff(extraCharDiff: Diff, missingCharDiff: Diff, tokenIndex: number): ProcessedDiff {
        const correctionType = extraCharDiff[1] == missingCharDiff[1] ? Correction.EQUAL : Correction.SIMILAR_CHAR;
        const message = extraCharDiff[1] == missingCharDiff[1] ? null : this.failRuleMessage;
        return {
            correctionType,
            displayAnswer: missingCharDiff[1],
            input: extraCharDiff[1],
            tokenIndex,
            charIndex: missingCharDiff[2],
            message,
        };
    }

    private diffPairDoesNotMatch(extraCharDiff: Diff, missingCharDiff: Diff) {
        this.tokenUnmatchedDiffs.push(extraCharDiff);
        missingCharDiff && this.tokenUnmatchedDiffs.push(missingCharDiff);
    }
}
