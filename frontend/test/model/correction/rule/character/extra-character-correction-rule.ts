import { ProcessedDiff } from "../../correction-result.model";
import { Correction } from "../../correction.enum";
import { Diff, DiffCompare } from "../../diff/diff.model";
import { AbstractCharCorrectionRule } from "../abstract-char-correction-rule.model";

export class ExtraCharacterCorrectionRule extends AbstractCharCorrectionRule {
    readonly failRuleMessage = "You added an extra character here";

    findRuleViolations(tokenDiffs: Diff[], tokenIndex: number) {
        const extraCharDiffs = tokenDiffs.filter(diff => diff[0] == DiffCompare.EXTRA);
        const missingCharDiffs = tokenDiffs.filter(diff => diff[0] == DiffCompare.MISSING);

        if (!extraCharDiffs.length) {
            return this.tokenUnmatchedDiffs.push(...missingCharDiffs);
        }

        extraCharDiffs.forEach(extraCharDiff => {
            const matchingIndexMissingCharDiffIndex = missingCharDiffs.findIndex(
                missingCharDiff => missingCharDiff[2] == extraCharDiff[2]
            );
            const matchingIndexMissingCharDiff = missingCharDiffs[matchingIndexMissingCharDiffIndex];
            if (matchingIndexMissingCharDiff) {
                this.tokenUnmatchedDiffs.push(extraCharDiff, matchingIndexMissingCharDiff);
                missingCharDiffs.splice(matchingIndexMissingCharDiffIndex, 1);
            } else {
                const processedDiff: ProcessedDiff = this.processViolatingDiff(extraCharDiff, tokenIndex);
                this.tokenViolations.push(processedDiff);
            }
        });

        this.tokenUnmatchedDiffs.push(...missingCharDiffs);
    }

    private processViolatingDiff(extraCharDiff: Diff, tokenIndex: number): ProcessedDiff {
        return {
            correctionType: Correction.EXTRA_CHAR,
            displayAnswer: extraCharDiff[1],
            tokenIndex,
            charIndex: extraCharDiff[2],
            message: this.failRuleMessage,
        };
    }
}
