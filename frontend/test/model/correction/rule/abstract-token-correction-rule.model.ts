import { ProcessedDiff } from "../correction-result.model";
import { Diff } from "../diff/diff.model";
import { CorrectionRuleResult } from "./correction-rule-result.model";
import { CorrectionRule } from "./correction-rule.model";

export abstract class AbstractTokenCorrectionRule implements CorrectionRule {
    abstract readonly failRuleMessage: string;
    ruleViolations: ProcessedDiff[][];
    unmatchedDiffs: Diff[][];

    constructor(public ruleScore: number) {}

    correct(diffedResult: Diff[][]): CorrectionRuleResult {
        this.ruleViolations = [];
        this.unmatchedDiffs = [];

        this.findRuleViolations(diffedResult);

        const penaltyScore = this.calculatePenaltyScore();

        return {
            matchedDiffs: this.ruleViolations,
            unprocessedDiffs: this.unmatchedDiffs,
            penaltyScore,
        };
    }

    private calculatePenaltyScore(): number {
        return this.ruleViolations
            .map((tokenDiff: ProcessedDiff[]) => tokenDiff[0])
            .reduce((acc, _) => (acc += this.ruleScore), 0);
    }

    protected abstract findRuleViolations(diffedResult: Diff[][]);
}
