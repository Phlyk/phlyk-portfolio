import { ProcessedDiff } from "../correction-result.model";
import { Diff } from "../diff/diff.model";

export interface CorrectionRuleResult {
    matchedDiffs: ProcessedDiff[][];
    unprocessedDiffs: Diff[][];
    penaltyScore: number;
}
