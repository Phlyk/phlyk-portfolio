import { ProcessedDiff } from "../../correction-result.model";
import { Correction } from "../../correction.enum";
import { Diff, DiffCompare } from "../../diff/diff.model";
import { AbstractTokenCorrectionRule } from "../abstract-token-correction-rule.model";

export class MissingTokenCorrectionRule extends AbstractTokenCorrectionRule {
    failRuleMessage = "You missed a shit word son";

    findRuleViolations(diffedResult: Diff[][]) {
        const missingTokenDiffs = diffedResult
            .map(tokenDiff => tokenDiff[0])
            .filter((tokenDiff: Diff) => tokenDiff[0] == DiffCompare.MISSING);
        const extraTokenDiffs = diffedResult
            .map(tokenDiff => tokenDiff[0])
            .filter((tokenDiff: Diff) => tokenDiff[0] == DiffCompare.EXTRA);

        missingTokenDiffs.forEach((missingTokenDiff, index) => {
            const matchedExtraDiffOnIndex = extraTokenDiffs.find(extraTokenDiff => extraTokenDiff[2] == missingTokenDiff[2]);
            const matchedExtraDiffOnAlignment = extraTokenDiffs[index];
            if (!matchedExtraDiffOnIndex && !matchedExtraDiffOnAlignment) {
                const processedDiff: ProcessedDiff = this.processViolatingDiff(missingTokenDiff);
                return this.ruleViolations.push([processedDiff]);
            }
            this.unmatchedDiffs.push([missingTokenDiff]);
        });
        this.unmatchedDiffs.push(...extraTokenDiffs.map(missingToken => [missingToken]));
    }

    private processViolatingDiff([_, diffString, diffIndex]: Diff): ProcessedDiff {
        return {
            correctionType: Correction.MISSING_TOKEN,
            displayAnswer: diffString,
            tokenIndex: diffIndex,
            message: this.failRuleMessage,
        };
    }
}
