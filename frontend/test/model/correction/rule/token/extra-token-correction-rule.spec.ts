import { ProcessedDiff } from "src/app/modules/learning/model/correction/correction-result.model";
import { Correction } from "src/app/modules/learning/model/correction/correction.enum";
import { Diff, DiffCompare } from "src/app/modules/learning/model/correction/diff/diff.model";
import { ExtraTokenCorrectionRule } from "src/app/modules/learning/model/correction/rule/token/extra-token-correction-rule";
import { makeNonEqualTokenDiffBetween, makeTokenDiffArray } from "../../correction/rule/shared-correction-test-helper";

describe("ExtraTokenCorrectionRule (isolated test)", () => {
    let sut: ExtraTokenCorrectionRule;

    beforeEach(() => {
        sut = new ExtraTokenCorrectionRule(1);
    });

    test("should match extra tokens", () => {
        const answer = "flerpy nerpens";
        const input = "flerpy nerpens jerpens";

        const expectedResult: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.EXTRA_TOKEN,
                    displayAnswer: "jerpens",
                    tokenIndex: 2,
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const correctedResult = sut.correct(makeTokenDiffArray(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedResult);
        expect(correctedResult.unprocessedDiffs).toHaveLength(0);
    });
    
    test("should match multiple extra tokens", () => {
        const answer = "flerpy nerpens";
        const input = "flerpy nerpens jerpens speepens";

        const expectedResult: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.EXTRA_TOKEN,
                    displayAnswer: "jerpens",
                    tokenIndex: 2,
                    message: sut.failRuleMessage,
                },
            ],
            [
                {
                    correctionType: Correction.EXTRA_TOKEN,
                    displayAnswer: "speepens",
                    tokenIndex: 3,
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const correctedResult = sut.correct(makeTokenDiffArray(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedResult);
        expect(correctedResult.unprocessedDiffs).toHaveLength(0);
    });

    test("should match multiple extra tokens in different order", () => {
        const answer = "flerpy nerpens";
        const input = "flerpy blerps nerpens jerpens";

        const expectedResult: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.EXTRA_TOKEN,
                    displayAnswer: "blerps",
                    tokenIndex: 1,
                    message: sut.failRuleMessage,
                },
            ],
            [
                {
                    correctionType: Correction.EXTRA_TOKEN,
                    displayAnswer: "jerpens",
                    tokenIndex: 3,
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const correctedResult = sut.correct(makeTokenDiffArray(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedResult);
        expect(correctedResult.unprocessedDiffs).toHaveLength(0);
    });

    test("should reject the token itself when they do not match", () => {
        const answer = "flerpy nerpens";
        const input = "flerpy nerpen";

        const expectedUnprocessedDiffs: Diff[][] = [[[DiffCompare.EXTRA, "nerpen", 1]], [[DiffCompare.MISSING, "nerpens", 1]]];

        const correctedResult = sut.correct(makeNonEqualTokenDiffBetween(answer, input));

        expect(correctedResult.matchedDiffs).toHaveLength(0);
        expect(correctedResult.unprocessedDiffs).toEqual(expectedUnprocessedDiffs);
    });

    test("should ignore wrong order tokens", () => {
        const answer = "flerpy nerpens";
        const input = "nerpens flerpy";

        const correctedResult = sut.correct(makeTokenDiffArray(answer, input));

        expect(correctedResult.unprocessedDiffs).toHaveLength(0);
        expect(correctedResult.matchedDiffs).toHaveLength(0);
    });

    test("should not match missing tokens", () => {
        const answer = "flerpy nerpens";
        const input = "nerpens";

        const correctedResult = sut.correct(makeTokenDiffArray(answer, input));

        const expectedUnprocessedDiffs: Diff[][] = [[[DiffCompare.MISSING, "flerpy", 0]]];

        expect(correctedResult.unprocessedDiffs).toEqual(expectedUnprocessedDiffs);
        expect(correctedResult.matchedDiffs).toHaveLength(0);
    });

    test("should not match tokens that are out of index alignment (post other rule processing) as extra", () => {
        const diffsToCorrect: Diff[][] = [
            [[DiffCompare.EXTRA, "similarWord1", 1]],
            [[DiffCompare.MISSING, "word1", 0]],
            [[DiffCompare.EXTRA, "similarWord2", 3]],
            [[DiffCompare.MISSING, "word2", 2]],
        ];

        const expectedUnprocessedDiffs: Diff[][] = [
            [[DiffCompare.EXTRA, "similarWord1", 1]],
            [[DiffCompare.EXTRA, "similarWord2", 3]],
            [[DiffCompare.MISSING, "word1", 0]],
            [[DiffCompare.MISSING, "word2", 2]],
        ]
        const correctedResult = sut.correct(diffsToCorrect);

        expect(correctedResult.matchedDiffs).toHaveLength(0);
        expect(correctedResult.unprocessedDiffs).toEqual(expectedUnprocessedDiffs);
    });

    test("KNWON LIMITATION - cannot correctly calculate the extra token when misaligned", () => {
        const answer = "can I even do this";
        const input = "yo can i iven do dis"

        const diffsToCorrect = makeNonEqualTokenDiffBetween(answer, input);

        const expectedProcessedDiffs: ProcessedDiff[][] = [
            [{
                correctionType: Correction.EXTRA_TOKEN,
                displayAnswer: "dis", // should be "yo"
                tokenIndex: 5,
                message: sut.failRuleMessage,
            }]
        ];

        const expectedUnprocessedDiffs: Diff[][] = [
            [[DiffCompare.EXTRA, "yo", 0]],
            [[DiffCompare.EXTRA, "i", 2]],
            [[DiffCompare.EXTRA, "iven", 3]],
            [[DiffCompare.MISSING, "I", 1]],
            [[DiffCompare.MISSING, "even", 2]],
            [[DiffCompare.MISSING, "this", 4]],
        ]
        
        const correctedResult = sut.correct(diffsToCorrect);
        
        expect(correctedResult.matchedDiffs).toEqual(expectedProcessedDiffs);
        expect(correctedResult.unprocessedDiffs).toEqual(expectedUnprocessedDiffs)
    });
});
