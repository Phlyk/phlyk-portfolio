import { ProcessedDiff } from "src/app/modules/learning/model/correction/correction-result.model";
import { Correction } from "src/app/modules/learning/model/correction/correction.enum";
import { Diff, DiffCompare } from "src/app/modules/learning/model/correction/diff/diff.model";
import { MissingTokenCorrectionRule } from "src/app/modules/learning/model/correction/rule/token/missing-token-correction-rule";
import { makeNonEqualTokenDiffBetween, makeTokenDiffArray } from "../shared-correction-test-helper";

describe("MissingTokenCorrectionRule (isolated test)", () => {
    let sut: MissingTokenCorrectionRule;
    
    beforeEach(() => {
        sut = new MissingTokenCorrectionRule(1);
    });
    
    test("should match missing tokens", () => {
        const answer = "flerpy nerpens jerpens";
        const input = "flerpy nerpens";

        const expectedResult: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.MISSING_TOKEN,
                    displayAnswer: "jerpens",
                    tokenIndex: 2,
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const correctedResult = sut.correct(makeTokenDiffArray(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedResult);
        expect(correctedResult.unprocessedDiffs).toHaveLength(0);
    });
    
    test("should match multiple missing tokens", () => {
        const answer = "flerpy nerpens jerpens speepens";
        const input = "flerpy nerpens";

        const expectedResult: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.MISSING_TOKEN,
                    displayAnswer: "jerpens",
                    tokenIndex: 2,
                    message: sut.failRuleMessage,
                },
            ],
            [
                {
                    correctionType: Correction.MISSING_TOKEN,
                    displayAnswer: "speepens",
                    tokenIndex: 3,
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const correctedResult = sut.correct(makeTokenDiffArray(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedResult);
        expect(correctedResult.unprocessedDiffs).toHaveLength(0);
    });

    test("should match multiple missing tokens in different order", () => {
        const answer = "flerpy blerps nerpens jerpens";
        const input = "flerpy nerpens";

        const expectedResult: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.MISSING_TOKEN,
                    displayAnswer: "blerps",
                    tokenIndex: 1,
                    message: sut.failRuleMessage,
                },
            ],
            [
                {
                    correctionType: Correction.MISSING_TOKEN,
                    displayAnswer: "jerpens",
                    tokenIndex: 3,
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const correctedResult = sut.correct(makeTokenDiffArray(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedResult);
        expect(correctedResult.unprocessedDiffs).toHaveLength(0);
    });

    test("should reject the token itself when they do not match", () => {
        const answer = "flerpy nerpens";
        const input = "flerpy nerpen";

        const expectedUnprocessedDiffs: Diff[][] = [[[DiffCompare.MISSING, "nerpens", 1]], [[DiffCompare.EXTRA, "nerpen", 1]]];

        const correctedResult = sut.correct(makeTokenDiffArray(answer, input));

        expect(correctedResult.matchedDiffs).toHaveLength(0);
        expect(correctedResult.unprocessedDiffs).toEqual(expectedUnprocessedDiffs);
    });

    test("should not match wrong order tokens", () => {
        const answer = "flerpy nerpens";
        const input = "nerpens flerpy";

        const correctedResult = sut.correct(makeTokenDiffArray(answer, input));

        expect(correctedResult.unprocessedDiffs).toHaveLength(0);
        expect(correctedResult.matchedDiffs).toHaveLength(0);
    });

    test("should not match extra tokens", () => {
        const answer = "flerpy";
        const input = "flerpy nerpens";

        const correctedResult = sut.correct(makeTokenDiffArray(answer, input));

        const expectedUnprocessedDiffs: Diff[][] = [[[DiffCompare.EXTRA, "nerpens", 1]]];

        expect(correctedResult.unprocessedDiffs).toEqual(expectedUnprocessedDiffs);
        expect(correctedResult.matchedDiffs).toHaveLength(0);
    });

    test("should not match tokens that are out of index alignment (post other rule processing) as missing", () => {
        const diffsToCorrect: Diff[][] = [
            [[DiffCompare.EXTRA, "similarWord1", 1]],
            [[DiffCompare.MISSING, "word1", 0]],
            [[DiffCompare.EXTRA, "similarWord2", 3]],
            [[DiffCompare.MISSING, "word2", 2]],
        ];

        const expectedUnprocessedDiffs: Diff[][] = [
            [[DiffCompare.MISSING, "word1", 0]],
            [[DiffCompare.MISSING, "word2", 2]],
            [[DiffCompare.EXTRA, "similarWord1", 1]],
            [[DiffCompare.EXTRA, "similarWord2", 3]],
        ]
        const correctedResult = sut.correct(diffsToCorrect);

        expect(correctedResult.matchedDiffs).toHaveLength(0);
        expect(correctedResult.unprocessedDiffs).toEqual(expectedUnprocessedDiffs);
    });

    test("KNWON LIMITATION - cannot correctly calculate the missing token when misaligned", () => {
        const answer = "hey can I even do this";
        const input = "can i iven do dis"

        const diffsToCorrect = makeNonEqualTokenDiffBetween(answer, input);

        const expectedProcessedDiffs: ProcessedDiff[][] = [
            [{
                correctionType: Correction.MISSING_TOKEN,
                displayAnswer: "this", // should be "hey"
                tokenIndex: 5,
                message: sut.failRuleMessage,
            }]
        ];

        const expectedUnprocessedDiffs: Diff[][] = [
            [[DiffCompare.MISSING, "hey", 0]],
            [[DiffCompare.MISSING, "I", 2]],
            [[DiffCompare.MISSING, "even", 3]],
            [[DiffCompare.EXTRA, "i", 1]],
            [[DiffCompare.EXTRA, "iven", 2]],
            [[DiffCompare.EXTRA, "dis", 4]],
        ]
        
        const correctedResult = sut.correct(diffsToCorrect);
        
        expect(correctedResult.matchedDiffs).toEqual(expectedProcessedDiffs);
        expect(correctedResult.unprocessedDiffs).toEqual(expectedUnprocessedDiffs)
    });
});
