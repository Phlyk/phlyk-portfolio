import { ProcessedDiff } from "src/app/modules/learning/model/correction/correction-result.model";
import { Correction } from "src/app/modules/learning/model/correction/correction.enum";
import { Diff, DiffCompare } from "src/app/modules/learning/model/correction/diff/diff.model";
import { WrongOrderTokenCorrectionRule } from "src/app/modules/learning/model/correction/rule/token/wrong-order-token-correction-rule";
import { makeNonEqualTokenDiffBetween, makeTokenDiffArray } from "../shared-correction-test-helper";

describe("WrongOrderTokenCorrectionRule (isolated test)", () => {
    let sut: WrongOrderTokenCorrectionRule;

    beforeEach(() => {
        sut = new WrongOrderTokenCorrectionRule(1);
    });

    test("should process wrong order tokens as a pair from answer to input", () => {
        const answer = "you flerpy pumps";
        const input = "you pumps flerpy";

        const expectedResult: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.WRONG_ORDER,
                    displayAnswer: "flerpy",
                    inputIndex: 2,
                    tokenIndex: 1,
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const correctedResult = sut.correct(makeTokenDiffArray(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedResult);
        assertNoNonEqualDiffs(correctedResult.unprocessedDiffs);
    });

    test("should process multiple wrong order pairs", () => {
        const answer = "scoopy loopy choops floops";
        const input = "floops choops scoopy loopy";

        const expectedResult: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.WRONG_ORDER,
                    displayAnswer: "choops",
                    tokenIndex: 2,
                    inputIndex: 1,
                    message: sut.failRuleMessage,
                },
            ],
            [
                {
                    correctionType: Correction.WRONG_ORDER,
                    displayAnswer: "floops",
                    tokenIndex: 3,
                    inputIndex: 0,
                    message: sut.failRuleMessage,
                },
            ],
        ];

        const correctedResult = sut.correct(makeTokenDiffArray(answer, input));

        expect(correctedResult.matchedDiffs).toEqual(expectedResult);
        assertNoNonEqualDiffs(correctedResult.unprocessedDiffs);
    });

    test("should be able to process wrong order pairs arriving in wrong order", () => {
        const diffsToCorrect: Diff[][] = [
            [[DiffCompare.WRONG_ORDER_ANSWER, "word1", 1]],
            [[DiffCompare.WRONG_ORDER_ANSWER, "word2", 9]],
            [[DiffCompare.WRONG_ORDER_INPUT, "word2", 4]],
            [[DiffCompare.WRONG_ORDER_INPUT, "word1", 3]],
        ];

        const expectedMatchedDiffs: ProcessedDiff[][] = [
            [
                {
                    correctionType: Correction.WRONG_ORDER,
                    displayAnswer: "word2",
                    tokenIndex: 9,
                    inputIndex: 4,
                    message: sut.failRuleMessage,
                },
            ],
            [
                {
                    correctionType: Correction.WRONG_ORDER,
                    displayAnswer: "word1",
                    tokenIndex: 1,
                    inputIndex: 3,
                    message: sut.failRuleMessage,
                },
            ]
        ];

        const correctedResult = sut.correct(diffsToCorrect);

        expect(correctedResult.matchedDiffs).toEqual(expectedMatchedDiffs);
        expect(correctedResult.unprocessedDiffs).toHaveLength(0);
    });

    test("should match wrong order pairs regardless of index (diff library matches \"sir\" as equal)", () => {
        const answer = "four diffs please sir";
        const input = "sir før diffs plise";

        const diffsToCorrect = makeNonEqualTokenDiffBetween(answer, input);

        const expectedProcessedDiffs: ProcessedDiff[][] = [[{
            message: sut.failRuleMessage,
            tokenIndex: 1,
            inputIndex: 2,
            displayAnswer: "diffs",
            correctionType: Correction.WRONG_ORDER
        }]];
        const expectedUnprocessedDiffs: Diff[][] = [
            [[DiffCompare.MISSING, "four", 0]],
            [[DiffCompare.MISSING, "please", 2]],
            [[DiffCompare.EXTRA, "før", 1]],
            [[DiffCompare.EXTRA, "plise", 3]],
        ];

        const correctedResult = sut.correct(diffsToCorrect);
        
        expect(correctedResult.matchedDiffs).toEqual(expectedProcessedDiffs)
        expect(correctedResult.unprocessedDiffs).toEqual(expectedUnprocessedDiffs)
    });


    test("should match wrong order pairs regardless of index (diff library matches \"diffs\" as equal)", () => {
        const answer = "four diffs please sir";
        const input = "sir før diffs plise extraTokenThatMessesThingsUp";

        const diffsToCorrect = makeNonEqualTokenDiffBetween(answer, input);

        const expectedProcessedDiffs: ProcessedDiff[][] = [[{
            message: sut.failRuleMessage,
            tokenIndex: 3,
            inputIndex: 0,
            displayAnswer: "sir",
            correctionType: Correction.WRONG_ORDER
        }]];
        const expectedUnprocessedDiffs: Diff[][] = [
            [[DiffCompare.EXTRA, "før", 1]],
            [[DiffCompare.MISSING, "four", 0]],
            [[DiffCompare.MISSING, "please", 2]],
            [[DiffCompare.EXTRA, "plise", 3]],
            [[DiffCompare.EXTRA, "extraTokenThatMessesThingsUp", 4]],
        ];

        const correctedResult = sut.correct(diffsToCorrect);
        
        expect(correctedResult.matchedDiffs).toEqual(expectedProcessedDiffs)
        expect(correctedResult.unprocessedDiffs).toEqual(expectedUnprocessedDiffs)
    });
    
    test.each([
        ["flerpy nerpens", "flerpy nerpen", [[[DiffCompare.MISSING, "nerpens", 1]], [[DiffCompare.EXTRA, "nerpen", 1]]]],
        ["flerpy", "flerpy nerpens", [[[DiffCompare.EXTRA, "nerpens", 1]]]],
        ["flerpy nerpens", "flerpy", [[[DiffCompare.MISSING, "nerpens", 1]]]],
    ])("should reject tokens that aren't wrong order", (answer, input, expectedUnprocessedDiffs: Diff[][]) => {
        const correctedResult = sut.correct(makeTokenDiffArray(answer, input));

        expect(correctedResult.matchedDiffs).toHaveLength(0);
        assertNonEqualDiffsEquals(correctedResult.unprocessedDiffs, expectedUnprocessedDiffs);
    });
});

function assertNoNonEqualDiffs(unexpectedDiffs: Diff[][]) {
    const nonEqualDiffs = unexpectedDiffs.filter(tokenDiffs => tokenDiffs[0][0] != DiffCompare.EQUAL);
    expect(nonEqualDiffs).toHaveLength(0);
}

function assertNonEqualDiffsEquals(unexpectedDiffs: Diff[][], expectedUnprocessedDiffs: Diff[][]) {
    const nonEqualDiffs = unexpectedDiffs.filter(tokenDiffs => tokenDiffs[0][0] != DiffCompare.EQUAL);
    expect(nonEqualDiffs).toEqual(expectedUnprocessedDiffs);
}
