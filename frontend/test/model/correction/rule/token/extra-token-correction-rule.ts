import { ProcessedDiff } from "../../correction-result.model";
import { Correction } from "../../correction.enum";
import { Diff, DiffCompare } from "../../diff/diff.model";
import { AbstractTokenCorrectionRule } from "../abstract-token-correction-rule.model";

export class ExtraTokenCorrectionRule extends AbstractTokenCorrectionRule {
    failRuleMessage = "You added some extra shit word son";

    oldfindRuleViolations(diffedResult: Diff[][]) {
        const extraTokenDiffs = diffedResult
            .map(tokenDiff => tokenDiff[0])
            .filter((tokenDiff: Diff) => tokenDiff[0] == DiffCompare.EXTRA);
        const missingTokenDiffs = diffedResult
            .map(tokenDiff => tokenDiff[0])
            .filter((tokenDiff: Diff) => tokenDiff[0] == DiffCompare.MISSING);

        extraTokenDiffs.forEach(extraTokenDiff => {
            const extraTokenDiffIndex = extraTokenDiff[2];
            if (!missingTokenDiffs.some(missingTokenDiff => missingTokenDiff[2] == extraTokenDiffIndex)) {
                const processedDiff: ProcessedDiff = this.processViolatingDiff(extraTokenDiff);
                return this.ruleViolations.push([processedDiff]);
            }
            this.unmatchedDiffs.push([extraTokenDiff]);
        });
        this.unmatchedDiffs.push(...missingTokenDiffs.map(missingToken => [missingToken]));
    }

    findRuleViolations(diffedResult: Diff[][]) {
        const extraTokenDiffs = diffedResult
            .map(tokenDiff => tokenDiff[0])
            .filter((tokenDiff: Diff) => tokenDiff[0] == DiffCompare.EXTRA);
        const missingTokenDiffs = diffedResult
            .map(tokenDiff => tokenDiff[0])
            .filter((tokenDiff: Diff) => tokenDiff[0] == DiffCompare.MISSING);

        extraTokenDiffs.forEach((extraTokenDiff, index) => {
            const matchedMissingDiffOnIndex = missingTokenDiffs.find(missingTokenDiff => missingTokenDiff[2] == extraTokenDiff[2]);
            const matchedMissingDiffOnAlignment = missingTokenDiffs[index];
            if (!matchedMissingDiffOnIndex && !matchedMissingDiffOnAlignment) {
                const processedDiff: ProcessedDiff = this.processViolatingDiff(extraTokenDiff);
                return this.ruleViolations.push([processedDiff]);
            }
            this.unmatchedDiffs.push([extraTokenDiff]);
        });
        this.unmatchedDiffs.push(...missingTokenDiffs.map(missingToken => [missingToken]));
    }

    private processViolatingDiff([_, diffString, diffIndex]: Diff): ProcessedDiff {
        return {
            correctionType: Correction.EXTRA_TOKEN,
            displayAnswer: diffString,
            tokenIndex: diffIndex,
            message: this.failRuleMessage,
        };
    }
}
