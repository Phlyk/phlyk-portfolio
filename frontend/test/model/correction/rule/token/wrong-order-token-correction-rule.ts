import { ArraysHelper } from "src/app/shared/helpers/arrays.helper";
import { ProcessedDiff } from "../../correction-result.model";
import { Correction } from "../../correction.enum";
import { Diff, DiffCompare } from "../../diff/diff.model";
import { AbstractTokenCorrectionRule } from "../abstract-token-correction-rule.model";

export class WrongOrderTokenCorrectionRule extends AbstractTokenCorrectionRule {
    failRuleMessage = "You almost there but got wrong order son";

    protected findRuleViolations(diffedResult: Diff[][]) {
        const [wrongOrderTokenDiffs, differentDiffs] = ArraysHelper.partition(diffedResult, tokenDiffs =>
            [DiffCompare.WRONG_ORDER_ANSWER, DiffCompare.WRONG_ORDER_INPUT].includes(tokenDiffs[0][0])
        );
        this.unmatchedDiffs.push(...differentDiffs);

        const wrongOrderDiffs = wrongOrderTokenDiffs.map((tokenDiffs: Diff[]) => tokenDiffs[0]);
        const wrongOrderPairs: { [x: string]: Diff } = {};
        wrongOrderDiffs.forEach((diff: Diff) => {
            const pairedToken = wrongOrderPairs[diff[1]];
            if (!pairedToken) {
                wrongOrderPairs[diff[1]] = diff;
            } else {
                const processedWrongOrderPair = this.processWrongOrderDiffs(pairedToken, diff);
                this.ruleViolations.push([processedWrongOrderPair]);
            }
        });
    }

    private processWrongOrderDiffs(firstToken: Diff, secondToken: Diff): ProcessedDiff {
        const wrongOrderAnswerToken = firstToken[0] == DiffCompare.WRONG_ORDER_ANSWER ? firstToken : secondToken;
        const wrongOrderInputToken = secondToken[0] == DiffCompare.WRONG_ORDER_INPUT ? secondToken : firstToken;
        return {
            correctionType: Correction.WRONG_ORDER,
            displayAnswer: wrongOrderAnswerToken[1],
            inputIndex: wrongOrderInputToken[2],
            tokenIndex: wrongOrderAnswerToken[2],
            message: this.failRuleMessage,
        };
    }
}
