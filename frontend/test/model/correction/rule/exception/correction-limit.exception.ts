export class CorrectionLimitException extends Error {
    constructor(message: string) {
        super(message);
        Object.setPrototypeOf(this, CorrectionLimitException.prototype);
        Error.captureStackTrace(this, this.constructor);
    }
}

