import { Diff } from "../diff/diff.model";
import { CorrectionRuleResult } from "./correction-rule-result.model";

export interface CorrectionRule {
    readonly failRuleMessage: string;
    ruleScore: number;

    correct(diffedResult: Diff[][]): CorrectionRuleResult;
}
