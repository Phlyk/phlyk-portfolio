import { ProcessedDiff } from "src/app/modules/learning/model/correction/correction-result.model";
import { CorrectionResults } from "src/app/modules/learning/model/correction/correction-results.model";
import { Correction } from "src/app/modules/learning/model/correction/correction.enum";
import { CorrectionRuleResult } from "src/app/modules/learning/model/correction/rule/correction-rule-result.model";
import { makeDummyCorrection } from "./correction-spec-helper";

describe("CorrectionResults (isolated test)", () => {
    let sut: CorrectionResults;

    beforeEach(() => {
        sut = new CorrectionResults(10);
    });

    test("should have correct corrections when the exercise has been correctly answered", () => {
        const bunchOCorrectCorrections: ProcessedDiff[][] = [
            [
                {charIndex: 0, tokenIndex: 0, correctionType: Correction.EQUAL}, 
                {charIndex: 2, tokenIndex: 0, correctionType: Correction.EQUAL}, 
            ],
            [
                {charIndex: 0, tokenIndex: 1, correctionType: Correction.EQUAL}, 
            ]
        ];
        sut.correctAnswer(bunchOCorrectCorrections);

        expect(sut.answerCorrect).toBeTruthy();
        expect(sut.unprocessedCorrections).toEqual([]);
        sut.correctionMap.forEach((corrections: ProcessedDiff[], tokenIndex: number) => {
            expect(corrections).toEqual(bunchOCorrectCorrections[tokenIndex]);
        });
    });

    test("should have incorrect corrections when the exercise has been answered beyond correction (fail)", () => {
        const bunchOFailCorrections: ProcessedDiff[][] = [
            [
                {charIndex: 0, tokenIndex: 0, correctionType: Correction.UNMATCHED}, 
                {charIndex: 2, tokenIndex: 0, correctionType: Correction.UNMATCHED}, 
            ],
            [
                {charIndex: 0, tokenIndex: 1, correctionType: Correction.UNMATCHED}, 
            ]
        ];
        sut.hardFail(bunchOFailCorrections);

        expect(sut.answerCorrect).toBeFalsy();
        expect(sut.unprocessedCorrections).toEqual([]);
        sut.correctionMap.forEach((corrections: ProcessedDiff[], tokenIndex: number) => {
            expect(corrections).toEqual(bunchOFailCorrections[tokenIndex]);
        });
    });

    test("should add an any token diff to an entry with key of token index ", () => {
        const diffTokenIndex = 2;
        const testEqualDiff: ProcessedDiff = {
            correctionType: Correction.EQUAL,
            tokenIndex: diffTokenIndex,
            displayAnswer: "flerpies",
        };
        sut.addTokenCorrection(testEqualDiff);
        expect(sut.correctionMap.get(diffTokenIndex)).toEqual([testEqualDiff]);
    });

    test.each([
        [null, 0],
        [[{ charIndex: 0 }], 1],
        [[{ charIndex: 6 }], 0],
        [[{ charIndex: 2 }], 1],
        [[{ charIndex: 0 }, { charIndex: 6 }], 1],
        [[{ charIndex: 0 }, { charIndex: 1 }, { charIndex: 6 }], 2],
        [[{ charIndex: 4 }, { charIndex: 6 }], 0],
        [[{ charIndex: 0 }, { charIndex: 6 }], 1],
    ])(
        "should, given already existing corrections [%o], add char diffs to index %d",
        (existingCharDiffs: ProcessedDiff[], expectedDiffIndex) => {
            const testTokenIndex = 1;
            const testEqualDiff: ProcessedDiff = { correctionType: Correction.EQUAL, charIndex: 2, displayAnswer: "flap" };

            existingCharDiffs && sut.correctionMap.set(testTokenIndex, existingCharDiffs);

            sut.addCharCorrection(testEqualDiff, testTokenIndex);

            const correctionsForIndex = sut.correctionMap.get(testTokenIndex);

            expect(correctionsForIndex.indexOf(testEqualDiff)).toEqual(expectedDiffIndex);
        }
    );

    test("should add all the matched diffs by the rule to the ongoing correction", () => {
        const charDiffsTokenIndex = 3;
        const testProcessedTokenDiff1: ProcessedDiff = { message: "schmerps", tokenIndex: 1, input: "flerps" };
        const testProcessedCharDiff1: ProcessedDiff = {
            message: "schmeeps",
            tokenIndex: charDiffsTokenIndex,
            charIndex: 0,
            input: "part1",
        };
        const testProcessedCharDiff2: ProcessedDiff = {
            message: "schmops",
            tokenIndex: charDiffsTokenIndex,
            charIndex: 5,
            input: "part2",
        };
        const dummyProcessorResult: CorrectionRuleResult = {
            matchedDiffs: [[testProcessedTokenDiff1], [testProcessedCharDiff1, testProcessedCharDiff2]],
            penaltyScore: 1,
            unprocessedDiffs: [],
        };

        sut.applyCorrection(dummyProcessorResult);

        const correctionsAtTokenIndex = sut.correctionMap.get(testProcessedTokenDiff1.tokenIndex);
        expect(correctionsAtTokenIndex).toContain(testProcessedTokenDiff1);

        const correctionsAtCharDiffTokenIndex1 = sut.correctionMap.get(charDiffsTokenIndex);
        const correctionsAtCharDiffTokenIndex2 = sut.correctionMap.get(charDiffsTokenIndex);
        expect(correctionsAtCharDiffTokenIndex1).toBe(correctionsAtCharDiffTokenIndex2);

        expect(correctionsAtCharDiffTokenIndex1).toContain(testProcessedCharDiff1);
        expect(correctionsAtCharDiffTokenIndex1).toContain(testProcessedCharDiff2);
    });

    test("should decrement the exercises score as per the penalty of the processor result", () => {
        const dummyProcessorResult: CorrectionRuleResult = {
            matchedDiffs: [],
            penaltyScore: 99,
            unprocessedDiffs: [],
        };

        sut.correctedScore = 100;

        sut.applyCorrection(dummyProcessorResult);

        expect(sut.correctedScore).toEqual(1);
    });

    test("should provide processed corrections sorted by token index and char index", () => {
        const testToken1CharCorrection1: ProcessedDiff = makeDummyCorrection(1, 0);
        const testToken1CharCorrection2: ProcessedDiff = makeDummyCorrection(1, 5);

        const testToken2CharCorrection1: ProcessedDiff = makeDummyCorrection(2, 0);
        const testToken2CharCorrection2: ProcessedDiff = makeDummyCorrection(2, 0);
        const testToken2CharCorrection3: ProcessedDiff = makeDummyCorrection(2, 4);

        const testToken3CharCorrection1: ProcessedDiff = makeDummyCorrection(3, 0);
        const testToken3CharCorrection2: ProcessedDiff = makeDummyCorrection(3, 3);
        const testToken3CharCorrection3: ProcessedDiff = makeDummyCorrection(3, 5);

        sut.correctionMap = new Map([
            [3, [testToken3CharCorrection3, testToken3CharCorrection2, testToken3CharCorrection1]],
            [1, [testToken1CharCorrection2, testToken1CharCorrection1]],
            [2, [testToken2CharCorrection1, testToken2CharCorrection3, testToken2CharCorrection2]],
        ]);

        const expectedSortedCorrections: ProcessedDiff[][] = [
            [testToken1CharCorrection1, testToken1CharCorrection2],
            [testToken2CharCorrection1, testToken2CharCorrection2, testToken2CharCorrection3],
            [testToken3CharCorrection1, testToken3CharCorrection2, testToken3CharCorrection3],
        ];

        expect(sut.getSortedCorrections()).toEqual(expectedSortedCorrections);
    });

    test.each([
        [Correction.EQUAL, Correction.MISSING_CHAR, -1],
        [Correction.MISSING_CHAR, Correction.EQUAL, 1],
    ])("should sort missing char corrections before any others if charIndex is the same", (firstCorrectionType, secondCorrectionType, sortedOrder) => {
        const testCharCorrection1: ProcessedDiff = {
            displayAnswer: "ti",
            input: "ti",
            tokenIndex: 0,
            charIndex: 0,
            correctionType: firstCorrectionType
        }
        const testCharCorrection2: ProcessedDiff = {
            displayAnswer: "bû",
            tokenIndex: 0,
            charIndex: 0,
            correctionType: secondCorrectionType,
        }

        sut.correctionMap = new Map([[0, [testCharCorrection1, testCharCorrection2]]]);
        const expectedOrder = sortedOrder < 0 
            ? [testCharCorrection2, testCharCorrection1]
            : [testCharCorrection1, testCharCorrection2];
        const expectedSortedCorrections: ProcessedDiff[][] = [expectedOrder];

        expect(sut.getSortedCorrections()).toEqual(expectedSortedCorrections);
    });
    test("", () => {
        const testCharCorrection1: ProcessedDiff = {
            displayAnswer: "ti",
            input: "ti",
            tokenIndex: 0,
            charIndex: 0,
            correctionType: Correction.EQUAL
        }
        const testCharCorrection2: ProcessedDiff = {
            displayAnswer: "bû",
            tokenIndex: 0,
            charIndex: 0,
            correctionType: Correction.MISSING_CHAR,
        }

        sut.correctionMap = new Map([[0, [testCharCorrection1, testCharCorrection2]]]);

        const expectedSortedCorrections: ProcessedDiff[][] = [
            [testCharCorrection2, testCharCorrection1]
        ];

        expect(sut.getSortedCorrections()).toEqual(expectedSortedCorrections);
    });
});
