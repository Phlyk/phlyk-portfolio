import { ProcessedDiff } from "src/app/modules/learning/model/correction/correction-result.model";

export function makeDummyCorrection(tokenIndex: number, charIndex: number): ProcessedDiff {
    const randomAnswer = Math.random().toString(36).substr(2, 5);
    const randomInput = Math.random().toString(36).substr(2, 5);
    return {
        displayAnswer: randomAnswer,
        input: randomInput,
        tokenIndex,
        charIndex,
    };
}
