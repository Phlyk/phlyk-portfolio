import 'jest-preset-angular';
import { TestUtils } from './utils/test-utils';

Object.defineProperty(window, 'innerWidth', { value: TestUtils.defaultWindowInnerWidth });
Object.defineProperty(window, 'CSS', { value: null });
Object.defineProperty(document, 'doctype', {
    value: '<!DOCTYPE html>'
});
Object.defineProperty(window, 'getComputedStyle', {
    value: () => {
        // Mocking the CSSStyleDeclaration object
        return {
            display: 'none',
            appearance: ['-webkit-appearance'],
            getPropertyValue: (pxCssClasses: string) => {
                switch (pxCssClasses) {
                    case "--search-section-spacing":
                    case "--lexeme-card-width":
                        return "55px"
                    default:
                        return "TESTMOCKNOVALUE";
                }
            }
        };
    }
});
/**
 * ISSUE: https://github.com/angular/material2/issues/7101
 * Workaround for JSDOM missing transform property
 */
Object.defineProperty(document.body.style, 'transform', {
    value: () => {
        return {
            enumerable: true,
            configurable: true,
        };
    },
});

Error.stackTraceLimit = 4
