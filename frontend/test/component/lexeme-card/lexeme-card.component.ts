import { LanguageDTO, LexemeWrapper, UserLexemeDTO } from '@COMPANYNAME/frontend-services';
import { Component, ElementRef, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { getHardcodedLanguage } from '../../../../LB_ss/src/app/shared/constants/data.constants';
import { ErrorsHelper } from '../../../../LB_ss/src/app/shared/helpers/errors.helper';
import { MessageService } from '../../../../LB_ss/src/app/shared/services/message.service';
import { WordbookService } from '../../../../LB_ss/src/app/shared/services/wordbook.service';

@Component({
    selector: 'app-lexeme-card',
    templateUrl: './lexeme-card.component.html',
    styleUrls: ['./lexeme-card.component.scss']
})
export class LexemeCardComponent implements OnInit, OnDestroy {
    @Input() lexemeWrapper: LexemeWrapper;
    @Input() loading: boolean;
    @Input() representation: string;
    @Input() definitionsRepresentation: string;
    @Input() selected: boolean = false;
    @Output() emitHeartClick: EventEmitter<boolean> = new EventEmitter();

    english: LanguageDTO;
    userLexeme: UserLexemeDTO;
    userLexemeSubscription: Subscription;
    
    inWordbook: boolean;
    showHeartButton: boolean;
    private clickedOnCurrentCardHeart = false;

    constructor(
        private router: Router,
        private wordbookService: WordbookService,
        private messageService: MessageService,
        private elementRef: ElementRef
    ) {
        this.english = getHardcodedLanguage("en");
    }

    ngOnInit() {
        if (!this.representation) {
            this.representation = this.lexemeWrapper.defaultRepresentation;
        }
        if (!this.definitionsRepresentation) {
            this.definitionsRepresentation = this.lexemeWrapper.definitionsRepresentation(this.english);
        }
        this.userLexemeSubscription = this.wordbookService.getUserLexeme(this.lexemeWrapper.lexeme.id).subscribe(userLexeme => {
            this.inWordbook = userLexeme ? userLexeme.savedDate != null : false;
            this.userLexeme = userLexeme;
        });
    }

    heartClick() {
        this.clickedOnCurrentCardHeart = true;
        const wordbookOperation$: Observable<UserLexemeDTO> = this.inWordbook ?
            this.wordbookService.removeLexemeFromWordbook(this.userLexeme) :
            this.wordbookService.addLexemeToWordbook(this.lexemeWrapper.getLexemeId());
        wordbookOperation$.subscribe(
            (mutationResponse: UserLexemeDTO) => this.handleWordbookLexemeOperation(mutationResponse),
            (error) => this.handleError(error)
        );
    }

    @HostListener('document:click', ['$event.target'])
    toggleSelectIfClickedOnComponent(eventTarget: HTMLElement) {
        if (this.clickedOnCurrentCardHeart === false) {
            if (this.elementRef.nativeElement.contains(eventTarget)) {
                this.toggleSelected();
            } else {
                this.unselect();
            }
        }
        this.clickedOnCurrentCardHeart = false;
    }

    @HostListener('mouseenter')
    onMouseEnterInComponent() {
        this.showHeartButton = true;
    }

    @HostListener('mouseleave')
    onMouseLeaveComponent() {
        this.showHeartButton = this.selected;
    }

    toggleSelected() {
        this.selected = !this.selected;
    }

    unselect() {
        this.selected = false;
    }

    private handleWordbookLexemeOperation(mutationResponse: UserLexemeDTO) {
        const operation = mutationResponse.savedDate ? "added" : "removed";
        const message = `Lexeme ${this.lexemeWrapper.defaultRepresentation} was ${operation} from your wordbook`;
        const snackBarRef = this.messageService.openSnackBar(message, true, 'GO TO WORDBOOK', false);
        snackBarRef.onAction().subscribe(() => {
            snackBarRef.dismiss();
            if (mutationResponse.savedDate) {
                // todo - use router params not local storage
                localStorage.setItem('lexemeIdFromLandingPage', this.lexemeWrapper.lexeme.id);
            }
            this.router.navigate(['wordbook']);
        });
        this.emitHeartClick.emit(this.inWordbook);
        this.inWordbook = !!mutationResponse.savedDate;
    }

    private handleError(error) {
        console.error(error);
        let message = "Something went wrong";
        if (error.hasOwnProperty('graphQLErrors')) {
            message = ErrorsHelper.extractGraphQLErrors(error.graphQLErrors);
        }
        this.messageService.openSnackBar(message, true, 'DISMISS', false);
    }

    ngOnDestroy(): void {
        this.userLexemeSubscription.unsubscribe();
    }
}
