import { LexemeDTO, LexemeWrapper } from '@COMPANYNAME/frontend-services';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { MessageService } from 'src/app/shared/services/message.service';
import { WordbookService } from 'src/app/shared/services/wordbook.service';
import { makeDummyLexemeDTO } from 'src/test/modules/learning/test-learning.factory';
import { AbstractMockObservableService } from 'src/test/utils/abstract-mock-observable.service';
import { LexemeCardComponent } from '../../../app/shared/components/lexeme-card/lexeme-card.component';


describe('LexemeCardComponent (shallow test)', () => {
    let component: LexemeCardComponent;
    let fixture: ComponentFixture<LexemeCardComponent>;

    let mockWordbookService: MockWordbookService;

    beforeEach(waitForAsync(() => {
        mockWordbookService = new MockWordbookService();
        TestBed.configureTestingModule({
            imports: [MaterialModule, RouterTestingModule],
            declarations: [LexemeCardComponent],
            providers: [
                { provide: WordbookService, useValue: mockWordbookService },
                { provide: MessageService, useClass: MockMessageService }
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LexemeCardComponent);
        component = fixture.componentInstance;
        component.lexemeWrapper = new LexemeWrapper(makeDummyLexemeDTO() as LexemeDTO);
    });

    it('should toggle card selected state', () => {
        fixture.detectChanges();
        expect(component.selected).toBeFalsy();
        component.toggleSelected();
        expect(component.selected).toBeTruthy();
    });

    it('should update the lexeme cards in wordbook status when change happens', () => {
        component.inWordbook = false;

        mockWordbookService.content = { savedDate: "randomNonNullDate92340234" };
        fixture.detectChanges();
        expect(component.inWordbook).toBeTruthy();
    });
});

class MockWordbookService extends AbstractMockObservableService {
    getUserLexeme(ID: string) {
        return this;
    }
}

class MockMessageService extends AbstractMockObservableService {

}
