import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AuthenticationService, UserDTO } from '@COMPANYNAME/frontend-services';
import { Subscription } from 'rxjs';
import { AuthModalClosePayload, AuthModalComponent, AuthModalConfigData } from '../../../../LB_ss/src/app/shared/modals/auth-modal/auth-modal';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
    loggedIn: boolean;
    user: UserDTO;
    userAuthenticateSubscription: Subscription;

    constructor(
        private dialog: MatDialog,
        private authenticationService: AuthenticationService,
        private router: Router
    ) { }

    ngOnInit() {
        this.loggedIn = this.authenticationService.isAuthenticated();
        this.userAuthenticateSubscription = this.authenticationService.userAuthenticateAction.subscribe((user: UserDTO) => {
            this.loggedIn = user ? true: false;
            this.user = user;
        });
    }

    ngOnDestroy() {
        this.userAuthenticateSubscription.unsubscribe();
    }

    openAuthModal() {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {} as AuthModalConfigData;
        dialogConfig.panelClass = "app-auth-dialog-panel";
        const dialogRef = this.dialog.open(AuthModalComponent, dialogConfig);
        dialogRef.afterClosed().subscribe((payload: AuthModalClosePayload) => {
            if (payload === "LOGGED_IN") {
                localStorage.setItem("freshLogIn", "true");
                this.router.navigate(['home']);
            } else if (payload === "SIGNED_UP") {
                localStorage.setItem('freshSignUp', "true");
                this.router.navigate(['home']);
            }
        });
        return false;
    }

    goHome() {
        this.router.navigate(['home']);
    }

    logOut() {
        this.authenticationService.logout();
        console.log("this.router.url:", this.router.url);
        this.router.navigate([decodeURI(this.router.url)]);
    }

}
