import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthenticationService } from '@COMPANYNAME/frontend-services';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { AbstractMockObservableService } from 'src/test/utils/abstract-mock-observable.service';
import { HeaderComponent } from './header.component';


describe('HeaderComponent (shallow test)', () => {
    let component: HeaderComponent;
    let fixture: ComponentFixture<HeaderComponent>;

    let mockAuthenticationService: MockAuthenticationService;

    beforeEach(waitForAsync(() => {
        mockAuthenticationService = new MockAuthenticationService();
        TestBed.configureTestingModule({
            declarations: [HeaderComponent],
            imports: [MaterialModule, RouterTestingModule],
            providers: [{ provide: AuthenticationService, useValue: mockAuthenticationService }]
        });
    }));

    it('should create', () => {
        fixture = TestBed.createComponent(HeaderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        expect(component).toBeTruthy();
    });

    it('should update the logged in status of the user when authenticate action happens', () => {
        mockAuthenticationService.content = true;
        fixture = TestBed.createComponent(HeaderComponent);

        component = fixture.componentInstance;
        expect(component.loggedIn).toBeFalsy();
        fixture.detectChanges();
        expect(component.loggedIn).toBeTruthy();
    });
});

class MockAuthenticationService extends AbstractMockObservableService {
    userAuthenticateAction = this;

    isAuthenticated() { return false; }

    getCurrentUserData() {
        return {
            username: "probablyCouldRefactorTheseTingsOut"
        }
    }
}

