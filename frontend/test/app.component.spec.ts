import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { NO_ERRORS_SCHEMA } from '@angular/compiler';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatSidenavHarness } from '@angular/material/sidenav/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ApolloTestingModule } from 'apollo-angular/testing';
import { LandingComponent } from 'src/app/modules/landing/components/landing.component';
import { LanguageCoursesComponent } from 'src/app/modules/learning/pages/language-courses/language-courses.component';
import { SidebarService } from 'src/app/modules/sidebar/services/sidebar.service';
import { WINDOW_WIDTH_FOR_SIDENAV_OPEN } from 'src/app/shared/constants/display.constants';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { PipeModule } from "src/app/shared/pipes/pipe.module";
import { AppComponent } from '../app/app.component';
import { TestUtils } from './utils/test-utils';


describe('AppComponent (shallow test)', () => {
    let component: AppComponent;
    let fixture: ComponentFixture<AppComponent>;
    let harnessLoader: HarnessLoader;
    let router: Router;
    let dummySidebarService: SidebarService;

    beforeEach(waitForAsync(() => {
        dummySidebarService = new SidebarService();
        LanguageCoursesComponent.prototype.ngOnInit = () => { };
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule.withRoutes([{
                    path: '', component: LandingComponent,
                },
                {
                    path: 'courses', component: LanguageCoursesComponent,
                }]),
                MaterialModule,
                NoopAnimationsModule,
                ApolloTestingModule,
                PipeModule
            ],
            declarations: [AppComponent, LandingComponent, LanguageCoursesComponent],
            schemas: [NO_ERRORS_SCHEMA],
            providers: [{ provide: SidebarService, useValue: dummySidebarService }]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AppComponent);
        component = fixture.componentInstance;
        router = TestBed.inject(Router);
        
        harnessLoader = TestbedHarnessEnvironment.loader(fixture);
        fixture.detectChanges();
    });

    afterEach(() => {
        TestUtils.resetWindowInnerWidth();
    });
    
    test("should calculate to display sidebar on initial load", () => {
        expect(component.onLandingPage).toBeDefined();
    });

    test("should not display side bar for landing page", () => {
        expect(router.url).toBe("/");
        expect(component.onLandingPage).toBeTruthy();
        expect(component.sidenavOpened).toBeFalsy();
    });

    test.each([
        [WINDOW_WIDTH_FOR_SIDENAV_OPEN + 1000, true],
        [WINDOW_WIDTH_FOR_SIDENAV_OPEN - 100, false],
    ])("should display side bar according to window width", async (windowWidth, expectedDisplay) => {
        TestUtils.setWindowInnerWidth(windowWidth);

        await fixture.ngZone.run(() => router.navigate(["courses"]));

        const sidenav: MatSidenavHarness = await harnessLoader.getHarness<MatSidenavHarness>(MatSidenavHarness);
        const sidenavOpenState: boolean = await sidenav.isOpen();

        expect(component.onLandingPage).toBeFalsy();
        expect(sidenavOpenState).toBe(expectedDisplay);
    });

    test("should recalculate the visibility of the sidenav upon navigation change", async () => {
        expect(router.url).toBe("/");

        await fixture.ngZone.run(() => router.navigate(["courses"]));

        const sidenav: MatSidenavHarness = await harnessLoader.getHarness<MatSidenavHarness>(MatSidenavHarness);
        let sidenavOpenState: boolean = await sidenav.isOpen();
        expect(router.url).toBe("/courses");
        expect(component.onLandingPage).toBeFalsy();
        expect(sidenavOpenState).toBeTruthy();

        await fixture.ngZone.run(() => router.navigate([""]));

        sidenavOpenState = await sidenav.isOpen();
        expect(router.url).toBe("/");
        expect(component.onLandingPage).toBeTruthy();
        expect(sidenavOpenState).toBeFalsy();
    });

    test("should react to screen resize events", () => {
        const sidenavCalculatorSpy = jest.spyOn(component as any, 'setSidenavVisibleState');

        const resizeEvent = new Event('resize');
        window.dispatchEvent(resizeEvent);

        expect(sidenavCalculatorSpy).toHaveBeenCalled();
    });

    test("should react to the when the sidebar service reveal stream", async () => {
        await fixture.ngZone.run(() => router.navigate(["courses"]));

        expect(component.onLandingPage).toBeFalsy();
        const sidenav: MatSidenavHarness = await harnessLoader.getHarness<MatSidenavHarness>(MatSidenavHarness);
        let sidenavOpenState: boolean = await sidenav.isOpen();

        expect(sidenavOpenState).toBeTruthy();

        dummySidebarService.revealSidenav(false);

        sidenavOpenState = await sidenav.isOpen();
        expect(sidenavOpenState).toBeFalsy();

        dummySidebarService.revealSidenav(true);

        sidenavOpenState = await sidenav.isOpen();
        expect(sidenavOpenState).toBeTruthy();
    });

    test("Should force the opening of the sidebar even if screen is too small", async () => {
        TestUtils.setWindowInnerWidth(WINDOW_WIDTH_FOR_SIDENAV_OPEN - 100);

        await fixture.ngZone.run(() => router.navigate(["courses"]));

        expect(component.onLandingPage).toBeFalsy();
        const sidenav: MatSidenavHarness = await harnessLoader.getHarness<MatSidenavHarness>(MatSidenavHarness);
        let sidenavOpenState: boolean = await sidenav.isOpen();

        expect(sidenavOpenState).toBeFalsy();

        dummySidebarService.revealSidenav(true);

        sidenavOpenState = await sidenav.isOpen();
        expect(sidenavOpenState).toBeTruthy();
    });
});
