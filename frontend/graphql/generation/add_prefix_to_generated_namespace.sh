#!/bin/bash

if [ $# -ne 1 ]; then
    echo ">> Required prefix parameter!"
    exit 1
fi

sedPath=$(which gsed)
if [ $? -ne 0 ]; then
    sedPath=$(which sed)
fi

projectDir=$(cd $(dirname 0); pwd -P)
srcDir=${projectDir}/projects/frontend-services/src/lib/services
namespacePrefix=$1

findGeneratedServiceFileCommand="find ${srcDir} -type f -name \"*.generated.ts\""
prefixNameSpaceCommand="\"${sedPath}\" -i -e \"s/export namespace \(.*\)/export namespace ${namespacePrefix}\1/g\""

echo srcDir:$srcDir
echo findGeneratedServiceFileCommand:$findGeneratedServiceFileCommand
echo prefixNameSpaceCommand:$prefixNameSpaceCommand

eval "${findGeneratedServiceFileCommand} | xargs ${prefixNameSpaceCommand}"

echo "Added '${namespacePrefix}' to all generated namespaces"
