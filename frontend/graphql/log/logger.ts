import { Inject, Injectable, InjectionToken } from '@angular/core';
import moment from 'moment';
import { Subject } from 'rxjs';
import { debounceTime, filter } from 'rxjs/operators';
import { LBEnvironment } from '../lb-environment.enum';
import { LOGGER_LB_ENVIRONMENT_INJECTION_TOKEN } from '../lb-environment.injection-token';
import { ForwardLogToBackendService } from './forward-logs-to-backend.service';
import { LogFields } from './log-fields';

export type LogType = 'ERROR' | 'WARN' | 'INFO' | 'DEBUG';

interface LogEntry {
    timestamp: any;
    type: LogType;
    message: string;
    data: LogFields;
}

enum LoggerEvents {
    Flush = 1
}

export interface LogMessageFields {
    timestamp: string,
    level: LogType,
    message: string,
    userId: string,
    url: string,
    stackTrace?: string,
    requestPath?: string,
    elapsedMilliseconds?: number,
    appState?: string, 
}

export const LOG_FLUSH_TIMER_INJECTION_TOKEN = new InjectionToken<number>('Time in ms which the logger flushes the bugger')

@Injectable({
    providedIn: "root"
})
export class Logger {
    private buffer: LogEntry[] = [];
    private flush = new Subject<LoggerEvents>();

    constructor(
        private forwardLogToBackendService: ForwardLogToBackendService, 
        @Inject(LOGGER_LB_ENVIRONMENT_INJECTION_TOKEN) private environment: LBEnvironment, 
        @Inject(LOG_FLUSH_TIMER_INJECTION_TOKEN) private logFlushTimer: number = 3000
    ) {
        this.flush
            .pipe(debounceTime(this.logFlushTimer), filter((event) => event === LoggerEvents.Flush))
            .subscribe(() => this.flushBuffer());
    }

    public log(type: LogType, message: string, data: LogFields) {
        this.buffer.push({
            timestamp: moment().toISOString(),
            type,
            message,
            data
        });
        this.flush.next(LoggerEvents.Flush);
    }

    private flushBuffer() {
        const data = this.buffer.splice(0);

        if (data.length === 0) {
            return;
        }

        data.map((entry) => this.buildBodyChunk(entry))
            .map((entryChunk) => this.sendLogsToEnvironment(entryChunk))
    }

    private sendLogsToEnvironment(entryChunk) {
        switch (this.environment) {
            case LBEnvironment.LOCAL:
            case LBEnvironment.TEST:
                this.sendLogsToConsole(entryChunk);
                break;
            case LBEnvironment.DEVELOPMENT:
            case LBEnvironment.STAGING:
            case LBEnvironment.PRODUCTION:
                this.sendLogsToRemote(entryChunk);
                break;
            default:
                this.sendLogsToConsole(entryChunk);
                break;
        }
    }

    private sendLogsToConsole(logBody: LogMessageFields) {
        const logMethod = logBody.level.toLowerCase();
        console[logMethod](logBody);
    }

    private sendLogsToRemote(logBody: LogMessageFields) {
        const forwardingErrorText = "There was an error forwarding the log to the backend";
        this.forwardLogToBackendService.forwardLogToBackend(logBody)
            .subscribe(({ data }) => {
                if (!data.forwardLog.success) {
                    console.error(forwardingErrorText, logBody)
                }
            }, (error) => {
                console.error('Forwarding error text', error);
            });
    }

    private buildBodyChunk(entry: LogEntry): LogMessageFields {
        const { timestamp, type, message, data } = entry;
        const body: LogMessageFields = {
            timestamp,
            level: type,
            message: message,
            userId: data.userId,
            url: data.url
        };
        if (data.stackTrace) body.stackTrace = data.stackTrace;
        if (data.elapsedTime) body.elapsedMilliseconds = data.elapsedTime;
        if (data.requestPath) body.requestPath = data.requestPath;

        return body;
    }
}
