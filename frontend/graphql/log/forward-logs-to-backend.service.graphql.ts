import gql from 'graphql-tag';

export const SEND_LOG_TO_BACKEND = gql`
    mutation ForwardLog($payload: LogFieldsInput!) {
        forwardLog(input: {logPayload: $payload}) {
            success
        }
    }
`;
