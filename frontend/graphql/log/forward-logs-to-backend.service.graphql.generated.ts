import * as Types from '../../../generated/graphql-types';

import gql from 'graphql-tag';
import { Injectable } from '@angular/core';
import * as Apollo from 'apollo-angular';
export type ForwardLogMutationVariables = Types.Exact<{
  payload: Types.LogFieldsInput;
}>;


export type ForwardLogMutation = (
  { __typename?: 'Mutation' }
  & { forwardLog?: Types.Maybe<(
    { __typename?: 'ForwardLogPayload' }
    & Pick<Types.ForwardLogPayload, 'success'>
  )> }
);

export namespace GQLForwardLog {
  export type Variables = ForwardLogMutationVariables;
  export type Mutation = ForwardLogMutation;
  export type ForwardLog = ForwardLogMutation['forwardLog'];
}

export const ForwardLogDocument = gql`
    mutation ForwardLog($payload: LogFieldsInput!) {
  forwardLog(input: {logPayload: $payload}) {
    success
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class ForwardLogGQL extends Apollo.Mutation<ForwardLogMutation, ForwardLogMutationVariables> {
    document = ForwardLogDocument;
    
  }