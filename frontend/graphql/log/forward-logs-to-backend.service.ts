import { Injectable } from '@angular/core';
import { FetchResult } from '@apollo/client/core';
import { Observable } from 'rxjs';
import { ForwardLogGQL, GQLForwardLog } from './forward-logs-to-backend.service.graphql.generated';
import { LogMessageFields } from './logger';

@Injectable({providedIn: 'root'})
export class ForwardLogToBackendService {

    constructor(private forwardLogToBackendMutation: ForwardLogGQL) { }

    forwardLogToBackend(logPayload: LogMessageFields): Observable<FetchResult<GQLForwardLog.Mutation>> {
        return this.forwardLogToBackendMutation.mutate({payload: logPayload});
    }
}
