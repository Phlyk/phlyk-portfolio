import { Injectable } from '@angular/core';
import { LogFields } from './log-fields';
import { Logger } from './logger';


@Injectable({
    providedIn: "root"
})
export class LogService {
    private readonly userId = "-1";

    constructor(private logger: Logger) { }

    public logHttpInfo(info: any, elapsedTime: number, requestPath: string) {
        const url = location.href;
        const logFields: LogFields = {
            userId: this.userId,
            requestPath,
            elapsedTime,
            url,
        };

        this.logger.log("INFO", `${info}`, logFields);
    }

    public logWarning(errorMsg: string, stackTraceStr?: string) {
        const url = location.href;

        const logFields: LogFields = {
            userId: this.userId,
            url: url,
            stackTrace: stackTraceStr
        };

        this.logger.log("WARN", errorMsg, logFields);
    }

    public logError(errorMsg: string, stackTraceStr?: string) {
        const url = location.href;

        const logFields: LogFields = {
            userId: this.userId,
            url: url,
            stackTrace: stackTraceStr
        };

        this.logger.log("ERROR", errorMsg, logFields);
    }

    public logInfo(info: any) {
        const url = location.href;

        const logFields: LogFields = {
            userId: this.userId,
            url
        };

        this.logger.log("INFO", info, logFields);
    }
}
