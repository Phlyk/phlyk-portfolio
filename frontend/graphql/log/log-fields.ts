export interface LogFields {
    userId: string;
    stackTrace?: string;
    elapsedTime?: number;
    requestPath?: string;
    url?: string;
}
