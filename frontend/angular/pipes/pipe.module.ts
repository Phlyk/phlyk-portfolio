import { NgModule } from '@angular/core';
import { MergeArrayPipe } from './merge-array.pipe';
import { ShuffleArrayPipe } from './shuffle-array.pipe';
import { WithLoadingSimplePipe } from './with-loading-simple.pipe';
import { WithLoadingPipe } from './with-loading.pipe';

@NgModule({
    declarations: [
        MergeArrayPipe,
        ShuffleArrayPipe,
        WithLoadingPipe,
        WithLoadingSimplePipe,
    ],
    exports: [
        MergeArrayPipe,
        ShuffleArrayPipe,
        WithLoadingPipe,
        WithLoadingSimplePipe,
    ],
    providers: [
    ],
})
export class PipeModule { }
