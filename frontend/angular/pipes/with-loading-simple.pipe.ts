import { Pipe, PipeTransform } from '@angular/core';
import { isObservable, Observable, of } from 'rxjs';
import { catchError, map, startWith } from 'rxjs/operators';
import { Loadable } from './with-loading.pipe';

@Pipe({
    name: 'withLoadingSimple',
})
export class WithLoadingSimplePipe implements PipeTransform {
    transform<T = any>(val: Observable<T>): Observable<Loadable<T>> {
        if (isObservable(val)) {
            return val.pipe(
                map((value: any) => ({ loading: false, value })),
                startWith({ loading: true }),
                catchError(error => {
                    console.error("ERROR in WithLoadingSimplePipe: ", error);
                    return of({ loading: false, error });
                })
            )
        }
        return val;
    }
}
// Generic Pipe issues loosing intelliSense - https://github.com/angular/angular/issues/21224
