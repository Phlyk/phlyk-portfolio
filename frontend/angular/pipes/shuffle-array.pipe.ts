import { Pipe, PipeTransform } from '@angular/core';
import { ArraysHelper } from '../helpers/arrays.helper';

@Pipe({
    name: 'shuffleArray'
})
export class ShuffleArrayPipe implements PipeTransform {

    transform<T>(array: T[]): T[] {
        if (!array) return [];
        return ArraysHelper.shuffle(array);
    }
}
