import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'mergeArray'
})
export class MergeArrayPipe implements PipeTransform {

    transform(array1: any[], array2: any[]): any[] {
        return array1.concat(array2);
    }

}
