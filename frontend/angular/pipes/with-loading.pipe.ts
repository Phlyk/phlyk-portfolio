import { Pipe, PipeTransform } from '@angular/core';
import { isObservable, Observable, of } from 'rxjs';
import { catchError, map, startWith } from 'rxjs/operators';

export type Loadable<T> = {
    loading: boolean;
    value?: T;
    error?: any;
};

export type StreamState = "start" | "resume" | "finish" | "error" | null; 

export type StartStopStream<T> = {
    type: StreamState;
    value?: T;
}

const defaultError = "WithLoadingPipe - something went wrong";

@Pipe({
    name: 'withLoading',
})
export class WithLoadingPipe implements PipeTransform {
    transform<T = any>(
        obs: Observable<StartStopStream<T>>,
        loadingStart: boolean = true
    ): Observable<Loadable<T>> {
        if (isObservable(obs)) {
            return obs.pipe(
                map((value: any) => ({
                    loading: value.type === "start" || value.type === "resume",
                    error: value.type === 'error' ? defaultError : '',
                    value: value.type ? value.value : value
                })),
                startWith({ loading: loadingStart }),
                catchError(error => {
                    console.error("ERROR in WithLoadingPipe: ", error);
                    return of({ loading: false, error });
                })
            );
        }
        return obs;
    }
}
