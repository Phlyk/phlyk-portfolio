import { animate, keyframes, state, style, transition, trigger } from '@angular/animations';
import { CORRECTION_BUTTON_ANIMATION_TIMEOUT } from '../constants/display.constants';
import { FADE_IN_ANIMATION_TIMEOUT, FADE_OUT_ANIMATION_TIMEOUT, styleHidden, styleVisible } from './base.animations';

export enum NextButtonAnimationState {
    HIDDEN = "hidden", // this is not needed?
    VISIBLE = "visible", // unanswered
    WHEN_CORRECT = "whenCorrect",
    WHEN_WRONG = "whenWrong"
}

export const styleOriginalScale = {
    transform: 'scale(1)'
};

export const NextButtonAnimations =
    trigger('nextButtonAnimations', [
        state(NextButtonAnimationState.HIDDEN, style(styleHidden)),
        state(NextButtonAnimationState.VISIBLE, style(styleVisible)),
        state(NextButtonAnimationState.WHEN_CORRECT, style(styleOriginalScale)),
        state(NextButtonAnimationState.WHEN_WRONG, style(styleOriginalScale)),
        transition(`${NextButtonAnimationState.VISIBLE} => ${NextButtonAnimationState.HIDDEN}`, [
            style(styleVisible),
            animate(FADE_OUT_ANIMATION_TIMEOUT, style(styleHidden))
        ]),
        transition(`${NextButtonAnimationState.HIDDEN} => ${NextButtonAnimationState.VISIBLE}`, [
            style(styleHidden),
            animate(FADE_IN_ANIMATION_TIMEOUT, style(styleVisible))
        ]),
        transition(`${NextButtonAnimationState.VISIBLE} => ${NextButtonAnimationState.WHEN_CORRECT}`, [
            animate(`${CORRECTION_BUTTON_ANIMATION_TIMEOUT}ms ease-in-out`, keyframes([
                style({ transform: "scale(1.2)", transformOrigin: "center center", animationTimingFunction: "ease-out", offset: 0.14 }),
                style({ transform: "scale(0.98)", animationTimingFunction: "ease-in", offset: 0.21 }),
                style({ transform: "scale(1.1)", animationTimingFunction: "ease-out", offset: 0.37 }),
                style({ transform: "scale(0.9)", animationTimingFunction: "ease-in", offset: 0.61 }),
                style({ transform: "scale(1)", animationTimingFunction: "ease-out", offset: 0.9 })
            ]))
        ]),
        transition(`${NextButtonAnimationState.VISIBLE} => ${NextButtonAnimationState.WHEN_WRONG}`, [
            animate(`${CORRECTION_BUTTON_ANIMATION_TIMEOUT}ms ease-in`, keyframes([
                style({ transform: 'translate3d(-1px, 0, 0)', offset: 0.1 }),
                style({ transform: 'translate3d(2px, 0, 0)', offset: 0.2 }),
                style({ transform: 'translate3d(-4px, 0, 0)', offset: 0.3 }),
                style({ transform: 'translate3d(4px, 0, 0)', offset: 0.4 }),
                style({ transform: 'translate3d(-4px, 0, 0)', offset: 0.5 }),
                style({ transform: 'translate3d(4px, 0, 0)', offset: 0.6 }),
                style({ transform: 'translate3d(-4px, 0, 0)', offset: 0.7 }),
                style({ transform: 'translate3d(2px, 0, 0)', offset: 0.8 }),
                style({ transform: 'translate3d(-1px, 0, 0)', offset: 0.9 })
            ]))
        ])
    ]);
    
export const NextExerciseButton =
    trigger("nextExerciseButton", [
        state("inView", style({
            position: "relative", transform: "scale(1) translateY(0) rotate(0deg)", opacity: 1
        })),
        state("outView", style({
            position: "relative", transform: "scale(0) translateY(100%) rotate(180deg)", opacity: 0
        })),
        transition(":enter", animate("1s ease")),
        transition(":leave", animate("1s ease")),
        transition("inView <=> outView", [
            animate("2s ease")
        ])
    ]);
