import { animate, animation, query, stagger, style, transition, trigger, useAnimation } from '@angular/animations';
import { SLIDE_IN_OUT_TIME, WATERFALL_STAGGER_TIME } from '../../../LB_ss/src/app/shared/constants/display.constants';

export const SlideInOutAnimation = animation([
    style({ opacity: "{{ from }}", transform: "translateY({{ to }}00%" }),
    animate("{{ duration }}ms ease-in", style({ opacity: "{{ to }}", transform: "translateY({{ from }}00%)" }))
], { params: { from: 0, to: 1, duration: SLIDE_IN_OUT_TIME } });

export const OutOfViewStyle = style({ opacity: 0, transform: "translateY(100%)" });

export const FeedTransition =
    trigger("feedTransition", [
        transition(":enter", [
            query(".waterfall-in-out", [
                OutOfViewStyle,
                stagger(WATERFALL_STAGGER_TIME, useAnimation(SlideInOutAnimation, {
                    params: {
                        from: 0,
                        to: 1
                    }
                }))
            ])
        ]),
        transition(":leave", [
            query(".waterfall-in-out", [
                stagger(WATERFALL_STAGGER_TIME, useAnimation(SlideInOutAnimation, {
                    params: {
                        from: 1,
                        to: 0
                    }
                }))
            ])
        ]),
        transition("* => inView", [
            query(".waterfall-in-out", [
                OutOfViewStyle,
                stagger(-WATERFALL_STAGGER_TIME, [
                    useAnimation(SlideInOutAnimation, {
                        params: {
                            from: 0,
                            to: 1,
                        }
                    })
                ])
            ])
        ]),
        transition("* => outView", [
            query(".waterfall-in-out", [
                stagger(WATERFALL_STAGGER_TIME, [
                    useAnimation(SlideInOutAnimation, {
                        params: {
                            from: 1,
                            to: 0,
                        }
                    })
                ])
            ]),
        ]),
    ]);
