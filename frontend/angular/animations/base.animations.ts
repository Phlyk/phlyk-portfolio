import { animate, animation, style, transition, trigger, useAnimation } from '@angular/animations';
import { ANIMATION_PAUSE, ANIMATION_TIMEOUT } from '../../../LB_ss/src/app/shared/constants/display.constants';

export const FADE_IN_ANIMATION_TIMEOUT = ANIMATION_TIMEOUT;
export const FADE_OUT_ANIMATION_TIMEOUT = ANIMATION_TIMEOUT;
export const PAUSE_BETWEEN_FADE_IN_OUT = ANIMATION_PAUSE;


export type TransitionStage = "inView" | "outView";

export const SpawnInOutAnimation = animation([
    style({ opacity: "{{ from }}" }),
    animate("{{ duration }}ms ease", style({ opacity: "{{ to }}" }))
], { params: { from: 0, to: 1, duration: "1s" } });

// move this into somewhere seperate as no longer base
// also is the same as the fade in out
export const SpawnAnimation =
    trigger('spawnAnimation', [
        transition(':enter', [
            useAnimation(SpawnInOutAnimation, {
                params: {
                    from: 0,
                    to: 1,
                    duration: "400ms"
                }
            }),
        ]),
        transition(':leave', [
            useAnimation(SpawnInOutAnimation, {
                params: {
                    from: 1,
                    to: 0,
                    duration: "400ms"
                }
            })
        ])
    ]);

export const FadeInAnimation =
    trigger('fadeInAnimation', [
        transition(':enter', [
            useAnimation(SpawnInOutAnimation, {
                params: {
                    from: 0,
                    to: 1,
                    duration: FADE_IN_ANIMATION_TIMEOUT
                }
            })
        ])
    ]);

export const FadeOutAnimation =
    trigger('fadeOutAnimation', [
        transition(':leave', [
            useAnimation(SpawnInOutAnimation, {
                params: {
                    from: 1,
                    to: 0,
                    duration: FADE_OUT_ANIMATION_TIMEOUT
                }
            })
        ])
    ]);

export const SubSpawnAnimation =
    trigger('subSpawnAnimation', [
        transition(':enter', [
            style({ opacity: 0, 'margin-left': '-16px' }),
            animate('150ms {{delay}}ms', style({ opacity: 1, 'margin-left': 'unset' })),
            animate('150ms', style({ opacity: 1, 'margin-left': 'unset' })),
        ], { params: { delay: 0 } }),
        transition(':leave', [
            animate('150ms', style({ opacity: 0, 'margin-left': '-16px' })),
        ], { params: { delay: 0 } })
    ]);

export const styleHidden = {
    top: "160px", opacity: 0 // was 16px just playing
};

export const styleVisible = {
    top: "0px", opacity: 1
};
