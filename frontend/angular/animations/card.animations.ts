import { animate, state, style, transition, trigger } from "@angular/animations";
import { styleHidden, styleVisible } from "./base.animations";

export const NextExerciseCardTransition = trigger("nextExerciseCard", [
    state(
        "outView",
        style({
            transform: "translateY(-100%)",
            opacity: 0,
            ...styleHidden,
        })
    ),
    state(
        "inView",
        style({
            transform: "translateY(0%)",
            opacity: 1,
            ...styleVisible,
        })
    ),
    transition(`:enter`, [style(styleHidden), animate(`3000ms ease`, style({ transform: "rotate(360deg)" }))]),
    transition(`* <=> *`, [animate(`3000ms ease`)]),
]);

export const NextLexemeCardTransition = trigger("nextExerciseLexemes", [
    state(
        "inView",
        style({
            position: "relative",
            transform: "translateX(0%)",
            opacity: 1,
        })
    ),
    state(
        "outView",
        style({
            position: "relative",
            transform: "translateX(100%)",
            opacity: 0,
        })
    ),
    transition(`* => inView`, animate("3000ms ease")),
    transition(`* => outView`, animate("3000ms ease")),
]);
