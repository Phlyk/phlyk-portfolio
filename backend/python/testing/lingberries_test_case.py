from abc import ABC

from django.db import connection
from django.test import TestCase


class COMPANYNAMETestCase(TestCase, ABC):

    def setUp(self):
        print("\n________________________________________________________________________")
        super().setUp()

    def assertInstanceFieldsDictEqual(self, expected, actual, message: str = None):
        expected_model_fields_dict = expected.__dict__
        actual_model_fields_dict = actual.__dict__
        del expected_model_fields_dict["_state"]
        del actual_model_fields_dict["_state"]

        assertion_message = message if message else f"expected the dict of the instance fields to be equal on both models"

        self.assertDictEqual(
            expected_model_fields_dict,
            actual_model_fields_dict,
            assertion_message
        )


class LBFixtureTestCase(COMPANYNAMETestCase):
    """ A test case that disables foreign key checks to avoid having to create every related entity
    in the COMPANYNAME domain. 
    This approach to test fixtures until a more efficient less cumbersome way can be found."""
    @classmethod
    def setUpClass(cls) -> None:
        with connection.cursor() as cursor:
            cursor.execute("SET FOREIGN_KEY_CHECKS = 0")
        super().setUpClass()

    @classmethod
    def tearDownClass(cls) -> None:
        with connection.cursor() as cursor:
            cursor.execute("SET FOREIGN_KEY_CHECKS = 1")
        super().tearDownClass()
