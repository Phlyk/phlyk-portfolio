from unittest.mock import MagicMock, patch

from app.schemas.feed.enquiry.enquiry_factory import EnquiryFactory
from app.schemas.feed.enquiry.types import ChoiceEnquiryType, FeedEnquiriesDataType
from tests.lingberries_test_case import LingberriesTestCase


class EnquiryFactoryTest(LingberriesTestCase):
    sut: EnquiryFactory

    test_language_id = 1
    mockWordChoiceEnquiryFactory = None
    mockPhraseChoiceEnquiryFactory = None

    def setUp(self):
        self.mockWordChoiceEnquiryFactory = MagicMock()
        self.mockPhraseChoiceEnquiryFactory = MagicMock()

        self.sut = EnquiryFactory()
        self.sut.choice_factories = [self.mockWordChoiceEnquiryFactory, self.mockPhraseChoiceEnquiryFactory]

    def testShouldOrchestrateTheCreationOfEveryTypeOfEnquiryForLearnable(self):
        dummyWordChoiceExercise = ChoiceEnquiryType("wordyFlerps")
        dummyPhraseChoiceExercise = ChoiceEnquiryType("phrasyNerps")
        self.mockWordChoiceEnquiryFactory.generate_choice_enquiries.return_value = [dummyWordChoiceExercise]
        self.mockPhraseChoiceEnquiryFactory.generate_choice_enquiries.return_value = [dummyPhraseChoiceExercise]

        dummy_feed = (["feedyLerps"])

        generated_enquiries: FeedEnquiriesDataType = self.sut.generate_enquiries_for(dummy_feed, self.test_language_id)

        self.mockWordChoiceEnquiryFactory.generate_choice_enquiries.assert_called_once_with(dummy_feed, self.test_language_id)
        self.mockPhraseChoiceEnquiryFactory.generate_choice_enquiries.assert_called_once_with(dummy_feed, self.test_language_id)
        self.assertIn(
            dummyWordChoiceExercise,
            generated_enquiries.choice_data,
            "Expected the mocked word choice exercise to be in the returned exercises"
        )
        self.assertIn(
            dummyPhraseChoiceExercise,
            generated_enquiries.choice_data,
            "Expected the mocked phrase choice exercise to be in the returned exercises"
        )
