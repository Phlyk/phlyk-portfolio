import re
from types import SimpleNamespace
from unittest.mock import patch

from app.helper.array_helper import splitter
from app.models.feed import Feed
from app.models.feed_example import FeedExample
from app.models.feed_lexeme import FeedLexeme
from app.schemas.feed.enquiry.choice.choice_enquiry_enum import ChoiceEnquiry
from app.schemas.feed.enquiry.choice.choice_enquiry_factory import AbstractChoiceEnquiryFactory, PhraseChoiceEnquiryFactory, WordChoiceEnquiryFactory
from app.schemas.feed.enquiry.enquiry_direction_enum import EnquiryDataDirection
from app.schemas.feed.enquiry.types import ChoiceEnquiryType
from tests.lingberries_test_case import LingberriesTestCase


class AbstractChoiceEnquiryFactoryTest():
    sut: AbstractChoiceEnquiryFactory

    def testShouldDefineTheChoiceTypeProvidedByTheFactory(self, _mockTranslationDefinition, _mockTargetDefinition, mockLoadEnquiryData):
        factory_type = self.sut.factory_choice_type
        self.assertIsNotNone(factory_type)
        self.assertIsInstance(factory_type, ChoiceEnquiry)

    def testShouldDefineTheTypeOfLearnablesItHandles(self, _mockTranslationDefinition, _mockTargetDefinition, mockLoadEnquiryData):
        learnable_type_handled = self.sut.learnable_type
        self.assertIsNotNone(learnable_type_handled)
        self.assertIsInstance(learnable_type_handled, str)

    def testShouldGenerateAnArrayOfChoicesForTheEnquiry(self, _mockTranslationDefinition, _mockTargetDefinition, mockLoadEnquiryData):
        self.assertGreaterEqual(len(self.sut.generate_choices_from("flerpens")), 1)

    def testShouldProvideOneCorrectChoiceForTheEnquiry(self, _mockTranslationDefinition, _mockTargetDefinition, mockLoadEnquiryData):
        test_choice_text = "lookingForDatCorrectness"
        generated_choices = self.sut.generate_choices_from(test_choice_text)
        correct_choices = list(filter(lambda x: x.correct is True, generated_choices))
        self.assertEqual(len(correct_choices), 1)
        self.assertEqual(correct_choices[0].choice_text, test_choice_text)

    def testShouldProvideMoreThanOneIncorrectChoiceForTheEnquiry(self, _mockTranslationDefinition, _mockTargetDefinition, mockLoadEnquiryData):
        test_choice_text = "lookingForDatFalseness"
        generated_choices = self.sut.generate_choices_from(test_choice_text)
        incorrect_choices = list(filter(lambda ce: ce.correct is False, generated_choices))
        self.assertGreater(len(incorrect_choices), 1)
        for incorrect_choice in incorrect_choices:
            self.assertNotEqual(incorrect_choice.choice_text, test_choice_text)

    def testShouldGenerateAStepNameThatCanBeReadByTheFrontEnd(self, _mockTranslationDefinition, _mockTargetDefinition, mockLoadEnquiryData):
        gened_name = self.sut.generate_step_type_name(EnquiryDataDirection.FROM_TARGET)
        self.assertIsInstance(gened_name, str)
        self.assertEqual(gened_name, f"{self.sut.factory_choice_type.value}_{EnquiryDataDirection.FROM_TARGET.value}")

    def testEnquiryTextShouldGoFromAndToTheDirectionRequested(self, _mockTranslationDefinition, _mockTargetDefinition, mockLoadEnquiryData):
        target_text = "flerpytarget"
        translation_text = "jiminytranslation"
        expected_enquiry_directions_test_data = [
            {
                "direction": EnquiryDataDirection.FROM_TARGET,
                "expected_from": target_text,
                "expected_to": translation_text
            },
            {
                "direction": EnquiryDataDirection.FROM_TRANSLATION,
                "expected_from": translation_text,
                "expected_to": target_text
            }
        ]

        for enquiry_direction_data in expected_enquiry_directions_test_data:
            with self.subTest():
                from_text, to_text = self.sut.get_to_and_from_text_for(target_text, translation_text, enquiry_direction_data['direction'])
                self.assertEqual(from_text, enquiry_direction_data['expected_from'])
                self.assertEqual(to_text, enquiry_direction_data['expected_to'])

    def testShouldGenerateEnquiriesInBothDirections(self, _mockTranslationDefinition, _mockTargetDefinition, mockLoadEnquiryData):
        feedExamples = [SimpleNamespace(id=123), SimpleNamespace(id=456)]
        mockLoadEnquiryData.return_value = feedExamples

        generated_choice_enquiries = self.sut.generate_choice_enquiries(self.dummy_feed, 99)

        self.assertEqual(len(generated_choice_enquiries), len(feedExamples) * 2)
        from_target_choice_enquiries, from_translation_choice_enquiries = splitter(
            generated_choice_enquiries,
            lambda ce: find_step_type_direction_from_step_string(ce.step_type) == EnquiryDataDirection.FROM_TARGET
        )
        self.assertEqual(len(from_target_choice_enquiries), len(feedExamples))
        self.assertEqual(len(from_translation_choice_enquiries), len(feedExamples))

    def assertThatGeneratedChoiceEnquiryContainsExerciseTexts(self, generated_choice_enquiry: ChoiceEnquiryType, target_text: str, translation_text: str):
        if (generated_choice_enquiry.from_text == target_text):
            matching_translation_choice = list(filter(lambda ce: ce.choice_text == translation_text, generated_choice_enquiry.choices))
            self.assertEqual(
                len(matching_translation_choice),
                1,
                "expected the choice enquiry to have the translation text amongst its choices"
            )
        elif (generated_choice_enquiry.from_text == translation_text):
            matching_target_choice = list(filter(lambda ce: ce.choice_text == target_text, generated_choice_enquiry.choices))
            self.assertEqual(
                len(matching_target_choice),
                1,
                "expected the choice enquiry to have the target text amongst its choices"
            )
        else:
            self.assertTrue(False, "could not find either target or translation texts amongst choice enquiry choices")


dummyTargetWord = "flerpyschwaps"
dummyTranslationWord = "sqwimbyschlaps"
@patch('app.schemas.feed.enquiry.choice.choice_enquiry_factory.WordChoiceEnquiryFactory.load_data_for_enquiry')
@patch('app.schemas.feed.enquiry.choice.choice_enquiry_factory.WordChoiceEnquiryFactory.get_target_definition_for', return_value=dummyTargetWord)
@patch('app.schemas.feed.enquiry.choice.choice_enquiry_factory.WordChoiceEnquiryFactory.get_translation_definition_for', return_value=dummyTranslationWord)
class WordChoiceEnquiryFactoryTest(AbstractChoiceEnquiryFactoryTest, LingberriesTestCase):

    sut: WordChoiceEnquiryFactory
    dummy_feed: Feed

    def setUp(self):
        self.sut = WordChoiceEnquiryFactory()
        self.dummy_feed = Feed()

    def testShouldGenerateEnquiriesContainingWord(self, __mockTranslationDefinition, __mockTargetDefinition, mockLoadEnquiryData):
        feedLexemes = [FeedLexeme(), FeedLexeme()]
        mockLoadEnquiryData.return_value = feedLexemes

        generated_choice_enquiry = self.sut.generate_choice_enquiries(self.dummy_feed, 99)

        mockLoadEnquiryData.assert_called_once_with(self.dummy_feed, 99)
        self.assertIsInstance(generated_choice_enquiry[0], ChoiceEnquiryType)
        self.assertEqual(len(generated_choice_enquiry), len(feedLexemes) * 2)

        for generated_choice_enquiry in generated_choice_enquiry:
            self.assertThatGeneratedChoiceEnquiryContainsExerciseTexts(generated_choice_enquiry, dummyTargetWord, dummyTranslationWord)


dummyTargetPhrase = "You're a wizard Harry..."
dummyTranslationPhrase = "I'm a what??"
@patch('app.schemas.feed.enquiry.choice.choice_enquiry_factory.PhraseChoiceEnquiryFactory.load_data_for_enquiry')
@patch('app.schemas.feed.enquiry.choice.choice_enquiry_factory.PhraseChoiceEnquiryFactory.get_target_definition_for', return_value=dummyTargetPhrase)
@patch('app.schemas.feed.enquiry.choice.choice_enquiry_factory.PhraseChoiceEnquiryFactory.get_translation_definition_for', return_value=dummyTranslationPhrase)
class PhraseChoiceEnquiryFactoryTest(AbstractChoiceEnquiryFactoryTest, LingberriesTestCase):

    sut: PhraseChoiceEnquiryFactory
    dummy_feed: Feed

    def setUp(self):
        self.dummy_feed = Feed()
        self.sut = PhraseChoiceEnquiryFactory()

    def testShouldGenerateEnquiriesContainingExamples(self, __mockTranslationDefinition, __mockTargetDefinition, mockLoadEnquiryData):
        dummyFeedExample1 = FeedExample()
        dummyFeedExample2 = FeedExample()
        feedExamples = [dummyFeedExample1, dummyFeedExample2]
        mockLoadEnquiryData.return_value = feedExamples

        generated_choice_enquiry = self.sut.generate_choice_enquiries(self.dummy_feed, 99)

        mockLoadEnquiryData.assert_called_once_with(self.dummy_feed, 99)
        self.assertIsInstance(generated_choice_enquiry[0], ChoiceEnquiryType)
        self.assertEqual(len(generated_choice_enquiry), len(feedExamples) * 2)

        for generated_choice_enquiry in generated_choice_enquiry:
            self.assertThatGeneratedChoiceEnquiryContainsExerciseTexts(generated_choice_enquiry, dummyTargetPhrase, dummyTranslationPhrase)


def find_step_type_direction_from_step_string(step_type: str) -> EnquiryDataDirection:
    split_regex = r"[a-z]+_[a-z]+_(\w+)"
    step_type_direction_matches = re.findall(split_regex, step_type)
    assert len(step_type_direction_matches) == 1
    return EnquiryDataDirection[step_type_direction_matches[0].upper()]
