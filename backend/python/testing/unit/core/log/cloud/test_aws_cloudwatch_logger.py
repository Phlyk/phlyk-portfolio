from unittest.mock import MagicMock, patch

from botocore.exceptions import ClientError

from app.core.exception.log_forwarding_rejected_exception import LogForwardingRejectedException
from app.core.log.cloud.aws_cloudwatch_logger import AwsCloudwatchLogger
from tests.unit.core.log.cloud.abstract_cloud_logger_test_suite import CloudLogForwardingTestSuite

module_in_test = "app.core.log.cloud.aws_cloudwatch_logger"

@patch(f'{module_in_test}.json.dumps', return_value="IAmASerialisedPayload")
@patch(f'{module_in_test}.boto3.client')
class AwsCloudwatchLoggerTest(CloudLogForwardingTestSuite):

    sut: AwsCloudwatchLogger

    def defaultSuccessfulLogClientSetUpWithAssertions(self, boto3_mock):
        boto3_mock_log_client = boto3_mock.return_value
        boto3_mock_log_client.describe_log_streams.return_value = mockLogSuccessfulDescribePayload

        self.sut = AwsCloudwatchLogger()

        boto3_mock_log_client.describe_log_streams.assert_called_once_with(
            logGroupName=AwsCloudwatchLogger.aws_log_group,
            logStreamNamePrefix=AwsCloudwatchLogger.aws_log_stream
        )
        boto3_mock_log_client.put_log_events.return_value = MagicMock()
        boto3_mock_log_client.exceptions.DataAlreadyAcceptedException = ClientError
        return boto3_mock_log_client

    def tearDown(self):
        """Reset the instances of AwsCloudwatchLogger as it is a Singleton"""
        AwsCloudwatchLogger._instances = {}
        return super().tearDown()

    def testShouldQueryForNextSequenceTokenOnInitalisation(self, boto3_mock, json_mock):
        self.defaultSuccessfulLogClientSetUpWithAssertions(boto3_mock)

        self.assertEquals(
            self.sut.next_sequence_token,
            mockLogInitialUploadSequenceToken,
            "expected the sut sequence token to be equal to the mock value provided"
        )

    def testShouldLetExceptionBubbleIfUnableToAcquireToken(self, boto3_mock, json_mock):
        boto3_mock_log_client = boto3_mock.return_value
        boto3_mock_log_client.describe_log_streams.side_effect = Exception

        with self.assertRaises(Exception):
            self.sut = AwsCloudwatchLogger()

            boto3_mock_log_client.describe_log_streams.assert_called_once_with(
                logGroupName=AwsCloudwatchLogger.aws_log_group,
                logStreamNamePrefix=AwsCloudwatchLogger.aws_log_stream
            )

    def testShouldUseTheInitialSequenceTokenForFirstPutLogRequest(self, boto3_mock, json_mock):
        boto3_mock_log_client = self.defaultSuccessfulLogClientSetUpWithAssertions(boto3_mock)

        self.sut.cloudwatch_put_log_event(1, "ahablablaha")

        put_log_events_args, put_log_events_kwargs = boto3_mock_log_client.put_log_events.call_args
        self.assertEqual(
            put_log_events_kwargs['sequenceToken'],
            mockLogInitialUploadSequenceToken,
            "expected the initial sequence token to have been used on the first put log request"
        )

    def testShouldSaveSequenceTokenOnSuccessfulLogPutAndReturnTrue(self, boto3_mock, json_mock):
        boto3_mock_log_client = self.defaultSuccessfulLogClientSetUpWithAssertions(boto3_mock)
        boto3_mock_log_client.put_log_events.return_value = makeDummyPutLogEventReturnPayload(rejected=False)
        log_payload = self.createLogPayload("I'mmaa firing my lazorrr")

        log_put_return_value = self.sut.log_to_aws_cloudwatch(log_payload)

        self.assertEquals(
            self.sut.next_sequence_token,
            mockLogNextUploadSequenceToken,
            "expected the next sequence token to get updated after successful log put request"
        )
        self.assertTrue(
            log_put_return_value,
            "expected the value returned from the put_log_event to be True"
        )

    def testShouldLogTheForwardedPayloadOnSuccess(self, boto3_mock, json_mock):
        boto3_mock_log_client = self.defaultSuccessfulLogClientSetUpWithAssertions(boto3_mock)
        boto3_mock_log_client.put_log_events.return_value = makeDummyPutLogEventReturnPayload(rejected=False)
        log_payload = self.createLogPayload("Log me, and then log me again. Weird I know...")

        with self.assertLogs(logger=module_in_test, level="INFO") as logger_context_manager:
            self.sut.log_to_aws_cloudwatch(log_payload)

            self.assertIn(
                log_payload.__str__(),
                logger_context_manager.output[0],
                "expected payload to get logged when the log forwarding is successful"
            )

    def testShouldExtractPayloadDateTimeToUnixTimestampAndSerialisePayload(self, boto3_mock, json_mock):
        boto3_mock_log_client = self.defaultSuccessfulLogClientSetUpWithAssertions(boto3_mock)
        boto3_mock_log_client.put_log_events.return_value = makeDummyPutLogEventReturnPayload(rejected=False)
        log_payload = self.createLogPayload("I'mmaa firing my lazorrr")

        self.sut.log_to_aws_cloudwatch(log_payload)

        put_log_events_args, put_log_events_kwargs = boto3_mock_log_client.put_log_events.call_args
        log_event_arguments = put_log_events_kwargs['logEvents'][0]
        expected_timestamp = self.testDateTimeTimestamp
        self.assertEqual(
            log_event_arguments['timestamp'],
            expected_timestamp,
            "expected timestamp to have been converted to millis and used in put_log_event request"
        )
        self.assertTrue(
            isinstance(log_event_arguments['message'], str),
            "expected log message to be of type str"
        )
        json_mock.assert_called_once_with(log_payload.to_log_format())
        self.assertIn(
            log_event_arguments['message'],
            "IAmASerialisedPayload",
            "expected the log event message to be the result of the return value of the mock json serialisation"
        )

    def testShouldRaiseACustomExceptionIfLogGetsRejected(self, boto3_mock, json_mock):
        boto3_mock_log_client = self.defaultSuccessfulLogClientSetUpWithAssertions(boto3_mock)
        boto3_mock_log_client.put_log_events.return_value = makeDummyPutLogEventReturnPayload(rejected=True)

        with self.assertRaises(LogForwardingRejectedException) as exception_cm:
            self.sut.log_to_aws_cloudwatch(self.createLogPayload("SquibidtyBopPap - I'm going to be rejected"))
        
        self.assertIn(
            mockRejectedLogEventsInfoPayload.__str__(),
            exception_cm.exception.__str__(),
            "expected the thrown exception to contain the rejectedLogEventsInfo"
        )

    def testShouldLogAtWarnLevelIfLogAlreadyAcceptedExceptionIsThrownAndReturnTrue(self, boto3_mock, json_mock):
        boto3_mock_log_client = self.defaultSuccessfulLogClientSetUpWithAssertions(boto3_mock)
        error_message = "The data has already been sent..."
        boto3_mock_log_client.put_log_events.side_effect = ClientError(
            {"Error": {"Code": "DataAlreadyAcceptedException", "Message": error_message}},
            "put_log_events"
        )

        with self.assertLogs(logger=module_in_test, level="WARN") as logger_context_manager:
            log_put_return_value = self.sut.log_to_aws_cloudwatch(self.createLogPayload("I have already sent this message"))

            self.assertIn(
                error_message,
                logger_context_manager.output[0],
                "expected the logged message at warn level to contain the DataAlreadyAcceptedException error message"
            )
            self.assertTrue(
                log_put_return_value,
                "expected the value returned from the put_log_event to be True"
            )

    def testShouldCatchAllOtherExceptionsAndLogErrorTypeAndMessageAndReturnFalse(self, boto3_mock, json_mock):
        boto3_mock_log_client = self.defaultSuccessfulLogClientSetUpWithAssertions(boto3_mock)
        boto3_mock_log_client.put_log_events.side_effect = Exception("flerps")

        with self.assertLogs(logger=module_in_test, level="ERROR") as logger_context_manager:
            log_payload_return_value = self.sut.log_to_aws_cloudwatch(self.createLogPayload("It's gonna go boom"))
            
            self.assertIn(
                "<class 'Exception'>",
                logger_context_manager.output[0],
                "expected the error type to get logged"
            )
            self.assertIn(
                "flerps",
                logger_context_manager.output[0],
                "expected the error message to get logged too"
            )
            self.assertFalse(
                log_payload_return_value,
                "expected the put_log_events to return false when an exception arises"
            )


# # Dummy data
# describe_log_streams
mockLogInitialUploadSequenceToken = "postNextLogWithThisToken123"
mockLogSuccessfulDescribePayload = {
    'logStreams': [
        {
            "logStreamName": "student-site-application.log",
            "uploadSequenceToken": mockLogInitialUploadSequenceToken,
            "someExtraFields": {"we": "dont", "care": "about"}
        }
    ]
}
# put_log_events
mockLogNextUploadSequenceToken = "logReceivedNextLogWithThisToken789"
mockRejectedLogEventsInfoPayload = {
    'tooNewLogEventStartIndex': 123,
    'tooOldLogEventEndIndex': 123,
    'expiredLogEventEndIndex': 123
}

def makeDummyPutLogEventReturnPayload(rejected: bool):
    responsePayload = {
        'nextSequenceToken': mockLogNextUploadSequenceToken,
    }
    if rejected:
        responsePayload['rejectedLogEventsInfo'] = mockRejectedLogEventsInfoPayload
    return responsePayload
