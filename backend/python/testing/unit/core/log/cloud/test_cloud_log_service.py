from unittest.mock import patch

from django.test import override_settings

from app.core.log.cloud.cloud_log_service import handle_log_forwarding
from app.schemas.log.log_fields_input import LogFieldsInput
from tests.unit.core.log.cloud.abstract_cloud_logger_test_suite import CloudLogForwardingTestSuite

dir_containing_module = "app.core.log.cloud"
module_in_test = dir_containing_module + ".cloud_log_service"


class CloudLogServiceTest(CloudLogForwardingTestSuite):

    dummy_payload: LogFieldsInput

    def setUp(self):
        super().setUp()
        self.dummy_log_message = "Don't mind me, just a harmless log"
        self.dummy_payload = self.createLogPayload(self.dummy_log_message)

    @override_settings(AWS_FLAG="I_AM_JEFF_BEZOS", I_AM_JEFF_BEZOS="Not None therefore True")
    def testShouldForwardLogsToAwsIfRunningOnAWS(self):
        with patch(f"{module_in_test}.AwsCloudwatchLogger") as mock_aws_logger:
            handle_log_forwarding(self.dummy_payload)
            mock_aws_logger.assert_called_once()
            mock_aws_logger.return_value.log_to_aws_cloudwatch.assert_called_once_with(self.dummy_payload)

    @override_settings(GCP_FLAG="I_AM_LARRY_PAGE", I_AM_LARRY_PAGE="Not None therefore True")
    def testShouldForwardLogsToGcpStackdriverIfRunningOnGCP(self):
        with patch(f"{module_in_test}.GcpStackdriverLogger") as mock_gcp_logger:
            handle_log_forwarding(self.dummy_payload)

            mock_gcp_logger.assert_called_once()
            mock_gcp_logger.return_value.log_to_gcp_stackdriver.assert_called_once_with(self.dummy_payload)
    
    @override_settings(AWS_FLAG="I_AM_JEFF_BEZOS", I_AM_JEFF_BEZOS=None)
    @override_settings(GCP_FLAG="I_AM_LARRY_PAGE", I_AM_LARRY_PAGE=None)
    def testShouldDefaultToLoggingNativelyIfNotRunningOnCloud(self):
        with self.assertLogs(logger="forwarded_frontend_log", level="INFO") as logger_cm:
            handle_log_forwarding(self.dummy_payload)
            self.assertIn(
                self.dummy_log_message,
                logger_cm.output[0],
                "expected the log payload message to be logged natively when not running on GCP or AWS"
            )

    @override_settings(AWS_FLAG="I_AM_JEFF_BEZOS", I_AM_JEFF_BEZOS=None)
    @override_settings(GCP_FLAG="I_AM_LARRY_PAGE", I_AM_LARRY_PAGE=None)
    def testShouldCallTheLoggingMethodCorrespondingToTheLogPayloadLevelWhenNativeLogging(self):
        non_default_dummy_payload = self.createLogPayload(self.dummy_log_message, log_level="ERROR")
        with self.assertLogs(logger="forwarded_frontend_log", level="ERROR") as logger_cm:
            handle_log_forwarding(non_default_dummy_payload)
            self.assertIn(
                self.dummy_log_message,
                logger_cm.output[0],
                "expected the log payload message to be logged at ERROR when the log_level of payload is ERROR"
            )
