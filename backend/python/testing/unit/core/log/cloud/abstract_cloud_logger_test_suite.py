from _datetime import datetime
from abc import ABC

from app.schemas.log.log_fields_input import LogFieldsInput
from tests.lingberries_test_case import LingberriesTestCase


class CloudLogForwardingTestSuite(LingberriesTestCase, ABC):

    testDateTimeString = "2020-03-27T07:55:48.221Z"
    testDateTimeTimestamp = 1585295748221

    def createLogPayload(self, log_message: str, log_level: str = "INFO", datetime_string: str = None) -> LogFieldsInput:
        if datetime_string is None:
            datetime_string = self.testDateTimeString
        payload = LogFieldsInput()
        payload.timestamp = datetime.strptime(datetime_string, "%Y-%m-%dT%H:%M:%S.%fZ")
        payload.message = log_message
        payload.level = log_level
        # Graphene sets these fields to None if not present in request
        payload.elapsed_milliseconds = None
        payload.request_path = None
        payload.app_state = None
        payload.stack_trace = None
        payload.url = None
        payload.user_id = None
        return payload
