from unittest.mock import patch

from app.core.exception.log_forwarding_rejected_exception import LogForwardingRejectedException
from app.core.log.cloud.gcp_stackdriver_logger import GcpStackdriverLogger
from tests.unit.core.log.cloud.abstract_cloud_logger_test_suite import CloudLogForwardingTestSuite

module_in_test = "app.core.log.cloud.gcp_stackdriver_logger"


@patch(f"{module_in_test}.gcp_logging.Client")
class GcpStackdriverLoggerTest(CloudLogForwardingTestSuite):

    sut: GcpStackdriverLogger

    def setUpClassAndPatches(self, gcp_logger_mock_client):
        super().setUp()
        self.sut = GcpStackdriverLogger()

    def tearDown(self):
        """Reset the instances of GcpStackdriverLogger as it is a Singleton"""
        GcpStackdriverLogger._instances = {}
        return super().tearDown()

    def testShouldCreateLoggerWithNameInClass(self, gcp_logger_mock_client):
        self.setUpClassAndPatches(gcp_logger_mock_client)
        gcp_logger_mock_client.return_value.logger.assert_called_once_with(name=GcpStackdriverLogger.gcp_log_name)

    def testShouldForwardTheLogToStackdriverAndReturnTrue(self, gcp_logger_mock_client):
        self.setUpClassAndPatches(gcp_logger_mock_client)
        example_received_log_payload = self.createLogPayload("LogMeBabyOneMoreTime")

        log_return_value = self.sut.log_to_gcp_stackdriver(example_received_log_payload)

        mock_log_struct_call = gcp_logger_mock_client.return_value.logger.return_value.log_struct
        mock_log_struct_call.assert_called_once()
        logger_call_args, logger_call_kwargs = mock_log_struct_call.call_args
        self.assertTrue(
            log_return_value,
            "expected the return value of the log forwarding to be true"
        )
        log_payload_forwarded = logger_call_kwargs["info"]
        self.assertEqual(
            log_payload_forwarded["message"],
            example_received_log_payload.message,
            "expected the log event message forwarded to gcp to be equal to the one provided"
        )
        self.assertEqual(
            log_payload_forwarded["level"],
            example_received_log_payload.level,
            "expected the log event level forwarded to gcp to be equal to the one provided"
        )

    def testShouldProvideContextualKwargsToStackdriverWhenLogForwarding(self, gcp_logger_mock_client):
        self.setUpClassAndPatches(gcp_logger_mock_client)
        provided_log_event_level = "INFO"
        dummy_test_payload = self.createLogPayload("FlerpyCherpyNerps", provided_log_event_level)

        self.sut.log_to_gcp_stackdriver(dummy_test_payload)

        mock_log_struct_call = gcp_logger_mock_client.return_value.logger.return_value.log_struct
        logger_call_args, logger_call_kwargs = mock_log_struct_call.call_args
        self.assertEqual(
            logger_call_kwargs['severity'],
            provided_log_event_level,
            "expected the log event to be forwarded with the level of the payload"
        )
        self.assertEqual(
            logger_call_kwargs['timestamp'],
            dummy_test_payload.timestamp,
            "expected the log event to be forwarded with the timestamp of the payload"
        )
        self.assertEqual(
            logger_call_kwargs['labels'],
            {"forwarded-log": "true"},
            "expected the log event to be forwarded with a label indicating that it was forwarded"
        )
        self.assertIn(
            GcpStackdriverLogger.source_location,
            logger_call_kwargs['source_location'].values(),
            "expected the log event to be forwarded with the source location set to the log origin"
        )

    def testShouldMapAngularLevelWarnToGcpSeverityWarning(self, gcp_logger_mock_client):
        self.setUpClassAndPatches(gcp_logger_mock_client)
        provided_log_event_level = "WARN"
        dummy_test_payload = self.createLogPayload("FlerpyCherpyNerps", provided_log_event_level)

        self.sut.log_to_gcp_stackdriver(dummy_test_payload)

        mock_log_struct_call = gcp_logger_mock_client.return_value.logger.return_value.log_struct
        logger_call_args, logger_call_kwargs = mock_log_struct_call.call_args
        self.assertEqual(
            logger_call_kwargs['severity'],
            "WARNING",
            "expected the log event severity to be forwarded with the level mapped to WARNING"
        )

    def testShouldLogNativelyWhenLoggingSuccess(self, gcp_logger_mock_client):
        self.setUpClassAndPatches(gcp_logger_mock_client)

        with self.assertLogs(logger=module_in_test, level="INFO") as logger_context_manager:
            dummy_log_payload = self.createLogPayload("LogMeBabyOneMoreTime")
            self.sut.log_to_gcp_stackdriver(dummy_log_payload)
            self.assertIn(
                dummy_log_payload.__str__(),
                logger_context_manager.output[0],
                "expected the forwarded payload to get logged natively on success"
            )

    def testShouldWrapErrorsInCustomExceptionAndLogNatively(self, gcp_logger_mock_client):
        self.setUpClassAndPatches(gcp_logger_mock_client)
        log_forwarding_exception_error_msg = "Oops, I went boom"
        mock_log_struct_call = gcp_logger_mock_client.return_value.logger.return_value.log_struct
        mock_log_struct_call.side_effect = Exception(log_forwarding_exception_error_msg)

        with self.assertLogs(logger=module_in_test, level="ERROR") as logger_cm, self.assertRaises(LogForwardingRejectedException) as error_cm:
            self.sut.log_to_gcp_stackdriver(self.createLogPayload("I'm gonna go boom"))
            self.assertIn(
                log_forwarding_exception_error_msg,
                error_cm.exception.__str__,
                "expected the originl log forwarding error msg to get wrapped in the  "
            )
            self.assertIn(
                log_forwarding_exception_error_msg,
                logger_cm.output[0],
                "expected the error message of the caught exception to get logged before rethrowing"
            )
