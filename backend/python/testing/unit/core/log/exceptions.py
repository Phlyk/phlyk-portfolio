from typing import Dict, Union


class COMPANYNAMEException(Exception):

    error_message_prefix = "LB Error Occured."

    def __init__(self, message):
        super().__init__(f"{self.error_message_prefix} {message}")


class LogForwardingRejectedException(COMPANYNAMEException):

    error_message_template = "Log forwarding failed to send to {platform} with reason: {failed_response}"

    def __init__(self, platform: str, failed_response: Union[Exception, str, int, Dict[str, str]]):
        super().__init__(self.error_message_template.format(platform=platform, failed_response=failed_response))
        self.platform = platform
        self.failed_response = failed_response

    def __str__(self):
        return self.error_message_template.format(platform=self.platform, failed_response=self.failed_response)
