from django.test import TestCase

from app.core.meta.singleton import Singleton


class SingletonTest(TestCase):
    
    def testShouldBeASingletonAndNotGetReinitted(self):
        sut1 = DummySingleton()
        sut2 = DummySingleton()
        self.assertEqual(
            sut1,
            sut2,
            "expected the DummySingleton class to always return same instance once initiated"
        )

class DummySingleton(metaclass=Singleton):
    pass
