from typing import Any, MutableMapping, Optional, Tuple, TypeVar

from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.utils import timezone

_T = TypeVar("_T", bound=models.Model, covariant=True)


class LearnableFeedManager(models.Manager):
    def update_or_create(self, defaults: Optional[MutableMapping[str, Any]], **kwargs: Any) -> Tuple[_T, bool]:
        try:
            user_feed_learnable = self.get(**kwargs)
            for key, value in defaults.items():
                setattr(user_feed_learnable, key, value)
            user_feed_learnable.last_seen = timezone.now()
            user_feed_learnable.save()
            created = False
        except ObjectDoesNotExist:
            creation_args = {
                **defaults,
                **kwargs,
                "first_seen": timezone.now(),
                "last_seen": timezone.now()
            }
            user_feed_learnable = self.create(**creation_args)
            created = True
        return (user_feed_learnable, created)


class LearnableWordbookManager(models.Manager):
    def add_user_learnable_to_wordbook(self, **kwargs: Any) -> _T:
        saved_date = timezone.now()
        try:
            user_feed_learnable = self.get(**kwargs)
            user_feed_learnable.saved_date = saved_date
            user_feed_learnable.save()
        except ObjectDoesNotExist:
            creation_args = {
                **kwargs,
                "saved_date": saved_date
            }
            user_feed_learnable = self.create(**creation_args)
        return user_feed_learnable

    def remove_user_learnable_from_wordbook(self, **kwargs: Any) -> _T:
        wordbook_lexeme = self.get(**kwargs)
        wordbook_lexeme.saved_date = None
        wordbook_lexeme.save()
        return wordbook_lexeme


class UserLearnableMixin(models.Model):
    ability = models.IntegerField(default=0)
    first_seen = models.DateTimeField(null=True)
    last_seen = models.DateTimeField(null=True)
    saved_date = models.DateTimeField(null=True)

    objects = models.Manager()
    feed_manager = LearnableFeedManager()
    wordbook_manager = LearnableWordbookManager()

    class Meta:
        abstract = True
