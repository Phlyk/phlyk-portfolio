from time import sleep

from django.utils import timezone

from COMPANYNAME.core.tests.COMPANYNAME_test_case import COMPANYNAMETestCase

from .models import DummyUserLearnable

dummy_learnable_ability = 2834

class UserLearnableFeedManagerTest(COMPANYNAMETestCase):
    mixin = DummyUserLearnable

    def testShouldCreateTheLearnableWithFirstAndLastSeenIfItDoesntExist(self):
        _in_memory_dummy_learnable, created = DummyUserLearnable.feed_manager.update_or_create(
            pk=1,
            defaults={"ability": dummy_learnable_ability}
        )

        self.assertTrue(created, "expected learnable to have been created as it doesn't exist")

        created_test_learnable: DummyUserLearnable = DummyUserLearnable.objects.get(pk=1)
        self.assertEquals(
            created_test_learnable.ability,
            dummy_learnable_ability,
            f"expected dummy learnable to have been persisted with ability as {dummy_learnable_ability}"
        )
        self.assertAlmostEqual(
            created_test_learnable.first_seen,
            created_test_learnable.last_seen,
            delta=timezone.timedelta(seconds=1),
            msg="expected the first seen and last seen to be the same, as model has just been created"
        )
        self.assertIsNotNone(
            created_test_learnable.first_seen,
            "timestamp of initial learnable seeing should not be null"
        )

    def testShoulOnlyChangeLastSeenWhenUpdatingAnExistingLearnable(self):
        fixture_learnable: DummyUserLearnable = _makeDummyLearnable()

        first_seen_time = fixture_learnable.first_seen
        self.assertIsNotNone(first_seen_time, "timestamp should be timezone.now()")
        self.assertEqual(
            first_seen_time,
            fixture_learnable.last_seen,
            "learnable has just been created so first and last times should be the same"
        )

        sleep(1) # otherwise the creation time and updated time are the same

        _in_memory_dummy_learnable, created = DummyUserLearnable.feed_manager.update_or_create(
            pk=1,
            defaults={"ability": dummy_learnable_ability}
        )

        self.assertFalse(created, "expected learnable to have been updated as it already exists")

        updated_test_learnable: DummyUserLearnable = DummyUserLearnable.objects.get(pk=1)

        self.assertEqual(
            updated_test_learnable.first_seen,
            first_seen_time,
            "expected no change to the initial seeing timestamp of the learnable"
        )
        self.assertNotEqual(
            updated_test_learnable.last_seen,
            first_seen_time,
            "expected the last_seen timestamp to be updated"
        )


class UserLearnableWordbookManagerTest(COMPANYNAMETestCase):
    mixin = DummyUserLearnable

    def testShouldAddSaveDateToSaveAlreadySeenLearnableToWordbook(self):
        seen_learnable: DummyUserLearnable = _makeDummyLearnable()
        self.assertIsNone(
            seen_learnable.saved_date,
            "expected newly created user learnable to not be in wordbook"
        )

        DummyUserLearnable.wordbook_manager.add_user_learnable_to_wordbook(pk=1)

        updated_learnable: DummyUserLearnable = DummyUserLearnable.objects.get(pk=1)

        self.assertEqual(
            updated_learnable.first_seen,
            seen_learnable.first_seen,
            "expected no changes to the 'seeing' of the learnable"
        )
        self.assertEqual(
            updated_learnable.last_seen,
            seen_learnable.last_seen,
            "expected no changes to the 'seeing' of the learnable"
        )
        self.assertIsNotNone(
            updated_learnable.saved_date,
            "expected learnable to have been updated with the timestamp of when added to wordbook"
        )

    def testShouldCreateEntryWithOnlySavedDateFilled(self):
        DummyUserLearnable.wordbook_manager.add_user_learnable_to_wordbook(pk=1)

        created_learnable: DummyUserLearnable = DummyUserLearnable.objects.get(pk=1)

        self.assertIsNone(
            created_learnable.first_seen,
            "expected a newly created wordbook learnable to not have been seen"
        )
        self.assertIsNone(
            created_learnable.last_seen,
            "expected a newly created wordbook learnable to not have been seen"
        )
        self.assertIsNotNone(
            created_learnable.saved_date,
            "expected the newly created wordbook learnable to have been saved with timestamp"
        )

    def testShouldRemoveTheSavedDateTimestampWhenUnsavingFromWordbook(self):
        _makeDummyLearnable(with_seen=False, in_wordbook=True)

        no_longer_wordbook_lexeme: DummyUserLearnable = DummyUserLearnable.wordbook_manager.remove_user_learnable_from_wordbook(pk=1)

        self.assertIsNone(
            no_longer_wordbook_lexeme.saved_date,
            "expected the learnable to have been removed, saved_date = None"
        )

def _makeDummyLearnable(with_seen: bool = True, in_wordbook: bool = False) -> DummyUserLearnable:
    creation_kwargs = {"pk": 1, "ability": 19}
    timezone_now = timezone.now()
    if with_seen:
        creation_kwargs.update({"first_seen": timezone_now, "last_seen": timezone_now})
    if in_wordbook:
        creation_kwargs.update({"saved_date": timezone_now})
    return DummyUserLearnable.objects.create(**creation_kwargs)
