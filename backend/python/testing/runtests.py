#!/usr/bin/env python
import os
import sys

import django
from django.conf import settings
from django.test.runner import DiscoverRunner
from django.test.utils import get_runner

if __name__ == "__main__":
    os.environ['DJANGO_SETTINGS_MODULE'] = 'backend.settings.test'
    django.setup()
    DiscoverRunnerClass = get_runner(settings)
    test_runner: DiscoverRunner = DiscoverRunnerClass(keepdb=True)
    failures = test_runner.run_tests(["COMPANYNAME"])
    sys.exit(bool(failures))
