import logging

from django.test.runner import DiscoverRunner

from COMPANYNAME.core.log.log_level_enum import LogLevel

runner_logger = logging.getLogger(__name__)


class UnmanagedModelTestRunner(DiscoverRunner):
    """
    Test runner that automatically makes all unmanaged models in your Django
    project managed for the duration of the test run, so that one doesn't need
    to execute the SQL manually to create them.

    Will be structured into an appropriate directory after
    """

    def __init__(self, **kwargs):
        from django.apps import apps

        super().__init__(**kwargs)

        all_models = apps.get_models()
        for m in all_models:
            runner_logger.log(LogLevel.DEBUG.value, f"Found model {m} - Managed:{m._meta.managed}")

        self.unmanaged_models = [m for m in all_models if not m._meta.managed]

    def setup_test_environment(self, *args, **kwargs):
        for m in self.unmanaged_models:
            m._meta.managed = True
            runner_logger.log(LogLevel.DEBUG, f"Modifying model {m} to be managed for testing - Managed:{m._meta.managed}")
        super().setup_test_environment(*args, **kwargs)

    def teardown_test_environment(self, *args, **kwargs):
        super().teardown_test_environment(*args, **kwargs)
        for m in self.unmanaged_models:
            m._meta.managed = False
            runner_logger.log(LogLevel.DEBUG, f"Resetting model {m} to be unmanaged - Managed:{m._meta.managed}")
