import re

from django.core.exceptions import ValidationError
from django.db import models


class SemanticFunction(models.Model):
    name = models.CharField(max_length=50)
    argument_map = models.CharField(max_length=255)
    definition = models.CharField(max_length=255)
    description = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'semantic_function'

    def __str__(self):
        return "#{0} ({1})".format(str(self.id), self.name)

    def validate_argument_map(self):
        m = re.findall(r'^(x[\d]+=[\w\d]+)(,x[\d]+=\w+)*$', self.argument_map)
        if not m:
            raise ValidationError(
                "Argument map ({0}) is not properly formed. It must be a comma separated list, e.g. x1=arg,x2=arg2 (no spaces)"
                .format(self.argument_map)
            )

    def get_arguments_nb(self):
        m = re.findall(r'x\d', self.argument_map)
        return len(m)
