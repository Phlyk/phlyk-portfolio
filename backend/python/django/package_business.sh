#!/bin/bash

if [ $# -ne 1 ]; then
    echo ">>    bash package_business.sh <version>"
    echo ">>    eg: bash package_business.sh develop"
    echo ">>    eg: bash package_business.sh 1.0.1.4"
    exit 1
fi

VERSION=$1

SCRIPT_PATH=$( cd $(dirname $0); pwd -P)
PROJECT_DIR=${SCRIPT_PATH}
BUILD_DIR=${PROJECT_DIR}/../build
mkdir -p ${BUILD_DIR}
ZIP_FILE=${BUILD_DIR}/backend.business.${VERSION}.zip

echo ">> Zipping up application code"
echo "SCRIPT_PATH:${SCRIPT_PATH}"
echo "PROJECT_DIR:${PROJECT_DIR}"
echo "BUILD_DIR:${BUILD_DIR}"
echo "ZIP_FILE:${ZIP_FILE}"

rm -f ${ZIP_FILE}

zip -r ${ZIP_FILE} app -x "*.DS_Store" -x "*/__pycache__*" -x "app/tests*"
zip -r ${ZIP_FILE} backend -x "*.DS_Store" -x "*/__pycache__*"
zip -r ${ZIP_FILE} infrastructure -x "*.DS_Store" -x "*/__pycache__*"
zip -r ${ZIP_FILE} requirements -x "*.DS_Store" -x "*/__pycache__*"
zip -r ${ZIP_FILE} scripts -x "*.DS_Store" -x "*/__pycache__*" -x "scripts/mysql/model*"
zip -r ${ZIP_FILE} server -x "*.DS_Store" -x "*/__pycache__*"
zip -r ${ZIP_FILE} main.py
zip -r ${ZIP_FILE} manage.py

exit 0
