# https://docs.gunicorn.org/en/stable/settings.html#settings
# To be implemented:
#   SSL
import multiprocessing

bind = "unix:/tmp/guni_backend.sock"
timeout = 90

workers = multiprocessing.cpu_count() * 1 + 1
threads = multiprocessing.cpu_count() * 1 + 1
worker_class = "gthread"

accesslog = "/var/log/gunicorn.access.log"
errorlog = "/var/log/gunicorn.error.log"
pidfile = "./gunicorn.pid"

## Debugging
loglevel = "info"
spew = False # Prints absolutely everything...
