import multiprocessing

bind = "127.0.0.1:8000"
timeout = 60

workers = 2

accesslog = "./gunicorn.access.log"
errorlog = "./gunicorn.error.log"
pidfile = "./gunicorn.pid"
