#!/bin/bash

# The script used to start the backend application & web server via ssh
# It assumes that:
#   - the code to be served has already been deployed onto the instance
#   - the needed apt packages have been installed
#   - virtualenv is installed and one has been created
#   - it is being run on an Ubuntu instance
#   - that the SSL certs have been already placed in $HOME/'certs'
# It will:
#   - start the gunicorn application server
#   - configure the nginx http + https web server
#   - restart the nginx http web server with the new config

set -e

if [ $# -ne 1 ]; then
    echo ">> Usage:"
    echo ">>    bash start_backend_server.sh <ENVIRONMENT> <SSL_CERT_FULLCHAIN> <SSL_CERT_PRIVKEY>"
    echo ">>    <ENVIRONMENT> - the cloud environment to build in: development, staging or production"
    echo ">>    <SSL_CERT_FULLCHAIN> - the public fullchain ssl .pem file provided by letsencrypt"
    echo ">>    <SSL_CERT_PRIVKEY> - the private ssl .pem file provided by letsencrypt"
    exit 1
fi

SCRIPT_PATH=$( cd $(dirname $0); pwd -P)
ENVIRONMENT=$1
INFRASTRUCTURE_PATH="${SCRIPT_PATH}/../infrastructure"

SSL_CERT_FULLCHAIN=${HOME}/certs/COMPANYNAME_server_fullchain.pem
SSL_CERT_PRIVKEY=${HOME}/certs/COMPANYNAME_server_privkey.pem
NGINX_SSL_DIR=/etc/nginx/ssl

echo "start_backend_server.sh:"
echo "SCRIPT_PATH:${SCRIPT_PATH}"
echo "ENVIRONMENT:${ENVIRONMENT}"

echo ">> Activating Virtual Environment"
source ${INFRASTRUCTURE_PATH}/python/create_and_activate_venv.sh backendvenv

echo ">> Installing required packages"
pip3 install -q -r requirements/${ENVIRONMENT}.txt

echo ">> Configuring & Restarting CloudWatch Agent"
sudo cp ${SCRIPT_PATH}/amazon-cloudwatch-agent.json /opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json
sudo amazon-cloudwatch-agent-ctl -a stop
sudo amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -c file:/opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json -s

echo ">> Copying Gunicorn systemd configuration"
sudo cp ${SCRIPT_PATH}/gunicorn.service /etc/systemd/system/gunicorn.service

echo ">> State of Gunicorn service already running"
systemctl status gunicorn || echo "Ignoring non 0 return code"

echo ">> Starting application server"
sudo systemctl daemon-reload
sudo systemctl restart gunicorn
sudo systemctl enable gunicorn

echo ">> Configuring NGINX Web Server"
sudo ln -sf ${SCRIPT_PATH}/nginx.conf /etc/nginx/sites-enabled/
sudo mkdir -p /etc/systemd/system/nginx.service.d
printf "[Service]\nExecStartPost=/bin/sleep 0.1\n" | sudo tee /etc/systemd/system/nginx.service.d/override.conf > /dev/null

sudo mkdir -p ${NGINX_SSL_DIR}
sudo cp ${SSL_CERT_FULLCHAIN} ${NGINX_SSL_DIR}/COMPANYNAME_server_fullchain.pem
sudo cp ${SSL_CERT_PRIVKEY} ${NGINX_SSL_DIR}/COMPANYNAME_server_privkey.pem
sudo chmod -R 700 ${NGINX_SSL_DIR}
sudo systemctl daemon-reload

echo ">> Testing NGINX web server"
sudo nginx -t

echo ">> State of NGINX web server already running"
systemctl status nginx || echo "Ignoring non 0 return code"

echo ">> Restarting NGINX Web Server with updated config"
sudo systemctl restart nginx
