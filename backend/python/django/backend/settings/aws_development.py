from .base import *

# The bucket to use for extra application data
APP_BUCKET = env('BACKEND_BUCKET')

# # Database config
DB_PASSWORD = env('DB_PASSWORD')
DB_HOST = env('DB_HOST')

DATABASES = {
    'default': {
        'ENGINE': DB_ENGINE,
        'HOST': DB_HOST,
        'NAME': DB_NAME,
        'USER': DB_USER,
        'PASSWORD': DB_PASSWORD,
    }
}

# # Application Config
# Debug mode - displays detailed error messages
DEBUG = True
# Secret key - used by Django as a cryptographic SALT (can define your own)
SECRET_KEY = env('DJANGO_SECRET_KEY')
# Allowed hosts - URLs django can serve
EC2_APPLICATION = env('EC2_APPLICATION')
EC2_SERVER_IP_DOTS_REPLACED = env('EC2_SERVER_IP').replace('.', '-')
EC2_SERVER_IP_HOSTNAME = 'ec2-{ip_string}.eu-central-1.compute.amazonaws.com'.format(ip_string=EC2_SERVER_IP_DOTS_REPLACED)
ALLOWED_HOSTS = ['localhost', EC2_SERVER_IP_HOSTNAME, 'backend-dev-aws.COMPANYNAME.com']
# Installed Apps - each project contains many apps (sort of like submodules)
INSTALLED_APPS += [
    'corsheaders'  # investigate if needed for prod
]
# Middleware - hooks into all request/ response interactions
MIDDLEWARE += [
    'corsheaders.middleware.CorsMiddleware'  # investigate if needed for prod
]

# # Logging & Error Config

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': STANDARD_LOGGING_FORMATTER
    },
    'handlers': {
        'console': CONSOLE_LOGGING_HANDLER,
        'file': FILE_LOGGING_HANDLER
    },
    'loggers': {
        '': {
            'level': LOG_LEVEL,
            'handlers': ['console', 'file']
        },
        'COMPANYNAME': {
            'level': LOG_LEVEL,
            'handlers': ['console', 'file'],
            'propagate': False
        },
        'django.request': {
            'level': LOG_LEVEL,
            'handlers': ['console', 'file'],
            'propagate': False
        },
        'django.db.backends': {
            'level': LOG_LEVEL,
            'handlers': ['console', 'file'],
            'propagate': False
        },
        'django.server': {
            'level': LOG_LEVEL,
            'handlers': ['console', 'file'],
            'propagate': False
        }
    }
}
# Let exceptions propagate up to the call hierachy when DEBUG is on
DEBUG_PROPAGATE_EXCEPTIONS = True

# # Network Config
# Cross origin resource sharing (CORS) - accessing content from another domain
CORS_ORIGIN_ALLOW_ALL = True
