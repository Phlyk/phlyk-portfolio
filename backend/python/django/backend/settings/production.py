import os

from .base import *



# # Database config
DB_PASSWORD = env('DB_PASSWORD')
DB_HOST = env('DB_HOST')

DATABASES = {
    'default': {
        'ENGINE': DB_ENGINE,
        'HOST': DB_HOST,
        'NAME': DB_NAME,
        'USER': DB_USER,
        'PASSWORD': DB_PASSWORD,
    }
}

# # Host Machine Config
# Google app engine - set by GAE when application is running in a GAE VM
GAE_APPLICATION = env('GAE_APPLICATION')


# # Application Config
# Debug mode - displays detailed error messages
DEBUG = False
# Secret key - used by Django as a cryptographic SALT (can define your own)
SECRET_KEY = env('DJANGO_SECRET_KEY')
# Allowed hosts - URLs django can serve
ALLOWED_HOSTS = ['fancy.gc.url.com']
# Installed Apps - each project contains many apps (sort of like submodules)
INSTALLED_APPS += [
    'corsheaders'  # investigate if needed for prod
]
# Middleware - hooks into all request/ response interactions
MIDDLEWARE += [
    'corsheaders.middleware.CorsMiddleware'  # investigate if needed for prod
]

# # Network Config
# Cross origin resource sharing (CORS) - accessing content from another domain
CORS_ORIGIN_ALLOW_ALL = False
CORS_ORIGIN_WHITELIST = ["http://localhost:8080"]
# Cross site request forgery - prevent unauthorised requests on our behalf
CSRF_TRUSTED_ORIGINS = []
