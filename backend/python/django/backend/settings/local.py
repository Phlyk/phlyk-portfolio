import os

from .base import *

# # Database config
DB_HOST = env('DB_HOST', default='127.0.0.1')
DB_PASSWORD = env('DB_PASSWORD')
DB_PORT = env('DB_PORT', default='3306')

DATABASES = {
    'default': {
        'ENGINE': DB_ENGINE,
        'HOST': DB_HOST,
        'PORT': DB_PORT,
        'NAME': DB_NAME,
        'USER': DB_USER,
        'PASSWORD': DB_PASSWORD,
        'TEST': {
            'NAME': 'test_' + DB_NAME
        }
    }
}

# # For mocking the cloud locally
# Google app engine - set by GAE when application is running in a GAE VM
# GAE_APPLICATION = env('GAE_APPLICATION', default=False)
# Absolute path to your credential key
GOOGLE_APPLICATION_CREDENTIALS = env('GOOGLE_APPLICATION_CREDENTIALS', default='be_quiet_google')
# The bucket to use for extra application data
APP_BUCKET = env('BACKEND_BUCKET', default='not-needed-right-now')

# # Application Config
# Debug mode - displays detailed error messages
DEBUG = True
# Secret key - used by Django as a cryptographic SALT (can define your own)
SECRET_KEY = env('DJANGO_SECRET_KEY', default='a49d115b-c380-4eea-bce5-ba3b4e7887b1')
# Allowed hosts - URLs django can serve
ALLOWED_HOSTS = ['localhost', '127.0.0.1', '0.0.0.0']
# Installed Apps - each project contains many apps (sort of like submodules)
INSTALLED_APPS += [
    'corsheaders'  # investigate if needed for prod
]
# Middleware - hooks into all request/ response interactions
MIDDLEWARE += [
    'corsheaders.middleware.CorsMiddleware'  # investigate if needed for prod
]

# # Logging & Error Config
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': STANDARD_LOGGING_FORMATTER,
        'django.server': DEFAULT_LOGGING['formatters']['django.server'],
    },
    'handlers': {
        'console': CONSOLE_LOGGING_HANDLER,
        'file': FILE_LOGGING_HANDLER,
        'django.server': DEFAULT_LOGGING['handlers']['django.server'],
    },
    'loggers': {
        '': {
            'level': LOG_LEVEL,
            'handlers': ['console', 'file']
        },
        'COMPANYNAME': {
            'level': LOG_LEVEL,
            'handlers': ['console', 'file'],
            'propagate': False
        },
        'django.request': {
            'level': 'DEBUG',
            'handlers': ['console', 'file'],
            'propagate': False
        },
        'django.db.backends': {
            'level': LOG_LEVEL,
            'handlers': ['console'],
            'propagate': False
        },
        'django.server': DEFAULT_LOGGING['loggers']['django.server']
    }
}
# Let exceptions propagate up to the call hierachy when DEBUG is on
DEBUG_PROPAGATE_EXCEPTIONS = True

# # Network Config
# Cross origin resource sharing (CORS) - accessing content from another domain
CORS_ORIGIN_ALLOW_ALL = True
