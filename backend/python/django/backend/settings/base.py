import datetime

import environ
from django.utils.log import DEFAULT_LOGGING

env = environ.Env()
PROJECT_ROOT = environ.Path(__file__) - 3
environ.Env.read_env(PROJECT_ROOT.file('.env'))

# # Project root

# # Application config
# Config path to application callable for WSGI Servers
WSGI_APPLICATION = 'backend.wsgi.application'
# URL Config
ROOT_URLCONF = 'backend.urls'
# HTML rendering
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
# # Cloud
AWS_FLAG = 'EC2_APPLICATION'
GCP_FLAG = 'GAE_APPLICATION'
# Cloud Storage bucket folder to find course fixtures
COURSES_FOLDER = 'courses/'

# # Logging Defaults
LOG_LEVEL = env('LOG_LEVEL', default='INFO')
STANDARD_LOGGING_FORMATTER = {
    'format': '<{asctime}> [{levelname}] - {name} : {message}',
    'style': '{',
}
CONSOLE_LOGGING_HANDLER = {
    'level': 'DEBUG',
    'class': 'logging.StreamHandler',
    'formatter': 'standard'
}

FILE_LOGGING_HANDLER = {
    'level': 'DEBUG',
    'class': 'logging.handlers.RotatingFileHandler',
    'filename': 'django_backend_app.log',
    'maxBytes': 1024*1024*2,  # 2 MB
    'backupCount': 5,
    'formatter': 'standard'
}

# # Default MySQL database setup
DB_ENGINE = 'django.db.backends.mysql'
DB_NAME = 'django_db'
DB_USER = 'django_user'

# # GraphQL settings
GRAPHENE = {
    'SCHEMA': 'COMPANYNAME.graphql.api.schema',
    'SCHEMA_OUTPUT': '../frontend-services/graphql/graphql-introspection-schema.json',
    'SCHEMA_INDENT': 4,
    'MIDDLEWARE': [
        'graphql_jwt.middleware.JSONWebTokenMiddleware',
        'graphene_django.debug.DjangoDebugMiddleware'
    ],
}
# Graphql Json web token - lifecycle config
GRAPHQL_JWT = {
    'JWT_VERIFY_EXPIRATION': True,
    'JWT_EXPIRATION_DELTA': datetime.timedelta(minutes=15),
    'JWT_REFRESH_EXPIRATION_DELTA': datetime.timedelta(days=7),
    'JWT_ALLOW_ARGUMENT': True,
}

# # Authentication
AUTHENTICATION_BACKENDS = [
    'COMPANYNAME.core.auth.email_backend.EmailBackend',
    # 'django.contrib.auth.backends.ModelBackend',
    'graphql_jwt.backends.JSONWebTokenBackend',
]

# # Password validation
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# # Installed Apps  - each project contains many apps (sort of like submodules)
INSTALLED_APPS = [
    # Local apps
    'COMPANYNAME.account',
    'COMPANYNAME.core',
    'COMPANYNAME.course',
    'COMPANYNAME.feed',
    'COMPANYNAME.grammar',
    'COMPANYNAME.graphql',
    'COMPANYNAME.language',
    'COMPANYNAME.lexeme',
    'COMPANYNAME.morphology',
    'COMPANYNAME.phrase',
    'COMPANYNAME.service',
    'COMPANYNAME.student',
    'COMPANYNAME.wordlist',
    # Django modules
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    # External apps
    'graphene_django',
    'django_filters',
]

# # Middleware - hooks into all request/ response interactions
MIDDLEWARE = [
    'backend.middleware.graphql_logging_middleware.GraphQLLoggingMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

# # Internationalization (i18n) - for translating the app
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# # Tests
TEST_RUNNER = "COMPANYNAME.core.tests.unmanaged_model_test_runner.UnmanagedModelTestRunner"
