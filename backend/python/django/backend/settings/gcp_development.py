from .base import *

# # Host Machine Config
# Google app engine - set by GAE when application is running in a GAE VM
GAE_APPLICATION = env('GAE_APPLICATION')
# The bucket to use for extra application data
APP_BUCKET = env('BACKEND_BUCKET')

# # Database config
DB_PASSWORD = env('DB_PASSWORD')
DB_HOST = env('DB_HOST')
CLOUD_SQL_PROXY_PREFIX = ''

DATABASES = {
    'default': {
        'ENGINE': DB_ENGINE,
        'HOST': CLOUD_SQL_PROXY_PREFIX + DB_HOST,
        'NAME': DB_NAME,
        'USER': DB_USER,
        'PASSWORD': DB_PASSWORD,
    }
}

# # Application Config
# Debug mode - displays detailed error messages
DEBUG = True
# Secret key - used by Django as a cryptographic SALT (can define your own)
SECRET_KEY = env('DJANGO_SECRET_KEY')
# Allowed hosts - URLs django can serve
ALLOWED_HOSTS = ['localhost', 'secret-secret-267015.appspot.com', 'backend-dev-gc.COMPANYNAME.com']
# Installed Apps - each project contains many apps (sort of like submodules)
INSTALLED_APPS += [
    'corsheaders'  # investigate if needed for prod
]
# Middleware - hooks into all request/ response interactions
MIDDLEWARE += [
    'corsheaders.middleware.CorsMiddleware'  # investigate if needed for prod
]

# # Logging & Error Config

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': STANDARD_LOGGING_FORMATTER,
        'django.server': DEFAULT_LOGGING['formatters']['django.server'],
    },
    'handlers': {
        'console': CONSOLE_LOGGING_HANDLER,
        'django.server': DEFAULT_LOGGING['handlers']['django.server'],
    },
    'loggers': {
        '': {
            'level': LOG_LEVEL,
            'handlers': ['console']
        },
        'COMPANYNAME': {
            'level': LOG_LEVEL,
            'handlers': ['console'],
            'propagate': False
        },
        'django.request': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False
        },
        'django.db.backends': {
            'level': LOG_LEVEL,
            'handlers': ['console'],
            'propagate': False
        },
        'django.server': DEFAULT_LOGGING['loggers']['django.server']
    }
}
# Let exceptions propagate up to the call hierachy when DEBUG is on
DEBUG_PROPAGATE_EXCEPTIONS = True

# # Network Config
# Cross origin resource sharing (CORS) - accessing content from another domain
CORS_ORIGIN_ALLOW_ALL = True
