"""
Standard WSGI config with the exception of a custom envs vars powered by Environ.
Helps to set databases, logging & enviornment specific flags.
"""

import os

import environ
from django.core.wsgi import get_wsgi_application

project_root = environ.Path(__file__) - 2
# read environment file (`.env`) for the application
environ.Env.read_env(project_root('.env'))
# Sets the default settings as local if they don't already exist
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "backend.settings.local")


application = get_wsgi_application()

print("Booted Backend Server with settings:'{settings}'".format(settings=os.environ.get('DJANGO_SETTINGS_MODULE')))
