import json
import logging

request_logger = logging.getLogger('graphql.request.logger')


class GraphQLLoggingMiddleware(object):
    """
    Provides full logging of requests and responses
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        """
        Adding request and response logging
        """
        if self.is_request_graphql(request):
            ascii_request_body: str = request.body.decode('utf-8')
            request_body: dict = json.loads(ascii_request_body)
            if request_body.get('query') and "IntrospectionQuery" in request_body['query']:
                request_operation_name = "IntrospectionQuery"
                request_variables = []
            else:
                request_operation_name = request_body['operationName']
                request_variables = request_body['variables']
            request_logger.log(
                logging.DEBUG,
                f"POST: {request.POST}. body: {request.body}",
                extra={
                    'tags': {
                        'url': request.build_absolute_uri() # for graphql this will always be /graphql/
                    }
                }
            )
            response = self.get_response(request)
            request_logger.log(
                logging.DEBUG,
                f"Response code: {response.status_code}. Response content: {response.content}"
            )
            request_logger.log(
                logging.INFO,
                f"GQLOperation: '{request_operation_name}'. Variables: {request_variables}. Response code: {response.status_code}"
            )
            return response
        return self.get_response(request)

    def is_request_graphql(self, request):
        return request.path.startswith('/graphql/') and request.method == "POST" and request.META.get('CONTENT_TYPE') == 'application/json'
