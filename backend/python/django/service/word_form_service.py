from dataclasses import dataclass, field
from typing import List

from COMPANYNAME.morphology.models.morph_ending import MorphEnding
from COMPANYNAME.morphology.models.morph_form import MorphForm
from COMPANYNAME.morphology.models.morph_operator import MorphOperator


@dataclass(frozen=True)
class DetailedAttributeDTO:
    key: str
    key_description: str
    value: str
    value_description: str


@dataclass(frozen=True)
class WordFormDTO:
    """DTO to represent a possible grammatical form or representation of a word."""

    value: str
    attributes: str
    morph_form: MorphForm  # need an extra ting tang
    morph_operator: MorphOperator
    orthography: int
    register: int
    variant: int
    mrule_default_attributes: str
    detailed_attributes: List[DetailedAttributeDTO] = field(init=False, default_factory=lambda: [])


def make_word_forms_for_lexeme_entry(
    lexeme_entry,
    morph_endings,
    main_form_ending_str,
    orthographies,
    registers,
    variants
) -> List[WordFormDTO]:
    word_forms = []
    morph_operators = MorphOperator.objects.filter(morph_regex_id=lexeme_entry.lexeme.mrule.morph_regex.id)
    main_form_ending_variant = lexeme_entry.get_main_form_ending(main_form_ending_str)
    entry = str(lexeme_entry.entry)
    # ONE STEM FOR EACH LEXEMEENTRY!
    stem = entry[0:len(entry)-len(main_form_ending_variant)]
    for morph_ending in morph_endings:
        morph_ending_word_form = make_word_forms_from_morph_ending(
            morph_ending,
            stem,
            morph_operators,
            orthographies,
            registers,
            variants
        )
        word_forms.extend(morph_ending_word_form)
    return word_forms


def make_word_forms_for_lexeme_entry_from_morph_forms(
    lexeme_entry,
    morph_form_ids,
    orthographies,
    registers,
    variants
) -> List[WordFormDTO]:
    morph_endings = MorphEnding.objects.filter(mrule_id=lexeme_entry.lexeme.mrule.id)
    if morph_form_ids:
        morph_endings = morph_endings.filter(morph_form_id__in=morph_form_ids)
    main_form_ending = None
    for morph_ending in morph_endings:
        if morph_ending.morph_form.main == True:
            main_form_ending = morph_ending
    if not main_form_ending:
        main_form_ending = MorphEnding.objects.filter(mrule_id=lexeme_entry.lexeme.mrule.id, morph_form__main=True).first()
    if not main_form_ending:
        return []
    return make_word_forms_for_lexeme_entry(lexeme_entry, morph_endings, main_form_ending.name, orthographies, registers, variants)


def get_word_forms_from_lexeme_and_morph_form(
    lexeme,
    morph_form,
    orthographies,
    registers,
    variants
) -> List[WordFormDTO]:
    word_forms = []
    for lexeme_entry in lexeme.lexeme_entries.all():
        lexeme_entry_word_forms = make_word_forms_for_lexeme_entry_from_morph_forms(
            lexeme_entry,
            [morph_form],
            orthographies,
            registers,
            variants
        )
        word_forms.extend(lexeme_entry_word_forms)
    return word_forms


def make_word_forms_from_morph_ending(morph_ending, stem, morph_operators, orthographies, registers, variants) -> List[WordFormDTO]:
    # Number of derived wordforms depends on semicolons, commas and slashes,
    # e.g. in Japanese る;る;ru the semicolons distinguish different orthographies
    word_forms = []
    values = []
    r = v = o = 0
    for morph_ending_split_ortho in morph_ending.name.split(';'):
        # e.g. ["る","る","ru"]
        # e.g. ["uje/yje,uj/yj,y"]
        r = 0
        for morph_ending_split_register in morph_ending_split_ortho.split(','):
            # e.g. ["る"]
            # e.g. ["uje/yje","uj/yj","y"]
            v = 0
            for morph_ending_split_variant in morph_ending_split_register.split('/'):
                # e.g. ["る"]
                # e.g. ["uje", "yje"]
                val = stem + morph_ending_split_variant
                applied_operator = None
                for morph_operator in morph_operators:
                    if morph_operator.name in morph_ending_split_variant:
                        val = morph_operator.apply(stem, morph_ending_split_variant)
                        if val != stem + morph_ending_split_variant:
                            applied_operator = morph_operator
                values.append({'value': val, 'orthography': o, 'register': r, 'variant': v, 'allow': True, 'applied_operator': applied_operator})
                v += 1
            r += 1
        o += 1
    for val in values:
        # if there are alternatives in orthogaphies (o > 1) and requirements for specific orthographies:
        if o > 1 and len(orthographies) > 0 and val['orthography'] not in orthographies:
            val['allow'] = False
        # if there are alternatives in registers (r > 1) and requirements for specific registers:
        if r > 1 and len(registers) > 0 and val['register'] not in registers:
            val['allow'] = False
        # if there are alternatives in variants (v > 1) and requirements for specific variants:
        if v > 1 and len(variants) > 0 and val['variant'] not in variants:
            val['allow'] = False

    attributes = morph_ending.morph_form.attributes_to_one_string(morph_ending.mrule.default_attributes)
    for val in values:
        if val['allow']:
            word_form = WordFormDTO(
                value=val['value'],
                attributes=attributes,
                morph_form=morph_ending.morph_form,
                morph_operator=val['applied_operator'],
                orthography=val['orthography'],
                register=val['register'],
                variant=val['variant'],
                mrule_default_attributes=morph_ending.mrule.default_attributes
            )
            word_forms.append(word_form)
    return word_forms
