import logging
from django.db.models import Q
from dataclasses import dataclass, field
from typing import List

from graphql_relay.node.node import from_global_id

from COMPANYNAME.grammar.models.constituent_form import ConstituentForm
from COMPANYNAME.grammar.models.constituent_form_word_form import ConstituentFormWordForm

from COMPANYNAME.grammar.constants.feature_structure import ATTR_OPTIONAL, ATTR_REQUIRED, ATTR_DISALLOWED

logger = logging.getLogger(__name__)


@dataclass
class GrammarParamValueTypeDTO:
    name: str
    description: str


@dataclass(frozen=True)
class GrammarAttributeTypeDTO:
    param: str
    param_values: List[GrammarParamValueTypeDTO]


def save_constituent_forms(constituent, constituent_forms):
    cf_ids = []
    position = 0
    for cf_input in constituent_forms:
        if cf_input.id:
            constituent_form = ConstituentForm.objects.get(pk=from_global_id(cf_input.id)[1])
        else:
            constituent_form = ConstituentForm(constituent_id=constituent.id)
        constituent_form.key = cf_input.key
        constituent_form.attributes = cf_input.attributes
        constituent_form.position = position
        position += 1
        constituent_form.save()
        cf_ids.append(constituent_form.id)
        save_constituent_form_word_forms(constituent_form, cf_input.word_forms)
    ConstituentForm.objects \
        .filter(constituent_id=constituent.id) \
        .exclude(id__in=cf_ids) \
        .delete()


def save_constituent_form_word_forms(constituent_form: ConstituentForm, constituent_form_word_forms):
    cfwf_ids = []
    position = 0
    for cfwf_input in constituent_form_word_forms:
        if cfwf_input.id:
            constituent_form_word_form = ConstituentFormWordForm.objects.get(pk=from_global_id(cfwf_input.id)[1])
        else:
            constituent_form_word_form = ConstituentFormWordForm(constituent_form_id=constituent_form.id)
        constituent_form_word_form.morph_form_id = None
        if cfwf_input.morph_form_id:
            constituent_form_word_form.morph_form_id = from_global_id(cfwf_input.morph_form_id)[1]
        constituent_form_word_form.lexeme_id = None
        if cfwf_input.lexeme_id:
            constituent_form_word_form.lexeme_id = from_global_id(cfwf_input.lexeme_id)[1]
        constituent_form_word_form.position = position
        position += 1
        constituent_form_word_form.save()
        cfwf_ids.append(constituent_form_word_form.id)
    ConstituentFormWordForm.objects \
        .filter(constituent_form_id=constituent_form.id) \
        .exclude(id__in=cfwf_ids) \
        .delete()


def get_constituent_forms_by_grammar_attributes(language_id: int, posp_name: str, attr_dict: dict):
    queryset = ConstituentForm.objects.filter(constituent__language_id=language_id, constituent__posp__posp=posp_name)
    return filter_out_constituent_forms_queryset_by_grammar_attributes(queryset, attr_dict)


def filter_out_constituent_forms_queryset_by_grammar_attributes(queryset, attr_dict):
    for attr_key, attr_val in attr_dict.items():
        if attr_val != ATTR_OPTIONAL:
            if attr_val in [ATTR_REQUIRED, ATTR_DISALLOWED]:
                queryset.filter(Q(attributes__icontains=f"{attr_key}={attr_val}") | ~Q(attributes__icontains=f"{attr_val}{attr_key}"))
            else:
                queryset = queryset.filter(attributes__icontains=f"{attr_key}={attr_val}")
                queryset = queryset.exclude(attributes__icontains=f"-{attr_key}")
                queryset = queryset.exclude(attributes__icontains=f"{attr_key}=-")
    return queryset


def get_possible_constituent_form_attributes(constituent) -> List[GrammarAttributeTypeDTO]:
    attributes = []
    dic = constituent.get_possible_grammar_attributes()
    for key in dic:
        grammar_param_values: List[GrammarParamValueTypeDTO] = []
        for value in dic[key]:
            grammar_param_values.append(
                GrammarParamValueTypeDTO(name=value, description=value)
            )
        attributes.append(GrammarAttributeTypeDTO(param=key, param_values=grammar_param_values))
    return attributes
