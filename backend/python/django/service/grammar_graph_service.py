import logging

from graphql.error import GraphQLError
from graphql_relay.node.node import from_global_id

from COMPANYNAME.core.utils.helper import convert_dict_to_attributes, convert_key_value_pairs_to_dic, is_base64
from COMPANYNAME.grammar.models.grammar_graph import GrammarGraph
from COMPANYNAME.grammar.models.grammar_graph_edge import GrammarGraphEdge
from COMPANYNAME.grammar.models.grammar_graph_node import GrammarGraphNode
from COMPANYNAME.graphql.grammar.types import GrammarGraphEdgeInput, GrammarGraphNodeInput

logger = logging.getLogger(__name__)


def save_graph_nodes(grammar_graph: GrammarGraph, graph_nodes: GrammarGraphNodeInput):
    """
    Nodes must have an ID:
        - if the ID is base64, it means the node is an existing gg_node in the database
        - if the ID is a simple string number, it means it is a temporary "entity ID"
          and the node does not exist yet
    """
    ggn_global_id_to_entity_id = {}
    for graph_node in graph_nodes:
        if graph_node.id and is_base64(graph_node.id):
            grammar_graph_node = GrammarGraphNode.objects.get(pk=from_global_id(graph_node.id)[1])
        else:
            grammar_graph_node = GrammarGraphNode(grammar_graph_id=grammar_graph.id)
        grammar_graph_node.label = graph_node.label
        grammar_graph_node.kind = graph_node.kind
        if graph_node.attributes:
            grammar_graph_node.attributes = graph_node.attributes
        grammar_graph_node.save()
        ggn_global_id_to_entity_id[graph_node.id] = grammar_graph_node.id
    GrammarGraphNode.objects \
        .filter(grammar_graph_id=grammar_graph.id) \
        .exclude(id__in=list(ggn_global_id_to_entity_id.values())) \
        .delete()
    return ggn_global_id_to_entity_id


def save_graph_edges(grammar_graph: GrammarGraph, graph_edges: GrammarGraphEdgeInput, ggn_global_id_to_entity_id={}):
    list_of_ids = []
    for graph_edge in graph_edges:
        if graph_edge.id and is_base64(graph_edge.id):
            grammar_graph_edge = GrammarGraphEdge.objects.get(pk=from_global_id(graph_edge.id)[1])
        else:
            grammar_graph_edge = GrammarGraphEdge(grammar_graph_id=grammar_graph.id)
        try:
            grammar_graph_edge.from_node_id = ggn_global_id_to_entity_id[graph_edge.from_node_id]
            grammar_graph_edge.to_node_id = ggn_global_id_to_entity_id[graph_edge.to_node_id]
        except KeyError:
            raise GraphQLError("Unable to find node IDs for edge '{0}'".format(graph_edge.label))
        grammar_graph_edge.label = graph_edge.label
        if graph_edge.attributes:
            grammar_graph_edge.attributes = graph_edge.attributes
        grammar_graph_edge.save()
        list_of_ids.append(grammar_graph_edge.id)
    GrammarGraphEdge.objects \
        .filter(grammar_graph_id=grammar_graph.id) \
        .exclude(id__in=list_of_ids) \
        .delete()


def clone_graph_nodes_and_edges(grammar_graph: GrammarGraph, clone_from_id: int):
    """ Perform a deep copy of the GrammarGraph foreign key relations.
    Note: this could exist on the model but would need some tidying and optimisation
    see - https://docs.djangoproject.com/en/2.2/topics/db/queries/#copying-model-instances """
    old_id_to_new_id = _clone_graph_nodes(grammar_graph, clone_from_id)
    _clone_graph_edges(grammar_graph, clone_from_id, old_id_to_new_id)


def _clone_graph_nodes(grammar_graph: GrammarGraph, clone_from_id: int):
    old_id_to_new_id = {}
    for node in GrammarGraphNode.objects.filter(grammar_graph_id=clone_from_id):
        graph_node = GrammarGraphNode(
            grammar_graph_id=grammar_graph.id,
            label=node.label,
            kind=node.kind,
            attributes=node.attributes
        )
        graph_node.save()
        old_id_to_new_id[node.id] = graph_node.id
    return old_id_to_new_id


def _clone_graph_edges(grammar_graph: GrammarGraph, clone_from_id: int, old_id_to_new_id={}):
    for edge in GrammarGraphEdge.objects.filter(grammar_graph_id=clone_from_id):
        graph_edge = GrammarGraphEdge(
            grammar_graph_id=grammar_graph.id,
            label=edge.label,
            attributes=edge.attributes,
            from_node_id=old_id_to_new_id[edge.from_node_id],
            to_node_id=old_id_to_new_id[edge.to_node_id]
        )
        graph_edge.save()
