import logging
import re
from dataclasses import dataclass, field
from typing import Dict, List, Tuple

from COMPANYNAME.core.utils.collections_helper import tuple_list_to_dict
from COMPANYNAME.feed.utils.feed_helper import param_value_pairs_dict_from_morphcode
from COMPANYNAME.grammar.constants.feature_structure import ATTR_REQUIRED, ATTR_DISALLOWED
from COMPANYNAME.morphology.models.morph_form import MorphForm
from COMPANYNAME.morphology.models.morph_param import MorphParam
from COMPANYNAME.morphology.models.morph_regex import MorphRegex

logger = logging.getLogger(__name__)


@dataclass
class MorphParamValueTypeDTO:
    name: str
    description: str


@dataclass(frozen=True)
class AttributeTypeDTO:
    param: str
    morph_param_values: List[MorphParamValueTypeDTO]


@dataclass
class DetailedAttributeTypeDTO:
    key: str
    key_description: str = field(init=False)
    value: str
    value_description: str = field(init=False)


def get_possible_attributes(morph_regex: MorphRegex) -> List[AttributeTypeDTO]:
    attributes = []
    dic = param_value_pairs_dict_from_morphcode(morph_regex.pattern)
    for key in dic:
        morph_param_values: List[MorphParamValueTypeDTO] = []
        for value in dic[key]:
            morph_param_value: MorphParamValueTypeDTO = _make_morph_param_value(morph_regex, value)
            morph_param_values.append(morph_param_value)
        attributes.append(AttributeTypeDTO(param=key, morph_param_values=morph_param_values))
    return attributes


def _make_morph_param_value(morph_regex: MorphRegex, morph_param_name: str) -> MorphParamValueTypeDTO:
    if morph_param_name == ATTR_REQUIRED or morph_param_name == ATTR_DISALLOWED:
        return MorphParamValueTypeDTO(name=morph_param_name, description=morph_param_name)
    else:
        morph_param = MorphParam.objects.filter(morph_regex_id=morph_regex.id) \
            .filter(name__contains=morph_param_name) \
            .filter(name__exact=morph_param_name).first()
        if morph_param:
            return MorphParamValueTypeDTO(name=morph_param.name, description=morph_param.description)
        else:
            return MorphParamValueTypeDTO(name=morph_param_name, description=morph_param_name)


def get_morph_form_detailed_attributes(
    morph_form: MorphForm,
    mrule_default_attributes=None,
    include_morph_regex_possible_attributes=False
) -> List[DetailedAttributeTypeDTO]:
    if morph_form.name == "MAINFORM":
        return None
    explanations = []
    attributes = _attributes_to_list_of_tuples(morph_form, mrule_default_attributes, include_morph_regex_possible_attributes)
    for key, value in attributes:
        explanation = DetailedAttributeTypeDTO(key=key, value=value)
        param_key_results = MorphParam.objects \
            .filter(morph_regex_id=morph_form.morph_regex.id) \
            .filter(name__contains=explanation.key) \
            .filter(name__exact=explanation.key)
        param_value_results = MorphParam.objects \
            .filter(morph_regex_id=morph_form.morph_regex.id) \
            .filter(name__contains=explanation.value) \
            .filter(name__exact=explanation.value)
        for param_key in param_key_results:
            if param_key.name == explanation.key:
                explanation.key_description = param_key.description
        for param_value in param_value_results:
            if param_value.name == explanation.value:
                explanation.value_description = param_value.description
        explanations.append(explanation)
    return explanations


def attributes_to_dic(morph_form, mrule_default_attributes=None) -> Dict[str, str]:
    attributes: List[Tuple[str, str]] = _attributes_to_list_of_tuples(morph_form, mrule_default_attributes)
    return tuple_list_to_dict(attributes)


def _attributes_to_list_of_tuples(
    morph_form: MorphForm,
    mrule_default_attributes=None,
    include_morph_regex_possible_attributes=False
) -> List[Tuple[str, str]]:
    attributes = morph_form.attributes_to_list_of_strings(mrule_default_attributes)
    tuples = []
    for attribute in attributes:
        tpl = None
        if attribute.startswith((ATTR_REQUIRED, ATTR_DISALLOWED)):
            tpl = (attribute[1:], attribute[0:1])
        else:
            pair = re.split(r'=', attribute)
            if len(pair) == 2:
                tpl = (pair[0], pair[1])
            else:
                logger.warn("Attribute {0} is not properly formed (morphForm #{1})".format(attribute, morph_form.id))
        if tpl:
            tuples.append(tpl)
    if include_morph_regex_possible_attributes:
        mr_possible_attributes = get_possible_attributes(morph_form.morph_regex)
        for mr_possible_attribute in mr_possible_attributes:
            found_tuple = list(filter(lambda x: mr_possible_attribute.param == x[0], tuples))
            if not found_tuple:
                tuples.append((mr_possible_attribute.param, ATTR_DISALLOWED))
    return tuples
