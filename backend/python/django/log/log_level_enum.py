from enum import IntEnum


class LogLevel(IntEnum):
    """LogLevel to standardise the level to log at. The numerical value is for Django's own `int` based log level"""
    CRITICAL = 50
    FATAL = CRITICAL
    ERROR = 40
    WARNING = 30
    WARN = WARNING
    INFO = 20
    DEBUG = 10
