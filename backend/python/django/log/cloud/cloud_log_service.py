import logging

from django.conf import settings

from COMPANYNAME.core.log.cloud.aws_cloudwatch_logger import AwsCloudwatchLogger
from COMPANYNAME.core.log.cloud.gcp_stackdriver_logger import GcpStackdriverLogger
from COMPANYNAME.core.log.log_level_enum import LogLevel
from COMPANYNAME.graphql.core.log.log_fields_input import LogFieldsInput

logger = logging.getLogger('forwarded_frontend_log')


def handle_log_forwarding(log_payload: LogFieldsInput) -> bool:
    if (getattr(settings, settings.AWS_FLAG, None)):
        aws_singleton_logger = AwsCloudwatchLogger()
        return aws_singleton_logger.log_to_aws_cloudwatch(log_payload)
    elif (getattr(settings, settings.GCP_FLAG, None)):
        gcp_singleton_logger = GcpStackdriverLogger()
        return gcp_singleton_logger.log_to_gcp_stackdriver(log_payload)
    else:
        logger.log(LogLevel[log_payload.level].value, log_payload)
        return True
