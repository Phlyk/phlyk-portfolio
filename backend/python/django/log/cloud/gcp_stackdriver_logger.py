import logging

from google.cloud import logging as gcp_logging

from COMPANYNAME.core.meta.singleton import Singleton
from COMPANYNAME.graphql.core.log.log_fields_input import LogFieldsInput

from ...exceptions import LogForwardingRejectedException


class GcpStackdriverLogger(metaclass=Singleton):

    logger = logging.getLogger(__name__)

    platform = "GCP"
    source_location = "StudentSite"
    gcp_log_name = "student-site-app-log"

    def __init__(self):
        client = gcp_logging.Client()
        self.gcp_logger = client.logger(name=self.gcp_log_name)

    def log_to_gcp_stackdriver(self, log_payload: LogFieldsInput) -> bool:
        formatted_payload = log_payload.to_log_format()

        try:
            self.gcp_logger.log_struct(
                info=formatted_payload,
                severity=self.map_angular_log_level_to_gcp_log_severity(log_payload.level),
                timestamp=log_payload.timestamp,
                labels={"forwarded-log": "true"},
                source_location={"file": self.source_location}
            )
        except Exception as e:
            self.logger.error(f"Unexpected error when attempting to put logs to GCP. Error: {type(e)} - {e}")
            raise LogForwardingRejectedException(self.platform, e)

        self.logger.info("Log forwarded: {}".format(log_payload))
        return True

    def map_angular_log_level_to_gcp_log_severity(self, log_level: str) -> str:
        if (log_level == "WARN"):
            return "WARNING"
        return log_level
