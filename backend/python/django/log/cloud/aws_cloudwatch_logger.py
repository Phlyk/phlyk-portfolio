import datetime
import json
import logging
from datetime import datetime
from typing import Dict

import boto3

from COMPANYNAME.graphql.core.log.log_fields_input import LogFieldsInput

from ...exceptions import LogForwardingRejectedException
from ...meta.singleton import Singleton


class AwsCloudwatchLogger(metaclass=Singleton):

    logger = logging.getLogger(__name__)
    next_sequence_token: str

    platform = "AWS"
    aws_log_group = 'StudentSiteLogs'
    aws_log_stream = 'student-site-application.log'

    def __init__(self):
        super().__init__()
        self.logger.info("Initialising AWS Cloudwatch logger")
        self.log_client = boto3.client('logs')
        self.next_sequence_token = self.get_next_sequence_token()

    def log_to_aws_cloudwatch(self, log_payload: LogFieldsInput) -> bool:
        log_timestamp: datetime = log_payload.timestamp
        timestamp_in_millis: str = str(round(log_timestamp.timestamp() * 1000))

        log_message = json.dumps(log_payload.to_log_format())
        try:
            response = self.cloudwatch_put_log_event(timestamp_in_millis, log_message)
            rejection_information = response.get('rejectedLogEventsInfo', None)
            if rejection_information:
                raise LogForwardingRejectedException(self.platform, rejection_information)
            self.next_sequence_token = response['nextSequenceToken']
            self.logger.info('Log forwarded: {}'.format(log_payload))
        except self.log_client.exceptions.DataAlreadyAcceptedException as e:
            self.logger.warn(f"Log has already been sent to AWS. Ignoring error. Message: {e}")
            return True
        except Exception as e:
            if (isinstance(e, LogForwardingRejectedException)):
                raise e
            self.logger.error(f"Unexpected error when attempting to put logs to AWS. Error: {type(e)} - {e}")
            return False
        return True

    def cloudwatch_put_log_event(self, timestamp_in_millis: str, log_message: str) -> Dict[str, str]:
        return self.log_client.put_log_events(
            logGroupName=self.aws_log_group,
            logStreamName=self.aws_log_stream,
            logEvents=[
                {
                    'timestamp': timestamp_in_millis,
                    'message': log_message
                }
            ],
            sequenceToken=self.next_sequence_token
        )

    def get_next_sequence_token(self) -> str:
        """ The Cloudwatch API requires us to retrieve the pointer to the last log put in order to put new logs
            This method queries the API to find that pointer upon logger start up"""
        response = self.log_client.describe_log_streams(
            logGroupName=self.aws_log_group,
            logStreamNamePrefix=self.aws_log_stream
        )
        next_token = response["logStreams"][0]["uploadSequenceToken"]
        self.logger.info(f"Starting log forwarding with token: {next_token}")
        return next_token
