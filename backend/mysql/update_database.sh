#!/bin/bash

# Script to update and optionally initalise db
# Assumptions:
#   virtualenv set up
#   infrastructure dir cloned if need to initalise db
#   python3 & pip3 are installed and on the PATH

function usage() {
    echo ">> Usage:"
    echo ">>    bash update_database.sh <DB_HOST> <DJANGO_USER_PASS> <ENVIRONMENT> <VIRTUALENV_NAME> [<INIT_DB_FLAG> <INFRASTRUCTURE_PATH> <ROOT_PASSWORD>]"
    echo ">>    DB_HOST - the MySQL host to initalise"
    echo ">>    DJANGO_USER_PASS - the password for the Django User for this environment"
    echo ">>    ENVIRONMENT - the environment to build in (values are [local, development, staging, production])"
    echo ">>    VIRTUALENV_NAME - the name of the virtualenv to activate"
    echo ">>    [INIT_DB_FLAG] - set to 1 to initialise the DB schema & Django user, 0 to ignore"
    echo ">>    [ROOT_PASSWORD] - only provide if INIT_DB_FLAG has been set to 1"
    exit 1
}

set -e 

if [ $# -lt 4 ] || [ $# -gt 6 ]; then
    usage
fi

SCRIPT_PATH=$( cd $(dirname $0); pwd -P)
PROJECT_PATH=$( cd ${SCRIPT_PATH}/../..; pwd -P)

DB_HOST=$1
DJANGO_USER_PASS=$2
ENVIRONMENT=$3
VIRTUALENV_NAME=$4
INIT_DB_FLAG=$5
ROOT_PASSWORD=$6

if [ $# -lt 5 ]; then
    echo ">> Not initalising db, setting flag to 0"
    INIT_DB_FLAG=0
fi

if [ ${INIT_DB_FLAG} -ne 1 ] && [ ${INIT_DB_FLAG} -ne 0 ]; then
    echo ">> INIT_DB_FLAG has to be 1 (to initalise database & django user) or 0 (to not)"
    exit 1
fi

if [ ${ENVIRONMENT} = "local" ]; then
    INFRASTRUCTURE_PATH=${PROJECT_PATH}/../infrastructure
    ENVIRONMENT=mac
    DJANGO_USER_HOST="localhost"
else 
    INFRASTRUCTURE_PATH=${PROJECT_PATH}/infrastructure
    DJANGO_USER_HOST="%"
fi

echo "update_database.sh:"
echo "SCRIPT_PATH:$SCRIPT_PATH"
echo "PROJECT_PATH:$PROJECT_PATH"
echo "INFRASTRUCTURE_PATH:${INFRASTRUCTURE_PATH}"
echo "DB_HOST:${DB_HOST}"
echo "ENVIRONMENT:${ENVIRONMENT}"
echo "VIRTUALENV_NAME:${VIRTUALENV_NAME}"
echo "INIT_DB_FLAG:${INIT_DB_FLAG}"


if [ ${INIT_DB_FLAG} -eq 1 ]; then
    if [ $# -ne 6 ]; then
        echo ">> Neccessary arguments for DB initalisation have not been provided"
        usage
    fi
    echo ">> Initialising Database Schema and Django User"
    bash ${INFRASTRUCTURE_PATH}/mysql/init_db_schema_and_users.sh ${ROOT_PASSWORD} ${DJANGO_USER_PASS} ${DJANGO_USER_HOST} ${DB_HOST}
fi

echo ">> Activating '${VIRTUALENV_NAME}'"
source ${INFRASTRUCTURE_PATH}/python/create_and_activate_venv.sh ${VIRTUALENV_NAME}

echo ">> Installing 'requirements/${ENVIRONMENT}.txt'"
pip3 install -q -r requirements/${ENVIRONMENT}.txt

echo ">> Creating '.env' file"
bash ${SCRIPT_PATH}/../create_env_file.sh DB_PASSWORD=${DJANGO_USER_PASS} DB_HOST=${DB_HOST} LOG_LEVEL=INFO 

echo ">> Running Django migrations"
python3 manage.py migrate

echo ">> Running manual migrations"
bash scripts/mysql/mysql_manual_migrations.sh django_user "${DJANGO_USER_PASS}" ${DB_HOST} django_db

echo ">> Database has been updated!"
