#!/bin/bash

if [ $# -ne 4 ] && [ $# -ne 5 ]; then
    echo ">> Usage:"
    echo ">>    bash init_db_schema_and_users.sh <ROOT_USER_PASS> <DJANGO_USER_PASS> <DJANGO_USER_HOST> <MYSQL_HOST> [<TEST>]"
    echo ">>    bash init_db_schema_and_users.sh fishyFishFace flippyFlapFlop % COMPANYNAME-something.2dg87stv.eu-central-1.rds.amazonaws.com"
    echo ">>    bash init_db_schema_and_users.sh fishyFishFace flippyFlapFlop localhost 127.0.0.1 settingupTestDB"
    exit 1
fi

ROOT_USER_NAME=root
ROOT_USER_PASS=$1
DJANGO_USER_PASS=$2
DJANGO_USER_HOST=$3

MYSQL_HOST=$4
IS_TEST=$5

if [ -z ${IS_TEST} ]; then
    DJANGO_USER_NAME="django_user"
    SCHEMA_TO_CREATE="django_db"
else
    DJANGO_USER_NAME="test_django_user"
    SCHEMA_TO_CREATE="test_django_db"
fi

DJANGO_USER_ID="'${DJANGO_USER_NAME}'@'${DJANGO_USER_HOST}'"

MYSQL_CONNECTION_STRING="mysql -h ${MYSQL_HOST} -u ${ROOT_USER_NAME} -p${ROOT_USER_PASS}"

echo "init_db_schema_and_users:"
echo "MYSQL_HOST:${MYSQL_HOST}"
echo "DJANGO_USER_ID:${DJANGO_USER_ID}"

echo ">> Creating '${SCHEMA_TO_CREATE}' schema"
MYSQL_CREATE_SCHEMA='CREATE SCHEMA IF NOT EXISTS `'${SCHEMA_TO_CREATE}'` DEFAULT CHARACTER SET utf8mb4'

${MYSQL_CONNECTION_STRING} -e "${MYSQL_CREATE_SCHEMA}"
DB_RETURN_CODE=$?

if [ ${DB_RETURN_CODE} -ne 0 ]; then
    echo ">> Issue creating DB Schema, see output and fix..."
    exit ${DB_RETURN_CODE}
fi

echo ">> Creating Django User"
MYSQL_CREATE_DJANGO_USER="CREATE USER IF NOT EXISTS ${DJANGO_USER_ID} IDENTIFIED BY '${DJANGO_USER_PASS}'"
MYSQL_GRANT_PRIVILEGES="GRANT ALL PRIVILEGES ON ${SCHEMA_TO_CREATE}.* TO ${DJANGO_USER_ID} WITH GRANT OPTION"
MYSQL_FLUSH_PRIVILEGES="FLUSH PRIVILEGES"
MYSQL_CREATE_DJANGO_USER_COMMAND="${MYSQL_CREATE_DJANGO_USER}; ${MYSQL_GRANT_PRIVILEGES}; ${MYSQL_FLUSH_PRIVILEGES};"

${MYSQL_CONNECTION_STRING} -e "${MYSQL_CREATE_DJANGO_USER_COMMAND}"

USER_RETURN_CODE=$?
if [ ${USER_RETURN_CODE} -ne 0 ]; then
    echo ">> Issue creating Django User, see output and fix..."
    exit ${USER_RETURN_CODE}
fi

echo ">> '${SCHEMA_TO_CREATE}' DB has been initalised with '${DJANGO_USER_NAME}' user"
