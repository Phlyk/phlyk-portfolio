#!/bin/bash

# Script assumes you have either:
#   - `cloud_sql_proxy` running in another terminal on port 3306
#       run by: ./cloud_sql_proxy -instances="${CLOUD_SQL_CONNECTION_NAME}"=tcp:${PORT_TO_EXPOSE}
#   - a local connection to a mysql server on port 3306
#   - run in sudo to create config file
# NB: CAREFUL if the script fails it will leave the config file with plaintext secrets inside!

function createConfigFile {
    echo ">> Creating local config file"
    CNF_PATH=$1
    mkdir -p $(dirname ${CNF_PATH})
    # sudo tee ${CNF_PATH} 
	sudo tee ${CNF_PATH} <<-EOF > /dev/null
	[client]
	user = ${DB_USER}
	password = ${DB_PASSWORD}
	host = ${DB_HOST}
	EOF
    sudo chmod 600 ${CNF_PATH}
}


function checkMigrationRun {
    MYSQL_SCHEMA_MIGRATION_QUERY="select count(*) from \`${DB_NAME}\`.\`django_migrations\` where name = '$1'"
    MYSQL_MIGRATION_QUERY_RESULT=$(mysql --defaults-extra-file=${CNF_PATH} ${DB_NAME} -e "${MYSQL_SCHEMA_MIGRATION_QUERY}")
    echo ${MYSQL_MIGRATION_QUERY_RESULT} | cut -d' ' -f2
}

function runMigrations {
    MIGRATION_DIR=$1
    echo MIGRATION_DIR:${MIGRATION_DIR}

    MIGRATION_AUDIT_SQL="INSERT INTO \`${DB_NAME}\`.\`django_migrations\` (app, name, applied) VALUES ('backend', 'MIGRATION_NAME', NOW())"
    MIGRATION_COUNTER=0
    for SQL_FILE_NAME in ${MIGRATION_DIR}*.sql; do
        [ -e "$SQL_FILE_NAME" ] || continue
        MIGRATION_FILENAME=$(basename ${SQL_FILE_NAME} .sql)
        MIGRATION_CHECK_OUTPUT=$(checkMigrationRun ${MIGRATION_FILENAME})

        if [ ${MIGRATION_CHECK_OUTPUT} -ne 0 ]; then
            echo -e ">> Skipping ${MIGRATION_FILENAME} as it has been run already\n"
            continue
        fi

        echo ">> Executing '$MIGRATION_FILENAME'"
        mysql --defaults-extra-file=${CNF_PATH} ${DB_NAME} < "${SQL_FILE_NAME}"

        INSERT_MIGRATION_TRACKER=${MIGRATION_AUDIT_SQL/MIGRATION_NAME/${MIGRATION_FILENAME}}
        echo -e ">> Adding Audit for '${MIGRATION_FILENAME}'\n"
        mysql --defaults-extra-file=${CNF_PATH} ${DB_NAME} -e "${INSERT_MIGRATION_TRACKER}"
        MIGRATION_COUNTER=$(expr $MIGRATION_COUNTER + 1)
    done

    echo ">> Ran ${MIGRATION_COUNTER} Migrations"
}

function checkAndRunMigrations {
    MIGRATION_DIR=$1
    MIGRATION_COMPLETE_NAME=$2
    MIGRATION_HUMAN_NAME=$3

    echo ">> Checking if ${MIGRATION_HUMAN_NAME} migrations need running:"
    nbMigrationCompleteAppears=$(checkMigrationRun ${MIGRATION_COMPLETE_NAME})

    if [ "${nbMigrationCompleteAppears}" == 1 ]; then
        echo ">> ${MIGRATION_HUMAN_NAME} migrations already run, skipping..."
    elif [ "${nbMigrationCompleteAppears}" == 0 ]; then
        echo -e "\n>> ${MIGRATION_HUMAN_NAME} migrations not run yet, running all ${MIGRATION_HUMAN_NAME} migration files..."
        runMigrations ${MIGRATION_DIR}
        echo -e ">> ${MIGRATION_HUMAN_NAME} migration complete\n"
    else
        echo ">> problem with data migrations appearing more than once... exiting"
        exit 1
    fi
}

set -e 

if [ $# -lt 4 ]; then
    echo ">> Usage:"
    echo ">>    mysql_manual_migrations.sh <DB_USER> <DB_PASSWORD> <DB_HOST> <DB_NAME> <FORCE>"
    echo ">>    eg: sudo mysql_manual_migrations.sh root iamafishface 127.0.0.1 [test_]django_db force"
    exit 1
fi

DB_USER=$1
DB_PASSWORD=$2
DB_HOST=$3
DB_NAME=$4
FORCE=$5


WORKING_DIRECTORY=$( pwd -P )
MYSQL_SCRIPTS_DIR="${WORKING_DIRECTORY}/scripts/mysql"
SCHEMA_DIR="${MYSQL_SCRIPTS_DIR}/schema/"
DATA_DIR="${MYSQL_SCRIPTS_DIR}/data/"
echo "mysql_manual_migrations:"
echo "WORKING_DIRECTORY:${WORKING_DIRECTORY}"
echo "MYSQL_SCRIPTS_DIR:${MYSQL_SCRIPTS_DIR}"
echo "DB_USER:${DB_USER}"
echo "DB_HOST:${DB_HOST}"
echo "DB_NAME:${DB_NAME}"
echo "FORCE:${FORCE}"


SCHEMA_MIGRATION_COMPLETE_NAME="999_manual_schema_migrations"
DATA_MIGRATION_COMPLETE_NAME="999_manual_data_migrations"

CNF_PATH="/etc/mysql/my.cnf"
createConfigFile ${CNF_PATH}

if [ ! -z ${FORCE} ]; then
    echo ">> Force Specified. Removing the old cap..."
    removeOldMigrationCapQuery="delete from \`${DB_NAME}\`.\`django_migrations\` where \`name\` in ('${SCHEMA_MIGRATION_COMPLETE_NAME}', '${DATA_MIGRATION_COMPLETE_NAME}')"
    mysql --defaults-extra-file=${CNF_PATH} ${DB_NAME} -e "${removeOldMigrationCapQuery}"
    echo ">> Removed old cap, ready to re-run" 
fi

checkAndRunMigrations ${SCHEMA_DIR} ${SCHEMA_MIGRATION_COMPLETE_NAME} "Schema"

checkAndRunMigrations ${DATA_DIR} ${DATA_MIGRATION_COMPLETE_NAME} "Data"

sudo rm ${CNF_PATH}

echo ">> DB migrations successful!"
