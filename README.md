# Phlyk's Portfolio
A collection of sanitised code created throughout my career as a SWE. Assembled to show proficiency in various aspects of modern Software Development. All the code mentioned will have be written or co-written by me.

## Overview
In each folder you'll find examples of proefficency in:
1. Infrastructure -- CI/CD (Gitlab & Jenkins 2), AWS (EC2, VPCs, IaC, & friends), SCM (Versioning, Tags), GCP (basics)
2. Front-end -- RxJS, Angular (Services, Animations, Pipes), NPM Packages, GraphQL
3. Backend -- Python (Django REST), Graphene Backend, Java Sprint Boot & Spring Integration, EnvVars, Feature Flags, etc... 

> **NOTE: This repo has been created for demonstrative purposes only and will not run by itself. The directory structure has been modified to prioritise navigability of key skills over functionality and as a result paths have been changed.**
